
#pragma once

namespace  stoplights_system
{

struct Segment;

/*************************************************************************************************/
template<class T> static bool callSrv(ros::ServiceClient& srv_client, T& srv_msg, ros::Duration timeout)
{
    if (timeout != ros::Duration(0) && !srv_client.waitForExistence(timeout))
    {
        ROS_ERROR_STREAM_THROTTLE(2.0, ros::this_node::getName() << ": timeout, service [" << srv_client.getService() << "] not available");
        return false;
    }
    else
    {
        bool srv_call_success = false;
        try
        {
            srv_call_success = srv_client.call(srv_msg);
        }
        catch(std::exception &e)
        {
            ROS_ERROR_STREAM_THROTTLE(2.0, ros::this_node::getName() << ": service [" << srv_client.getService() << "] call failed. " << e.what());
            return false;
        }
        if (!srv_call_success)
        {
            ROS_ERROR_STREAM_THROTTLE(2.0, ros::this_node::getName() << ": service [" << srv_client.getService() << "] call failed");
            return false;
        }
    }
    return true;
}

/*************************************************************************************************/
static std::string stringReq(const ifollow_mas_msgs::MRAreaMngr::Request& req)
{
    std::stringstream ss;
    ss << "{req: " << int(req.req) << ", area_id: " << req.area_id << ", slot_id: " << req.slot_id << ", vehicle_id: " << req.vehicle_id  << ", occupied_slots: {";
    if (!req.occupied_slots.empty())
    {
        ss << req.occupied_slots[0] << ": " << req.occupied_slots_vehicles[0];
        for (auto it = req.occupied_slots.begin()+1; it != req.occupied_slots.end(); ++it)
        {
            ss << ", " << *it << ": " << req.occupied_slots_vehicles[std::distance(req.occupied_slots.begin(), it)];
        }
    }
    ss << "}";
    return ss.str();
}

/**
 * @brief Helper function to obtain the closest point on a line segment w.r.t. a reference point
 * @param point 2D point
 * @param line_start 2D point representing the start of the line segment
 * @param line_end 2D point representing the end of the line segment
 * @return Closest point on the line segment
 */
inline Eigen::Vector2d closest_point_on_line_segment_2d(const Eigen::Ref<const Eigen::Vector2d>& point, const Eigen::Ref<const Eigen::Vector2d>& line_start, const Eigen::Ref<const Eigen::Vector2d>& line_end)
{
    Eigen::Vector2d diff = line_end - line_start;
    double sq_norm = diff.squaredNorm();

    if (sq_norm == 0)
        return line_start;

    double u = ((point.x() - line_start.x()) * diff.x() + (point.y() - line_start.y()) * diff.y()) / sq_norm;

    if (u <= 0)
    {
        return line_start;
    }
    else if (u >= 1)
    {
        return line_end;
    }
    else
    {
        return line_start + u * diff;
    }
}

/*************************************************************************************************/
inline geometry_msgs::Quaternion computeSegmentOrientation(const Segment& _seg)
{
    double yaw = atan2(double(_seg.end[1] - _seg.start[1]), double(_seg.end[0] - _seg.start[0]));

    return tf::createQuaternionMsgFromYaw(yaw);
}

/*************************************************************************************************/
inline geometry_msgs::Quaternion computeSegmentOrientationWithNext(const Segment& _seg, const Segment& _next_seg)
{
    const double d_ss_x = _next_seg.start[0] - _seg.start[0];
    const double d_ss_y = _next_seg.start[1] - _seg.start[1];
    const double d_se_x = _next_seg.start[0] - _seg.end[0];
    const double d_se_y = _next_seg.start[1] - _seg.end[1];
    const double d_ee_x = _next_seg.end[0] - _seg.end[0];
    const double d_ee_y = _next_seg.end[1] - _seg.end[1];
    const double d_es_x = _next_seg.end[0] - _seg.start[0];
    const double d_es_y = _next_seg.end[1] - _seg.start[1];

    // Distance start to start point
    const double dss = std::hypot(d_ss_x, d_ss_y);
    // Distance end to end point
    const double dee = std::hypot(d_ee_x, d_ee_y);
    // Distance start to end point
    const double dse = std::hypot(d_se_x, d_se_y);
    // Distance end to start point
    const double des = std::hypot(d_es_x, d_es_y);

    double yaw;

    // Check if the segment are connected via start->start or end->start
    if (((dss < dee) && (dss < dse)) || ((des < dee) && (des < dse)))
    {
        yaw = atan2(double(_seg.start[1] - _seg.end[1]),
                    double(_seg.start[0] - _seg.end[0]));
    }
    // The segment are connected via end->end or start->end
    else
    {
        yaw = atan2(double(_seg.end[1] - _seg.start[1]),
                    double(_seg.end[0] - _seg.start[0]));
    }

    return tf::createQuaternionMsgFromYaw(yaw);
}

/*************************************************************************************************/
inline void applyOffsets(ifollow_nav_msgs::LocalBehaviorMoveGoal& goal)
{
        
    // cppcheck-suppress compareBoolExpressionWithInt
    if ((true == std::isfinite(goal.offset_position_x)) && (true == std::isfinite(goal.offset_position_y)) &&
        ((abs(goal.offset_position_x) > 1e-6) || (abs(goal.offset_position_y) > 1e-6)))
    {
        double yaw = conv_ops::getYaw(goal.target_pose.pose.orientation);
        double c_yaw = std::cos(yaw);
        double s_yaw = std::sin(yaw);

        goal.target_pose.pose.position.x += (c_yaw * goal.offset_position_x) - (s_yaw * goal.offset_position_y);
        goal.target_pose.pose.position.y += (s_yaw * goal.offset_position_x) + (c_yaw * goal.offset_position_y);
    }

    goal.offset_position_x = 0.0;
    goal.offset_position_y = 0.0;
}

/*************************************************************************************************/
inline ifollow_nav_msgs::LocalBehaviorMoveActionGoal initActionGoal(const geometry_msgs::PoseStamped& input_goal)
{
    // Output variable
    ifollow_nav_msgs::LocalBehaviorMoveActionGoal action_goal;

    action_goal.goal.target_pose.pose = input_goal.pose;
    action_goal.goal.target_pose.header.stamp = ros::Time::now();
    action_goal.goal.target_pose.header.frame_id = input_goal.header.frame_id;
    action_goal.goal.use_line_tol = true;
    action_goal.goal.authorize_overwriting = true;
    action_goal.goal.eps_position_x     = 0.2;
    action_goal.goal.eps_position_y     = 0.4;
    action_goal.goal.eps_orientation    = 0.2;
    action_goal.goal.area_id = invalid_id;
    stoplights_system::applyOffsets(action_goal.goal);
    action_goal.goal.goal_profile = "default";

    return action_goal;
}

}
