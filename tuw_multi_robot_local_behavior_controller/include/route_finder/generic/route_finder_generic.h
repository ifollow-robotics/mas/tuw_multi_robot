#pragma once

namespace stoplights_system
{
    /**
     * @brief Simple class interface to compute the route locally (directly on the robot) or via the service (by calling a service to the stoplight)
     * 
     */
class RouteFinderInterface
{
    public:

    /**
     * @brief 
     * 
     */
    virtual RouteFinderError computeRoute(const Eigen::Vector3d& _starts, const Eigen::Vector3d& _goals, const std::vector<int>& robot_groups) = 0;

    /**
     * @brief Get the Route object
     * 
     * @return std::vector<uint32_t> 
     */
    virtual std::vector<uint32_t> getRoute() const = 0;

    /**
     * @brief Construct a new init Route Graph object
     * 
     */
    virtual void initRouteGraph(const ros::NodeHandle& nh_, const tuw_multi_robot_msgs::Graph& new_graph) = 0;
};

};