#pragma once

namespace stoplights_system
{
class RouteFinderRobot : public RouteFinderInterface
{   
public:
    route_finder::SingleRobotRouteFinder    local_route_finder_;

    RouteFinderRobot();
    RouteFinderError computeRoute(const Eigen::Vector3d& _starts, const Eigen::Vector3d& _goals, const std::vector<int>& robot_groups) override;
    std::vector<uint32_t> getRoute() const override;
    void initRouteGraph(const ros::NodeHandle& nh_, const tuw_multi_robot_msgs::Graph& new_graph) override;

};

};