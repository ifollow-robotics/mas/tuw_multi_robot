
#pragma once

namespace stoplights_system
{
class RouteFinderServer : public RouteFinderInterface
{
public:
    RouteFinderServer(ros::NodeHandle& n, const uint16_t& robot_id);
    RouteFinderError computeRoute(const Eigen::Vector3d& _starts, const Eigen::Vector3d& _goals, const std::vector<int>& robot_groups) override;
    std::vector<uint32_t> getRoute() const override;
    void initRouteGraph(const ros::NodeHandle& nh_, const tuw_multi_robot_msgs::Graph& new_graph) override;

private:
    std::vector<uint32_t> route_;
    ros::ServiceClient  route_finder_client_;
    uint16_t robot_id_;
};

};