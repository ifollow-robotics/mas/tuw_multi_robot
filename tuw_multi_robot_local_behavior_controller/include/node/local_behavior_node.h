// -*- mode: c++; coding: utf-8; tab-width: 4 -*-
/*******************************************************************************
* 2-Clause BSD License
*
* Copyright (c) 2019, iFollow Robotics
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
*   list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*   this list of conditions and the following disclaimer in the documentation
*   and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* Author: Romain Desarzens
* Author: José Mendes Filho (mendesfilho@pm.me)
* Maintainer: José Mendes Filho (mendesfilho@pm.me)
*******************************************************************************/

#pragma once

#include <ros/ros.h>
#include <algorithm>
#include <memory>
#include <mutex>
#include <math.h>
#include <limits.h>
#include <vector>
#include <Eigen/Core>
#include <iostream>
#include <queue>

#include <std_msgs/UInt32.h>
#include <std_msgs/String.h>
#include <std_msgs/UInt8.h>

#include <ifollow_nav_msgs/GetViapoints.h>

#include <ifollow_mas_msgs/StoplightsSysResponse.h>
#include <ifollow_mas_msgs/Graph.h>
#include <ifollow_mas_msgs/StoplightsUpdateHorizon.h>
#include <tuw_multi_robot_msgs/Graph.h>
#include <ifollow_mas_msgs/AreaArray.h>
#include <ifollow_mas_msgs/ConnectedVehicles.h>
#include <ifollow_mas_msgs/LastReservedSegment.h>
#include <ifollow_mas_msgs/GoToMrArea.h>
#include <ifollow_mas_msgs/GenerateRoute.h>
#include <ifollow_utils/str_ops.h>
#include <ifollow_utils/node_heart_beat.h>

#include <tf/transform_datatypes.h>

#include <ifollow_utils/conv_ops.h>
#include <ifollow_utils/mas/load_area_msgs.h>

#include <ifollow_srv_msgs/SetString.h>
#include <ifollow_srv_msgs/SetBrakeCommand.h>

#include <visualization_msgs/MarkerArray.h>

#include <ifollow_mas_msgs/MRAreaMngr.h>
#include <angles/angles.h>
#include <base_footprint_selector/base_footprint_selector.h>

#include <nav_msgs/Path.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/Pose2D.h>
#include <actionlib/client/simple_action_client.h>
#include <move_base_msgs/MoveBaseAction.h>
#include <tf/transform_listener.h>

#include <tuw_multi_robot_msgs/Route.h>
#include <ifollow_mas_msgs/GetCurrentRoute.h>
#include <ifollow_nav_msgs/LocalBehaviorMoveActionGoal.h>
#include <ifollow_nav_msgs/GetGoalParameters.h>
#include <ifollow_nav_msgs/LocalBehaviorMoveAction.h>
#include <actionlib/server/simple_action_server.h>
#include <std_msgs/Int32.h>
#include <ifollow_utils/projection_on_graph.h>
#include <route_finder.h>
#include <ifollow_utils/distance_calculations.h>
#include <ifollow_mas_msgs/NewRoute.h>
#include <iipp_recursive_approach/util.h>

#include <std_srvs/Trigger.h>

#include <node/local_behavior_type.h>
#include "tools/local_behavior_tools.h"
#include <stoplights/stoplights.h>
#include <route_finder/route_finder.h>

/*! \namespace stoplights_system
 *
 * Namespace for stop lights system related components
 */

namespace stoplights_system
{

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
typedef std::shared_ptr<MoveBaseClient> MoveBaseClientPtr;

typedef actionlib::SimpleActionServer<ifollow_nav_msgs::LocalBehaviorMoveAction> LocalBehaviorMoveServer;
typedef std::shared_ptr<LocalBehaviorMoveServer> LBMoveServerPtr;

/*! \class LocalBehaviorNode
 *  \brief Implements all the local behavior of the robot (adaptive goal position, restricted route tracking, interface with central server, etc)
 */
class LocalBehaviorNode
{
public:

    /*!
     *  \brief Constructor for LocalBehaviorNode
     *  \param n : ros node handle
     */
    LocalBehaviorNode(ros::NodeHandle &n);

    /**
     * @brief Main function
     * 
     */
    void run();

    /**
     * @brief Update de behavior
     * 
     */
    void update();

    /**
     * @brief Tell if the node is fully init
     * 
     * @return true 
     * @return false 
     */
    bool isInit();

    /**
     * @brief Get the Last Segment Published on the topic local_behavior/curr_seg_id
     * 
     * @return uint32_t 
     */
    uint32_t getLastSegmentPublished();

    bool isRouteComputedByServer();
    void updateGoalStatus(uint8_t new_status);
    ifollow_nav_msgs::GoalExtendedStatus getGoalStatus();
    bool isBasePoseInit();
    Eigen::Vector3d getRobotPosition();
    std::string getFrameId();
    
    std::vector<int32_t> getRoute()
    {
        return route_;
    }
    
    void setRoute(const std::vector<int32_t>& new_route)
    {
        route_ = new_route;
    }

    /**
     * @brief Return the current standalone mode
     * 
     */
    bool getStandaloneMode()
    {
        return behavior_stoplights_.standalone_mode;
    }

    void setGotNewRoute()
    {
        got_new_route_ = true;
    }

    geometry_msgs::PoseArray getViaPoints()
    {
        return viapoints_;
    }
    
    /**
     * @brief Compute the via points list from the route
     * 
     */
    void computeViaPointsFromRoute_();

    void initRerouting_();

    void updateRouteFinderMode();
    
    /**
     * @brief Compute a new route on the input goal
     * 
     * @param _goal 
     */
    void planRoute(ifollow_nav_msgs::LocalBehaviorMoveGoal goal);

    /**
     * @brief 
     * 
     * @param first_free_slot_it 
     * @param parking_slots 
     * @param target_pose 
     * @return true 
     * @return false 
     */
    bool askAccessForFirstAvailableStloToServer(const int32_t& area_id, const std::vector<ifollow_utils::mas::ParkingSlot>::iterator& first_free_slot_it, const std::vector<ifollow_utils::mas::ParkingSlot>& parking_slots, geometry_msgs::PoseStamped& target_pose);

    /**
     * @brief Update the area status by asking to the server
     * 
     * @param area_id 
     */
    void askAreaStatusToServer(const int32_t& area_id);

    /**
     * @brief Get the First Available Slots for the input area
     * 
     * @param parking_slots 
     * @param first_available_slot 
     * @param cand_parking_slot_idx 
     */
    void getFirstAvailableSlots(std::vector<ifollow_utils::mas::ParkingSlot>& parking_slots, std::vector<ifollow_utils::mas::ParkingSlot>::iterator& first_available_slot, bool& is_area_already_reserved);

    /**
     * @brief Function called when the robot is trying to go to a specific Area
     * 
     * @param area_id 
     */
    void tryToGoToMrArea(const int32_t area_id);

    /*!
     *  \brief Reset the state values of local behavior
     */

    void reset();

    /**
     * @brief Check if a rerouting is requested by the stoplights
     * 
     * @param stop_point 
     */
    bool isReroutingRequiered();

    ros::NodeHandle n_;         // Node handler to the root node
    ros::NodeHandle n_param_;   // Node handler to the current node

private:
    std::mutex route_mutex_;                                    // Mutex to update the route and the robot's position on the route
    std::mutex viapoints_mutex_;                                // Mutex to update the via points list
    std::mutex mb_ac_mutex_;                                    // Mutex for the action server with move base
    std::mutex lb_goal_mutex_;                                  // Mutex for the action server with the task manager
    bool got_graph_     = false;                                // Is the graph is loaded
    bool got_new_route_;                                        // True if the robot has a new route
    bool is_init_;                                              // True if the graph has beend received
    bool base_pose_init_ = false;                               // True when the robot has been init
    bool go_to_area_external_request_;                          // True if the new goal is instead an mr Area
    std::string frame_id_;                                      // Frame_id of the robot
    std::string robot_name_;                                    // Name of the robot
    uint16_t robot_id_;                                         // Robot ID
    ros::ServiceServer viapoints_srv_;                          // Service server to provide viapoints
    MoveBaseClientPtr mb_ac_;                                   // Move_base action client
    LBMoveServerPtr as_;                                        // MoveBase action server
    ros::Subscriber sub_graph_;                                 // Graph Topic
    ros::Subscriber sub_graph_areas_;                           // Area topic
    ros::Subscriber mode_sm_sub_;                               // Robot Mode Subscriber
    ros::Subscriber sub_odom_;                                  // Odometry subscriber
    ros::Subscriber sub_connected_vehicles_;                    // Subscriber to get the robot's list connected
    ros::Subscriber sub_voronoi_graph_;                         // Subscriber for the segment publish by nav_governor
    ros::Subscriber sub_simplified_footprint_;                  // Subscriber fot the footprint
    ros::Subscriber sub_robot_goal_;                            // Subscriber for the navigation goal
    ros::Publisher action_goal_pub_;                            // Publisher for the action server goal
    ros::Publisher route_pub_;                                  // Publisher for new route
    HeartBeats heart_beat_;                                      // HeartBeat structure
    ros::ServiceClient mb_halt_client_;                         // Service to tell moveBase to stop
    ros::ServiceServer mas_nav_srv_;                            // Service to ask a segment reservation
    ros::ServiceServer lights_server_update_last_segment_;      // Service to update the last reserved segment
    ros::ServiceServer get_current_route_;                      // Service to get the current robot's route
    ros::ServiceServer goal_parameters_service_;                // Service to get the goal parameters defined by the task manager
    std::unique_ptr<ros::Rate> rate_;                           // Rate of the node
    Eigen::Vector3d robot_pos_;                                 // robots currently used by the planner
    std::vector<int32_t> route_;                                // Robot's Route
    ifollow_mas_msgs::Graph graph_;                             // Current Graph
    std::map<int32_t, Segment> segments_;                       // Segment map
    geometry_msgs::PoseArray viapoints_;                        // Viapoints Associated to the current route
    geometry_msgs::PoseArray viapoints_to_send_;                // Viapoints to be sent to the planner
    geometry_msgs::PoseStamped last_goal_sent_;                 // Stores last goal sent to move_base
    ifollow_nav_msgs::LocalBehaviorMoveGoal final_goal_;        // Current robot's goal
    ifollow_nav_msgs::LocalBehaviorMoveFeedback feedback_;      // FeedBack of the action server with the task manager 
    ifollow_mas_msgs::ConnectedVehicles connected_vehicles_;    // List of connected vehicules
    BehaviorStoplights behavior_stoplights_;                    // Behavior to manage the stoplights reservation
    Eigen::Vector2d graph_origin_;                              // Graph origin
    double graph_resolution_;                                   // Graph resolution
    ifollow_nav_msgs::GoalExtendedStatus lb_goal_status_;       // Goal status for the action server with the task manager
    ros::Rate loop_rate_;                                       // Loop rate of the action server with the task manager
    ifollow_nav_msgs::GoalParameters default_goal_parameters_;   // Default goal parameters to use if not defined in the file local_behavior.yaml
    std::map<std::string, ifollow_nav_msgs::GoalParameters> goal_parameters_map_; // Map of the goal parameters defined in the file local_behavior.yaml
    stoplights_system::RouteFinderInterface*                route_finder_;
    std::unique_ptr<stoplights_system::RouteFinderRobot>    robot_route_finder_;
    std::unique_ptr<stoplights_system::RouteFinderServer>   server_route_finder_;
    bool use_server_route_finder_;
    uint16_t route_id_;

    /**
     * @brief Init the callback used by the node
     * 
     */
    void initCallBack_();

    /**
     * @brief Init the parameters list use by the node
     * 
     */
    void initParameters_();
    /**
     * @brief Manage the received final_goal check if there is a valid area_id or not
     */
    void sendGoalToPositionOrArea_();

    /**
     * @brief Get the Current Route_ of the robot
     * 
     * @param req 
     * @param resp 
     * @return true 
     * @return false 
     */
    bool getCurrentRoute_(ifollow_mas_msgs::GetCurrentRoute::Request& req, ifollow_mas_msgs::GetCurrentRoute::Response& resp);

    /**
     * @brief Function to update the last reserved segment from the callback
     * 
     * @param req 
     * @param resp 
     * @return true 
     * @return false 
     */
    bool updateLastSegmentReserved_(ifollow_mas_msgs::LastReservedSegment::Request& req, ifollow_mas_msgs::LastReservedSegment::Response& resp);

    /**
     *  @brief Callback to get the route provided by the router
     */
    void initNewRoute_(bool force_robot_route_finder);

    /**
     * @brief Update the robot's state when the robot is freely moving on its route
     * 
     */
    void updateBehaviorStateMoveOnRoad();

    /**
     * @brief Update the robot's state when the robot doesn't has access to the last segment of its horizon
     * 
     */
    void updateBehaviorStateMoveRestricted();

    /**
     * @brief Update the robot's state when the robot doesn't has access to its next segments
     * 
     */
    void updateBehaviorStateWaitingAuth();

    /*!
     *  \brief Service callback called by the planner to get the viapoints
     */
    bool sendViapointsCB_(ifollow_nav_msgs::GetViapoints::Request  &req, ifollow_nav_msgs::GetViapoints::Response &res);

    /*!
     *  \brief Callback to get the odometry data of the robot
     */
    void odomCB_(const nav_msgs::Odometry::ConstPtr& _odom);

    /**
     * @brief CallBack use for the segments topic
     * 
     * @param graph 
     */
    void subGraphCB_(const ifollow_mas_msgs::Graph& graph);

    /**
     * @brief Callback use for the areas topic
     * 
     * @param msg 
     */
    void graphAreasCB_(const ifollow_mas_msgs::AreaArray& msg);

    /**
     * @brief Callback to update if the robot is in teleop or not 
    */
    void modeSmCB_(const std_msgs::StringConstPtr & msg);

    /**
     * @brief CallBack to get the connected vehicules list
     * 
     * @param connected_vehicles 
     */
    void subConnectedVehiclesCB_(const ifollow_mas_msgs::ConnectedVehicles::ConstPtr& connected_vehicles);

    /**
     * @brief CallBack to get the goal parameters
     * 
     * @param connected_vehicles 
     */
    bool getGoalParametersCB_(ifollow_nav_msgs::GetGoalParameters::Request& req, ifollow_nav_msgs::GetGoalParameters::Response& goal_parameters);

    /**
     * @brief CallBack to the service to request the segment's reservation
     * 
     * @param req 
     * @param resp 
     * @return true 
     * @return false 
     */
    bool masNavReqCB_(ifollow_srv_msgs::SetString::Request &req, ifollow_srv_msgs::SetString::Response &resp);

    /**
     * @brief CallBack to manage the graph data
     * 
     * @param msg 
     */
    void graphCB_(const tuw_multi_robot_msgs::Graph& msg);

    /**
     * @brief CallBack to manage the new received goal
     * 
     * @param goal 
     */
    void goalCB_(const geometry_msgs::PoseStamped::ConstPtr& goal);

    void manageNavigationGoal_(const ifollow_nav_msgs::LocalBehaviorMoveGoalConstPtr &goal);

    /**
     * @brief Cancel the current robot's goal 
     * 
     * @param stop_point 
     */
    void cancelCurrentMoveBaseGoal_(const geometry_msgs::PointStamped& stop_point);

    /**
     * @brief Project the point on the segment and add its to the viaPoints list
     * 
     * @param projected_position 
     * @param start_pos_2d 
     * @param end_pos_2d 
     */
    void addProjectedPointToViaPoints_(const Eigen::Vector2d& projected_position, const Eigen::Vector2d& start_pos_2d,  const Eigen::Vector2d& end_pos_2d, bool is_last_segment);

    /**
     * @brief Manage the new final goal
     * 
     * @return true 
     * @return false 
     */
    bool sendFinalGoal_();

    /**
     * @brief run the router to find a route for the received goal
     */
    bool computeRoute();

    /**
     * @brief 
     * 
     */
    void initGoalParameters();
};

/*************************************************************************************************/
static double distanceToSegment(const Segment& seg, const Eigen::Vector3d& _position)
{
    Eigen::Vector2d position;
    position = Eigen::Vector2d{_position[0], _position[1]};

    Eigen::Vector2d n = seg.end - seg.start;

    // Check if the segment length is long enough 
    if (std::hypot(n[0], n[1]) > std::numeric_limits<double>::epsilon())
    {
        Eigen::Vector2d pa = seg.start - position;

        double c = n.dot(pa);
        
        // Closest point is a
        if (c > 0.0f)
        {
            return std::sqrt(pa.dot(pa));
        }

        Eigen::Vector2d bp = position - seg.end;

        // Closest point is b
        if (n.dot(bp) > 0.0f)
        {
            return std::sqrt(bp.dot(bp));
        }

        // Closest point is between a and b
        Eigen::Vector2d e = pa - n * (c / n.dot(n));

        return std::sqrt(e.dot(e));
    }
    else
    {
        return std::hypot(seg.start[0] - _position[0], seg.start[1] - _position[1]);
    }
}

/*************************************************************************************************/
static int32_t getSegment(const std::map<int32_t, Segment>& segments, const Eigen::Vector3d& position)
{
    double minDist = std::numeric_limits<double>::infinity();
    int32_t segment = invalid_id;

    // Select the segment which contains the robot center
    // EDIT: Select the segment which is closer to to robot pose independent of segment width
    for (const auto& [id, seg] : segments)
    {
        double d = distanceToSegment(seg, position);

        if (d < minDist)
        {
            segment = id;
            minDist = d;
        }
    }

    return segment;
}

}  // namespace stoplights_client
