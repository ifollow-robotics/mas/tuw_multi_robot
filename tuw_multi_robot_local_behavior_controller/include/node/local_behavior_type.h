
#include <ifollow_mas_msgs/AreaArray.h>
#include <ifollow_utils/mas/load_area_msgs.h>

namespace stoplights_system
{
    static const double node_frequency  = 15.0; // Rate of the local_behavior_node ~ 0.066 secs
    static const int8_t invalid_id      = -1;   // Invalid id
    
    struct Segment
    {
        Eigen::Vector2d start;
        Eigen::Vector2d end;
        int32_t area_id;
    };

    /*! \enum RobotState
    *  \brief Describes the robot state regarding to local behavior
    */
    enum RobotState 
    {
        NAV_FINISHED,       ///<  Robot is not following any route or has reached goal
        WAITING_AUTH,       ///<  Robot is waiting at a segment and tries to get authorization for the restricted segment
        MOVE_RESTRICTED,    ///<  Robot is moving along the received router BUT will be blocked soon by another robot
        MOVE_ON_ROAD,       ///<  Robot is moving along the received route
        MOVE_OFF_ROAD,      ///<  Robot is moving but not based on a route
        NAV_SLAVE,          ///<  Robot is not moving autonomously and only its master robot is responsible for the segment reservation
        UNKNOWN             ///<  Default status the robot should never be at this state
    };

    enum RouteFinderError
    {
        NO_ROUTE_FOUND = -100,  // No route has been found between the start and the end point
        SERVER_ERROR,           // Error while asking a route to the server
        ROUTE_OK = 0            // A valid route has been found
    };

    struct MRArea 
    {
        ifollow_mas_msgs::Area area_definition;                         // Area definition from the topic
        std::vector<ifollow_nav_msgs::Point2D> simplified;              // List of 2D points to defined the area's shape
        std::vector<ifollow_utils::mas::ParkingSlot> parking_slots;     // List of parking's slots
        ifollow_nav_msgs::Point2D centroid;                             // Center of the area
        double orientation;                                             // Orientation of the area
    };
};