// -*- mode: c++; coding: utf-8; tab-width: 4 -*-
/*******************************************************************************
* 2-Clause BSD License
*
* Copyright (c) 2019, iFollow Robotics
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
*   list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*   this list of conditions and the following disclaimer in the documentation
*   and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* Author: Romain Desarzens
* Author: José Mendes Filho (mendesfilho@pm.me)
* Maintainer: José Mendes Filho (mendesfilho@pm.me)
*******************************************************************************/

#pragma once

#include <ifollow_mas_msgs/StoplightsSys.h>
#include <ifollow_mas_msgs/SimplifiedRoute.h>
#include <ifollow_mas_msgs/StoplightsUpdateHorizon.h>
#include <tuw_multi_robot_msgs/Route.h>
#include <ifollow_utils/ansi_colors.h>
#include <ifollow_utils/id_ops.h>

/*! \namespace stoplights_system
 *
 * Namespace for stop lights system related components
 */

namespace stoplights_system
{

/*! \class StopLightsClientInterface
 *  \brief Interface for a stop lights client class responsible to handle communication with the stop lights system server
 */
class StopLightsClientInterface
{
public:
    StopLightsClientInterface() {
    }
    virtual ~StopLightsClientInterface()
    {
    }

    /*! \brief request authorization to enter a segment
     *  \param actual_segment_id : index of the segment preceding the restricted segment
     *  \param restricted_segment_id : index of the restricted segment the robot wants to access
     *  \return true if the authorization is granted
     */
    virtual ifollow_mas_msgs::StoplightsSysResponse requestAuthorization(const uint32_t & actual_segment_id, const uint32_t & restricted_segment_id, std::vector<uint16_t>& alternative_seq, bool& rerouting_requiered) = 0;

    /*! \brief request authorization to enter a segment
     *  \param distance_to_stop: Request to reserve all the segment on this distance
     *  \return true if the authorization is granted
     */
    virtual ifollow_mas_msgs::StoplightsUpdateHorizonResponse requestHorizon(double& distance_to_stop, std::vector<uint16_t>& alternative_seq, bool& rerouting_requiered) = 0;

    /*! \brief advertise the server that the robot is freeing a segment
     *  \param new_route : new route
     *  \return true if the communication was done correctly
     */
    virtual bool advertiseNewRoute(const std::vector<int32_t>& new_route) = 0;

    /*! \brief advertise the server that the robot is freeing a segment
     *  \return true if the communication was done correctly
     */
    virtual bool resetRoute() = 0;
};

/*! \class StopLightsClientService
 *  \brief Implements StopLightsClientInterface using ROS service to communicate with the server
 */
class StopLightsClientService : public StopLightsClientInterface
{
public:

    /*! \brief Constructor of StopLightsClientService
     *  \param _n : node handle
     *  \param _robot_id : id of the robot that will communicate with the server
     */
    StopLightsClientService(const ros::NodeHandle & _n, const std::string & _robot_id);

    /*! \brief Destructor of StopLightsClientService
     *
     */
    virtual ~StopLightsClientService()
    {
    };

    /*! \brief request authorization to enter a segment
     *  \param actual_segment_id : index of the segment preceding the restricted segment
     *  \param restricted_segment_id : index of the restricted segment the robot wants to access
     *  \return true if the authorization is granted
     */
    virtual ifollow_mas_msgs::StoplightsSysResponse requestAuthorization(const uint32_t & actual_segment_id, const uint32_t & restricted_segment_id, std::vector<uint16_t>& alternative_seq, bool& rerouting_requiered);

    /*! \brief request authorization to enter a segment
     *  \param distance_to_stop: Request to reserve all the segment on this distance
     *  \return true if the authorization is granted
     */
    virtual ifollow_mas_msgs::StoplightsUpdateHorizonResponse requestHorizon(double& distance_to_stop, std::vector<uint16_t>& alternative_seq, bool& rerouting_requiered);

    /*! \brief advertise the server that the robot is freeing a segment
     *  \param new_route : new route
     *  \return true if the communication was done correctly
     */
    virtual bool advertiseNewRoute(const std::vector<int32_t>& new_route);

    /*! \brief advertise the server that the robot is freeing a segment
     *  \return true if the communication was done correctly
     */
    virtual bool resetRoute();


private:
    ros::NodeHandle n_;
    ros::ServiceClient lights_client_in_;
    ros::ServiceClient lights_client_horizon_;
    ros::ServiceClient lights_client_route_;
    std::string robot_id_;
};


/*! \class StopLightsClientSingleRobot
 *  \brief Implements StopLightsClientInterface for a single robot mode. Authorize every request of segment access.
 */
class StopLightsClientSingleRobot : public StopLightsClientInterface
{

public:
    StopLightsClientSingleRobot() {
    };
    virtual ~StopLightsClientSingleRobot()
    {
    };

    /*! \brief request authorization to enter a segment
     *  \param actual_segment_id : index of the segment preceding the restricted segment
     *  \param restricted_segment_id : index of the restricted segment the robot wants to access
     *  \return always true
     */
    virtual ifollow_mas_msgs::StoplightsSysResponse requestAuthorization(const uint32_t& actual_segment_id, const uint32_t& restricted_segment_id, std::vector<uint16_t>& alternative_seq, bool& rerouting_requiered)
    {
        ifollow_mas_msgs::StoplightsSysResponse resp;
        resp.ack = true;
        resp.alternative_seg_seq.clear();

        return resp;
    }

    /*! \brief request segment reservation
     *  \param distance_to_stop: Request to reserve all the segment on this distance
     *  \return always true
     */
    virtual ifollow_mas_msgs::StoplightsUpdateHorizonResponse requestHorizon(double& distance_to_stop, std::vector<uint16_t>& alternative_seq, bool& rerouting_requiered)
    {
        ifollow_mas_msgs::StoplightsUpdateHorizonResponse resp;
        resp.ack = true;
        return resp;
    }

    /*! \brief advertise the server that the robot is freeing a segment
     *  \param new_route : new route
     *  \return true if the communication was done correctly
     */
    virtual bool advertiseNewRoute(const std::vector<int32_t>& new_route)
    {
        return true;
    }

    /*! \brief advertise the server that the robot is freeing a segment
     *  \return true if the communication was done correctly
     */
    virtual bool resetRoute()
    {
        return true;
    }

};

}  // namespace stoplights_client
