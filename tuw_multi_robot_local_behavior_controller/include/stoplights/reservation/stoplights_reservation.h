
#pragma once

namespace stoplights_system
{

class stoplightsReservation
{

public:
    std::vector<int32_t>::iterator current_segment_it;  // iterator pointing to the route's segment before the segment with restricted access
    uint8_t rejected_request_penalty;                   // Penalty to avoid the spam of the reservation request to the stoplights srv, the max value is 5 => 1 secondes between request
    int32_t last_trajectory_segment;                    // Last segment id of the route, use in case of undocking to lock the las segment
    ros::Time penalty_request_time;                     // Penalty to avoid to request a segment when the stoplights has already rejected the request
    ros::Time last_rejected_reservation_request;        // Time of the last rejected request from the stoplights server
    std::map<int32_t, Segment> segments_;               // Segment map
    std::vector<int32_t> route;                         // Current robot's route
    Eigen::Vector3d robot_pos;                          // robots current position
    Eigen::Vector3d robot_goal;                         // robots current goal position
    std::shared_ptr<stoplights_system::StopLightsClientInterface> stop_lights_client;   // handle the communication with the server
    bool is_route_required;                             // True if a rerouting is required by the stoplights
    std::vector<uint16_t> alternative_seq;              // List of alternative segments in case of rerouting

    stoplightsReservation();

    /**
     * @brief 
     * 
     * @return true 
     * @return false 
     */
    bool isTimeToRequestServer();
    
    /**
     * @brief 
     * 
     * @return true 
     * @return false 
     */
    virtual bool isOnLastRouteSegment() = 0;

    /**
     * @brief 
     * 
     */
    void resetRequestPenalty();
    
    /**
     * @brief 
     * 
     */
    void updateRequestPenalty();
    
    /**
     * @brief Set the Request Penalty Time object
     * 
     * @param penalty_time 
     */
    void setRequestPenaltyTime(const double& penalty_time);

    /**
     * @brief Get the Last Route Segment_ object
     * 
     * @return int32_t 
     */
    int32_t getLastRouteSegment_();

    /**
     * @brief Set the Last Route Segment_ object
     * 
     * @param last_route_segment 
     */
    void setLastRouteSegment_();

    /**
     * @brief 
     * 
     * @return true 
     * @return false 
     */
    bool isStoplightsClientValid();

    /**
     * @brief 
     * 
     * @param new_route 
     */
    void advertiseNewRouteToStoplights(const std::vector<int32_t>& new_route);

    /**
     * @brief 
     * 
     * @param current_segment_id 
     * @param next_segment_id 
     * @return ifollow_mas_msgs::StoplightsSysResponse 
     */
    ifollow_mas_msgs::StoplightsSysResponse requestSegmentAuthorization(const uint32_t& current_segment_id, const uint32_t& next_segment_id);

    /**
     * @brief 
     * 
     * @param _segments 
     */
    void initSegments(const std::map<int32_t, Segment>& _segments);

    bool updateLastSegmentFromServerRequest(const uint16_t& last_segment_id, const uint8_t& penality_time);

    void updateHorizon_(const uint16_t offset_segment_idx);

    bool isReroutingRequired();

    void resetRerouting();

    /**
     * @brief 
     * 
     * @param route 
     */
    virtual void resetRoute() = 0;

    /**
     * @brief 
     * 
     * @param new_route 
     */
    virtual void initRoute(const std::vector<int32_t>& new_route, const geometry_msgs::Point& goal_point) = 0;

    /**
     * @brief 
     * 
     * @param current_segment_it 
     */
    virtual void updateRemainingAccess(const int32_t& current_segment_it) = 0;

    /**
     * @brief 
     * 
     * @return true 
     * @return false 
     */
    virtual bool isAccessStillBlocked() = 0;

    /**
     * @brief 
     * 
     * @return true 
     * @return false 
     */
    virtual bool isAccessRestricted() = 0;

    /**
     * @brief 
     * 
     */
    virtual void updateSegmentReservation() = 0;

    /**
     * @brief 
     * 
     * @return true 
     * @return false 
     */
    virtual bool isAccessBlocked() = 0;

    /**
     * @brief 
     * 
     * @return true 
     * @return false 
     */
    virtual bool isAccessUnblocked() = 0;

    /**
     * @brief Get the Stop Point object
     * 
     * @return geometry_msgs::PointStamped 
     */
    virtual geometry_msgs::PointStamped getStopPoint() = 0;

    virtual void updateReservationFromPosition(const Eigen::Vector3d& robot_pos, bool is_in_teleop) = 0;

    virtual bool isRequestBehind() = 0;
};

};

