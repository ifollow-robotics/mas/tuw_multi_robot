
#pragma once

namespace stoplights_system
{

class SegmentReservation : public stoplightsReservation
{

public:
    std::vector<int32_t>::iterator last_accessible_segment_it;  // iterator pointing to the route's segment before the segment with restricted access
    int dist_to_restricted_segment; // number of segments between the stopping segment and the restricted segment
    int nb_segment_horizon;         // number of segments between the last viapoint provided and the robot position

    SegmentReservation(ros::NodeHandle n);

    ifollow_mas_msgs::StoplightsSysResponse requestSegmentAuthorization(const uint32_t& current_segment_id, const uint32_t& next_segment_id);

    bool isOnLastRouteSegment();
    
    void resetRoute();
    void updateRemainingAccess(const int32_t& current_segment_it);
    void initRoute(const std::vector<int32_t>& new_route, const geometry_msgs::Point& goal_point);

    bool isAccessStillBlocked();
    bool isAccessRestricted();
    void updateSegmentReservation();
    bool isAccessBlocked();
    bool isAccessUnblocked();
    geometry_msgs::PointStamped getStopPoint();
    void updateReservationFromPosition(const Eigen::Vector3d& robot_pos, bool is_in_teleop);
    bool isRequestBehind();
};

};
