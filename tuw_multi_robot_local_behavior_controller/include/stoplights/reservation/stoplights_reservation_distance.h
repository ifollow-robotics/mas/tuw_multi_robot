
#pragma once

namespace stoplights_system
{
    static const double min_horizon_distance_constant  = 0.1;

class DistanceReservation : public stoplightsReservation
{

public:
    double max_reserved_horizon;    // Distance ahead of the robot's position to reserve
    double min_reserved_horizon;    // Distance ahead of the robot's position to reserve
    double remaining_horizon;       // horizon left on the robot's route 
    double reserved_horizon;        // Horizon reserved by the robot

    /**
     * @brief Construct a new Distance Reservation object
     * 
     * @param n 
     */
    DistanceReservation(ros::NodeHandle n);

    bool isOnLastRouteSegment();   
    void resetRoute();
    void updateRemainingAccess(const int32_t& current_segment_it);
    void initRoute(const std::vector<int32_t>& new_route, const geometry_msgs::Point& goal_point);
    bool isAccessStillBlocked();
    bool isAccessRestricted();
    void updateSegmentReservation();
    bool isAccessBlocked();
    bool isAccessUnblocked();
    geometry_msgs::PointStamped getStopPoint();
    geometry_msgs::Point stopPointOnCurrentSegment(bool direction_forward, bool current_robot_segment, const double& remaining_distance, const Segment& Segment);
    bool getDirectionOnSegment(const stoplights_system::Segment& current_segment, const stoplights_system::Segment& next_segment);

    void updateRemainingDistance();
    void updateReservationFromPosition(const Eigen::Vector3d& robot_pos, bool is_in_teleop);
    bool isRequestBehind();
};

};
