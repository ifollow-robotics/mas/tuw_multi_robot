// -*- mode: c++; coding: utf-8; tab-width: 4 -*-
/*******************************************************************************
* 2-Clause BSD License
*
* Copyright (c) 2019, iFollow Robotics
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
*   list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*   this list of conditions and the following disclaimer in the documentation
*   and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* Author: GOSSEYE Aurélien
* Maintainer: GOSSEYE Aurelien (mendesfilho@pm.me)
*******************************************************************************/

#pragma once

namespace stoplights_system
{

enum stoplightsRequestToServerState
{
    STOPLIGHTS_REQUEST_STATE_NO_REQUEST,    // No request to the server will be done to manage the stoplights  
    STOPLIGHTS_REQUEST_STATE_POSSIBLE,      // Request will done only if there is other's robots connected in the factory
    STOPLIGHTS_REQUEST_STATE_MANDATORY      // Request are mandatory even if the robot is alone
};

class BehaviorStoplights
{

public:
    bool standalone_mode;                                   // True if single robot mode
    bool prev_standalone_mode;                              // Previous state of the standalone mode
    bool try_reach_goal;                                    // True if the robot is reaching the end of the route and not an intermediary goal
    bool is_in_teleop;                                      // True if the robot is in teleop, this is use to update the last reserved segment of the robot when the topic "odom" is publishing
    int current_area;                                       // Current area associated to the robot
    int current_slot;                                       // Current slot inside the area associated to the robot
    tuw::RouteProgressMonitor progress_monitor;             // Track the motion of the robot on the route and provides the actual occupied segment
    RobotState robot_state;                                 // State of the robot
    std::map<int, MRArea> areas;                            // List of MR area associated with the graph
    uint32_t last_seg_to_pub_id;                            // Last segment published, use in case of undocking to lock the las segment
    ros::Publisher curr_seg_publisher;                      // Publish the current segment id (use by nav_governor)
    ros::Publisher traffic_status_publisher;                // Display the current traffic status of the robot (gree/red light) 
    ros::ServiceClient mr_area_mngr_client;                 // Service to call when the robot need to go to the MR Area
    ros::Duration no_connected_vehicles_pub_timeout;        // Time out to activate the standalone mode if the last publiscation is too old
    ros::Time last_connected_vehicles_publication_time;     // Last the topic "connected_vehicles" has publish use to update the standalone mode
    std::shared_ptr<stoplights_system::stoplightsReservation> stoplights_reservation_manager;     // Manage the segment reservation with the server                                             

    /**
     * @brief Return the current standalone mode
     * 
     */
    bool getStandaloneMode()
    {
        return standalone_mode;
    }

    /**
     * @brief 
     * 
     * @param node_handle 
     * @param robot_name 
     */
    void init(ros::NodeHandle& node_handle, const std::string& robot_name);

    /**
     * @brief Function called by the robot to ask the mr_area_mgnr to leave its current area
     * 
     * @param robot_id 
     * @return true 
     * @return false 
     */
    bool requestToLeaveMrArea(const uint16_t robot_id);

    /**
     * @brief Init the parking slots of the input area
     * 
     * @param area 
     * @param area_msgs 
     */
    void segmentAreaIntoParkingSlots(MRArea& area, const ifollow_mas_msgs::Area& area_msgs);

    /**
     * @brief Update the parking occupation from the input message
     * 
     * @param area_id 
     * @param occupation_message 
     */
    void updateParkingOccupationFromMessage(const int32_t& area_id, const ifollow_mas_msgs::MRAreaMngr& occupation_message);

    /**
     * @brief Update the stoplights mode (standalone or not)
     * 
     */
    void updateStoplightsMode();

    int32_t getCurrentSegmentOnRoute();

    geometry_msgs::PointStamped getStopPoint();

    /*!
     *  \brief Request authorization for the restricted segments for a given amount of segments and compute the last accessible segment
     */
    void updateReservationHorizon();

    /**
     * @brief Reset the route of the robot
     * 
     */
    bool isReroutingRequired();

    void resetRerouting();

    /**
     * @brief Publish the current robot's segment
     * 
     */
    void publishCurrSeg(const Eigen::Vector3d& robot_pos, const std::vector<int32_t> & route, const std::map<int32_t, Segment>& segments);

    /**
     * @brief Update the traffic status
     *
     * @param _robot_state
     */
    void updateTrafficStatus(RobotState _robot_state);

    /**
     * @brief Request access to the input area
     * 
     * @param robot_id
     * @param area_id 
     * @param robot_pos
     * @param cand_parking_slot 
     * @param goal 
     * @return true 
     * @return false 
     */
    bool requestToEnterMrArea(const uint16_t robot_id, int32_t area_id, const Eigen::Vector3d& robot_pos, std::vector<ifollow_utils::mas::ParkingSlot>::iterator cand_parking_slot, geometry_msgs::PoseStamped& goal);

    stoplightsRequestToServerState getIgnoreStoplightsStatus() const {return stoplights_request_state_;}
    
private:
    std::shared_ptr<stoplights_system::StopLightsClientInterface> standalone_stop_lights_client_;   // ByPass the server for the segment request
    std::shared_ptr<stoplights_system::StopLightsClientInterface> mrs_stop_lights_client_;          // handle the communication with the server for the segment reservation
    stoplightsRequestToServerState stoplights_request_state_;                                       // Tell the state fo the stoplights request
};

};