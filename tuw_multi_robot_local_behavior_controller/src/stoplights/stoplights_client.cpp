/*******************************************************************************
* 2-Clause BSD License
*
* Copyright (c) 2019, iFollow Robotics
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
*   list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*   this list of conditions and the following disclaimer in the documentation
*   and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* Author: Romain Desarzens
* Author: José Mendes Filho (mendesfilho@pm.me)
* Maintainer: José Mendes Filho (mendesfilho@pm.me)
*******************************************************************************/

#include "stoplights/stoplights_client.h"

namespace stoplights_system {

/*************************************************************************************************/
StopLightsClientService::StopLightsClientService(const ros::NodeHandle& _n, const std::string& _robot_id) :
    n_(_n),
    robot_id_(_robot_id)
{
    // Creates client for the server services
    lights_client_in_ = n_.serviceClient<ifollow_mas_msgs::StoplightsSys>("stoplights_sys_in");
    lights_client_horizon_  = n_.serviceClient<ifollow_mas_msgs::StoplightsUpdateHorizon>("stoplights_reserve_horizon");
    lights_client_route_ = n_.serviceClient<ifollow_mas_msgs::SimplifiedRoute>("stoplights_new_route");
}

/*************************************************************************************************/
ifollow_mas_msgs::StoplightsSysResponse StopLightsClientService::requestAuthorization(
    const uint32_t& actual_segment_id, const uint32_t& restricted_segment_id, std::vector<uint16_t>& alternative_seq, bool& rerouting_requiered)
{

    ifollow_mas_msgs::StoplightsSys srv;
    srv.request.robot_id = id_ops::encodeId(robot_id_);
    srv.request.access_seg_id = restricted_segment_id;
    srv.request.curr_seg_id = actual_segment_id;

    srv.response.ack = false;
    srv.response.blocking_robot = id_ops::encodeId(robot_id_);
    srv.response.alternative_seg_seq.clear();

    if (!lights_client_in_.waitForExistence(ros::Duration(1)))
    {

        ROS_ERROR_STREAM(ros::this_node::getName() << ": timeout, service 'stoplights_sys_in' not available");
    }
    else
    {

        bool srv_call_success = lights_client_in_.call(srv);

        if (srv_call_success)
        {
            if (false == srv.response.alternative_seg_seq.empty())
            {
                alternative_seq = srv.response.alternative_seg_seq;
                rerouting_requiered = true;
            }
        }
    }

    return srv.response;
}

/*************************************************************************************************/
ifollow_mas_msgs::StoplightsUpdateHorizonResponse StopLightsClientService::requestHorizon(double& distance_to_stop, std::vector<uint16_t>& alternative_seq, bool& rerouting_requiered)
{
    ifollow_mas_msgs::StoplightsUpdateHorizon srv;
    srv.request.robot_id = id_ops::encodeId(robot_id_);
    srv.request.distance = distance_to_stop;
    distance_to_stop = 0.0;

    srv.response.ack = false;

    if (!lights_client_horizon_.waitForExistence(ros::Duration(1)))
    {
        ROS_ERROR_STREAM(ros::this_node::getName() << ": timeout, service 'stoplights_sys_in' not available");
    }
    else
    {
        bool srv_call_success = lights_client_horizon_.call(srv);
        distance_to_stop = srv.response.distance_to_stop;

        if (srv_call_success)
        {
            if (false == srv.response.alternative_seg_seq.empty())
            {
                alternative_seq = srv.response.alternative_seg_seq;
                rerouting_requiered = true;
            }
        }
    }

    return srv.response;
}

/*************************************************************************************************/
bool StopLightsClientService::advertiseNewRoute(const std::vector<int32_t>& new_route)
{
    ifollow_mas_msgs::SimplifiedRoute srv;
    srv.request.robot_id = id_ops::encodeId(robot_id_);
    
    // Loop on the route's segment
    for (const auto& segment : new_route)
    {
        srv.request.segment_ids.push_back(segment);
    }

    if (!lights_client_route_.waitForExistence(ros::Duration(1)))
    {

        ROS_ERROR_STREAM(ros::this_node::getName() << ": timeout, service 'stoplights_new_route' not available");
        return false;
    }
    else
    {

        bool srv_call_success = lights_client_route_.call(srv);

        if (srv_call_success)
        {
            return true;
        }
        else
        {
            ROS_ERROR_STREAM_THROTTLE(1.0, ros::this_node::getName() << ": new route service call failed");
            return false;
        }
    }
}

/*************************************************************************************************/
bool StopLightsClientService::resetRoute()
{
    ifollow_mas_msgs::SimplifiedRoute srv;
    srv.request.robot_id = id_ops::encodeId(robot_id_);

    if (!lights_client_route_.waitForExistence(ros::Duration(1)))
    {

        ROS_ERROR_STREAM(ros::this_node::getName() << ": timeout, service 'stoplights_new_route' not available");
        return false;
    }
    else
    {

        bool srv_call_success = lights_client_route_.call(srv);

        if (srv_call_success)
        {
            return true;
        }
        else
        {
            ROS_ERROR_STREAM_THROTTLE(1.0, ros::this_node::getName() << ": reset route service call failed");
            return false;
        }
    } 
}

} // namespace stoplights_client
