
#include "node/local_behavior_node.h"

namespace stoplights_system 
{

/*************************************************************************************************/
void LocalBehaviorNode::tryToGoToMrArea(const int32_t area_id)
{
    // Local variables
    move_base_msgs::MoveBaseGoal goal;
    bool able_to_enter_area = false;
    bool is_area_already_reserved = false;
    std::vector<ifollow_utils::mas::ParkingSlot>::iterator first_free_slot_it; 

    // Check if the input area is valid
    if (behavior_stoplights_.areas.count(area_id) == 0)
    {
        ROS_WARN_STREAM("The input area id " << area_id << " is not valid.");
        return;
    }
    
    // While loop, try to get a free parking slots
    while (false == able_to_enter_area)
    {
        // Update the local status of the area
        getFirstAvailableSlots(behavior_stoplights_.areas[area_id].parking_slots, first_free_slot_it, is_area_already_reserved);

        // Check if we find a parking slot for the input robot
        if (true == is_area_already_reserved)
        {
            // Request to enter inside the area
            able_to_enter_area = behavior_stoplights_.requestToEnterMrArea(robot_id_, area_id, robot_pos_, first_free_slot_it, goal.target_pose);
        }
        else
        {
            // Check if there is no slot available in the area
            if (first_free_slot_it == behavior_stoplights_.areas[area_id].parking_slots.end())
            {
                ROS_INFO_STREAM_THROTTLE(2.0, ros::this_node::getName() << ": " << FG_B_L_MAGENTA << "no free slots in area " << 
                    area_id << " nb slots:" << behavior_stoplights_.areas[area_id].parking_slots.size() << RESET);

                // Cancel the goals
                mb_ac_->cancelAllGoals();
                mb_ac_->waitForResult();

                // Ask to the server the current state of the area
                askAreaStatusToServer(area_id);
            }
            else
            {
                // Ask access for the first available slot to the server
                able_to_enter_area = askAccessForFirstAvailableStloToServer(area_id, first_free_slot_it, behavior_stoplights_.areas[area_id].parking_slots, goal.target_pose);
            }
        }
        // Wait one second before asking again to enter in the zone
        if (false == able_to_enter_area)
        {
            ros::Duration(1).sleep();
        }
    }

    // Update the message data
    goal.target_pose.header.stamp = ros::Time::now();
    goal.target_pose.header.frame_id = frame_id_;

    // Send the new goal
    mb_ac_->sendGoal(goal);
}

/*************************************************************************************************/
bool LocalBehaviorNode::askAccessForFirstAvailableStloToServer(const int32_t& area_id, const std::vector<ifollow_utils::mas::ParkingSlot>::iterator& first_free_slot_it, const std::vector<ifollow_utils::mas::ParkingSlot>& parking_slots, geometry_msgs::PoseStamped& target_pose)
{
    bool able_to_enter_area;

    // Check if the first fleet slots is valid
    if (first_free_slot_it != parking_slots.end())
    {
        // Request to the server the access to the the area
        able_to_enter_area = behavior_stoplights_.requestToEnterMrArea(robot_id_, area_id, robot_pos_, first_free_slot_it, target_pose);
    }
    else
    {
        ROS_ERROR_STREAM_THROTTLE(2.0, ros::this_node::getName() << ": " << FG_B_L_MAGENTA << " There is at least 1 free slots but last_occupied_slot_it and first_free_slot_it are invalid" << RESET);
        able_to_enter_area = false;
    }

    return able_to_enter_area;
}

/*************************************************************************************************/
void LocalBehaviorNode::askAreaStatusToServer(const int32_t& area_id)
{
    bool srv_call_success = false;;
    ifollow_mas_msgs::MRAreaMngr area_request_msg;

    // ask server for an update on the occupancy, no request to enter or leave
    area_request_msg.request.req = ifollow_mas_msgs::MRAreaMngr::Request::INFO; // query info about area
    area_request_msg.request.area_id = area_id;

    try
    {
        srv_call_success = behavior_stoplights_.mr_area_mngr_client.call(area_request_msg);
    }
    catch(std::exception &e)
    {
        ROS_ERROR_STREAM_THROTTLE(2.0, ros::this_node::getName() << ": service [" << behavior_stoplights_.mr_area_mngr_client.getService() << "] call failed. " << e.what());
    }

    // Check if we can enter in the area
    if ((true == srv_call_success) && (true == area_request_msg.response.resp))
    {
        // Update the local occupancy information for area_id
        for (int i_parking = 0; i_parking < behavior_stoplights_.areas[area_id].parking_slots.size(); ++i_parking)
        {
            // Check if the current parking slots is available in the area
            auto it = std::find(area_request_msg.response.occupied_slots.begin(), area_request_msg.response.occupied_slots.end(), i_parking);
            if (it != area_request_msg.response.occupied_slots.end())
            {
                // Get the slot idx
                auto found_idx = std::distance(area_request_msg.response.occupied_slots.begin(), it);
                
                // Update the slots status
                behavior_stoplights_.areas[area_id].parking_slots[i_parking].occupation_id = area_request_msg.response.occupied_slots_vehicles[found_idx];
            }
            else
            {
                // Reset the slot status
                behavior_stoplights_.areas[area_id].parking_slots[i_parking].occupation_id = invalid_id;
            }
        }
    }
    else
    {
        // ERROR calling the server
        ros::Duration(4).sleep();
    }
}

/*************************************************************************************************/
void LocalBehaviorNode::getFirstAvailableSlots(std::vector<ifollow_utils::mas::ParkingSlot>& parking_slots, std::vector<ifollow_utils::mas::ParkingSlot>::iterator& first_available_slot, bool& is_area_already_reserved)
{
    first_available_slot = parking_slots.end();
    is_area_already_reserved = false;

    // Loop on the parking slot list of the area
    for (auto it_parking = parking_slots.begin(); it_parking != parking_slots.end(); ++it_parking)
    {
        // Check if the slot is occupied by a robot
        if (it_parking->occupation_id != invalid_id) // ps not free
        {
            // Check if the slots is not already occupied by the requesting robot
            if (uint16_t(it_parking->occupation_id) == robot_id_)
            {
                // Set the first available slots
                first_available_slot = it_parking;
                is_area_already_reserved = true;
                break;
            }
        }
        else 
        {
            // Set  the current slots as the first available slots 
            first_available_slot = it_parking;
            break;
        }
    }
}

}
