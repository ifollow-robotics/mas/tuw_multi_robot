
#include "node/local_behavior_node.h"

namespace stoplights_system 
{

/*************************************************************************************************/
stoplightsReservation::stoplightsReservation()
{
    last_rejected_reservation_request = ros::Time(0.0);
    last_trajectory_segment    = -1;
    penalty_request_time        = ros::Time(0.0);
    is_route_required = false;
}

/*************************************************************************************************/
bool stoplightsReservation::isTimeToRequestServer()
{
    ros::Time current_time = ros::Time::now();
    
    return ((current_time - last_rejected_reservation_request) > (ros::Duration(0.2) * rejected_request_penalty)) && 
        (current_time > penalty_request_time);
}

/*************************************************************************************************/
void stoplightsReservation::updateRequestPenalty()
{
    // Update the last rejected request
    last_rejected_reservation_request = ros::Time::now();

    // Increment the request penalty
    rejected_request_penalty = std::min<float>(rejected_request_penalty + 1, 5u);
}

/*************************************************************************************************/
void stoplightsReservation::setRequestPenaltyTime(const double& penalty_time)
{
    penalty_request_time = ros::Time::now() + ros::Duration(penalty_time);      
}

/*************************************************************************************************/
void stoplightsReservation::resetRequestPenalty()
{
    rejected_request_penalty = 0u;
}

/*************************************************************************************************/
int32_t stoplightsReservation::getLastRouteSegment_()
{
    return last_trajectory_segment;
}

/*************************************************************************************************/
void stoplightsReservation::setLastRouteSegment_()
{
    last_trajectory_segment = *current_segment_it;
}

/*************************************************************************************************/
bool stoplightsReservation::isStoplightsClientValid()
{
    return (nullptr != stop_lights_client);
}

/*************************************************************************************************/
void stoplightsReservation::advertiseNewRouteToStoplights(const std::vector<int32_t>& new_route)
{
    stop_lights_client->advertiseNewRoute(new_route);
}

/*************************************************************************************************/
ifollow_mas_msgs::StoplightsSysResponse stoplightsReservation::requestSegmentAuthorization(const uint32_t& current_segment_id, const uint32_t& next_segment_id)
{
    return stop_lights_client->requestAuthorization(current_segment_id, next_segment_id, alternative_seq, is_route_required);
}

/*************************************************************************************************/
void stoplightsReservation::initSegments(const std::map<int32_t, Segment>& _segments)
{
    segments_ = _segments;
}

/*************************************************************************************************/
bool stoplightsReservation::updateLastSegmentFromServerRequest(const uint16_t& last_segment_id, const uint8_t& penality_time)
{
    bool success = true;

    // Check if the robot has a current itinerary
    if (false == route.empty())
    {
        // Loop on the segment list
        for (std::vector<int32_t>::iterator current_segment = route.begin(); current_segment != route.end(); ++current_segment)
        {
            // Check if the current segment is the requested segment
            if (*current_segment == last_segment_id)
            {
                // The requested segment is not before the current segment position, it is not possible
                if (current_segment > current_segment_it)
                {
                    // Update the new last restricted segment
                    updateRemainingAccess(last_segment_id);   
                    setRequestPenaltyTime(penality_time);
                }
                else
                {
                    ROS_WARN_STREAM("The new last reserved segment " << last_segment_id << 
                        " is before the current robot segment " << *current_segment_it);
    
                    success = false;
                }

                break;
            }
        }
    }
    else
    {
        // Ignore the request because the robot has no current itinerary
    }

    return success;
}
    
/*************************************************************************************************/
void stoplightsReservation::updateHorizon_(const uint16_t offset_segment_idx)
{
    if (true == route.empty())
    {
        ROS_ERROR_STREAM(ros::this_node::getName() << ": route is empty, aborting");
        return;
    }
    else
    {
        // Get segment that the robot has reached and check if we are leaving it
        current_segment_it = route.begin() + offset_segment_idx;

        if (true == isTimeToRequestServer())
        {
            updateSegmentReservation();
        }
    }
}

/*************************************************************************************************/
bool stoplightsReservation::isReroutingRequired()
{
    return is_route_required;
}

/*************************************************************************************************/
void stoplightsReservation::resetRerouting()
{
    is_route_required = false;
}

};