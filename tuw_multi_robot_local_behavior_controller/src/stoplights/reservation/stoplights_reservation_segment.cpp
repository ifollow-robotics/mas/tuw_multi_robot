
#include "node/local_behavior_node.h"

namespace stoplights_system 
{
    
/*************************************************************************************************/
SegmentReservation::SegmentReservation(ros::NodeHandle n) : stoplightsReservation()
{
    last_accessible_segment_it = route.begin();
    current_segment_it = route.begin();

    n.param<int>("local_behavior/dist_to_restricted_segment",  dist_to_restricted_segment, 2);
    n.param<int>("local_behavior/nb_segment_horizon",          nb_segment_horizon, 3);

    if (dist_to_restricted_segment < 1)
    {
        throw std::runtime_error("Distance to segment should be bigger than 1");
    }
}

/*************************************************************************************************/
bool SegmentReservation::isOnLastRouteSegment()
{
    // Check if the robot is in the last segment
    return ((false == route.empty()) && 
        ((std::distance(last_accessible_segment_it, route.end()) <= 1) ||
        (std::distance(current_segment_it, route.end()) <= 1)));
}

/*************************************************************************************************/
void SegmentReservation::resetRoute()
{   
    route.clear();

    last_accessible_segment_it = route.begin();
    current_segment_it = route.begin();

    resetRequestPenalty();
}

/*************************************************************************************************/
void SegmentReservation::initRoute(const std::vector<int32_t>& new_route, const geometry_msgs::Point& goal_point)
{
    route = new_route;
    last_accessible_segment_it = (route.begin() + 1);
    current_segment_it = route.begin();
    robot_goal = {goal_point.x, goal_point.y, goal_point.z};
}

/*************************************************************************************************/
void SegmentReservation::updateRemainingAccess(const int32_t& new_segment_it)
{
    // Update the last accessible route segment
    std::vector<int32_t>::iterator new_last_authorized_segment = std::find(route.begin(), route.end(), new_segment_it);

    if(new_last_authorized_segment != route.end())
    {
        last_accessible_segment_it = new_last_authorized_segment;
    }
}

/*************************************************************************************************/
bool SegmentReservation::isAccessRestricted()
{
    int distance_to_last_accessible_segment = std::distance(current_segment_it, last_accessible_segment_it);

    // Check the distance to the last available segment
    if ((distance_to_last_accessible_segment >= dist_to_restricted_segment) &&
        (distance_to_last_accessible_segment < nb_segment_horizon))
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*************************************************************************************************/
bool SegmentReservation::isAccessStillBlocked()
{
    if((std::distance(current_segment_it, last_accessible_segment_it) < dist_to_restricted_segment) &&
        (std::distance(last_accessible_segment_it, route.end()) > 1) &&
        (*current_segment_it != route.back()))
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*************************************************************************************************/
void SegmentReservation::updateSegmentReservation()
{
    std::vector<int32_t>::iterator it;

    // Check if we need to resolve a jump in the segment reservation this case shouldn't reverve happend
    if (last_accessible_segment_it < current_segment_it)
    {
        // Use min to avoid to have unreserved segment on the robot's route
        it = std::min(last_accessible_segment_it, current_segment_it + 1);

        ROS_WARN_STREAM("Jump in the segment reservation robot segment: " << *current_segment_it <<
            " last accessible segment: " << *last_accessible_segment_it);
    }
    else
    {
        // Use min to avoid to have unreserved segment on the robot's route
        it = std::max(last_accessible_segment_it, current_segment_it + 1);
    }

    while (it != route.end())
    {
        // Ask authorization for the segment at the requested distance
        if (std::distance(current_segment_it, last_accessible_segment_it) <= nb_segment_horizon)
        {
            if (it != route.begin())
            {
                // Request authorization to the server
                ifollow_mas_msgs::StoplightsSysResponse resp = stop_lights_client->requestAuthorization(*(it - 1), *it, alternative_seq, is_route_required);

                // Check if the request is accepted or not
                if (false == resp.ack)
                {
                    updateRequestPenalty();
                    break;
                }
                else
                {
                    // Reset the penalty
                    resetRequestPenalty();
                }
            }
            else
            {
                ROS_ERROR_STREAM(ros::this_node::getName() << ": " << FG_B_L_MAGENTA << "can't request access for the first segment" << RESET);
            }
        }
        else
        {
            break;
        }

        it++;

        // If the segment is either large enough or we granted access to the segment,
        // we increment the iterator
        ++last_accessible_segment_it;
        // If the horizon has been updated then the robot is moving
    }
}

/*************************************************************************************************/
bool SegmentReservation::isAccessBlocked()
{
    // Get the distance to the last accesible segment on the itinerary
    int distance_to_last_accessible_segment = std::distance(current_segment_it, last_accessible_segment_it);

    return (distance_to_last_accessible_segment < dist_to_restricted_segment);
}

/*************************************************************************************************/
bool SegmentReservation::isAccessUnblocked()
{
    // Get the distance to the last accesible segment on the itinerary
    int distance_to_last_accessible_segment = std::distance(current_segment_it, last_accessible_segment_it);

    // Get the distance from the end itinerary segment
    int distance_to_end_segment = std::distance(current_segment_it, route.end());

    return ((distance_to_last_accessible_segment > dist_to_restricted_segment) ||
        (distance_to_last_accessible_segment == distance_to_end_segment));
}

/*************************************************************************************************/
geometry_msgs::PointStamped SegmentReservation::getStopPoint()
{
    geometry_msgs::PointStamped point_to_stop;
    
    std::vector<int32_t>::iterator it = std::max(last_accessible_segment_it - 1, current_segment_it);

    point_to_stop.point.x = (segments_[*it].end[0] + segments_[*it].start[0]) / 2.;
    point_to_stop.point.y = (segments_[*it].end[1] + segments_[*it].start[1]) / 2.;

    ROS_INFO_STREAM(ros::this_node::getName() << " Stop position " << (*it) << " ["<< point_to_stop.point.x << "," << point_to_stop.point.y << "]");

    point_to_stop.header.frame_id = "world";

    return point_to_stop;
}

/*************************************************************************************************/
void SegmentReservation::updateReservationFromPosition(const Eigen::Vector3d& robot_pos, bool is_in_teleop) 
{
    // Check if the robot is in teleop or not
    if ((true == is_in_teleop) && (last_accessible_segment_it < current_segment_it))
    {
        last_accessible_segment_it = current_segment_it + 1;
    }       
}

/*************************************************************************************************/
bool SegmentReservation::isRequestBehind()
{
    return (last_accessible_segment_it < current_segment_it);
}

};