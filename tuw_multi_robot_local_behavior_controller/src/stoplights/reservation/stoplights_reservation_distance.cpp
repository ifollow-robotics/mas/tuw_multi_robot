
#include "node/local_behavior_node.h"

namespace stoplights_system 
{

/*************************************************************************************************/
DistanceReservation::DistanceReservation(ros::NodeHandle n) : stoplightsReservation()
{
    n.param<double>("local_behavior/max_reserved_horizon", max_reserved_horizon, 3.0);
    n.param<double>("local_behavior/min_reserved_horizon", min_reserved_horizon, 1.0);

    reserved_horizon    = 0.0;
    remaining_horizon   = 0.0;
}

/*************************************************************************************************/
bool DistanceReservation::isOnLastRouteSegment()
{
    return ((false == route.empty()) && (std::distance(current_segment_it, route.end()) <= 1));
}

/*************************************************************************************************/
void DistanceReservation::resetRoute()
{
    route.clear();

    current_segment_it = route.begin();

    resetRequestPenalty();
}

/*************************************************************************************************/
void DistanceReservation::updateRemainingAccess(const int32_t& new_segment_it)
{
    std::vector<int32_t>::iterator new_last_authorized_segment = std::find(route.begin(), route.end(), new_segment_it);

    if(new_last_authorized_segment != route.end())
    {
        reserved_horizon = min_horizon_distance_constant;
    }
}

/*************************************************************************************************/
void DistanceReservation::initRoute(const std::vector<int32_t>& new_route, const geometry_msgs::Point& goal_point)
{
    route = new_route;
    current_segment_it  = route.begin();
    reserved_horizon    = min_horizon_distance_constant;  // Default distance
    remaining_horizon   = 0.11; // Default distance
    robot_goal = {goal_point.x, goal_point.y, goal_point.z};
}

/*************************************************************************************************/
bool DistanceReservation::isAccessStillBlocked()
{
    return ((reserved_horizon < min_reserved_horizon) && (remaining_horizon > reserved_horizon) &&
            ((false == route.empty()) && (*(current_segment_it) != route.back())));
}

/*************************************************************************************************/
bool DistanceReservation::isAccessRestricted()
{
    return ((reserved_horizon < remaining_horizon) && (reserved_horizon < (max_reserved_horizon + min_reserved_horizon) / 2.0));
}

/*************************************************************************************************/
void DistanceReservation::updateSegmentReservation()
{
    // Ask authorization for the segment at the requested distance
    if ((reserved_horizon * 0.9) < max_reserved_horizon)
    {
        // Update the current reserved horizon distance
        reserved_horizon = max_reserved_horizon;

        // Request authorization to the server
        ifollow_mas_msgs::StoplightsUpdateHorizonResponse resp = stop_lights_client->requestHorizon(reserved_horizon, alternative_seq, is_route_required);

        // Check if the request is accepted or not
        if ((false == resp.ack) && (reserved_horizon < min_reserved_horizon))
        {
            updateRequestPenalty();
        }
        else
        {
            // Reset the penalty
            resetRequestPenalty();
        }
    }
}

/*************************************************************************************************/
bool DistanceReservation::isAccessBlocked()
{
    return (reserved_horizon < min_reserved_horizon);
}

/*************************************************************************************************/
bool DistanceReservation::isAccessUnblocked()
{
    return ((reserved_horizon > 1.1*(min_reserved_horizon + max_reserved_horizon)/2.0) || (reserved_horizon >= remaining_horizon));
}

/*************************************************************************************************/
geometry_msgs::PointStamped DistanceReservation::getStopPoint()
{
    geometry_msgs::PointStamped stop_point;
    stop_point.header.frame_id = "world";
    double remaining_distance = 0.0;
    double projectionX, projectionY;
    bool direction_forward  = true;
    double distance_to_stop = reserved_horizon;

    if (true == route.empty())
    {
        stop_point.point.x = std::numeric_limits<double>::quiet_NaN();
        stop_point.point.y = std::numeric_limits<double>::quiet_NaN();

        return stop_point;
    }
    else if (1 == route.size())
    {
        // Compute the distance between the current robot's position and the horizon to stop
        remaining_distance = std::hypot((robot_goal[0] - robot_pos[0]), (robot_goal[1] - robot_pos[1]));

        // The distance to the goal is below the distance to stop
        if (remaining_distance <= distance_to_stop)
        {
            stop_point.point.x = robot_goal[0];
            stop_point.point.y = robot_goal[1];
        }
        else
        {
            const double coeff = distance_to_stop / remaining_distance;
            stop_point.point.x = robot_pos[0] + ((robot_goal[0] - robot_pos[0]) * coeff);
            stop_point.point.y = robot_pos[1] + ((robot_goal[1] - robot_pos[1]) * coeff);
        }

        return stop_point;
    }

    // By default the robot will stop at the end of the road
    direction_forward = !getDirectionOnSegment(segments_[route[route.size() - 1]], segments_[route[route.size() - 2]]);

    if (true == direction_forward)
    {
        stop_point.point.x = segments_[route.back()].end.x();
        stop_point.point.y = segments_[route.back()].end.y();
    }
    else
    {
        stop_point.point.x = segments_[route.back()].start.x();
        stop_point.point.y = segments_[route.back()].start.y();
    }

    auto& current_segment       = segments_[*(current_segment_it)];
    auto& next_robot_segment    = segments_[*(current_segment_it + 1)];
    
    double start_x = segments_[*(current_segment_it)].start.x();
    double start_y = segments_[*(current_segment_it)].start.y();
    double end_x = segments_[*(current_segment_it)].end.x();
    double end_y = segments_[*(current_segment_it)].end.y();

    pog::projectPositionOnSegment(segments_[*(current_segment_it)].start, segments_[*(current_segment_it)].end, robot_pos[0], robot_pos[1], projectionX, projectionY);
                
    // Check if the current end point is equal to the linked start point
    if (std::hypot((end_x - next_robot_segment.start.x()), (end_y - next_robot_segment.start.y())) < (0.01))
    {
        // The current segment is connected to the first point of the other segment
        remaining_distance = hypot((projectionX - end_x), (projectionY - end_y));
        direction_forward = true;
    }
    else if (std::hypot((end_x - next_robot_segment.end.x()), (end_y - next_robot_segment.end.y())) < (0.01))
    {
        // The current segment is connected to the first point of the other segment
        remaining_distance = hypot((projectionX - end_x), (projectionY - end_y));
        direction_forward = true;
    }
    // Check if the start point is equal to the end point
    else if (std::hypot((start_x - next_robot_segment.end.x()), (start_y - next_robot_segment.end.y())) < (0.01))
    {   
        // The current segment is connected to the first point of the other segment
        remaining_distance = hypot((projectionX - start_x), (projectionY - start_y));
        direction_forward = false;
    }
    else if (std::hypot((start_x - next_robot_segment.start.x()), (start_y - next_robot_segment.start.y())) < (0.01))
    {
        // The current segment is connected to the last point of the other segment
        remaining_distance = hypot((projectionX - start_x), (projectionY - start_y));
        direction_forward = false;
    }

    // Check if we need to stop on the current segment
    if (remaining_distance > distance_to_stop)
    {        
        stop_point.point = stopPointOnCurrentSegment(direction_forward, true, distance_to_stop, current_segment);
    }
    else
    {
        distance_to_stop -= remaining_distance;

        // Loop on the remaining route's segment
        for (auto tmp_segment_it = (current_segment_it + 1) ; tmp_segment_it != route.end(); ++tmp_segment_it)
        {
            auto& tmp_segment = segments_[*tmp_segment_it];
            // Project the robot position on the segment
            start_x = tmp_segment.start.x();
            start_y = tmp_segment.start.y();
            end_x   = tmp_segment.end.x();
            end_y   = tmp_segment.end.y();

            // Compute the segment distance
            remaining_distance = hypot((end_x - start_x), (end_y - start_y));

            // Check if its not the last segment of the route
            if (tmp_segment_it < (route.end() - 1))
            {
                direction_forward = getDirectionOnSegment(segments_[*(tmp_segment_it)], segments_[*(tmp_segment_it + 1)]);
            }
            else
            {
                direction_forward = !getDirectionOnSegment(segments_[*(tmp_segment_it)], segments_[*(tmp_segment_it - 1)]);
            }

            if (remaining_distance > distance_to_stop)
            {
                stop_point.point = stopPointOnCurrentSegment(direction_forward, false, distance_to_stop, tmp_segment);
                break;
            }

            distance_to_stop -= remaining_distance;
        }
    }
    
    return stop_point;
}

/*************************************************************************************************/
geometry_msgs::Point DistanceReservation::stopPointOnCurrentSegment(bool direction_forward, bool current_robot_segment, const double& remaining_distance, const Segment& segment)
{
    geometry_msgs::Point stop_point;
    double start_x, start_y, end_x, end_y, dx, dy, projectionX, projectionY, length;

    // Check the direction on the segment
    if (true == direction_forward)
    {
        // Check if this is the current robot's segment
        if (true == current_robot_segment)
        {
            // The start point is the current robot's position
            pog::projectPositionOnSegment(segment.start, segment.end, robot_pos[0], robot_pos[1], projectionX, projectionY);
            start_x = projectionX;
            start_y = projectionY;
        }
        else
        {
            // The start point is the start point of the segment
            start_x = segment.start.x();
            start_y = segment.start.y();
        }

        // The end point is the end point of the segment
        end_x = segment.end.x();
        end_y = segment.end.y();
    }
    else
    {
        // Check if this is the current robot's segment
        if (true == current_robot_segment)
        {
            // The start point is the current robot's position
            pog::projectPositionOnSegment(segment.start, segment.end, robot_pos[0], robot_pos[1], projectionX, projectionY);
            start_x = projectionX;
            start_y = projectionY;
        }
        else
        {
            // The start point is the end point of the segment
            start_x = segment.end.x();
            start_y = segment.end.y();
        }

        // The end point is the start point of the segment
        end_x = segment.start.x();
        end_y = segment.start.y();
    }

    dx = (end_x - start_x);
    dy = (end_y - start_y);
    
    // Compute the length on the segment
    length = std::fmax(std::hypot(dx, dy), remaining_distance);

    stop_point.x = start_x + (dx * (remaining_distance / length));
    stop_point.y = start_y + (dy * (remaining_distance / length));

    return stop_point;
}

/*************************************************************************************************/
void DistanceReservation::updateReservationFromPosition(const Eigen::Vector3d& new_robot_pos, bool is_in_teleop)
{            
    robot_pos = new_robot_pos;
    updateRemainingDistance();
}

/*************************************************************************************************/
void DistanceReservation::updateRemainingDistance()
{
    double projectionX, projectionY, projectionEndX, projectionEndY;
    double remaining_distance = 0.0;

    // Check if the route is empty
    if (true == route.empty())
    {
        // The remaining distance is reset
        remaining_horizon = 0.11;
        return;
    }

    // Check the route size is at least two segments size
    if (route.size() > 1)
    {
        auto& current_segment       = segments_[*(current_segment_it)];
        auto& next_robot_segment    = segments_[*(current_segment_it + 1)];
        
        double start_x = segments_[*(current_segment_it)].start.x();
        double start_y = segments_[*(current_segment_it)].start.y();
        double end_x = segments_[*(current_segment_it)].end.x();
        double end_y = segments_[*(current_segment_it)].end.y();

        // Project the robot's position on the graph
        pog::projectPositionOnSegment(segments_[*(current_segment_it)].start, segments_[*(current_segment_it)].end, robot_pos[0], robot_pos[1], projectionX, projectionY);

        // Compute the direction on the current robot's segment
        bool is_going_forward = getDirectionOnSegment(current_segment, next_robot_segment);

        // Check if the robot is going forward on the segment (from start to end)
        if (true == is_going_forward)
        {
            // The current segment is connected to the first point of the other segment
            remaining_distance = std::hypot((projectionX - end_x), (projectionY - end_y));
        }
        else
        {
            // The current segment is connected to the first point of the other segment
            remaining_distance = std::hypot((projectionX - start_x), (projectionY - start_y));
        }

        // Loop on the remaining route's segment
        for (auto tmp_segment_it = (current_segment_it + 1) ; tmp_segment_it < (route.end() - 1); ++tmp_segment_it)
        {
            auto& tmp_segment = segments_[*tmp_segment_it];

            // Project the robot position on the segment
            start_x = tmp_segment.start.x();
            start_y = tmp_segment.start.y();
            end_x   = tmp_segment.end.x();
            end_y   = tmp_segment.end.y();

            // Compute the segment distance
            remaining_distance += std::hypot((end_x - start_x), (end_y - start_y));
        }

        // Compute the direction between the last segment and the segment before the last (the result is opposite)
        is_going_forward = !getDirectionOnSegment(segments_[route.back()], segments_[route[route.size() - 2]]);

        pog::projectPositionOnSegment(segments_[route.back()].start, segments_[route.back()].end, robot_goal[0], robot_goal[1], projectionX, projectionY);

        // Check the direction on the last segment
        if (true == is_going_forward)
        {
            remaining_distance += std::hypot((projectionX - segments_[route.back()].start.x()), (projectionY - segments_[route.back()].start.y()));
        }
        else
        {
            remaining_distance += std::hypot((projectionX - segments_[route.back()].end.x()), (projectionY - segments_[route.back()].end.y()));
        }
    }
    else
    {
        // Project the start and end point of the route 
        pog::projectPositionOnSegment(segments_[route.back()].start, segments_[route.back()].end, robot_pos[0], robot_pos[1], projectionX, projectionY);
        pog::projectPositionOnSegment(segments_[route.back()].start, segments_[route.back()].end, robot_goal[0], robot_goal[1], projectionEndX, projectionEndY);

        remaining_distance = std::hypot((projectionX - projectionEndX), (projectionY - projectionEndY));
    }

    remaining_horizon = remaining_distance;
}

/**
 * @brief Helper function to obtain the closest point on a line segment w.r.t. a reference point
 * @param point 2D point
 * @param line_start 2D point representing the start of the line segment
 * @param line_end 2D point representing the end of the line segment
 * @return true is the direction on the current segment is from start to end, return false otherwise
 */
bool DistanceReservation::getDirectionOnSegment(const stoplights_system::Segment& current_segment, const stoplights_system::Segment& next_segment)
{
    bool direction_forward      = true;
    double distance_tolerance   = 0.01;
    
    const Eigen::Vector2d& current_start    = current_segment.start; 
    const Eigen::Vector2d& current_end      = current_segment.end;
    const Eigen::Vector2d& next_start       = next_segment.start; 
    const Eigen::Vector2d& next_end         = next_segment.end;

    // Check if the current end point is equal to the linked start point
    if ((std::hypot((current_end.x() - next_start.x()), (current_end.y() - next_start.y())) < distance_tolerance) ||
        (std::hypot((current_end.x() - next_end.x()), (current_end.y() - next_end.y())) < distance_tolerance))
    {
        // Do nothing already true
    }
    // Check if the start point is equal to the end point
    else if ((std::hypot((current_start.x() - next_end.x()), (current_start.y() - next_end.y())) < distance_tolerance) ||
        (std::hypot((current_start.x() - next_start.x()), (current_start.y() - next_start.y())) < distance_tolerance))
    {   
        // The current segment is connected to the first point of the other segment
        direction_forward = false;
    }
    else
    {
        ROS_WARN_STREAM("Unconnected segments"); 
    }

    return direction_forward;
} 

/*************************************************************************************************/
bool DistanceReservation::isRequestBehind()
{
    return false;
}

};