/* Copyright (c) 2017, TU Wien
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * Neither the name of the <organization> nor the
 *    names of its contributors may be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY TU Wien ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL TU Wien BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Edited by: José Mendes
 *
 */

#include <node/local_behavior_node.h>
#include <stoplights/stoplights_progressMonitor.h>

namespace  tuw {

/*************************************************************************************************/
RouteProgressMonitor::Segment::Segment(double x0, double y0, double x1, double y1)
{
    start   = {x0, y0};
    end     = {x1, y1};
    state = SEGMENT_STATE_AHEAD;
}

/*************************************************************************************************/
RouteProgressMonitor::RouteProgressMonitor()
    : idx_active_segment_(0)
{
}

/*************************************************************************************************/
void RouteProgressMonitor::init(std::map<int32_t, stoplights_system::Segment>& _segments, const std::vector<int32_t>& route )
{
    segments_.clear();

    for (size_t i = 0; i < route.size(); i++)
    {
        const auto &seg = _segments[route[i]];

        SegmentPtr s = SegmentPtr(new RouteProgressMonitor::Segment(seg.start[0], seg.start[1], seg.end[0], seg.end[1]));
        if (i == 0)
        {
            s->state = SEGMENT_STATE_ACTIVE;
        }

        segments_.push_back(s);
    }

    idx_active_segment_ = 0;
}

/*************************************************************************************************/
uint16_t RouteProgressMonitor::getProgress()
{
    return idx_active_segment_;
}

/*************************************************************************************************/
void RouteProgressMonitor::updateProgress(const Eigen::Vector3d& position)
{
    if (true == segments_.empty())
    {
        return;
    }

    // Compute distance to segment
    float min_dist = FLT_MAX;
    int idx_closest_seg = stoplights_system::invalid_id;
    int i = 0;

    for (auto segment : segments_)
    {
        Eigen::Vector2d ep, ls, le;
        ep << position[0], position[1];
        segment->distance = dist_calc::distance_point_to_segment_2d(ep, segment->start, segment->end);

        if (min_dist >= segment->distance)
        {
            idx_closest_seg = i;
            min_dist = segment->distance;
        }
        ++i;
    }

    if (segments_.size() > 1)
    {
        for (size_t i_segment = 0; i_segment < (segments_.size() - 1); i_segment++)
        {
            SegmentPtr curr = segments_[i_segment];
            SegmentPtr next = segments_[i_segment + 1];
            // If the current segment is reached
            if ( (curr->state == SEGMENT_STATE_ACTIVE) && (next->state == SEGMENT_STATE_AHEAD) )
            {
                double d_curr = curr->distance;
                double d_next = next->distance;

                // Check if robot is closer from the next segment than the current one
                if ((d_next <= d_curr) || (idx_closest_seg > i_segment))
                {
                    curr->state = SEGMENT_STATE_INACTIVE;
                    next->state = SEGMENT_STATE_ACTIVE;
                    idx_active_segment_ = static_cast<uint16_t>(i_segment + 1);
                    break;
                }
            }
        }
    }
    else
    {
        segments_[0]->state = SEGMENT_STATE_ACTIVE;
    }
}

};
