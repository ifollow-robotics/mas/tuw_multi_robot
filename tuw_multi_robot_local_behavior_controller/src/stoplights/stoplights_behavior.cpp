/*******************************************************************************
* 2-Clause BSD License
*
* Copyright (c) 2019, iFollow Robotics
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
*   list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*   this list of conditions and the following disclaimer in the documentation
*   and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* Author: GOSSEYE Aurélien
* Author: GOSSEYE Aurélien
* Maintainer: GOSSEYE Aurélien
*******************************************************************************/

#include "node/local_behavior_node.h"

namespace stoplights_system
{

/*************************************************************************************************/
void BehaviorStoplights::init(ros::NodeHandle& node_handle, const std::string& robot_name)
{
    stoplights_request_state_        = STOPLIGHTS_REQUEST_STATE_POSSIBLE;
    standalone_mode                 = true;
    current_area                    = static_cast<int>(invalid_id);
    current_slot                    = static_cast<int>(invalid_id);
    standalone_stop_lights_client_  = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();
    mrs_stop_lights_client_         = std::make_shared<stoplights_system::StopLightsClientService>(node_handle, robot_name);
    traffic_status_publisher   = node_handle.advertise<std_msgs::UInt8>("local_behavior/traffic_status", 1, true);
    curr_seg_publisher         = node_handle.advertise<std_msgs::UInt32>("local_behavior/curr_seg_id",   1, true);
    std::string stoplights_reservation_mode;
    double timeout = 0.0;

    node_handle.param<double>("local_behavior/no_connected_vehicles_pub_timeout", timeout, 30.);
    no_connected_vehicles_pub_timeout = ros::Duration(timeout);

    // Check if the new reservation mode is authorized
    if (true == node_handle.hasParam("local_behavior/stoplights_request_state"))
    {
        std::string stoplights_reservation_state_string;

        // get the reservation mode preference
        node_handle.getParam("local_behavior/stoplights_request_state", stoplights_reservation_state_string);

        // Check the stoplights request state
        if ("Ignored" == stoplights_reservation_state_string)
        {
            ROS_WARN_STREAM("The Robot will not ask stoplights for any reservation");
            stoplights_request_state_ = STOPLIGHTS_REQUEST_STATE_NO_REQUEST;
        }
        else if ("Authorized" == stoplights_reservation_state_string)
        {
            ROS_WARN_STREAM("The Robot will ask stoplights for reservation");
            stoplights_request_state_ = STOPLIGHTS_REQUEST_STATE_POSSIBLE;
        }
        else if ("Mandatory" == stoplights_reservation_state_string)
        {
            ROS_WARN_STREAM("The Robot will always ask for reservation");
            stoplights_request_state_ = STOPLIGHTS_REQUEST_STATE_MANDATORY;
        }
        else
        {
            ROS_WARN_STREAM("The param local_behavior/stoplights_request_state is not valid");
        }
    }
    else if(true == node_handle.hasParam("local_behavior/ignore_stoplights_even_in_mrs"))
    {
        bool tmp_ignore_stoplights_status = false;
        node_handle.param<bool>("local_behavior/ignore_stoplights_even_in_mrs", tmp_ignore_stoplights_status, tmp_ignore_stoplights_status );

        stoplights_request_state_ = tmp_ignore_stoplights_status ? STOPLIGHTS_REQUEST_STATE_NO_REQUEST : STOPLIGHTS_REQUEST_STATE_POSSIBLE;
    }
    else
    {
        // Do nothing
    }

    // Check if the pref is defined
    if(true == node_handle.hasParam("local_behavior/stoplights_reservation_mode"))
    {
        // get the reservation mode preference
        node_handle.getParam("local_behavior/stoplights_reservation_mode", stoplights_reservation_mode);

        if ("Segment" == stoplights_reservation_mode)
        {
            stoplights_reservation_manager = std::make_shared<stoplights_system::SegmentReservation>(node_handle);
        }
        else if("Distance" == stoplights_reservation_mode)
        {
            stoplights_reservation_manager = std::make_shared<stoplights_system::DistanceReservation>(node_handle);
        }
        else
        {
            ROS_WARN_STREAM("Unknowned reservation type " << stoplights_reservation_mode.c_str());
            stoplights_reservation_manager = std::make_shared<stoplights_system::SegmentReservation>(node_handle);
        }
    }
    else
    {
        stoplights_reservation_manager = std::make_shared<stoplights_system::SegmentReservation>(node_handle);
    }

    // Set the default stop_lights_client as standalone
    stoplights_reservation_manager->stop_lights_client = standalone_stop_lights_client_;
    robot_state             = NAV_FINISHED;
    try_reach_goal          = false;
    prev_standalone_mode    = false;
}

/*************************************************************************************************/
bool BehaviorStoplights::requestToLeaveMrArea(const uint16_t robot_id)
{
    ifollow_mas_msgs::MRAreaMngr msg;
    msg.request.req = ifollow_mas_msgs::MRAreaMngr::Request::OUT;
    msg.request.area_id     = current_area;
    msg.request.slot_id     = current_slot;
    msg.request.vehicle_id  = robot_id;

    bool srv_call_success = false;
    // cppcheck-suppress variableScope
    int ps_idx = 0;

    // Check if the input area exist
    if (areas.find(current_area) != areas.end())
    {
        for (auto& ps : areas[current_area].parking_slots)
        {
            if (ps.occupation_id != invalid_id)
            {
                msg.request.occupied_slots.push_back(ps_idx);
                msg.request.occupied_slots_vehicles.push_back(ps.occupation_id);
            }
            ++ps_idx;
        }
    }

    try
    {
        srv_call_success = mr_area_mngr_client.call(msg);
    }
    catch(std::exception &e)
    {
        ROS_ERROR_STREAM_THROTTLE(2.0, ros::this_node::getName() << ": service [" << mr_area_mngr_client.getService() << "] call failed. " << e.what());
        return false;
    }

    // Check if the answer is negative
    if (false == msg.response.resp)
    {
        updateParkingOccupationFromMessage(current_area, msg);
    }

    return (srv_call_success && msg.response.resp);
}

/*************************************************************************************************/
void BehaviorStoplights::updateParkingOccupationFromMessage(const int32_t& area_id, const ifollow_mas_msgs::MRAreaMngr& occupation_message)
{
    // Check if the input area exist
    if (areas.find(area_id) != areas.end())
    {
        // Loop on the parking slots
        for (int i = 0; i < areas[area_id].parking_slots.size(); ++i)
        {
            // Find the current occupied slots
            auto it = std::find(occupation_message.response.occupied_slots.begin(), occupation_message.response.occupied_slots.end(), i);

            // Check if we find it
            if (it != occupation_message.response.occupied_slots.end())
            {
                // Update the occupation status
                auto found_idx = std::distance(occupation_message.response.occupied_slots.begin(), it);
                areas[area_id].parking_slots[i].occupation_id = occupation_message.response.occupied_slots_vehicles[found_idx];
            }
            else
            {
                // Set the default value for the occupied slots
                areas[area_id].parking_slots[i].occupation_id = invalid_id;
            }
        }
    }
}

/*************************************************************************************************/
void BehaviorStoplights::segmentAreaIntoParkingSlots(MRArea& area, const ifollow_mas_msgs::Area& area_msgs)
{
    // look at graph to define the orientation of the slots      => theta
    // load maximum number of robots in this environment less 1  => max_capacity
    // min_capacity always equals to 1                           => min_capacity
    // the robots' footprint for this env. give min bounding box => l_min, w_min
    // max bounding box of few times the min bbox is sensible    => l_max, w_max
    //
    // count number of vertex in area
    //   if != 4 TODO
    //   else
    //     find rectangle oriented at theta that "fits the best" the area
    //
    // get L, W values for the area's rectable
    //
    // we have an algorithm (iipp_ra) that given {l, w, L, W} with l, w being:
    // - l: dimension of parking slots along the defined direction theta
    // - w: dimension of parking slots orthogonal the defined direction theta
    // returns the maximum number of slots (c) that can fit the rectagle and
    // their position
    //
    // if L ou W smaller than l, w respectively, L, W equals l, w and
    // there is only one slot, at the centroid. Return.
    //
    // in addition to that, we could optimize for l, w such that the capacity (c),
    // l, and y are maximized (more and bigger slots)
    // constraints/bounds:
    // - min_capacity <= c <= max_capacity
    // - l_min <= l <= l_max
    // - w_min <= w <= w_max
    //
    //  once the packing problem is solved, we can use the "priority vertex"
    //  to define the order/priority of the slots.
    //
    //  HINT for defining the priority vertex:
    //  priority vertex should be the the farthest along theta from the
    //  direction robots are more likely to come to enter the area
    //  +
    //  the closest to the graph
    //
    //  we can order based on the shortest distance of each slot center poisiton
    //  to the priority vertex

    // DUMMY test of iipp lib
    // std::cout << ptoDiv1 << std::endl; // var from iipp project
    // auto s = newSet(10);               // function from iipp project
    // Loop on the points list
    ifollow_nav_msgs::Point2D pt;
    double avg_x = 0;
    double avg_y = 0;
    // cppcheck-suppress variableScope
    double L, W, c_theta, s_theta, theta, l_min, w_min;
    int** sol;
    int q[4];

    // Loop on the point list
    for (const auto& area_point : area_msgs.points)
    {
        avg_x += area_point.x;
        avg_y += area_point.y;
    }

    pt.x = avg_x / area_msgs.points.size();
    pt.y = avg_y / area_msgs.points.size();
    area.area_definition = area_msgs;
    area.centroid = pt;

    theta = atan2( (area_msgs.points[1].y - area_msgs.points[0].y), (area_msgs.points[1].x - area_msgs.points[0].x));

    base_footprint_selector::getMinDimensionsOfLargerFootprint(l_min, w_min); // try loading something first

    Eigen::Matrix2d rot;
    c_theta = cos(theta);
    s_theta = sin(theta);

    // x, y are wrt world
    // theta is wrt to world

    // rotate points of -theta
    // cppcheck-suppress constStatement
    rot << c_theta, s_theta, -s_theta, c_theta;

    bool greater_dim_is_along_x = true;

    // find rectangle oriented at theta that "fits the best" the area => np
    Eigen::Matrix<double, 2, 4> simpl_points;

    if (area.area_definition.points.size() == 4)
    {
        Eigen::Matrix<double, 2, 4> points;
        points << area.area_definition.points[0].x, area.area_definition.points[1].x, area.area_definition.points[2].x, area.area_definition.points[3].x,
            area.area_definition.points[0].y, area.area_definition.points[1].y, area.area_definition.points[2].y, area.area_definition.points[3].y;

        simpl_points = ifollow_utils::mas::computeLengthAndWidthOfArea(points, rot, L, W, greater_dim_is_along_x);

        int res = solveIIPP(round(L * 100), round(W * 100), round(l_min * 100), round(w_min * 100), q, &sol);

        // Create the slot for the area
        area.parking_slots = ifollow_utils::mas::createParkingSlots_(area.area_definition.id, simpl_points, rot, greater_dim_is_along_x, res, sol);

        for (auto i = 0; i < res; ++i)
        {
            delete[] sol[i];
        }
        delete[] sol;
    }
}

/*************************************************************************************************/
void BehaviorStoplights::updateStoplightsMode()
{
    // Check if the robot is not sleeping for too long
    if ((stoplightsRequestToServerState::STOPLIGHTS_REQUEST_STATE_MANDATORY == stoplights_request_state_))
    {
        standalone_mode = false;
        stoplights_reservation_manager->stop_lights_client = mrs_stop_lights_client_;
    }
    else if ((stoplightsRequestToServerState::STOPLIGHTS_REQUEST_STATE_NO_REQUEST == stoplights_request_state_))
    {
        standalone_mode = true;
        stoplights_reservation_manager->stop_lights_client = standalone_stop_lights_client_;
    }
    else if ((ros::Time::now() - last_connected_vehicles_publication_time) > no_connected_vehicles_pub_timeout)
    {
        standalone_mode = true;
    }

    if (standalone_mode && !prev_standalone_mode)
    {
        ROS_INFO_STREAM(ros::this_node::getName() << ": " << FG_B_L_MAGENTA << "standalone_mode!" << RESET);

        // Check if the standalone mode is authorized
        if (stoplightsRequestToServerState::STOPLIGHTS_REQUEST_STATE_MANDATORY != stoplights_request_state_)
        {
            stoplights_reservation_manager->stop_lights_client = standalone_stop_lights_client_;
        }
    }
    else if (!standalone_mode && prev_standalone_mode)
    {
        ROS_INFO_STREAM(ros::this_node::getName() << ": " << FG_B_L_MAGENTA << "mrs_mode!" << RESET);

        // Check if the multi-robot is authorized
        if (stoplightsRequestToServerState::STOPLIGHTS_REQUEST_STATE_NO_REQUEST != stoplights_request_state_)
        {
            stoplights_reservation_manager->stop_lights_client = mrs_stop_lights_client_;
        }
        else
        {
            ROS_INFO_STREAM(ros::this_node::getName() << ": " << FG_B_L_MAGENTA << "but going to run red ligths anyway (ignore stoplights param is set)" << RESET);
        }
    }

    if (standalone_mode != prev_standalone_mode)
    {
        prev_standalone_mode = standalone_mode;
    }

}

/*************************************************************************************************/
int32_t BehaviorStoplights::getCurrentSegmentOnRoute()
{
    return *(stoplights_reservation_manager->current_segment_it);
}

/*************************************************************************************************/
geometry_msgs::PointStamped BehaviorStoplights::getStopPoint()
{
    return stoplights_reservation_manager->getStopPoint();
}

/*************************************************************************************************/
void BehaviorStoplights::updateReservationHorizon()
{
    stoplights_reservation_manager->updateHorizon_(progress_monitor.getProgress());
}

/*************************************************************************************************/
bool BehaviorStoplights::isReroutingRequired()
{
    return stoplights_reservation_manager->isReroutingRequired();
}

/*************************************************************************************************/
void BehaviorStoplights::resetRerouting()
{
    stoplights_reservation_manager->resetRerouting();
}


/*************************************************************************************************/
void BehaviorStoplights::publishCurrSeg(const Eigen::Vector3d& robot_pos, const std::vector<int32_t>& route, const std::map<int32_t, Segment>& segments)
{
    // Message to publish
    std_msgs::UInt32 msg;

    int32_t seg_to_pub_id = invalid_id;

    // Check if the robot has a current route
    if (false == route.empty())
    {
        // Get the current segment id
        seg_to_pub_id = *(route.begin() + progress_monitor.getProgress());
    }
    // publish current segment based on simple odom and graph
    else if (((0 == std::isnan(robot_pos[0])) &&
                (0 == std::isnan(robot_pos[1])) &&
                (0 == std::isnan(robot_pos[2]))))
    {
        // Get the segment based on the graph
        seg_to_pub_id = getSegment(segments, robot_pos);
    }
    else
    {
        ROS_WARN_STREAM("The robot has no current route and its position is not valid.");
    }

    // Check if this is a new current segment id and the value is valid
    if ((seg_to_pub_id != last_seg_to_pub_id) && (invalid_id != seg_to_pub_id))
    {
        msg.data = seg_to_pub_id;
        curr_seg_publisher.publish(msg);
        last_seg_to_pub_id = seg_to_pub_id;
    }
}

/*************************************************************************************************/
void BehaviorStoplights::updateTrafficStatus(RobotState _robot_state)
{
    // local variable
    std_msgs::UInt8 traffic_status_message;
    traffic_status_message.data = static_cast<uint8_t>(2);

    // Update the robot state
    robot_state = _robot_state;

    // Check if the robot is in standalone mode or not
    if (false == standalone_mode)
    {
        switch (robot_state)
        {
            // The two cases have identical behavior
            case MOVE_OFF_ROAD:
            case MOVE_ON_ROAD:
            {
                traffic_status_message.data = static_cast<uint8_t>(0);
                break;
            }
            case MOVE_RESTRICTED:
            {
                traffic_status_message.data = static_cast<uint8_t>(1);
                break;
            }
            // The two cases have identical behavior
            case WAITING_AUTH:
            case UNKNOWN:
            {
                // Check if the last reserved segment is behind its current position
                if (true == stoplights_reservation_manager->isRequestBehind())
                {
                    // Robot Jumped on its route and can't catch up
                    traffic_status_message.data = static_cast<uint8_t>(6);
                }
                else
                {
                    traffic_status_message.data = static_cast<uint8_t>(2);
                }
                break;
            }
            case NAV_FINISHED:
            {
                traffic_status_message.data = static_cast<uint8_t>(3);
                break;
            }
            case NAV_SLAVE:
            {
                traffic_status_message.data = static_cast<uint8_t>(4);
                break;
            }
            default:
            {
                ROS_WARN_STREAM("The input robot state is not valid " << robot_state);
                break;
            }
        }
    }
    else
    {
        traffic_status_message.data = static_cast<uint8_t>(5);
    }

    traffic_status_publisher.publish(traffic_status_message);
}

/*************************************************************************************************/
bool BehaviorStoplights::requestToEnterMrArea(const uint16_t robot_id, int32_t area_id, const Eigen::Vector3d& robot_pos, std::vector<ifollow_utils::mas::ParkingSlot>::iterator cand_parking_slot, geometry_msgs::PoseStamped& goal)
{
    ifollow_mas_msgs::MRAreaMngr msg;
    msg.request.req = ifollow_mas_msgs::MRAreaMngr::Request::IN;
    msg.request.area_id = area_id;
    msg.request.slot_id = std::distance(areas[area_id].parking_slots.begin(), cand_parking_slot);
    msg.request.vehicle_id = robot_id;

    int ps_idx = 0;
    for (auto& ps : areas[area_id].parking_slots)
    {
        if (ps.occupation_id != -1)
        {
            msg.request.occupied_slots.push_back(ps_idx);
            msg.request.occupied_slots_vehicles.push_back(ps.occupation_id);
        }
        ++ps_idx;
    }

    ROS_INFO_STREAM_THROTTLE(2.0, ros::this_node::getName() << ": " << FG_B_L_MAGENTA << "call mr_area_mngr with " << stringReq(msg.request) << RESET);

    bool srv_call_success = false;
    try
    {
        srv_call_success = mr_area_mngr_client.call(msg);
    }
    catch(std::exception &e)
    {
        ROS_ERROR_STREAM_THROTTLE(2.0, ros::this_node::getName() << ": service [" << mr_area_mngr_client.getService() << "] call failed. " << e.what());
    }

    if (srv_call_success && msg.response.resp)
    {
        goal.pose.position.x = cand_parking_slot->pose.x;
        goal.pose.position.y = cand_parking_slot->pose.y;

        ROS_WARN_STREAM_THROTTLE(2.0, ros::this_node::getName() << ": " << FG_B_L_MAGENTA << "sending goal to move_base for slot " <<
            msg.request.slot_id << " [" << goal.pose.position.x << ", " << goal.pose.position.y << "]" << RESET);

        ROS_INFO_STREAM("Area Goal position: " << goal.pose.position.x << " " << goal.pose.position.y);

        // if diff of robot's orientation and cand_parking_slot->pose.theta is greater than 90deg, rotation goal of 180
        if (std::abs(angles::normalize_angle(cand_parking_slot->pose.theta - robot_pos[2])) <= M_PI_2)
        {
            goal.pose.orientation = tf::createQuaternionMsgFromYaw(cand_parking_slot->pose.theta);
        }
        else
        {
            goal.pose.orientation = tf::createQuaternionMsgFromYaw(cand_parking_slot->pose.theta + M_PI);
        }

        current_area = msg.request.area_id;
        current_slot = msg.request.slot_id;

        areas[area_id].parking_slots[msg.request.slot_id].occupation_id = robot_id; // update the internal copy of occupation

        return true;
    }
    else
    {
        ROS_WARN_STREAM_THROTTLE(2.0, ros::this_node::getName() << ": " << FG_B_L_MAGENTA << "call failed or server sent false" << RESET);
        if (srv_call_success && !msg.response.resp)
        {
            ROS_WARN_STREAM_THROTTLE(2.0, ros::this_node::getName() << ": " << FG_B_L_MAGENTA << "server sent false" << RESET);
            updateParkingOccupationFromMessage(area_id, msg);
        }

        return false;
    }
}

};
