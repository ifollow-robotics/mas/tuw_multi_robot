#include "node/local_behavior_node.h"

namespace stoplights_system
{

/*************************************************************************************************/
void LocalBehaviorNode::initCallBack_()
{
    // Topic Subscriber
    sub_connected_vehicles_     = n_.subscribe("connected_vehicles",                1,  &LocalBehaviorNode::subConnectedVehiclesCB_, this);
    sub_graph_                  = n_.subscribe("nav_governor/augmented_graph",      1,  &LocalBehaviorNode::subGraphCB_, this);
    sub_graph_areas_            = n_.subscribe("nav_governor/graph_areas",          1,  &LocalBehaviorNode::graphAreasCB_, this);
    mode_sm_sub_                = n_.subscribe("lowLvl/manager/mode/sm_state",      1,  &LocalBehaviorNode::modeSmCB_, this);
    sub_odom_                   = n_.subscribe<nav_msgs::Odometry>("global/odom",   1,  &LocalBehaviorNode::odomCB_, this );
    sub_robot_goal_             = n_.subscribe<geometry_msgs::PoseStamped>("goal",  1,  &LocalBehaviorNode::goalCB_, this);
    sub_voronoi_graph_          = n_.subscribe("segments",                          1,  &LocalBehaviorNode::graphCB_, this);

    // Topic Publisher
    action_goal_pub_            = n_.advertise<ifollow_nav_msgs::LocalBehaviorMoveActionGoal>("single_robot_router/goal", 1);
    route_pub_                  = n_param_.advertise<ifollow_mas_msgs::NewRoute>("route", 3, true);

    // Service
    viapoints_srv_              = n_.advertiseService("get_viapoints",      &LocalBehaviorNode::sendViapointsCB_,       this);
    mas_nav_srv_                = n_param_.advertiseService("mas_nav_req",  &LocalBehaviorNode::masNavReqCB_,           this);
    get_current_route_          = n_.advertiseService("get_current_route",  &LocalBehaviorNode::getCurrentRoute_,       this);
    goal_parameters_service_    = n_.advertiseService("get_goal_parameters", &LocalBehaviorNode::getGoalParametersCB_,  this);
    lights_server_update_last_segment_  = n_.advertiseService("update_last_segment_reserved", &LocalBehaviorNode::updateLastSegmentReserved_, this);

    std::string local_planner_name = "";
    std::string full_param_name = "move_base/base_local_planner";
    n_.searchParam("move_base/base_local_planner", full_param_name);
    n_.param(full_param_name, local_planner_name, local_planner_name);

    local_planner_name = str_ops::strReplaceAll(local_planner_name, "::", "/");

    std::string delimiter = "/";

    if (local_planner_name.find(delimiter) != std::string::npos)
    {
        local_planner_name = local_planner_name.substr(
            local_planner_name.find_last_of(delimiter) + delimiter.length(),
            local_planner_name.length());
    }

    // Client Service
    mb_halt_client_         = n_.serviceClient<ifollow_srv_msgs::SetBrakeCommand>("move_base/" + local_planner_name + "/brake_to_a_halt");
    behavior_stoplights_.mr_area_mngr_client    = n_.serviceClient<ifollow_mas_msgs::MRAreaMngr>("mr_area_mngr");

    // Single robot router
}

/*************************************************************************************************/
bool LocalBehaviorNode::masNavReqCB_(ifollow_srv_msgs::SetString::Request &req,
                                     ifollow_srv_msgs::SetString::Response &resp)
{
    // Local variable
    ifollow_mas_msgs::StoplightsSysResponse sl_resp;
    std::stringstream ss;

    // We request the last_trajectory segment if it is valid, the current robot's segment otherwise
    int32_t last_route_segment = behavior_stoplights_.stoplights_reservation_manager->getLastRouteSegment_();

    int requested_procedure_segment = (last_route_segment != invalid_id) ? last_route_segment : getSegment(segments_, robot_pos_);

    // Try to reserve the current robot's segment
    sl_resp = behavior_stoplights_.stoplights_reservation_manager->requestSegmentAuthorization(requested_procedure_segment, requested_procedure_segment);

    // Check if we can reserved the current segment or if we already reserved it
    if ((true == sl_resp.ack) || (0 == sl_resp.blocking_robot))
    {
        // Update the traffic status as blocked
        behavior_stoplights_.updateTrafficStatus(MOVE_ON_ROAD);
        
        ss << "Successfully reserved segment: " << requested_procedure_segment;
        resp.message = ss.str();
        resp.success = true;
    }
    else
    {
        // Update the traffic status as unblocked
        behavior_stoplights_.updateTrafficStatus(WAITING_AUTH);

        ss << "Unable to reserve segment: " << requested_procedure_segment;
        resp.message = ss.str();
        resp.success = false;
    }

    return true;
}

/*************************************************************************************************/
bool LocalBehaviorNode::getCurrentRoute_(ifollow_mas_msgs::GetCurrentRoute::Request& req,
                                         ifollow_mas_msgs::GetCurrentRoute::Response& resp)
{
    std::scoped_lock lock(route_mutex_);

    for (const uint32_t& tmp_route_id : route_)
    {
        resp.segment_ids.push_back(tmp_route_id);
    }

    return true;
}

/*************************************************************************************************/
bool LocalBehaviorNode::updateLastSegmentReserved_(ifollow_mas_msgs::LastReservedSegment::Request& req, ifollow_mas_msgs::LastReservedSegment::Response& resp)
{
    std::scoped_lock lock(route_mutex_);

    resp.success = behavior_stoplights_.stoplights_reservation_manager->updateLastSegmentFromServerRequest(req.last_segment_id, req.penality_time);

    return true;
}

/*************************************************************************************************/
void LocalBehaviorNode::goalCB_(const geometry_msgs::PoseStamped::ConstPtr& goal)
{
    ROS_INFO_STREAM(ros::this_node::getName() << FG_B_L_CYAN << " Goal topic callback, wrapping "
                    "the PoseStamped in the action message "
                    "and re-sending to the server." << RESET);

    // Check if the goal orientation is valid or if there is an area id defined in the goal
    if (true == conv_ops::isQuaternionValid(goal->pose.orientation))
    {
        // Init the action goal
        ifollow_nav_msgs::LocalBehaviorMoveActionGoal action_goal = initActionGoal(*goal);

        // Publish the new action goal
        action_goal_pub_.publish(action_goal);
    }
    else
    {
        ROS_ERROR_STREAM(ros::this_node::getName() << ": discarding goal!");
    }
}

/*************************************************************************************************/
void LocalBehaviorNode::graphCB_(const tuw_multi_robot_msgs::Graph& msg)
{
    if (false == msg.vertices.empty())
    {
        route_finder_->initRouteGraph(n_, msg);

        got_graph_ = true;
    }
}

/*************************************************************************************************/
void LocalBehaviorNode::odomCB_(const nav_msgs::Odometry::ConstPtr &odom)
{
    if (odom->header.frame_id == frame_id_)
    {
        // Update the robot position
        robot_pos_ = Eigen::Vector3d{odom->pose.pose.position.x,
                                    odom->pose.pose.position.y,
                                    conv_ops::getYaw(odom->pose.pose.orientation)};

        // Set the base as init
        base_pose_init_ = true;

        {
            std::scoped_lock lock(route_mutex_);

            // Check if the graph is init
            if (true == is_init_)
            {
                // Update the robot position on its route
                behavior_stoplights_.progress_monitor.updateProgress(robot_pos_);
                
                behavior_stoplights_.stoplights_reservation_manager->updateReservationFromPosition(robot_pos_, behavior_stoplights_.is_in_teleop);
            }
        }
    }
    else
    {
        ROS_ERROR_STREAM("The frame_id for topic global/odom is not valid.");
    }
}

/*************************************************************************************************/
bool LocalBehaviorNode::sendViapointsCB_(ifollow_nav_msgs::GetViapoints::Request& req, ifollow_nav_msgs::GetViapoints::Response& res)
{
    {
        std::scoped_lock lock(viapoints_mutex_);
        if ((behavior_stoplights_.robot_state != WAITING_AUTH) && (false == go_to_area_external_request_))
        {
            res.viapoints = viapoints_to_send_;
            res.route_id = route_id_;
        }
        else
        {
            ROS_INFO_STREAM(ros::this_node::getName() << ": " << FG_B_L_MAGENTA <<
                            "robot is in WAITING_AUTH state, sending empty viapoints" << RESET);
        }
    }

    {
        std::scoped_lock lock(route_mutex_);
        // send information about where to prune global plan
        auto seg_to_pub = route_.begin() + std::max(behavior_stoplights_.progress_monitor.getProgress() - 4, 0);
        if (seg_to_pub != route_.begin())
        {
            auto& seg = segments_[(*seg_to_pub)];
            res.current_seg_start.x = seg.start.x();
            res.current_seg_start.y = seg.start.y();
            res.current_seg_start.theta = std::atan2(seg.end.y() - seg.start.y(), seg.end.x() - seg.start.x());
        }
        else
        {
            // Indicate this is an invalid current_seg_start so global_planner doesn't do a wrong prune
            res.current_seg_start.x = std::numeric_limits<double>::quiet_NaN();
        }
    }

    ROS_INFO_STREAM(ros::this_node::getName() << ": " << FG_B_L_MAGENTA << "viapoints sent size: " << res.viapoints.poses.size() << " " << route_.size() << RESET);

    if(true == got_new_route_)
    {
        // Advertise the stoplights about the new route
        behavior_stoplights_.stoplights_reservation_manager->advertiseNewRouteToStoplights(route_);
        behavior_stoplights_.resetRerouting();
        got_new_route_ = false;
    }

    return true;
}

/*************************************************************************************************/
bool LocalBehaviorNode::getGoalParametersCB_(ifollow_nav_msgs::GetGoalParameters::Request& request, ifollow_nav_msgs::GetGoalParameters::Response& response)
{
    // Lock the mutex to avoid any update on the goal value
    std::scoped_lock lock(route_mutex_);

    // Get the goal timestamp
    response.stamp = final_goal_.target_pose.header.stamp;

    // Check the if the parameters exist in the local_behavior_params.yaml file
    if (goal_parameters_map_.find(final_goal_.goal_profile) != goal_parameters_map_.end())
    {
        response.goal_params = goal_parameters_map_[final_goal_.goal_profile];
    }
    else
    {
        ROS_WARN_STREAM("The param local_behavior/goal_parameters isn't defined in the file param/navigation/local_behavior_params.yaml");

        response.goal_params = default_goal_parameters_;
    }

    return true;
}

/*************************************************************************************************/
void LocalBehaviorNode::subConnectedVehiclesCB_(const ifollow_mas_msgs::ConnectedVehicles::ConstPtr& connected_vehicles)
{
    auto now = ros::Time::now();
    if ((connected_vehicles->vehicles_id.size() > 1) &&
        (now - connected_vehicles->stamp <= behavior_stoplights_.no_connected_vehicles_pub_timeout))
    {
        behavior_stoplights_.standalone_mode = false;
    }
    else if ((connected_vehicles->vehicles_id.size() == 1) &&
             (connected_vehicles->vehicles_id[0] != robot_id_) &&
             (now - connected_vehicles->stamp <= behavior_stoplights_.no_connected_vehicles_pub_timeout))
    {
        behavior_stoplights_.standalone_mode = false;
    }
    else
    {
        behavior_stoplights_.standalone_mode = true;
    }

    behavior_stoplights_.last_connected_vehicles_publication_time = now;
}

/*************************************************************************************************/
void LocalBehaviorNode::subGraphCB_(const ifollow_mas_msgs::Graph& graph)
{
    graph_ = graph;

    for (const auto& v : graph_.vertices)
    {
        Segment seg;

        seg.start << v.path.front().x, v.path.front().y;
        seg.end << v.path.back().x, v.path.back().y;
        seg.area_id = v.area_id;
        segments_[v.id] = seg;
    }

    // Set the segments map in the reservation manager
    behavior_stoplights_.stoplights_reservation_manager->initSegments(segments_);

    is_init_ = true;
    sub_graph_.shutdown();
}

/*************************************************************************************************/
void LocalBehaviorNode::graphAreasCB_(const ifollow_mas_msgs::AreaArray& msg)
{
    // Loop on the input area list
    for (const auto& area_msgs : msg.areas)
    {
        // Check if the area is a traffic parking area
        if (std::find(area_msgs.type.begin(), area_msgs.type.end(), ifollow_mas_msgs::Area::AREA_TYPE_TRAFFIC_PARKING) != area_msgs.type.end())
        {
            // Check if we have enough points in the area
            if (area_msgs.points.size() > 2)
            {
                // Init the area
                behavior_stoplights_.areas[area_msgs.id] = MRArea();

                // Create the parking slots associated to the area
                behavior_stoplights_.segmentAreaIntoParkingSlots(behavior_stoplights_.areas[area_msgs.id], area_msgs);
            }
            else
            {
                ROS_WARN_STREAM("The area " << area_msgs.id << " is not valid: empty point list.");
            }
        }
    }

    sub_graph_areas_.shutdown();
}

/*************************************************************************************************/
void LocalBehaviorNode::modeSmCB_(const std_msgs::StringConstPtr& msg)
{
    // Check if the robot is in teleop or not
    if (msg->data == "Teleop")
    {
        behavior_stoplights_.is_in_teleop = true;
    }
    else
    {
        behavior_stoplights_.is_in_teleop = false;
    }
}

/*************************************************************************************************/
bool LocalBehaviorNode::isBasePoseInit()
{
    return base_pose_init_;
}

/*************************************************************************************************/
Eigen::Vector3d LocalBehaviorNode::getRobotPosition()
{
    return robot_pos_;
}

/*************************************************************************************************/
std::string LocalBehaviorNode::getFrameId()
{
    return frame_id_;
}

/*************************************************************************************************/
bool LocalBehaviorNode::isReroutingRequiered()
{
    return behavior_stoplights_.isReroutingRequired();
}

}