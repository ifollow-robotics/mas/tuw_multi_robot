// -*- mode: c++; coding: utf-8; tab-width: 4 -*-
/*******************************************************************************
* 2-Clause BSD License
*
* Copyright (c) 2019, iFollow Robotics
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
*   list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*   this list of conditions and the following disclaimer in the documentation
*   and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* Author: Romain Desarzens
* Author: José Mendes Filho (mendesfilho@pm.me)
* Maintainer: José Mendes Filho (mendesfilho@pm.me)
*******************************************************************************/

#include "node/local_behavior_node.h"
#include <ifollow_utils/misc.h>        // only used here to print the route as log easily

using namespace misc;

int main ( int argc, char** argv )
{
    ros::init(argc, argv, "local_behavior_node", ros::init_options::NoRosout); // initializes the ros node with default name
    ros::NodeHandle n;

    stoplights_system::LocalBehaviorNode ctrl(n);
    ctrl.run();

    return 0;
}

namespace stoplights_system
{

/*************************************************************************************************/
LocalBehaviorNode::LocalBehaviorNode(ros::NodeHandle &n)
    : n_(n)
    , n_param_("~")
    , mb_ac_(new MoveBaseClient("move_base", true))
    , as_(new LocalBehaviorMoveServer(n_, "single_robot_router", boost::bind(&LocalBehaviorNode::manageNavigationGoal_, this, _1), false)) // autostart false
    , got_new_route_(false)
    , is_init_(false)
    , go_to_area_external_request_(false)
    , loop_rate_(10)
    , heart_beat_(n_, 0.05, "Local_behavior")
    , route_id_(1)
{
    connected_vehicles_.vehicles_id.clear();
    connected_vehicles_.stamp = ros::Time(0);

    last_goal_sent_     = geometry_msgs::PoseStamped();
    viapoints_          = geometry_msgs::PoseArray();
    viapoints_to_send_  = geometry_msgs::PoseArray();

    rate_ = std::make_unique<ros::Rate>(stoplights_system::node_frequency);

    updateGoalStatus(ifollow_nav_msgs::GoalExtendedStatus::PENDING);

    graph_origin_   = Eigen::Vector2d{std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()};
    robot_pos_      = Eigen::Vector3d{std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN()};

    // Init the node parameters
    initParameters_();

    // Init behavior
    behavior_stoplights_.init(n_, robot_name_);

    // Init the callback use by the node
    initCallBack_();

    // Start the action server for the goal management
    as_->start();
}

/*************************************************************************************************/
void LocalBehaviorNode::initParameters_()
{
    use_server_route_finder_ = false;

    n_param_.getParam("robot_name", robot_name_);
    robot_id_ = id_ops::encodeId(robot_name_);
    frame_id_ = "world";

    // Check if the param is defined
    if (true == n_.hasParam("stoplights_srv/route_finder_centralized"))
    {
        // Get the param value
        n_.getParam("stoplights_srv/route_finder_centralized", use_server_route_finder_);
    }
    else
    {
        // Set the default value for the variable
        use_server_route_finder_ = false;
    }

    robot_route_finder_     = std::make_unique<RouteFinderRobot>();
    server_route_finder_    = std::make_unique<RouteFinderServer>(n_, robot_id_);

    if (true == use_server_route_finder_)
    {
        route_finder_ = server_route_finder_.get();
    }
    else
    {
        route_finder_ = robot_route_finder_.get();
    }

    initGoalParameters();
}

/*************************************************************************************************/
void LocalBehaviorNode::run()
{
    while (ros::ok())
    {
        {
            std::scoped_lock lock(viapoints_mutex_);
            viapoints_to_send_ = viapoints_;
        }

        rate_->sleep();

        // Manage the latest publications
        ros::spinOnce();

        {
            std::scoped_lock lock(route_mutex_);

            // Update the robot's behavior
            update();
        }
    }
}

/*************************************************************************************************/
void LocalBehaviorNode::updateRouteFinderMode()
{
    // Check if the route is computed by the server
    if (true == isRouteComputedByServer())
    {
        route_finder_ = server_route_finder_.get();
    }
    else
    {
        route_finder_ = robot_route_finder_.get();
    }
}

/*************************************************************************************************/
bool LocalBehaviorNode::isRouteComputedByServer()
{
    // Return if the robot is in standalone mode or is forced to compute the route itself
    return ((false == behavior_stoplights_.standalone_mode) && (true == use_server_route_finder_));
}

/*************************************************************************************************/
void LocalBehaviorNode::update()
{
    // Update the stoplights mode
    behavior_stoplights_.updateStoplightsMode();

    // Update the route finder mode
    updateRouteFinderMode();

    // Check if the graph is properly init
    if ((true ==  is_init_) && (true == got_graph_))
    {
        // Check if the robot has a current route
        if (false == route_.empty())
        {
            // Update the stoplihgts
            behavior_stoplights_.updateReservationHorizon();

            if (false == behavior_stoplights_.isReroutingRequired())
            {
                switch (behavior_stoplights_.robot_state)
                {
                // The robot is moving to its goal
                case MOVE_ON_ROAD:
                {
                    updateBehaviorStateMoveOnRoad();

                    break;
                }
                // The robot is moving to its goal, but soon will be blocked by another robot
                case MOVE_RESTRICTED:
                {
                    updateBehaviorStateMoveRestricted();

                    break;
                }
                // If the robot is waiting authorization for a restricted segment
                case WAITING_AUTH:
                {
                    updateBehaviorStateWaitingAuth();

                    break;
                }
                // If the robot is stopped, just update the callback
                case UNKNOWN:
                {
                    ROS_ERROR_STREAM_THROTTLE(2.0, ros::this_node::getName() << ": The robot state is not valid " << behavior_stoplights_.robot_state);
                    break;
                }
                case NAV_SLAVE:
                {
                    ROS_WARN_STREAM("Not implemented yet.");
                    break;
                }
                case MOVE_OFF_ROAD:
                case NAV_FINISHED:
                {
                    route_.clear();
                    break;
                }
                }
            }
            else
            {
                initRerouting_();
            }
        }

        behavior_stoplights_.publishCurrSeg(robot_pos_, route_, segments_);
    }
    else
    {
        ROS_WARN_STREAM_THROTTLE(10.0, ros::this_node::getName() <<
            ": Initialization is not done yet" <<
            ". received graph: " << std::boolalpha << is_init_ <<
            ". transformed graph: " << std::boolalpha << got_graph_);
    }

    // Publish the heartBeat topic
    heart_beat_.update();
}

/*************************************************************************************************/
void LocalBehaviorNode::initRerouting_()
{
    // Clear the previous route
    route_.clear();

    // Set got new route at true for the function computeViaPointsFromRoute_()
    got_new_route_ = true;

    // Loop on the alternative sequence to init the new route
    for (const uint16_t& segment : behavior_stoplights_.stoplights_reservation_manager->alternative_seq)
    {
        route_.push_back(segment);
    }

    // Init the new route
    behavior_stoplights_.stoplights_reservation_manager->initRoute(route_, final_goal_.target_pose.pose.position); // x,y,z
    behavior_stoplights_.progress_monitor.init(segments_, route_);

    // Compute the new global trajectory from the route
    computeViaPointsFromRoute_();

    // Increment the route id to avoid issu in the global_spline_planner which call getViaPoints service
    route_id_++;
}

/*************************************************************************************************/
void LocalBehaviorNode::computeViaPointsFromRoute_()
{
    if ((false == got_new_route_) || (true == route_.empty()))
    {
        return;
    }

    viapoints_.header.seq = 0;
    viapoints_.header.stamp = ros::Time::now();
    viapoints_.header.frame_id = frame_id_;
    viapoints_.poses.clear();

    geometry_msgs::Pose pose;
    pose.orientation.x = 0;
    pose.orientation.y = 0;
    pose.orientation.z = 0;
    pose.orientation.w = 1;

    // Check the route size
    if (1 == route_.size())
    {
        // Set the current robot's position and goal position
        Eigen::Vector2d start_pos_2d    = Eigen::Vector2d(robot_pos_[0], robot_pos_[1]);
        Eigen::Vector2d end_pos_2d      = Eigen::Vector2d(final_goal_.target_pose.pose.position.x, final_goal_.target_pose.pose.position.y);

        // Add start position in the via points list
        Eigen::Vector2d projected_position = closest_point_on_line_segment_2d(start_pos_2d, segments_[route_[0]].start, segments_[route_[0]].end);
        addProjectedPointToViaPoints_(projected_position, segments_[route_[0]].start, segments_[route_[0]].end, false);

        // Add End position in the via points list
        projected_position = closest_point_on_line_segment_2d(end_pos_2d, segments_[route_[0]].start, segments_[route_[0]].end);
        addProjectedPointToViaPoints_(projected_position, segments_[route_[0]].start, segments_[route_[0]].end, true);

        // Add target point in the list
        pose = final_goal_.target_pose.pose;

        // Check if the orientation of the navigation goal is valid
        if (conv_ops::isQuaternionValid(pose.orientation))
        {
            // Add it to the list
            viapoints_.poses.push_back(pose);
        }
        else
        {
            // Use the segment's orientation
            auto orientation = stoplights_system::computeSegmentOrientation(segments_[route_.back()]);
            pose.orientation = orientation;

            // Add to the list
            viapoints_.poses.push_back(pose);
        }
    }
    else
    {
        viapoints_.poses.resize(route_.size());

        // go up to the second last segment
        int i = 0;

        for (auto it = route_.begin(); it != (route_.end() - 1); it++)
        {
            pose.position.x = segments_[*it].end[0];
            pose.position.y = segments_[*it].end[1];

            auto orientation = stoplights_system::computeSegmentOrientationWithNext(segments_[*it], segments_[*(it + 1)]);
            if (!conv_ops::isQuaternionValid(orientation))
            {
                orientation = stoplights_system::computeSegmentOrientation(segments_[*it]);
                if (conv_ops::isQuaternionValid(orientation))
                {
                    pose.orientation = orientation;
                }
            }
            else
            {
                pose.orientation = orientation;
            }

            viapoints_.poses[i++] = pose;
        }

        pose = final_goal_.target_pose.pose;

        if (conv_ops::isQuaternionValid(pose.orientation))
        {
            viapoints_.poses[i] = pose;
        }
        else
        {
            auto orientation = stoplights_system::computeSegmentOrientation(segments_[route_.back()]);
            pose.orientation = orientation;
            viapoints_.poses[i] = pose;
        }
    }
}

/*************************************************************************************************/
void LocalBehaviorNode::addProjectedPointToViaPoints_(const Eigen::Vector2d& projected_position, const Eigen::Vector2d& start_pos_2d, const Eigen::Vector2d& end_pos_2d, bool is_last_segment)
{
    geometry_msgs::Pose pose;

    // Check if the projected point is equal to the segment's start point
    if (std::hypot((projected_position[0] - start_pos_2d[0]), (projected_position[1] - start_pos_2d[1])) < 0.001)
    {
        pose.position.x = start_pos_2d[0];
        pose.position.y = start_pos_2d[1];

        // Check if this is the last segment of the route
        if (false == is_last_segment)
        {
            // Use the segment's orientation for the point
            pose.orientation = stoplights_system::computeSegmentOrientation(segments_[route_[0]]);
        }
        else
        {
            // Use the opposite of the segment's orientation for the point
            pose.orientation = tf::createQuaternionMsgFromYaw(atan2(double(segments_[route_[0]].start[1] - segments_[route_[0]].end[1]),
                                                                    double(segments_[route_[0]].start[0] - segments_[route_[0]].end[0])));
        }

        viapoints_.poses.push_back(pose);
    }
    // Check if the projected point is equal to the segment's end point
    else if (std::hypot((projected_position[0] - end_pos_2d[0]), (projected_position[1] - end_pos_2d[1])) < 0.001)
    {
        pose.position.x = end_pos_2d[0];
        pose.position.y = end_pos_2d[1];

        // Check if this is the last segment of the route
        if (true == is_last_segment)
        {
            // Use the segment's orientation for the point
            pose.orientation = stoplights_system::computeSegmentOrientation(segments_[route_[0]]);
        }
        else
        {
            // Use the opposite of the segment's orientation for the point
            pose.orientation = tf::createQuaternionMsgFromYaw(atan2(double(segments_[route_[0]].start[1] - segments_[route_[0]].end[1]),
                                                                    double(segments_[route_[0]].start[0] - segments_[route_[0]].end[0])));
        }

        viapoints_.poses.push_back(pose);
    }
    else
    {
        // Don't add the points
    }
}

/*************************************************************************************************/
bool LocalBehaviorNode::sendFinalGoal_()
{
    // Local variable
    std::scoped_lock lock(viapoints_mutex_);
    geometry_msgs::PoseStamped last_goal_sent_tf_hack, last_goal_sent_correct_frame;

    // Check if the via points list is valid
    if (true == viapoints_.poses.empty())
    {
        ROS_WARN_STREAM(ros::this_node::getName() << ": " << FG_B_L_MAGENTA << "not sending goal to move_base because there are no viapoints" << RESET);
        return false;
    }

    // Check if the frame id is valid
    if ((final_goal_.target_pose.header.frame_id == "") && 
        (true == conv_ops::isQuaternionValid(final_goal_.target_pose.pose.orientation)))
    {
        ROS_WARN_STREAM(ros::this_node::getName() << ": " << FG_B_L_MAGENTA << "not sending goal to move_base because final goal has invalid frame_id or orientation" << RESET);
        return false;
    }

    // first time we send a goal
    if (last_goal_sent_.header.frame_id == "")
    {
        ROS_INFO_STREAM(FG_L_RED << ros::this_node::getName() << ": " << FG_B_L_MAGENTA << "send new goal: " << final_goal_.target_pose.pose.position.x << ", " << final_goal_.target_pose.pose.position.y << RESET);

        // Sent the new goal's position or area
        sendGoalToPositionOrArea_();

        return true;
    }

    // Check if we didn't already send this goal
    if (last_goal_sent_.header.stamp == final_goal_.target_pose.header.stamp)
    {
        return false;
    }

    // Check if we have a new road
    if (false == got_new_route_)
    {
        ROS_WARN_STREAM(ros::this_node::getName() << ": " << FG_B_L_MAGENTA << "not sending goal to move_base because ROUTE didn't arrive yet" << RESET);
        return false;
    }

    last_goal_sent_correct_frame = last_goal_sent_;

    double x_diff = last_goal_sent_correct_frame.pose.position.x - final_goal_.target_pose.pose.position.x;
    double y_diff = last_goal_sent_correct_frame.pose.position.y - final_goal_.target_pose.pose.position.y;
    double ang_diff = angles::normalize_angle(tf::getYaw(last_goal_sent_correct_frame.pose.orientation) - tf::getYaw(final_goal_.target_pose.pose.orientation));

    // Send a goal only if it is a new one
    if ((std::abs(x_diff) > 1e-4) ||
        (std::abs(y_diff) > 1e-4) ||
        (std::abs(ang_diff) > 1e-4))
    {
        ROS_INFO_STREAM(FG_L_RED << ros::this_node::getName() << ": " << FG_B_L_MAGENTA <<
                        "send new goal: " << final_goal_.target_pose.pose.position.x <<
                        ", " << final_goal_.target_pose.pose.position.y << " (diff= " <<
                        x_diff << ", " << y_diff << ", " << ang_diff << ")" << RESET);

        // Sent the new goal's position or area
        sendGoalToPositionOrArea_();

        // Wait for move base action server
        return true;
    }
    else if (true == got_new_route_)
    {
        ROS_ERROR_STREAM(ros::this_node::getName() << ": " << FG_B_L_MAGENTA << "goal pose didn't change, the time stamp did, and we got a new route, so let's send the goal" << RESET);

        // Sent the new goal's position or area
        sendGoalToPositionOrArea_();

        return true;
    }
    else
    {
        ROS_ERROR_STREAM(ros::this_node::getName() << ": " << FG_B_L_MAGENTA << "not sending goal to move_base because final goal target pose didn't change (although time stamp has! Verify code!)" << RESET);

        return false;
    }
}

/*************************************************************************************************/
void LocalBehaviorNode::sendGoalToPositionOrArea_()
{
    // Local variable
    move_base_msgs::MoveBaseGoal goal;

    // Lock the mutex
    std::scoped_lock lock(mb_ac_mutex_);

    // Wait for the server action
    mb_ac_->waitForServer();

    // Check if the goal is a simple position or not
    if (-1 == final_goal_.area_id)
    {
        // Set the local position
        goal.target_pose = final_goal_.target_pose;

        // Send the new goal
        mb_ac_->sendGoal(goal);

        // Update the last send goal
        last_goal_sent_ = final_goal_.target_pose;
    }
    else
    {
        // Try to go to the input area_id
        tryToGoToMrArea(final_goal_.area_id);
    }
}

/*************************************************************************************************/
void LocalBehaviorNode::updateBehaviorStateMoveOnRoad()
{
    if (false == go_to_area_external_request_)
    {
        computeViaPointsFromRoute_();
        sendFinalGoal_();
    }

    // Check if the robot has a road and the last reserved segment is the end of the route or if the robot is already at the end of its route
    if (true == behavior_stoplights_.stoplights_reservation_manager->isOnLastRouteSegment())
    {
        behavior_stoplights_.try_reach_goal = true;
    }

    // Check if move_base has reached goal
    bool mb_done = false;
    {
        std::scoped_lock lock(mb_ac_mutex_);
        mb_done = mb_ac_->getState().isDone();
    }

    if (true == mb_done)
    {
        if (true == behavior_stoplights_.try_reach_goal)
        {
            // If we are reaching destination, then robot will be stopped
            ROS_INFO_STREAM(ros::this_node::getName() << ": " << FG_B_L_MAGENTA << "robot has reached the goal" << RESET);

            updateGoalStatus(ifollow_nav_msgs::GoalExtendedStatus::SUCCEEDED);

            go_to_area_external_request_ = false;

            // Update the traffic status
            behavior_stoplights_.updateTrafficStatus(NAV_FINISHED);

            // Update the last trajectory segment to start a new procedure mode
            behavior_stoplights_.stoplights_reservation_manager->setLastRouteSegment_();
        }
        else if (false == go_to_area_external_request_)
        {
            // advertise single robot router
            updateGoalStatus(ifollow_nav_msgs::GoalExtendedStatus::STALLED);

            // Update the traffic status to restected to manage inconsistancy with mb_done and try_reach_goal value
            behavior_stoplights_.updateTrafficStatus(MOVE_RESTRICTED);
        }
    }
    else if ((false == behavior_stoplights_.try_reach_goal) && (false == go_to_area_external_request_))
    {
        // Get the distance to the last accesible segment on the itinerary
        if (true == behavior_stoplights_.stoplights_reservation_manager->isAccessRestricted())
        {
            // Update the traffic status
            behavior_stoplights_.updateTrafficStatus(MOVE_RESTRICTED);
        }
    }
    else if ((lb_goal_status_.value != ifollow_nav_msgs::GoalExtendedStatus::ACTIVE) && 
        (false == go_to_area_external_request_))
    {
        updateGoalStatus(ifollow_nav_msgs::GoalExtendedStatus::ACTIVE);
    }
    else
    {
        // Do nothing
    }

}

/*************************************************************************************************/
void LocalBehaviorNode::updateBehaviorStateMoveRestricted()
{
    // Check if the segment reservation is restricted or not
    if (true == behavior_stoplights_.stoplights_reservation_manager->isAccessBlocked())
    {
        // If we are not reaching destination, then the robot will wait for authorization
        ROS_WARN_STREAM(ros::this_node::getName() << ": " << FG_B_L_MAGENTA
            << "robot reached the minimum distance to a restrict segment. Cancel move_base goals..." << RESET);

        cancelCurrentMoveBaseGoal_(behavior_stoplights_.getStopPoint());

        updateGoalStatus(ifollow_nav_msgs::GoalExtendedStatus::STALLED);

        // Update the traffic status
        behavior_stoplights_.updateTrafficStatus(WAITING_AUTH);

        got_new_route_ = true; // previous route will become a "new" route again as soon as we get authorization
        last_goal_sent_ = geometry_msgs::PoseStamped(); // and last goal sent is reset so sendFinalGoal_ can do his job
        last_goal_sent_.header.stamp = ros::Time(0);
        last_goal_sent_.header.frame_id = "";
    }
    if (true == behavior_stoplights_.stoplights_reservation_manager->isAccessUnblocked())
    {
        // Update the traffic status
        behavior_stoplights_.updateTrafficStatus(MOVE_ON_ROAD);
    }
}

/*************************************************************************************************/
void LocalBehaviorNode::updateBehaviorStateWaitingAuth()
{
    if (true == behavior_stoplights_.stoplights_reservation_manager->isAccessStillBlocked())
    {
        // Stay at waiting state
    }
    else if (behavior_stoplights_.current_area != invalid_id && behavior_stoplights_.current_slot != invalid_id)
    {
        // robot will move unless ...
        // check with mr_area_mngr too
        if (true == behavior_stoplights_.requestToLeaveMrArea(robot_id_))
        {
            behavior_stoplights_.areas[behavior_stoplights_.current_area].parking_slots[behavior_stoplights_.current_slot].occupation_id = invalid_id;
            behavior_stoplights_.current_area = invalid_id;
            behavior_stoplights_.current_slot = invalid_id;

            // Update the traffic status
            behavior_stoplights_.updateTrafficStatus(MOVE_ON_ROAD);
        }
    }
    else
    {
        // Update the traffic status
        behavior_stoplights_.updateTrafficStatus(MOVE_ON_ROAD);
    }
}

/*************************************************************************************************/
void LocalBehaviorNode::initNewRoute_(bool force_robot_route_finder)
{
    ifollow_mas_msgs::NewRoute new_route;
    new_route.robot_id = robot_id_;

    // Get the new robot's route
    const std::vector<uint32_t> route = (false == force_robot_route_finder) ? route_finder_->getRoute() : robot_route_finder_->getRoute();

    // Check if the route is valid
    if (false == route.empty())
    {
        new_route.segment_ids = route;        

        ROS_INFO_STREAM(ros::this_node::getName() << ": " FG_B_L_MAGENTA
                        "new route of size " << new_route.segment_ids.size() << ": " RESET <<
                        new_route.segment_ids);

        // Reset the variable to go to mrArea
        go_to_area_external_request_ = false;

        final_goal_.target_pose.header.stamp = ros::Time::now();

        reset();
        std::copy(new_route.segment_ids.begin(),
                  new_route.segment_ids.end(),
                  std::back_inserter(route_));
        got_new_route_ = true;
        route_id_++;

        // Advertise the stoplights about the new route
        behavior_stoplights_.stoplights_reservation_manager->advertiseNewRouteToStoplights(route_);

        updateGoalStatus(ifollow_nav_msgs::GoalExtendedStatus::PENDING);

        // Init the last accessible segment
        behavior_stoplights_.stoplights_reservation_manager->initRoute(route_, final_goal_.target_pose.pose.position); // x,y,z

        behavior_stoplights_.progress_monitor.init(segments_, route_);
        behavior_stoplights_.updateTrafficStatus(WAITING_AUTH);

        ROS_INFO_STREAM(ros::this_node::getName() << ": " << FG_B_L_MAGENTA << "Robot enter waiting state");

        route_pub_.publish(new_route);
    }
    else
    {
        ROS_WARN_STREAM("No Route Found");
    }
}

/*************************************************************************************************/
void LocalBehaviorNode::reset()
{
    std::scoped_lock lock(viapoints_mutex_);

    ROS_INFO_STREAM(FG_L_RED << ros::this_node::getName() << ": " << FG_B_L_MAGENTA << "resetting local behavior" << RESET);

    // For every segment in the previous route, advertise the server that they are free
    behavior_stoplights_.stoplights_reservation_manager->resetRoute();

    route_.clear();
    viapoints_ = geometry_msgs::PoseArray();

    // Update the traffic status
    behavior_stoplights_.updateTrafficStatus(NAV_FINISHED);

    behavior_stoplights_.try_reach_goal = false;
}

/*************************************************************************************************/
void LocalBehaviorNode::cancelCurrentMoveBaseGoal_(const geometry_msgs::PointStamped& stop_point)
{
    ROS_INFO_STREAM(ros::this_node::getName() << ": " << FG_B_L_MAGENTA << "called cancelCurrentMoveBaseGoal_" << RESET);

    std::scoped_lock lock(mb_ac_mutex_);
    mb_ac_->waitForServer();

    // Get the area associated with the current segment
    int32_t area_id = segments_[behavior_stoplights_.getCurrentSegmentOnRoute()].area_id;

    ifollow_srv_msgs::SetBrakeCommand srv;

    srv.request.stop_pose.header    = stop_point.header;
    srv.request.stop_pose.point.x   = stop_point.point.x;
    srv.request.stop_pose.point.y   = stop_point.point.y;
    srv.request.use_stop_pose       = true;

    callSrv(mb_halt_client_, srv, ros::Duration(0.5));

    // Check if the robot has an area
    if (area_id == -1)
    {
        mb_ac_->cancelAllGoals();
        mb_ac_->waitForResult();
    }
    else // a goal to enter this mr area has to be defined (based on the predefined slots and conversation with the server)
    {
        // Try to send the robot the the area
        tryToGoToMrArea(area_id);
    }
}

/*************************************************************************************************/
void LocalBehaviorNode::manageNavigationGoal_(const ifollow_nav_msgs::LocalBehaviorMoveGoalConstPtr &_goal)
{
    // Wait until the graph is properly init
    while (false == is_init_)
    {
        ROS_WARN_STREAM_THROTTLE(10.0, ros::this_node::getName() << ": Wait for graph to be inited before managing new goal.");
    }

    ifollow_nav_msgs::LocalBehaviorMoveResult result;

    // Check if an area is defined in the goal
    if ((-1 != _goal->area_id))
    {
        // Try to go to the input mrArea
        tryToGoToMrArea(_goal->area_id);

        // Wait one second before asking again
        ros::Duration(1.0).sleep();

        go_to_area_external_request_ = true;

        // Update the traffic status
        behavior_stoplights_.updateTrafficStatus(MOVE_ON_ROAD);

        updateGoalStatus(ifollow_nav_msgs::GoalExtendedStatus::SUCCEEDED);
    }
    // Check if the goal is valid
    else if (false == conv_ops::isQuaternionValid(_goal->target_pose.pose.orientation))
    {
        conv_ops::initPose(result.base_pose.pose);
        if (base_pose_init_)
        {
            conv_ops::vector3DToPose(robot_pos_, result.base_pose.pose);
            result.base_pose.header.frame_id = frame_id_;
            result.base_pose.header.stamp = ros::Time::now();
        }
        result.status.value = ifollow_nav_msgs::GoalExtendedStatus::ABORTED;
        result.status.text = "Aborting on goal because it was sent with an invalid quaternion";
        as_->setAborted(result, result.status.text);

        updateGoalStatus(ifollow_nav_msgs::GoalExtendedStatus::PENDING);

        return;
    }
    else
    {
        planRoute(*_goal);
    }

    while (true == ros::ok())
    {
        if (true == as_->isPreemptRequested())
        {
            if (true == as_->isNewGoalAvailable())
            {
                conv_ops::initPose(result.base_pose.pose);
                if (true == base_pose_init_)
                {
                    conv_ops::vector3DToPose(robot_pos_, result.base_pose.pose);
                    result.base_pose.header.frame_id = frame_id_;
                    result.base_pose.header.stamp = ros::Time::now();
                }
                result.status.value = ifollow_nav_msgs::GoalExtendedStatus::PREEMPTED;
                result.status.text = "This goal was canceled because another goal was recieved by the simple action server";
                as_->setPreempted(result, result.status.text);

                updateGoalStatus(ifollow_nav_msgs::GoalExtendedStatus::PENDING);

                // if we're active and a new goal is available, we'll accept it, but we won't shut anything down
                ifollow_nav_msgs::LocalBehaviorMoveGoal new_goal = *as_->acceptNewGoal();

                // Check if the new goal has an area defined
                if ((-1 != new_goal.area_id))
                {
                    // Try to go to the input mrArea
                    tryToGoToMrArea(new_goal.area_id);

                    // Wait one second before asking again
                    ros::Duration(1.0).sleep();

                    go_to_area_external_request_ = true;

                    // Update the traffic status
                    behavior_stoplights_.updateTrafficStatus(MOVE_ON_ROAD);
                    updateGoalStatus(ifollow_nav_msgs::GoalExtendedStatus::SUCCEEDED);
                }
                else if (false == conv_ops::isQuaternionValid(new_goal.target_pose.pose.orientation))
                {
                    if (true == base_pose_init_)
                    {
                        conv_ops::vector3DToPose(robot_pos_, result.base_pose.pose);
                        result.base_pose.header.frame_id = frame_id_;
                        result.base_pose.header.stamp = ros::Time::now();
                    }
                    result.status.value = ifollow_nav_msgs::GoalExtendedStatus::ABORTED;
                    result.status.text = "Aborting on goal because it was sent with an invalid quaternion";
                    as_->setAborted(result, result.status.text);
                    updateGoalStatus(ifollow_nav_msgs::GoalExtendedStatus::PENDING);

                    return;
                }
                else
                {
                    planRoute(new_goal);
                }
            }
            else
            {
                mb_ac_->cancelAllGoals();
                mb_ac_->waitForResult();
                reset();

                conv_ops::initPose(result.base_pose.pose);

                if (true == base_pose_init_)
                {
                    conv_ops::vector3DToPose(robot_pos_, result.base_pose.pose);
                    result.base_pose.header.frame_id = frame_id_;
                    result.base_pose.header.stamp = ros::Time::now();
                }

                result.status.value = ifollow_nav_msgs::GoalExtendedStatus::PREEMPTED;
                result.status.text = "This goal was canceled";

                as_->setPreempted(result, result.status.text);

                updateGoalStatus(ifollow_nav_msgs::GoalExtendedStatus::PENDING);

                return;

            }
        }

        ifollow_nav_msgs::GoalExtendedStatus lb_goal_st;
        {
            std::scoped_lock lock(lb_goal_mutex_);
            lb_goal_st = lb_goal_status_;
        }

        if ((true == as_->isActive()) && 
            (true == base_pose_init_) && 
            (lb_goal_st.value == ifollow_nav_msgs::GoalExtendedStatus::SUCCEEDED))
        {
            conv_ops::initPose(result.base_pose.pose);
            result.status = lb_goal_st;

            if (true == base_pose_init_)
            {
                conv_ops::vector3DToPose(robot_pos_, result.base_pose.pose);
                result.base_pose.header.frame_id = frame_id_;
                result.base_pose.header.stamp = ros::Time::now();
            }

            as_->setSucceeded(result);

            updateGoalStatus(ifollow_nav_msgs::GoalExtendedStatus::PENDING);

            return;
        }
        else if ((true == as_->isActive()) && (true == base_pose_init_) &&
                 (lb_goal_st.value != ifollow_nav_msgs::GoalExtendedStatus::SUCCEEDED) &&
                 (lb_goal_st.value != ifollow_nav_msgs::GoalExtendedStatus::ABORTED) &&
                 (lb_goal_st.value != ifollow_nav_msgs::GoalExtendedStatus::REJECTED) &&
                 (lb_goal_st.value != ifollow_nav_msgs::GoalExtendedStatus::PREEMPTED) &&
                 (lb_goal_st.value != ifollow_nav_msgs::GoalExtendedStatus::RECALLED))
        {
            feedback_.status = lb_goal_st;

            if (true == base_pose_init_)
            {
                conv_ops::vector3DToPose(robot_pos_, feedback_.base_pose.pose);
                feedback_.base_pose.header.frame_id = frame_id_;
                feedback_.base_pose.header.stamp = ros::Time::now();
            }
            as_->publishFeedback(feedback_);
        }

        loop_rate_.sleep();
    }

    // if the node is killed then we'll abort and return
    conv_ops::initPose(result.base_pose.pose);
    
    if (true == base_pose_init_)
    {
        conv_ops::vector3DToPose(robot_pos_, result.base_pose.pose);
        result.base_pose.header.frame_id = frame_id_;
        result.base_pose.header.stamp = ros::Time::now();
    }

    result.status.value = ifollow_nav_msgs::GoalExtendedStatus::ABORTED;
    result.status.text = "Aborting on the goal because the node has been killed";
    as_->setAborted(result, result.status.text);
    updateGoalStatus(ifollow_nav_msgs::GoalExtendedStatus::PENDING);

    return;
}

/*************************************************************************************************/
void LocalBehaviorNode::planRoute(ifollow_nav_msgs::LocalBehaviorMoveGoal goal)
{
    ROS_INFO_STREAM(ros::this_node::getName() << FG_B_L_CYAN <<
                    " Planning route to reach goal at (" <<
                    goal.target_pose.pose.position.x << ", " << goal.target_pose.pose.position.y <<
                    ") (wrt \"" << goal.target_pose.header.frame_id << "\")" << RESET);

    std::scoped_lock lock(route_mutex_);
    stoplights_system::applyOffsets(goal);

    // Update the final pos
    final_goal_ = goal;

    // Compute the new plan
    if (false == computeRoute())
    {
        ifollow_nav_msgs::LocalBehaviorMoveResult result;
        result.status.value = ifollow_nav_msgs::GoalExtendedStatus::ABORTED;
        result.status.text = "Aborting on goal because no route was found";

        as_->setAborted(result, result.status.text);
    }

    updateGoalStatus(ifollow_nav_msgs::GoalExtendedStatus::PENDING);
}

/*************************************************************************************************/
bool LocalBehaviorNode::computeRoute()
{
    // Initialize data for single robot
    Eigen::Vector3d starts, goals;

    starts = robot_pos_;
    
    // cppcheck-suppress compareBoolExpressionWithInt
    if (false == std::isfinite(robot_pos_.x()))
    {
        ROS_ERROR_STREAM(ros::this_node::getName() <<
            ": Invalid current robot pose, planning a route will fail. "
            "Check global/odom publications");
    }

    goals = Eigen::Vector3d{final_goal_.target_pose.pose.position.x, final_goal_.target_pose.pose.position.y, tf::getYaw(final_goal_.target_pose.pose.orientation)};
    if (true == got_graph_)
    {
        std::vector<int> robot_groups;
    
        RouteFinderError route_status = route_finder_->computeRoute(starts, goals, robot_groups);
    
        // Compute a route between the start and the end point
        if (ROUTE_OK == route_status)
        {
            // Init the new route on the robot
            initNewRoute_(false);

            return true;
        }
        // Check if the server had an error
        else if((SERVER_ERROR == route_status) && 
            (true == isRouteComputedByServer()))
        {
            // Try to make the robot compute the route instead of the server
            if (ROUTE_OK == robot_route_finder_->computeRoute(starts, goals, robot_groups))
            {
                // Init the new route on the robot
                initNewRoute_(true);

                return true;
            }
        }
    }
    else
    {
        ROS_INFO_STREAM (n_param_.getNamespace().c_str() <<  ": Multi Robot Router: No Map or Graph received");
    }

    return false;
}

/*************************************************************************************************/
void LocalBehaviorNode::initGoalParameters()
{
    default_goal_parameters_.beyond_goal_coll_check_dist    = 0.0;
    default_goal_parameters_.arc_length_allowed_goal_zone   = 0.0;
    default_goal_parameters_.align_path_with_goal           = false;
    default_goal_parameters_.use_turtle_mode                = false;
    default_goal_parameters_.use_avoidance_traj             = false;
    default_goal_parameters_.altp_min_distance_to_goal      = 0.0;

    XmlRpc::XmlRpcValue goal_param_list;
    ifollow_nav_msgs::GoalParameters goal_parameters;

    // Check if the parameters exist in the local_behavior.yaml file
    if (true == n_.hasParam("local_behavior/goal_parameters"))
    {
        // Get the goal parameters list
        n_.getParam("local_behavior/goal_parameters", goal_param_list);

        // Loop on the goal parameters list
        for (int i = 0; i < goal_param_list.size(); ++i)
        {
            // Get the current goal parameters
            const XmlRpc::XmlRpcValue& current_parameters = goal_param_list[i];

            std::string name = std::string(current_parameters["name"]);

            goal_parameters.beyond_goal_coll_check_dist =
                XmlRpc::getValue<double>(current_parameters, "beyond_goal_coll_check_dist", default_goal_parameters_.beyond_goal_coll_check_dist);
            goal_parameters.align_path_with_goal =
                XmlRpc::getValue<bool>(current_parameters, "align_path_with_goal", default_goal_parameters_.align_path_with_goal);
            goal_parameters.arc_length_allowed_goal_zone =
                XmlRpc::getValue<double>(current_parameters, "arc_length_allowed_goal_zone", default_goal_parameters_.arc_length_allowed_goal_zone);
            goal_parameters.use_turtle_mode =
                XmlRpc::getValue<bool>(current_parameters, "use_turtle_mode", default_goal_parameters_.use_turtle_mode);
            goal_parameters.use_avoidance_traj =
                XmlRpc::getValue<bool>(current_parameters, "use_avoidance_traj", default_goal_parameters_.use_avoidance_traj);
            goal_parameters.altp_min_distance_to_goal =
                XmlRpc::getValue<double>(current_parameters, "altp_min_distance_to_goal", default_goal_parameters_.altp_min_distance_to_goal);

            goal_parameters_map_[name] = goal_parameters;
        }
    }
    else
    {
        ROS_WARN_STREAM("No goal_parameters defined in the file local_behavior.yaml");
    }
}

/*************************************************************************************************/
void LocalBehaviorNode::updateGoalStatus(uint8_t new_status)
{
    std::scoped_lock lock(lb_goal_mutex_);
    // Update the value status
    lb_goal_status_.value = new_status;

    switch (new_status)
    {
    case ifollow_nav_msgs::GoalExtendedStatus::PENDING:
    {
        lb_goal_status_.text = "Pending - local behavior hasn't update status yet";
        break;
    }
    case ifollow_nav_msgs::GoalExtendedStatus::SUCCEEDED:
    {
        lb_goal_status_.text = "Robot has reached the goal";
        break;
    }
    case ifollow_nav_msgs::GoalExtendedStatus::STALLED:
    {
        lb_goal_status_.text = "Robot is stalled waiting for segment access authorization";
        break;
    }
    case ifollow_nav_msgs::GoalExtendedStatus::ACTIVE:
    {
        lb_goal_status_.text = "Robot is moving";
        break;
    }
    case ifollow_nav_msgs::GoalExtendedStatus::ABORTED:
    {
        lb_goal_status_.text = "Aborting on goal";
        break;
    }

    }
}

/*************************************************************************************************/
ifollow_nav_msgs::GoalExtendedStatus LocalBehaviorNode::getGoalStatus()
{
    return lb_goal_status_;
}

/*************************************************************************************************/
bool LocalBehaviorNode::isInit()
{
    return is_init_;
}

/*************************************************************************************************/
uint32_t LocalBehaviorNode::getLastSegmentPublished()
{
    return behavior_stoplights_.last_seg_to_pub_id;
}

}  // namespace stoplights_client
