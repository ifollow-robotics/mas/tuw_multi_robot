#include "node/local_behavior_node.h"

namespace stoplights_system 
{

/*************************************************************************************************/
RouteFinderServer::RouteFinderServer(ros::NodeHandle& n, const uint16_t& robot_id) : RouteFinderInterface()
{
    route_finder_client_  = n.serviceClient<ifollow_mas_msgs::GenerateRoute>("stoplights_compute_route");
    robot_id_ = robot_id;
}

/*************************************************************************************************/
RouteFinderError RouteFinderServer::computeRoute(const Eigen::Vector3d& _starts, const Eigen::Vector3d& _goals, const std::vector<int>& robot_groups)
{
    route_.clear();

    ifollow_mas_msgs::GenerateRoute new_route;
    new_route.request.robot_id      = robot_id_;
    new_route.request.goal.x        = _goals[0];
    new_route.request.goal.y        = _goals[1];
    new_route.request.goal.theta    = _goals[2];

    // Call the service on the server
    if (true == route_finder_client_.call(new_route))
    {
        // Check if the computed route is empty
        if (true == new_route.response.route.empty())   
        {
            return NO_ROUTE_FOUND;
        }
        else
        {
            // Check if the stoplights found a route
            for (const uint16_t& segment : new_route.response.route)
            {
                route_.push_back(segment);
            }
        }
    }
    else
    {
        // The server had an issu
        return SERVER_ERROR;
    }

    return ROUTE_OK;
}

/*************************************************************************************************/
std::vector<uint32_t> RouteFinderServer::getRoute() const
{
    return route_;
}

/*************************************************************************************************/
void RouteFinderServer::initRouteGraph(const ros::NodeHandle& nh_, const tuw_multi_robot_msgs::Graph& new_graph)
{
    // Do nothing
    return;
}

}