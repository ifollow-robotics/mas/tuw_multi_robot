#include "node/local_behavior_node.h"

namespace stoplights_system 
{

/*************************************************************************************************/
RouteFinderRobot::RouteFinderRobot() : RouteFinderInterface()
{
    local_route_finder_ = route_finder::SingleRobotRouteFinder();
}

/*************************************************************************************************/
RouteFinderError RouteFinderRobot::computeRoute(const Eigen::Vector3d& _starts, const Eigen::Vector3d& _goals, const std::vector<int>& robot_groups)
{
    // Compute the new route using the route finder
    if (true == local_route_finder_.makePlan(_starts, _goals, robot_groups))
    {
        return ROUTE_OK;
    }
    else
    {
        return NO_ROUTE_FOUND;
    }
}

/*************************************************************************************************/
std::vector<uint32_t> RouteFinderRobot::getRoute() const
{
    const std::vector<uint16_t> simplified_route = local_route_finder_.getSimplifiedRoute(); 
    std::vector<uint32_t> output_route;

    for (const uint16_t& segment : simplified_route)
    {
        output_route.push_back(segment);        
    }
    
    return output_route;
}

/*************************************************************************************************/
void RouteFinderRobot::initRouteGraph(const ros::NodeHandle& nh_, const tuw_multi_robot_msgs::Graph& new_graph)
{
    local_route_finder_.initGraphFromMessage(nh_, new_graph);
}

}