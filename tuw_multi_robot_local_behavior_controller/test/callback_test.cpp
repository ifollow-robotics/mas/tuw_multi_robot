#include <gtest/gtest.h>
#include "ros/ros.h"
#include <node/local_behavior_node.h>
#include <std_srvs/SetBool.h>

/*************************************************************************************************/
class TestSubscriber : public ::testing::Test
{
    public:
        bool receivedMessage = false;
        ros::NodeHandle n;  
        ros::Subscriber action_goal_pub_;

        TestSubscriber() : receivedMessage(false) 
        {
            action_goal_pub_ = n.subscribe("single_robot_router/goal", 1, &TestSubscriber::callback, this);

        };

        void callback(const ifollow_nav_msgs::LocalBehaviorMoveActionGoalPtr& newMessage)
        {
            receivedMessage = true;
        }
};

class LocalBehaviorNodeTest : public ::testing::Test
{
public:
    ros::NodeHandle n;  
    stoplights_system::LocalBehaviorNode local_behavior_node;

protected:

    LocalBehaviorNodeTest(): local_behavior_node(n)
    {
        ros::Publisher action_goal_pub_ = n.advertise<ifollow_mas_msgs::Graph>("nav_governor/augmented_graph", 1, true);
        ifollow_mas_msgs::Graph graph;
        ifollow_mas_msgs::Vertex vertex;

        vertex.path.resize(2);
        vertex.area_id = -1;
        vertex.id = 0;
        vertex.path[0].x = 1.0;
        vertex.path[0].y = 2.0;
        vertex.path[1].x = 3.0;
        vertex.path[1].y = 4.0;
        graph.vertices.push_back(vertex);
        vertex.id = 1;
        vertex.path[0].x = 3.0;
        vertex.path[0].y = 2.0;
        vertex.path[1].x = 5.0;
        vertex.path[1].y = 4.0;
        graph.vertices.push_back(vertex);
        vertex.id = 2;
        vertex.path[0].x = 5.0;
        vertex.path[0].y = 2.0;
        vertex.path[1].x = 7.0;
        vertex.path[1].y = 4.0;
        graph.vertices.push_back(vertex);
        vertex.id = 3;
        vertex.path[0].x = 7.0;
        vertex.path[0].y = 2.0;
        vertex.path[1].x = 9.0;
        vertex.path[1].y = 4.0;
        graph.vertices.push_back(vertex);
        
        action_goal_pub_.publish(graph);
        ros::Duration(0.5).sleep();

    };
};

/*************************************************************************************************/
TEST_F(LocalBehaviorNodeTest, masNavReq)
{  
    ros::AsyncSpinner spinner(1);
    spinner.start();
    local_behavior_node.getRoute();
    ros::ServiceClient tmp_service_client_ = n.serviceClient<ifollow_srv_msgs::SetString>("tuw_multi_robot_local_behavior_controller_test/mas_nav_req");

    // Test 01: Call the service mas_nav_req 
    ifollow_srv_msgs::SetString req;

    EXPECT_TRUE(tmp_service_client_.exists());
    EXPECT_TRUE(tmp_service_client_.call(req));
    EXPECT_TRUE(req.response.success);
}

/*************************************************************************************************/
TEST_F(LocalBehaviorNodeTest, getCurrentRoute)
{
    ros::AsyncSpinner spinner(1);
    spinner.start();

    ros::ServiceClient tmp_service_client_ = n.serviceClient<ifollow_mas_msgs::GetCurrentRoute>("get_current_route");

    // Test 01: Call the service get_current_route 
    ifollow_mas_msgs::GetCurrentRoute route_test;

    EXPECT_TRUE(tmp_service_client_.exists());
    EXPECT_TRUE(tmp_service_client_.call(route_test));
    EXPECT_TRUE(route_test.response.segment_ids.empty());
}

/*************************************************************************************************/
TEST_F(LocalBehaviorNodeTest, updateLastSegmentReserved_)
{
    ros::AsyncSpinner spinner(1);
    spinner.start();

    ros::ServiceClient tmp_service_client_ = n.serviceClient<ifollow_mas_msgs::LastReservedSegment>("/update_last_segment_reserved");

    // Test 01: Call the service mas_nav_req 
    ifollow_mas_msgs::LastReservedSegment req;

    EXPECT_TRUE(tmp_service_client_.exists());
    EXPECT_TRUE(tmp_service_client_.call(req));
    EXPECT_TRUE(req.response.success);
}

/*************************************************************************************************/
TEST_F(TestSubscriber, goalCB_)
{
    ros::AsyncSpinner spinner(1);
    spinner.start();
    ros::NodeHandle n;
    stoplights_system::LocalBehaviorNode local_behavior_node(n);
    double waiting_time = 0.01;
    ros::Publisher sub_robot_goal_  = n.advertise<geometry_msgs::PoseStamped>("goal", 1, true);

    // Test 01: Check the goal callback when the input goal's orientation is not valid
    geometry_msgs::PoseStamped goal_position;
    sub_robot_goal_.publish(goal_position);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();
    EXPECT_FALSE(receivedMessage);

    // // Test 02: Check the goal callback when the input goal's orientation is valid
    // goal_position.pose.orientation.z = 1.0;
    // sub_robot_goal_.publish(goal_position);
    // ros::Duration(waiting_time).sleep();
    // ros::spinOnce();
    // ros::Duration(waiting_time).sleep();
    // ros::spinOnce();
    // EXPECT_TRUE(receivedMessage);
}

/*************************************************************************************************/
TEST(LocalBehaviorNode, graphCB)
{
    double waiting_time = 0.01;
    ros::AsyncSpinner spinner(1);
    spinner.start();

    ros::NodeHandle n;
    stoplights_system::LocalBehaviorNode local_behavior_node(n);
    ros::Publisher sub_voronoi_graph_ = n.advertise<tuw_multi_robot_msgs::Graph>("segments", 1, true);
    tuw_multi_robot_msgs::Graph graph;

    sub_voronoi_graph_.publish(graph);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();
    EXPECT_TRUE(true);
}

/*************************************************************************************************/
TEST(LocalBehaviorNode, odomCB_)
{
    double waiting_time = 0.01;
    ros::AsyncSpinner spinner(1);
    spinner.start();

    ros::NodeHandle n;
    stoplights_system::LocalBehaviorNode local_behavior_node(n);
    ros::Publisher sub_voronoi_graph_ = n.advertise<nav_msgs::Odometry>("global/odom", 1, true);
    nav_msgs::Odometry odom;
    odom.pose.pose.position.x = 10.0;
    odom.pose.pose.position.y = 20.0;
    odom.header.frame_id = "world";

    // Test 01: Check when this is the first odom received but the graph is not init
    EXPECT_FALSE(local_behavior_node.isBasePoseInit());
    sub_voronoi_graph_.publish(odom);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();

    Eigen::Vector3d robot_pos = local_behavior_node.getRobotPosition();
    EXPECT_TRUE(local_behavior_node.isBasePoseInit());
    EXPECT_EQ(local_behavior_node.getFrameId(), "world");
    EXPECT_NEAR(robot_pos[0], 10.0, 1e-6);
    EXPECT_NEAR(robot_pos[1], 20.0, 1e-6);

    // Test 02: Check when this is the second odom received and the graph is init 
    ros::Publisher action_goal_pub_ = n.advertise<ifollow_mas_msgs::Graph>("nav_governor/augmented_graph", 1, true);
    ifollow_mas_msgs::Graph graph;
    ifollow_mas_msgs::Vertex vertex;

    vertex.path.resize(2);
    vertex.area_id = -1;
    vertex.id = 0;
    vertex.path[0].x = 1.0;
    vertex.path[0].y = 2.0;
    vertex.path[1].x = 3.0;
    vertex.path[1].y = 4.0;
    graph.vertices.push_back(vertex);
    vertex.id = 1;
    vertex.path[0].x = 3.0;
    vertex.path[0].y = 2.0;
    vertex.path[1].x = 5.0;
    vertex.path[1].y = 4.0;
    graph.vertices.push_back(vertex);
    vertex.id = 2;
    vertex.path[0].x = 5.0;
    vertex.path[0].y = 2.0;
    vertex.path[1].x = 7.0;
    vertex.path[1].y = 4.0;
    graph.vertices.push_back(vertex);
    vertex.id = 3;
    vertex.path[0].x = 7.0;
    vertex.path[0].y = 2.0;
    vertex.path[1].x = 9.0;
    vertex.path[1].y = 4.0;
    graph.vertices.push_back(vertex);
    
    action_goal_pub_.publish(graph);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();
    sub_voronoi_graph_.publish(odom);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();
}

/*************************************************************************************************/
TEST(LocalBehaviorNode, odomCB_02)
{
    double waiting_time = 0.01;
    ros::AsyncSpinner spinner(1);
    spinner.start();

    ros::NodeHandle n;
    stoplights_system::LocalBehaviorNode local_behavior_node(n);
    ros::Publisher sub_voronoi_graph_ = n.advertise<nav_msgs::Odometry>("global/odom", 1, true);
    nav_msgs::Odometry odom;
    
    // Test 01: manage a topic odom when the frame id is not valid 
    odom.pose.pose.position.x = 10.0;
    odom.pose.pose.position.y = 20.0;
    odom.header.frame_id = "invalid";

    sub_voronoi_graph_.publish(odom);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();

    EXPECT_FALSE(local_behavior_node.isBasePoseInit());
}

/*************************************************************************************************/
TEST(LocalBehaviorNode, sendViapointsCB_)
{
    double waiting_time = 0.01;
    ros::AsyncSpinner spinner(1);
    spinner.start();

    ros::NodeHandle n;
    stoplights_system::LocalBehaviorNode local_behavior_node(n);
    ifollow_nav_msgs::GetViapoints viapoints_request;
    ros::ServiceClient get_viaPoints_client_ = n.serviceClient<ifollow_nav_msgs::GetViapoints>("get_viapoints");
    
    // Call the service send via points CB
    // Test 01: Check the service call when the robot is not blocked by traffic
    get_viaPoints_client_.call(viapoints_request);
    EXPECT_TRUE(std::isnan(viapoints_request.response.current_seg_start.x));

    // Test 02: Check the service call when the robot has a valid position and a route
    ros::Publisher action_goal_pub_ = n.advertise<ifollow_mas_msgs::Graph>("nav_governor/augmented_graph", 1, true);
    ifollow_mas_msgs::Graph graph;
    ifollow_mas_msgs::Vertex vertex;
    ros::Publisher sub_voronoi_graph_ = n.advertise<nav_msgs::Odometry>("global/odom", 1, true);
    nav_msgs::Odometry odom;
    odom.pose.pose.position.x = 6.5;
    odom.pose.pose.position.y = 1.2;
    odom.header.frame_id = "world";

    vertex.path.resize(2);
    vertex.area_id = -1;
    vertex.id = 0;
    vertex.path[0].x = 1.0;
    vertex.path[0].y = 2.0;
    vertex.path[1].x = 3.0;
    vertex.path[1].y = 2.0;
    graph.vertices.push_back(vertex);
    vertex.id = 1;
    vertex.path[0].x = 3.0;
    vertex.path[0].y = 2.0;
    vertex.path[1].x = 5.0;
    vertex.path[1].y = 2.0;
    graph.vertices.push_back(vertex);
    vertex.id = 2;
    vertex.path[0].x = 5.0;
    vertex.path[0].y = 2.0;
    vertex.path[1].x = 7.0;
    vertex.path[1].y = 2.0;
    graph.vertices.push_back(vertex);
    vertex.id = 3;
    vertex.path[0].x = 7.0;
    vertex.path[1].x = 9.0;
    graph.vertices.push_back(vertex);
    vertex.id = 4;
    vertex.path[0].x = 9.0;
    vertex.path[1].x = 11.0;
    graph.vertices.push_back(vertex);
    vertex.id = 5;
    vertex.path[0].x = 11.0;
    vertex.path[1].x = 13.0;
    graph.vertices.push_back(vertex);
    vertex.id = 6;
    vertex.path[0].x = 13.0;
    vertex.path[1].x = 15.0;
    graph.vertices.push_back(vertex);
    vertex.id = 7;
    vertex.path[0].x = 15.0;
    vertex.path[1].x = 17.0;
    graph.vertices.push_back(vertex);
    vertex.id = 8;
    vertex.path[0].x = 17.0;
    vertex.path[1].x = 19.0;
    graph.vertices.push_back(vertex);
    
    action_goal_pub_.publish(graph);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();
    std::vector<int> route = {1,2,3,4,5,6,7,8};
    local_behavior_node.setRoute(route);

    sub_voronoi_graph_.publish(odom);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();   

    get_viaPoints_client_.call(viapoints_request);
    // TODO AGO
    // EXPECT_NEAR(viapoints_request.response.current_seg_start.x, 2.0, 1e-6);
    // EXPECT_NEAR(viapoints_request.response.current_seg_start.y, 1.0, 1e-6);
}

/*************************************************************************************************/
TEST(LocalBehaviorNode, subConnectedVehiclesCB_)
{
    ros::AsyncSpinner spinner(1);
    spinner.start();
    
    ros::NodeHandle n;
    n.setParam("robot_name", "ilogistics_4_0032");
    n.setParam("move_base/base_local_planner", "pf_local_planner::PFLocalPlannerROS");
    stoplights_system::LocalBehaviorNode local_behavior_node(n);
    double waiting_time = 0.05;

    // Test 01: The input message is empty and there is only one robot
    ifollow_mas_msgs::ConnectedVehicles message;
    ros::Publisher pub_test = n.advertise<ifollow_mas_msgs::ConnectedVehicles>("connected_vehicles", 1, true);
    pub_test.publish(message);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();
    EXPECT_TRUE(local_behavior_node.getStandaloneMode());

    // Test 02: The input message has only one robot and its not the input robot id and the stamp is not valid
    message.vehicles_id.push_back(65533);
    pub_test.publish(message);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();
    EXPECT_TRUE(local_behavior_node.getStandaloneMode());
    
    // Test 021: The input message has only one robot and its not the input robot id and the stamp is valid
    message.stamp = ros::Time::now();
    pub_test.publish(message);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();
    EXPECT_FALSE(local_behavior_node.getStandaloneMode());
    
    // Test 022: The input message has only one robot and its not the input robot id and the stamp is valid
    message.vehicles_id.clear();
    message.vehicles_id.push_back(4130);
    pub_test.publish(message);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();
    EXPECT_FALSE(local_behavior_node.getStandaloneMode());

    // Test 03: The input message has more than one robot id but the timestamp is not valid
    message.vehicles_id.push_back(4131);
    message.stamp = ros::Time(0);
    pub_test.publish(message);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();
    EXPECT_TRUE(local_behavior_node.getStandaloneMode());

    // Test 03: The input message has more than one robot id but the timestamp is not valid
    message.stamp = ros::Time::now();
    pub_test.publish(message);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();
    EXPECT_FALSE(local_behavior_node.getStandaloneMode());
}

/*************************************************************************************************/
TEST(LocalBehaviorNode, subGraphCB_)
{
    double waiting_time = 0.01;
    ros::NodeHandle n;
    stoplights_system::LocalBehaviorNode local_behavior_node(n);
    ros::AsyncSpinner spinner(1);
    spinner.start();

    // Test 01: Check that the callback is node crashing when managing a the segment topic
    ros::Publisher segment_pub = n.advertise<tuw_multi_robot_msgs::Graph>("segments", 1, this);
    tuw_multi_robot_msgs::Graph graph_test;
    
    segment_pub.publish(graph_test);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();

    EXPECT_TRUE(true);
}

/*************************************************************************************************/
TEST(LocalBehaviorNode, graphAreasCB_)
{
    double waiting_time = 0.01;
    ros::NodeHandle n;
    stoplights_system::LocalBehaviorNode local_behavior_node(n);
    ros::AsyncSpinner spinner(1);
    spinner.start();

    ros::Publisher areas_pub = n.advertise<ifollow_mas_msgs::AreaArray>("nav_governor/graph_areas", 1, this);

    ifollow_mas_msgs::AreaArray area_test;
    ifollow_mas_msgs::Area area_msgs;

    // Test 01: Check when the area is not of type PARKING
    area_msgs.type.push_back(ifollow_mas_msgs::Area::AREA_TYPE_NO_STOP);
    area_msgs.points.resize(4);
    area_msgs.id = 1;
    area_msgs.points[0].x = 1.0;
    area_msgs.points[0].y = 1.0;
    area_msgs.points[1].x = 1.0;
    area_msgs.points[1].y = 5.0;
    area_msgs.points[2].x = 5.0;
    area_msgs.points[2].y = 5.0;
    area_msgs.points[3].x = 5.0;
    area_msgs.points[3].y = 1.0;
    area_test.areas.push_back(area_msgs);

    // Test 02: Check when the number of points inside the area is not valid (below two)
    area_msgs.type.push_back(ifollow_mas_msgs::Area::AREA_TYPE_TRAFFIC_PARKING);
    area_msgs.id = 2;
    area_msgs.points.resize(2);
    area_msgs.points[0].x = 1.0;
    area_msgs.points[0].y = 1.0;
    area_msgs.points[1].x = 2.0;
    area_msgs.points[1].y = 1.0;
    area_test.areas.push_back(area_msgs);

    // Test 03: Checn when the area is fully valid
    area_msgs.type.push_back(ifollow_mas_msgs::Area::AREA_TYPE_TRAFFIC_PARKING);
    area_msgs.id = 3;
    area_msgs.points.resize(4);
    area_msgs.points[0].x = 1.0;
    area_msgs.points[0].y = 1.0;
    area_msgs.points[1].x = 1.0;
    area_msgs.points[1].y = 5.0;
    area_msgs.points[2].x = 5.0;
    area_msgs.points[2].y = 5.0;
    area_msgs.points[3].x = 5.0;
    area_msgs.points[3].y = 1.0;
    area_test.areas.push_back(area_msgs);

    areas_pub.publish(area_test);

    ros::Duration(waiting_time).sleep();
    ros::spinOnce();
}

/*************************************************************************************************/
TEST(LocalBehaviorNode, modeSmCB_)
{
    double waiting_time = 0.01;
    ros::NodeHandle n;
    stoplights_system::LocalBehaviorNode local_behavior_node(n);
    ros::AsyncSpinner spinner(1);
    spinner.start();

    ros::Publisher modeSm_pub = n.advertise<std_msgs::String>("lowLvl/manager/mode/sm_state", 1, this);
    std_msgs::String modeSm;
    modeSm.data = "Teleop";
    modeSm_pub.publish(modeSm);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();

    modeSm.data = "Invalid";
    modeSm_pub.publish(modeSm);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();

    EXPECT_TRUE(true);
}

/*************************************************************************************************/
TEST(LocalBehaviorNode, getGoalParametersCB_01)
{
    ifollow_nav_msgs::GetGoalParameters goalParameters;

    ros::NodeHandle n;
    stoplights_system::LocalBehaviorNode local_behavior_node(n);
    ros::AsyncSpinner spinner(1);
    spinner.start();
    ros::ServiceClient tmp_service_client_ =n.serviceClient<ifollow_nav_msgs::GetGoalParameters>("get_goal_parameters");
    
    // Test 01: Check when the requested goal parameters is not defined in the list
    EXPECT_TRUE(tmp_service_client_.exists());
    EXPECT_TRUE(tmp_service_client_.call(goalParameters));
    EXPECT_NEAR(goalParameters.response.goal_params.beyond_goal_coll_check_dist, 0.0, 1e-6);
    EXPECT_FALSE(goalParameters.response.goal_params.align_path_with_goal);
    EXPECT_NEAR(goalParameters.response.goal_params.arc_length_allowed_goal_zone, 0.0, 1e-6);
    EXPECT_FALSE(goalParameters.response.goal_params.use_turtle_mode);
    EXPECT_FALSE(goalParameters.response.goal_params.use_avoidance_traj);
}

/*************************************************************************************************/
TEST(LocalBehaviorNode, getGoalParametersCB_02)
{
    ifollow_nav_msgs::GetGoalParameters goalParameters;
    ros::NodeHandle n;
    XmlRpc::XmlRpcValue test01, tmp;
    test01.setSize(1);
    tmp["name"] = std::string("first_picking");
    tmp["beyond_goal_coll_check_dist"] = (double)0.1;
    tmp["align_path_with_goal"] = true;
    tmp["use_turtle_mode"]      = true;
    tmp["use_avoidance_traj"]   = true;
    tmp["arc_length_allowed_goal_zone"] = (double)0.4;
    test01[0] = tmp;
    n.setParam("/local_behavior/goal_parameters", test01);

    stoplights_system::LocalBehaviorNode local_behavior_node(n);
    ros::AsyncSpinner spinner(1);
    spinner.start();
    ros::ServiceClient tmp_service_client_ =n.serviceClient<ifollow_nav_msgs::GetGoalParameters>("get_goal_parameters");
    ifollow_nav_msgs::LocalBehaviorMoveGoal goal;
    goal.target_pose.pose.position.x = 6.5;
    goal.target_pose.pose.position.y = 2.0;
    goal.goal_profile = "first_picking";
    local_behavior_node.planRoute(goal);

    // Test 01: Check when the requested goal parameters is defined in the list
    EXPECT_TRUE(tmp_service_client_.exists());
    EXPECT_TRUE(tmp_service_client_.call(goalParameters));
    EXPECT_NEAR(goalParameters.response.goal_params.beyond_goal_coll_check_dist, 0.1, 1e-6);
    EXPECT_TRUE(goalParameters.response.goal_params.align_path_with_goal);
    EXPECT_NEAR(goalParameters.response.goal_params.arc_length_allowed_goal_zone, 0.4, 1e-6);
    EXPECT_TRUE(goalParameters.response.goal_params.use_turtle_mode);
    EXPECT_TRUE(goalParameters.response.goal_params.use_avoidance_traj);

    n.deleteParam("/local_behavior/goal_parameters");
}

/*************************************************************************************************/
TEST(LocalBehaviorNode, getCurrentRoute_)
{
    ros::NodeHandle n;
    stoplights_system::LocalBehaviorNode local_behavior_node(n);
    ros::AsyncSpinner spinner(1);
    spinner.start();
    ros::ServiceClient tmp_service_client_ =n.serviceClient<ifollow_mas_msgs::GetCurrentRoute>("get_current_route");

    // Test 01: Call the service when the route is empty
    ifollow_mas_msgs::GetCurrentRoute requested_route;

    EXPECT_TRUE(tmp_service_client_.exists());
    EXPECT_TRUE(tmp_service_client_.call(requested_route));
    EXPECT_TRUE(requested_route.response.segment_ids.empty());

    // Test 02: Call the service when the route is not empty
    std::vector<int> route = {1,2,3};
    local_behavior_node.setRoute(route);
    EXPECT_TRUE(tmp_service_client_.exists());
    EXPECT_TRUE(tmp_service_client_.call(requested_route));
    EXPECT_EQ(route.size(), requested_route.response.segment_ids.size());
    EXPECT_EQ(route[0], requested_route.response.segment_ids[0]);
    EXPECT_EQ(route[1], requested_route.response.segment_ids[1]);
    EXPECT_EQ(route[2], requested_route.response.segment_ids[2]);

}
