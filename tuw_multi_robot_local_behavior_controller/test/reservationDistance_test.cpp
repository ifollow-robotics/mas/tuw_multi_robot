#include <gtest/gtest.h>
#include "ros/ros.h"
#include <node/local_behavior_node.h>

/*************************************************************************************************/
TEST(distanceReservation, constructor)
{
    // Test 01: Make sure all the variable are properly initialized by the constructor
    ros::NodeHandle n;
    stoplights_system::DistanceReservation distance_reservation = stoplights_system::DistanceReservation(n);
    
    EXPECT_EQ(1.0, distance_reservation.min_reserved_horizon);
    EXPECT_EQ(3.0, distance_reservation.max_reserved_horizon);
    EXPECT_EQ(0.0, distance_reservation.reserved_horizon);
    EXPECT_EQ(0.0, distance_reservation.remaining_horizon);

    // Test 02: Check when the the parameters are properly set
    n.setParam("local_behavior/min_reserved_horizon", 5.0);
    n.setParam("local_behavior/max_reserved_horizon", 6.0);
    distance_reservation = stoplights_system::DistanceReservation(n);
    
    EXPECT_EQ(5.0, distance_reservation.min_reserved_horizon);
    EXPECT_EQ(6.0, distance_reservation.max_reserved_horizon);
    EXPECT_EQ(0.0, distance_reservation.reserved_horizon);
    EXPECT_EQ(0.0, distance_reservation.remaining_horizon);

    n.deleteParam("local_behavior/min_reserved_horizon");
    n.deleteParam("local_behavior/max_reserved_horizon");
}

/*************************************************************************************************/
TEST(distanceReservation, isOnLastRouteSegment)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation distance_reservation = stoplights_system::DistanceReservation(n);
    
    // Check 01: Test when the robot has no current route
    EXPECT_FALSE(distance_reservation.isOnLastRouteSegment());

    // Check 02: The robot has a route but is not on its last route and its not reserved neither
    std::vector<int32_t> new_route = {1, 2, 3, 4};
    geometry_msgs::Point goal_point;
    distance_reservation.initRoute(new_route, goal_point);
    EXPECT_FALSE(distance_reservation.isOnLastRouteSegment());

    // Check 03: The robot is on its last segment
    distance_reservation.current_segment_it = distance_reservation.current_segment_it + 3;
    EXPECT_TRUE(distance_reservation.isOnLastRouteSegment());
}

/*************************************************************************************************/
TEST(distanceReservation, resetRoute)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation distance_reservation = stoplights_system::DistanceReservation(n);
    distance_reservation.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    // Test 01: Call the function and check that the route is properly updated as well as the related variables
    std::vector<int32_t> new_route = {1, 2, 3, 4};
    geometry_msgs::Point goal_point;
    distance_reservation.initRoute(new_route, goal_point);
    distance_reservation.current_segment_it = distance_reservation.current_segment_it + 1;
    distance_reservation.resetRoute();
    EXPECT_TRUE(distance_reservation.route.empty());
    EXPECT_EQ(distance_reservation.route.begin(), distance_reservation.current_segment_it); 
}

/*************************************************************************************************/
TEST(distanceReservation, initRoute)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation distance_reservation = stoplights_system::DistanceReservation(n);
    distance_reservation.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    // Test 01: Call the function and check that the route is properly updated as well as the related variables
    std::vector<int32_t> new_route = {1, 2, 3, 4};
    geometry_msgs::Point goal_point;
    distance_reservation.initRoute(new_route, goal_point);

    EXPECT_EQ(4, distance_reservation.route.size());
    EXPECT_EQ(distance_reservation.route.begin(), distance_reservation.current_segment_it); 
}

/*************************************************************************************************/
TEST(distanceReservation, updateRemainingAccess)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation distance_reservation = stoplights_system::DistanceReservation(n);
    distance_reservation.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    // Test 01: Check when the input value in not in the current route
    distance_reservation.updateRemainingAccess(10);
    distance_reservation.reserved_horizon = 10.;
    EXPECT_NEAR(distance_reservation.reserved_horizon, 10., 1e-6);

    // Test 02: Check when the input value is NOT in the route
    std::vector<int32_t> new_route = {1, 2, 3, 4};
    geometry_msgs::Point goal_point;
    distance_reservation.initRoute(new_route, goal_point);
    distance_reservation.reserved_horizon = 10.;

    distance_reservation.updateRemainingAccess(10);
    EXPECT_NEAR(distance_reservation.reserved_horizon, 10., 1e-6);

    // Test 03: Check when the input value is in the route
    distance_reservation.updateRemainingAccess(3);
    EXPECT_NEAR(distance_reservation.reserved_horizon, stoplights_system::min_horizon_distance_constant, 1e-6);
}

/*************************************************************************************************/
TEST(distanceReservation, isAccessRestricted)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation distance_reservation = stoplights_system::DistanceReservation(n);
    distance_reservation.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    // Test 01: Check when the robot is not restricted (no remaining horizon and no reserved horizon)
    EXPECT_FALSE(distance_reservation.isAccessRestricted());

    // Test 02: Check when the robot is not restricted
    distance_reservation.reserved_horizon = 4.0;
    EXPECT_FALSE(distance_reservation.isAccessRestricted());

    // Test 03: Check when the robot is restricted
    distance_reservation.reserved_horizon = 2.0;
    EXPECT_FALSE(distance_reservation.isAccessRestricted());

    // Test 03: Check when the robot is not restricted (blocked)
    distance_reservation.reserved_horizon = 0.7;
    distance_reservation.remaining_horizon = 1.0;
    EXPECT_TRUE(distance_reservation.isAccessRestricted());
}

/*************************************************************************************************/
TEST(distanceReservation, isAccessStillBlocked)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation distance_reservation = stoplights_system::DistanceReservation(n);
    distance_reservation.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    // Test 01: Check when a robot has a route, distance above the min distance and distance above remaining
    std::vector<int32_t> new_route = {1, 2, 3, 4};
    geometry_msgs::Point goal_point;
    distance_reservation.initRoute(new_route, goal_point);
    distance_reservation.reserved_horizon   = 1.1;
    distance_reservation.remaining_horizon  = 2.0;
    distance_reservation.current_segment_it = distance_reservation.route.begin() + 2;
    EXPECT_FALSE(distance_reservation.isAccessStillBlocked());

    // Test 02: Check when the robot reserved horizon is below the min value
    distance_reservation.reserved_horizon   = 0.1;
    EXPECT_TRUE(distance_reservation.isAccessStillBlocked());

    // Test 02: Check when the robot remaining horizon is below the reserved horizon
    distance_reservation.reserved_horizon   = 1.1;
    distance_reservation.remaining_horizon  = 1.0;
    EXPECT_FALSE(distance_reservation.isAccessStillBlocked());

    // Test 03: Check when the robot is at the end of its route
    distance_reservation.remaining_horizon  = 2.0;
    distance_reservation.current_segment_it = distance_reservation.route.begin() + 3;
    EXPECT_FALSE(distance_reservation.isAccessStillBlocked());

    // Test 03: Check when the robot is at the end of its route
    distance_reservation.resetRoute();
    EXPECT_FALSE(distance_reservation.isAccessStillBlocked());

}

/*************************************************************************************************/
TEST(distanceReservation, isAccessBlocked)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation distance_reservation = stoplights_system::DistanceReservation(n);
    distance_reservation.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    // Test 01: Check when the robot is below the blocking treeshold
    EXPECT_TRUE(distance_reservation.isAccessBlocked());

    // Test 02: Check when the robot is NOT below the blocking treeshold
    distance_reservation.reserved_horizon = 2.0;
    EXPECT_FALSE(distance_reservation.isAccessBlocked());
}

/*************************************************************************************************/
TEST(distanceReservation, isAccessUnblocked)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation distance_reservation = stoplights_system::DistanceReservation(n);
    distance_reservation.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    std::vector<int32_t> new_route = {1, 2, 3, 4};
    geometry_msgs::Point goal_point;
    distance_reservation.initRoute(new_route, goal_point);

    // Test 01: Check when the robot is below the restricted treeshold
    EXPECT_FALSE(distance_reservation.isAccessUnblocked());

    // Test 02: Check when the robot is not below the restricted treeshold
    distance_reservation.reserved_horizon = 3.0;
    EXPECT_TRUE(distance_reservation.isAccessUnblocked());
}

/*************************************************************************************************/
TEST(distanceReservation, getStopPoint)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation distance_reservation = stoplights_system::DistanceReservation(n);
    distance_reservation.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    std::vector<int32_t> new_route = {1, 2, 3, 4, 5};
    geometry_msgs::Point goal_point;
    distance_reservation.initRoute(new_route, goal_point);

    std::map<int32_t, stoplights_system::Segment> segments_test;
    stoplights_system::Segment seg;

    seg.start << 1.0, 2.0;
    seg.end << 3.0, 2.0;
    distance_reservation.segments_[1] = seg;
    seg.start << 3.0, 2.0;
    seg.end << 5.0, 2.0;
    distance_reservation.segments_[2] = seg;
    seg.start << 5.0, 2.0;
    seg.end << 7.0, 2.0;
    distance_reservation.segments_[3] = seg;
    seg.start << 7.0, 2.0;
    seg.end << 9.0, 2.0;
    distance_reservation.segments_[4] = seg;
    seg.start << 11.0, 2.0;
    seg.end << 9.0, 2.0;
    distance_reservation.segments_[5] = seg;
    
    // Test 01: Check when the last accessible segment is behind the current robot's segment
    distance_reservation.robot_pos.x()      = 1.2;
    distance_reservation.robot_pos.y()      = 2.0;
    distance_reservation.reserved_horizon   = 0.0;
    geometry_msgs::PointStamped point_test = distance_reservation.getStopPoint();
    EXPECT_NEAR(1.2, point_test.point.x, 1e-6);
    EXPECT_NEAR(2.0, point_test.point.y, 1e-6);
    
    // Test 02: Check when the last accessible segment is ahead of the current robot's segment
    distance_reservation.reserved_horizon = 1.0;
    point_test = distance_reservation.getStopPoint();
    EXPECT_NEAR(2.2, point_test.point.x, 1e-6);
    EXPECT_NEAR(2.0, point_test.point.y, 1e-6);
        
    // Test 03: Check when the last accessible segment is ahead of the current robot's segment
    distance_reservation.reserved_horizon = 4.5;
    point_test = distance_reservation.getStopPoint();
    EXPECT_NEAR(5.7, point_test.point.x, 1e-6);
    EXPECT_NEAR(2.0, point_test.point.y, 1e-6);

    // Test 04: Check when the last accessible segment is ahead of the current robot's segment
    distance_reservation.reserved_horizon = 9.7;
    point_test = distance_reservation.getStopPoint();
    EXPECT_NEAR(10.9, point_test.point.x, 1e-6);
    EXPECT_NEAR(2.0,  point_test.point.y, 1e-6);
    
    // Test 05: Check when the last accessible segment is ahead of the current robot's segment
    distance_reservation.reserved_horizon = 15.0;
    point_test = distance_reservation.getStopPoint();
    EXPECT_NEAR(11.0, point_test.point.x, 1e-6);
    EXPECT_NEAR(2.0,  point_test.point.y, 1e-6);

    // Test 06: Check when the last accesible segment is the next segment and the robot goes on revert
    distance_reservation.robot_pos.x()      = 8.2;
    distance_reservation.robot_pos.y()      = 2.0;
    distance_reservation.reserved_horizon   = 10.0;
    distance_reservation.current_segment_it = distance_reservation.route.begin() + 3;
    point_test = distance_reservation.getStopPoint();
    EXPECT_NEAR(11.0, point_test.point.x, 1e-6);
    EXPECT_NEAR(2.0,  point_test.point.y, 1e-6);

    // Test 07: Check when the last accesible segment is the current segment and the robot goes on revert
    distance_reservation.robot_pos.x()      = 10.0;
    distance_reservation.robot_pos.y()      = 2.0;
    distance_reservation.reserved_horizon   = 10.0;
    distance_reservation.current_segment_it = distance_reservation.route.begin() + 4;
    point_test = distance_reservation.getStopPoint();
    EXPECT_NEAR(11.0, point_test.point.x, 1e-6);
    EXPECT_NEAR(2.0,  point_test.point.y, 1e-6);

    // Test 08: Check when the direction on the segment is backward
    new_route = {5, 4, 3, 2, 1};
    distance_reservation.initRoute(new_route, goal_point);
    distance_reservation.reserved_horizon = 15.0;
    distance_reservation.robot_pos.x()      = 6.0;
    distance_reservation.robot_pos.y()      = 2.0;
    distance_reservation.current_segment_it = distance_reservation.route.begin() + 2;
    point_test = distance_reservation.getStopPoint();
    EXPECT_NEAR(1.0, point_test.point.x, 1e-6);
    EXPECT_NEAR(2.0,  point_test.point.y, 1e-6);

    // Test 09: Check return stopPoint when the route is empty
    new_route.clear();
    distance_reservation.initRoute(new_route, goal_point);
    point_test = distance_reservation.getStopPoint();
    EXPECT_TRUE(std::isnan(point_test.point.x));
    EXPECT_TRUE(std::isnan(point_test.point.y));

    // Test 10: Check when the robot is going forward on the last segment
    new_route.clear();
    new_route = {1, 2, 3};
    distance_reservation.robot_pos.x()      = 1.2;
    distance_reservation.robot_pos.y()      = 2.0;
    distance_reservation.initRoute(new_route, goal_point);
    distance_reservation.reserved_horizon   = 1.0;
    point_test = distance_reservation.getStopPoint();
    EXPECT_NEAR(2.2, point_test.point.x, 1e-6);
    EXPECT_NEAR(2.0, point_test.point.y, 1e-6);

    // Test 11: Check when the robot has one segment and the reserved segment is bigger than the segment's length
    new_route.clear();
    new_route = {1};
    distance_reservation.robot_pos.x()      = 1.2;
    distance_reservation.robot_pos.y()      = 2.0;
    goal_point.x = 1.8;
    goal_point.y = 2.0;
    distance_reservation.initRoute(new_route, goal_point);
    distance_reservation.reserved_horizon   = 0.9;
    point_test = distance_reservation.getStopPoint();
    EXPECT_NEAR(1.8, point_test.point.x, 1e-6);
    EXPECT_NEAR(2.0, point_test.point.y, 1e-6);

    // Test 12: Check when the robot has one segment and the reserved segment is bigger than the segment's length
    new_route.clear();
    new_route = {1};
    distance_reservation.robot_pos.x()  = 1.2;
    distance_reservation.robot_pos.y()  = 2.0;
    goal_point.x = 1.8;
    goal_point.y = 2.0;
    distance_reservation.initRoute(new_route, goal_point);
    distance_reservation.reserved_horizon   = 0.5;
    point_test = distance_reservation.getStopPoint();
    EXPECT_NEAR(1.7, point_test.point.x, 1e-6);
    EXPECT_NEAR(2.0, point_test.point.y, 1e-6);
}

/*************************************************************************************************/
TEST(distanceReservation, isRequestBehind)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation distance_reservation = stoplights_system::DistanceReservation(n);

    // Test 01: Check the default return of the function
    EXPECT_FALSE(distance_reservation.isRequestBehind());
}

/*************************************************************************************************/
TEST(distanceReservation, updateSegmentReservation)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation distance_reservation = stoplights_system::DistanceReservation(n);
    distance_reservation.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    // Test 01: Check when the current reserved horizon is already above the max limitation
    distance_reservation.reserved_horizon = 10.0;
    distance_reservation.updateSegmentReservation();
    EXPECT_NEAR(distance_reservation.reserved_horizon, 10.0, 1e-6);

    // Test 02: Check when the current reserved horizon is below the max limitation
    distance_reservation.reserved_horizon = 1.0;
    distance_reservation.updateSegmentReservation();
    EXPECT_NEAR(distance_reservation.reserved_horizon, 3.0, 1e-6);
    EXPECT_EQ(0, distance_reservation.rejected_request_penalty);

}

/*************************************************************************************************/
TEST(distance, updateReservationFromPosition)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation distance_reservation = stoplights_system::DistanceReservation(n);
    Eigen::Vector3d robot_pos;
    robot_pos[0] = 1.5;
    robot_pos[1] = 2.0;

    std::vector<int32_t> new_route = {1, 2, 3, 4, 5};

    std::map<int32_t, stoplights_system::Segment> segments_test;
    stoplights_system::Segment seg;

    seg.start << 1.0, 2.0;
    seg.end << 3.0, 2.0;
    distance_reservation.segments_[1] = seg;
    seg.start << 3.0, 2.0;
    seg.end << 5.0, 2.0;
    distance_reservation.segments_[2] = seg;
    seg.start << 5.0, 2.0;
    seg.end << 7.0, 2.0;
    distance_reservation.segments_[3] = seg;
    seg.start << 7.0, 2.0;
    seg.end << 9.0, 2.0;
    distance_reservation.segments_[4] = seg;
    seg.start << 11.0, 2.0;
    seg.end << 9.0, 2.0;
    distance_reservation.segments_[5] = seg;

    // Test 01: Check when the robot has no current route
    distance_reservation.updateReservationFromPosition(robot_pos, true);
    EXPECT_NEAR(distance_reservation.remaining_horizon, 0.11, 1e-6);

    // Test 02: Check when the robot has a route 
    geometry_msgs::Point goal_point;
    goal_point.x = 11.0;
    goal_point.y = 2.0;

    distance_reservation.initRoute(new_route, goal_point);
    distance_reservation.updateReservationFromPosition(robot_pos, true);
    EXPECT_NEAR(distance_reservation.remaining_horizon, 9.5, 1e-6);
}

/*************************************************************************************************/
TEST(distance, updateHorizon)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation distance_reservation = stoplights_system::DistanceReservation(n);
    distance_reservation.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    std::vector<int32_t> new_route = {1, 2, 3, 4, 5};
    uint16_t offset_segment_idx = 1;
    geometry_msgs::Point goal_point;

    // Test 01: Check when the robot has no current route
    distance_reservation.updateHorizon_(offset_segment_idx);
    EXPECT_EQ(distance_reservation.current_segment_it, distance_reservation.route.begin());

    // Test 02: Check that the current_segment id is properly updated
    distance_reservation.initRoute(new_route, goal_point);
    distance_reservation.updateHorizon_(offset_segment_idx);
    EXPECT_EQ(distance_reservation.current_segment_it, distance_reservation.route.begin() + offset_segment_idx);
}

/*************************************************************************************************/
TEST(distance, updateRemainingDistance_01)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation distance_reservation = stoplights_system::DistanceReservation(n);
    std::vector<int32_t> new_route = {1};
    stoplights_system::Segment seg;
    geometry_msgs::Point goal_point;

    seg.start << 1.0, 2.0;
    seg.end << 3.0, 2.0;
    distance_reservation.segments_[1] = seg;
    seg.start << 3.0, 2.0;
    seg.end << 5.0, 2.0;
    distance_reservation.segments_[2] = seg;
    seg.start << 5.0, 2.0;
    seg.end << 7.0, 2.0;
    distance_reservation.segments_[3] = seg;
    seg.start << 7.0, 2.0;
    seg.end << 9.0, 2.0;
    distance_reservation.segments_[4] = seg;
    seg.start << 9.0, 2.0;
    seg.end << 11.0, 2.0;
    distance_reservation.segments_[5] = seg;

    // Test when the route is empty
    distance_reservation.robot_pos[0] = 1.5;
    distance_reservation.robot_pos[1] = 2.0;
    distance_reservation.robot_pos[2] = 0.0;
    goal_point.x = 1.8;
    goal_point.y = 2.0;
    goal_point.z = 0.0;
    distance_reservation.updateRemainingDistance();
    EXPECT_NEAR(distance_reservation.remaining_horizon, 0.11, 1e-6);

    // Test when the route size is equal to 1
    new_route = {1};
    distance_reservation.initRoute(new_route, goal_point);
    distance_reservation.updateRemainingDistance();
    EXPECT_NEAR(distance_reservation.remaining_horizon, 0.3, 1e-6);

    // Test when the route size is equal to 3 and the robot goes forward on the current segment
    new_route = {1, 2, 3};
    goal_point.x = 6.0;
    goal_point.y = 2.0;
    goal_point.z = 0.0;
    distance_reservation.initRoute(new_route, goal_point);
    distance_reservation.updateRemainingDistance();
    EXPECT_NEAR(distance_reservation.remaining_horizon, 4.5, 1e-6);
}

/*************************************************************************************************/
TEST(distance, updateRemainingDistance_02)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation distance_reservation = stoplights_system::DistanceReservation(n);
    std::vector<int32_t> new_route = {1};
    stoplights_system::Segment seg;
    geometry_msgs::Point goal_point;

    seg.start << 3.0, 2.0;
    seg.end << 1.0, 2.0;
    distance_reservation.segments_[1] = seg;
    seg.start << 3.0, 2.0;
    seg.end << 5.0, 2.0;
    distance_reservation.segments_[2] = seg;
    seg.start << 5.0, 2.0;
    seg.end << 7.0, 2.0;
    distance_reservation.segments_[3] = seg;
    seg.start << 7.0, 2.0;
    seg.end << 9.0, 2.0;
    distance_reservation.segments_[4] = seg;
    seg.start << 11.0, 2.0;
    seg.end << 9.0, 2.0;
    distance_reservation.segments_[5] = seg;

    // Test when the route size is equal to 3 and the robot goes backward on the current segment
    new_route.clear();
    new_route = {1, 2, 3};
    distance_reservation.robot_pos[0] = 1.5;
    distance_reservation.robot_pos[1] = 2.0;
    distance_reservation.robot_pos[2] = 0.0;
    goal_point.x = 6.0;
    goal_point.y = 2.0;
    goal_point.z = 0.0;
    distance_reservation.initRoute(new_route, goal_point);
    distance_reservation.updateRemainingDistance();
    EXPECT_NEAR(distance_reservation.remaining_horizon, 4.5, 1e-6);

    // Test when the route size is equal to 3 and the robot goes backward on the last segment 
    new_route.clear();
    new_route = {3, 4, 5};
    distance_reservation.robot_pos[0] = 6.0;
    distance_reservation.robot_pos[1] = 2.0;
    distance_reservation.robot_pos[2] = 0.0;
    goal_point.x = 10.0;
    goal_point.y = 2.0;
    goal_point.z = 0.0;
    distance_reservation.initRoute(new_route, goal_point);
    distance_reservation.updateRemainingDistance();
    EXPECT_NEAR(distance_reservation.remaining_horizon, 4.0, 1e-6);
}

/*************************************************************************************************/
TEST(distance, getDirectionOnSegment)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation distance_reservation = stoplights_system::DistanceReservation(n);
    stoplights_system::Segment current_seg, next_seg;

    // Test 01: Check when the end point is connected to the start point
    current_seg.start   << 1.0, 2.0;
    current_seg.end     << 3.0, 2.0;
    next_seg.start   << 3.0, 2.0;
    next_seg.end     << 5.0, 2.0;
    EXPECT_TRUE(distance_reservation.getDirectionOnSegment(current_seg, next_seg));

    // Test 02: Check when the end point is connected to the end point
    current_seg.start   << 1.0, 2.0;
    current_seg.end     << 3.0, 2.0;
    next_seg.start   << 5.0, 2.0;
    next_seg.end     << 3.0, 2.0;
    EXPECT_TRUE(distance_reservation.getDirectionOnSegment(current_seg, next_seg));

    // Test 03: Check when the start point is connected to the start point
    current_seg.start   << 3.0, 2.0;
    current_seg.end     << 1.0, 2.0;
    next_seg.start   << 3.0, 2.0;
    next_seg.end     << 5.0, 2.0;
    EXPECT_FALSE(distance_reservation.getDirectionOnSegment(current_seg, next_seg));

    // Test 04: Check when the start point is connected to the end point
    current_seg.start   << 3.0, 2.0;
    current_seg.end     << 1.0, 2.0;
    next_seg.start   << 5.0, 2.0;
    next_seg.end     << 3.0, 2.0;
    EXPECT_FALSE(distance_reservation.getDirectionOnSegment(current_seg, next_seg));

    // Test 01: Check when the two segments are not connected
    current_seg.start   << 1.0, 2.0;
    current_seg.end     << 3.0, 2.0;
    next_seg.start   << 30.0, 2.0;
    next_seg.end     << 50.0, 2.0;
    EXPECT_TRUE(distance_reservation.getDirectionOnSegment(current_seg, next_seg));
}

/*************************************************************************************************/
TEST(distance, stopPointOnCurrentSegment)
{
    geometry_msgs::Point stop_point;
    ros::NodeHandle n;
    stoplights_system::DistanceReservation distance_reservation = stoplights_system::DistanceReservation(n);
    bool direction_forward, current_robot_segment;
    double remaining_distance= 1.5;
    stoplights_system::Segment current_seg;
    current_seg.start   << 1.0, 2.0;
    current_seg.end     << 3.0, 2.0;
    distance_reservation.robot_pos[0] = 1.2;
    distance_reservation.robot_pos[1] = 2.0;
    distance_reservation.robot_pos[2] = 0.0;

    // Test 01: Check when the direction is forward current_robot_segment is true 
    direction_forward       = true;
    current_robot_segment   = true;
    stop_point = distance_reservation.stopPointOnCurrentSegment(direction_forward, current_robot_segment, remaining_distance, current_seg);
    EXPECT_NEAR(stop_point.x, 2.7, 1e-6);
    EXPECT_NEAR(stop_point.y, 2.0, 1e-6);

    // Test 02: Check when the direction is forward current_robot_segment is false
    direction_forward       = true;
    current_robot_segment   = false;
    stop_point = distance_reservation.stopPointOnCurrentSegment(direction_forward, current_robot_segment, remaining_distance, current_seg);
    EXPECT_NEAR(stop_point.x, 2.5, 1e-6);
    EXPECT_NEAR(stop_point.y, 2.0, 1e-6);

    // Test 03: Check when the direction is backward current_robot_segment is true
    direction_forward       = false;
    current_robot_segment   = true;
    stop_point = distance_reservation.stopPointOnCurrentSegment(direction_forward, current_robot_segment, remaining_distance, current_seg);
    EXPECT_NEAR(stop_point.x, 1.0, 1e-6);
    EXPECT_NEAR(stop_point.y, 2.0, 1e-6);

    // Test 04: Check when the direction is backward current_robot_segment is false
    direction_forward       = false;
    current_robot_segment   = false;
    stop_point = distance_reservation.stopPointOnCurrentSegment(direction_forward, current_robot_segment, remaining_distance, current_seg);
    EXPECT_NEAR(stop_point.x, 1.5, 1e-6);
    EXPECT_NEAR(stop_point.y, 2.0, 1e-6);
}
