#include <gtest/gtest.h>
#include "ros/ros.h"
#include <node/local_behavior_node.h>

/*************************************************************************************************/
class routeFinderTest : public ::testing::Test
{
    public:
        tuw_multi_robot_msgs::Graph graph;

        routeFinderTest() 
        {
            graph.resolution = 0.05;
            int nb_segments = 10;
            
            // Create the graph
            for (auto i_segment = 0; i_segment < nb_segments; ++i_segment)
            {
                tuw_multi_robot_msgs::Vertex v; 
                v.id        = i_segment;
                v.valid     = true;
                v.weight    = 1;
                v.width     = 1;
                v.path.resize(2);

                v.path[0].x = (i_segment) * 1.0 / graph.resolution;
                v.path[0].y = 1.0 / graph.resolution;
                v.path[1].x = (i_segment + 1) * 1.0 / graph.resolution;
                v.path[1].y = 1.0 / graph.resolution;
                
                if (i_segment > 0)
                {
                    v.predecessors.push_back(i_segment - 1);
                }

                if (i_segment < (nb_segments - 1))
                {
                    v.successors.push_back(i_segment + 1);
                }

                graph.vertices.push_back(v);
            }
        }
};

/*************************************************************************************************/
class routeFinderServerTest : public ::testing::Test
{
    public:
        ros::ServiceServer  service_in;
        ros::NodeHandle n;  
        std::vector<uint16_t> route_test;
        tuw_multi_robot_msgs::Graph graph;
        
        routeFinderServerTest() 
        {
            service_in = n.advertiseService("stoplights_compute_route", &routeFinderServerTest::callback, this);
            
            graph.resolution = 0.05;
            int nb_segments = 10;
            
            // Create the graph
            for (auto i_segment = 0; i_segment < nb_segments; ++i_segment)
            {
                tuw_multi_robot_msgs::Vertex v; 
                v.id        = i_segment;
                v.valid     = true;
                v.weight    = 1;
                v.width     = 1;
                v.path.resize(2);

                v.path[0].x = (i_segment) * 1.0 / graph.resolution;
                v.path[0].y = 1.0 / graph.resolution;
                v.path[1].x = (i_segment + 1) * 1.0 / graph.resolution;
                v.path[1].y = 1.0 / graph.resolution;
                
                if (i_segment > 0)
                {
                    v.predecessors.push_back(i_segment - 1);
                }

                if (i_segment < (nb_segments - 1))
                {
                    v.successors.push_back(i_segment + 1);
                }

                graph.vertices.push_back(v);
            }
        }

        bool callback(ifollow_mas_msgs::GenerateRoute::Request& request, ifollow_mas_msgs::GenerateRoute::Response& response)
        {
            response.route = route_test;

            return true;
        }
};

/*************************************************************************************************/
TEST_F(routeFinderTest, computeRouteRobot)
{
    ros::NodeHandle n;
    stoplights_system::RouteFinderRobot route_finder_test;

    Eigen::Vector3d start_point, end_point;
    start_point[0] = 0.5;
    start_point[1] = 1.0;
    start_point[2] = 0.0;
    end_point[0] = 5.5;
    end_point[1] = 1.0;
    end_point[2] = 0.0;
    std::vector<int> robot_groups;
    
    // Test 01: Check when the route finder is NOT properly init (no graph defined)
    EXPECT_EQ(stoplights_system::RouteFinderError::NO_ROUTE_FOUND, route_finder_test.computeRoute(start_point, end_point, robot_groups));

    // Test 02: Check when the route finder is properly init
    route_finder_test.initRouteGraph(n, graph);
    EXPECT_EQ(stoplights_system::RouteFinderError::ROUTE_OK, route_finder_test.computeRoute(start_point, end_point, robot_groups));
}

/*************************************************************************************************/
TEST_F(routeFinderTest, getRouteRobot)
{
    ros::NodeHandle n;
    stoplights_system::RouteFinderRobot route_finder_test;

    Eigen::Vector3d start_point, end_point;
    start_point[0] = 0.5;
    start_point[1] = 1.0;
    start_point[2] = 0.0;
    end_point[0] = 5.5;
    end_point[1] = 1.0;
    end_point[2] = 0.0;
    std::vector<int> robot_groups;
    route_finder_test.initRouteGraph(n, graph);

    // Test 01: Call the function when there is no current route
    EXPECT_TRUE(route_finder_test.getRoute().empty());

    // Test 02: Call the function when there is a current route
    route_finder_test.computeRoute(start_point, end_point, robot_groups);
    EXPECT_EQ(6, route_finder_test.getRoute().size());
}

/*************************************************************************************************/
TEST_F(routeFinderTest, initRouteGraphRobot)
{
    ros::NodeHandle n;
    stoplights_system::RouteFinderRobot route_finder_test;

    route_finder_test.initRouteGraph(n, graph);
    EXPECT_TRUE(true);
}

/*************************************************************************************************/
TEST_F(routeFinderTest, computeRouteServer_01)
{
    ros::AsyncSpinner spinner(1);
    spinner.start();

    ros::NodeHandle n;
    uint16_t robot_id = 0;
    Eigen::Vector3d start_point, end_point;
    stoplights_system::RouteFinderServer route_finder_test = stoplights_system::RouteFinderServer(n, robot_id);
    route_finder_test.initRouteGraph(n, graph);    
    start_point[0] = 0.5;
    start_point[1] = 1.0;
    start_point[2] = 0.0;
    end_point[0] = 5.5;
    end_point[1] = 1.0;
    end_point[2] = 0.0;
    std::vector<int> robot_groups;

    // Test 01: Check when the service is not defined
    EXPECT_EQ(stoplights_system::RouteFinderError::SERVER_ERROR, route_finder_test.computeRoute(start_point, end_point, robot_groups));
}

/*************************************************************************************************/
TEST_F(routeFinderServerTest, computeRouteServer_02)
{
    ros::AsyncSpinner spinner(1);
    spinner.start();

    ros::NodeHandle n;
    uint16_t robot_id = 0;
    Eigen::Vector3d start_point, end_point;
    stoplights_system::RouteFinderServer route_finder_test = stoplights_system::RouteFinderServer(n, robot_id);
    route_finder_test.initRouteGraph(n, graph);    
    start_point[0] = 0.5;
    start_point[1] = 1.0;
    start_point[2] = 0.0;
    end_point[0] = 5.5;
    end_point[1] = 1.0;
    end_point[2] = 0.0;
    std::vector<int> robot_groups;

    // Test 02: Check when the service is defined but there is no possible route
    EXPECT_EQ(stoplights_system::RouteFinderError::NO_ROUTE_FOUND, route_finder_test.computeRoute(start_point, end_point, robot_groups));

    // Test 03: Check when the service is defined and the route is valid
    route_test = {1, 2, 3, 4, 5};
    EXPECT_EQ(stoplights_system::RouteFinderError::ROUTE_OK, route_finder_test.computeRoute(start_point, end_point, robot_groups));
}

/*************************************************************************************************/
TEST_F(routeFinderServerTest, get_route)
{
    ros::AsyncSpinner spinner(1);
    spinner.start();

    ros::NodeHandle n;
    uint16_t robot_id = 0;
    Eigen::Vector3d start_point, end_point;
    stoplights_system::RouteFinderServer route_finder_test = stoplights_system::RouteFinderServer(n, robot_id);
    route_finder_test.initRouteGraph(n, graph);    
    start_point[0] = 0.5;
    start_point[1] = 1.0;
    start_point[2] = 0.0;
    end_point[0] = 5.5;
    end_point[1] = 1.0;
    end_point[2] = 0.0;
    std::vector<int> robot_groups;

    // Test 01: Check when no route has been computed
    EXPECT_TRUE(route_finder_test.getRoute().empty());

    // Test 02: Check when the route has been computed
    route_test = {1, 2, 3, 4, 5};
    EXPECT_EQ(stoplights_system::RouteFinderError::ROUTE_OK, route_finder_test.computeRoute(start_point, end_point, robot_groups));
    EXPECT_EQ(route_finder_test.getRoute().size(), 5);
}

/*************************************************************************************************/
TEST_F(routeFinderTest, initRouteGraphServer)
{
    ros::AsyncSpinner spinner(1);
    spinner.start();

    ros::NodeHandle n;
    uint16_t robot_id = 0;
    stoplights_system::RouteFinderServer route_finder_test = stoplights_system::RouteFinderServer(n, robot_id);
    
    // Test 01: Make sure there is no crash when the function is called
    route_finder_test.initRouteGraph(n, graph);    
    EXPECT_TRUE(true);
}
