#include <gtest/gtest.h>
#include "ros/ros.h"
#include <node/local_behavior_node.h>

/*************************************************************************************************/
TEST(router_traceback, computeSegmentOrientation)
{
    stoplights_system::Segment _seg;
    _seg.start[0]   = 1.0;
    _seg.start[1]   = 2.0;
    _seg.end[0]     = 3.0;
    _seg.end[1]     = 2.0;

    // Compute the orientation
    geometry_msgs::Quaternion out_orientation = stoplights_system::computeSegmentOrientation(_seg);

    EXPECT_NEAR(out_orientation.x, 0.0, 1e-6);
    EXPECT_NEAR(out_orientation.y, 0.0, 1e-6);
    EXPECT_NEAR(out_orientation.z, 0.0, 1e-6);
    EXPECT_NEAR(out_orientation.w, 1.0, 1e-6);
}

/*************************************************************************************************/
TEST(router_traceback, computeSegmentOrientationWithNext)
{
    stoplights_system::Segment _seg_a, _seg_b;
    _seg_a.start[0]   = 1.0;
    _seg_a.start[1]   = 2.0;
    _seg_a.end[0]     = 3.0;
    _seg_a.end[1]     = 2.0;

    _seg_b.start[0]   = 3.0;
    _seg_b.start[1]   = 2.0;
    _seg_b.end[0]     = 5.0;
    _seg_b.end[1]     = 2.0;

    // Test 01: Compute the orientation when the point are connected via the next end point
    geometry_msgs::Quaternion out_orientation = stoplights_system::computeSegmentOrientationWithNext(_seg_a, _seg_b);

    EXPECT_NEAR(out_orientation.x, 0.0, 1e-6);
    EXPECT_NEAR(out_orientation.y, 0.0, 1e-6);
    EXPECT_NEAR(out_orientation.z, 0.0, 1e-6);
    EXPECT_NEAR(out_orientation.w, 1.0, 1e-6);

    // Test 02: Compute the orientation when the point are connected via the next start point
    out_orientation = stoplights_system::computeSegmentOrientationWithNext(_seg_b, _seg_a);

    EXPECT_NEAR(out_orientation.x, 0.0, 1e-6);
    EXPECT_NEAR(out_orientation.y, 0.0, 1e-6);
    EXPECT_NEAR(out_orientation.z, 1.0, 1e-6);
    EXPECT_NEAR(out_orientation.w, 0.0, 1e-6);
}

/*************************************************************************************************/
TEST(functions, applyOffsets)
{
    ifollow_nav_msgs::LocalBehaviorMoveGoal goal_test;

    // Test 01: Check when the offset x is nan
    goal_test.offset_position_x = std::numeric_limits<double>::quiet_NaN();
    goal_test.offset_position_y = 10.0;
    goal_test.target_pose.pose.position.x = 1.0;
    goal_test.target_pose.pose.position.y = 2.0;
    goal_test.target_pose.pose.orientation.w = 1.0;
    stoplights_system::applyOffsets(goal_test);
    EXPECT_NEAR(goal_test.offset_position_x, 0.0, 1e-6);
    EXPECT_NEAR(goal_test.offset_position_y, 0.0, 1e-6);
    EXPECT_NEAR(goal_test.target_pose.pose.position.x, 1.0, 1e-6);
    EXPECT_NEAR(goal_test.target_pose.pose.position.y, 2.0, 1e-6);
        
    // Test 02: Check when the offset y is nan
    goal_test.offset_position_x = 10.0;
    goal_test.offset_position_y = std::numeric_limits<double>::quiet_NaN();
    goal_test.target_pose.pose.position.x = 1.0;
    goal_test.target_pose.pose.position.y = 2.0;
    goal_test.target_pose.pose.orientation.w = 1.0;
    stoplights_system::applyOffsets(goal_test);
    EXPECT_NEAR(goal_test.offset_position_x, 0.0, 1e-6);
    EXPECT_NEAR(goal_test.offset_position_y, 0.0, 1e-6);
    EXPECT_NEAR(goal_test.target_pose.pose.position.x, 1.0, 1e-6);
    EXPECT_NEAR(goal_test.target_pose.pose.position.y, 2.0, 1e-6);

    // Test 02: Check when the offset is not big enough
    goal_test.offset_position_x = 1e-7;
    goal_test.offset_position_y = 1e-7;
    goal_test.target_pose.pose.position.x = 1.0;
    goal_test.target_pose.pose.position.y = 2.0;
    goal_test.target_pose.pose.orientation.w = 1.0;
    stoplights_system::applyOffsets(goal_test);
    EXPECT_NEAR(goal_test.offset_position_x, 0.0, 1e-9);
    EXPECT_NEAR(goal_test.offset_position_y, 0.0, 1e-9);
    EXPECT_NEAR(goal_test.target_pose.pose.position.x, 1.0, 1e-9);
    EXPECT_NEAR(goal_test.target_pose.pose.position.y, 2.0, 1e-9);
    
    // Test 03: Check when the input goal and offset is valid
    goal_test.offset_position_x = 3.0;
    goal_test.offset_position_y = 5.0;
    goal_test.target_pose.pose.position.x = 1.0;
    goal_test.target_pose.pose.position.y = 2.0;
    goal_test.target_pose.pose.orientation.w = 1.0;
    stoplights_system::applyOffsets(goal_test);
    EXPECT_NEAR(goal_test.offset_position_x, 0.0, 1e-6);
    EXPECT_NEAR(goal_test.offset_position_y, 0.0, 1e-6);
    EXPECT_NEAR(goal_test.target_pose.pose.position.x, 4.0, 1e-6);
    EXPECT_NEAR(goal_test.target_pose.pose.position.y, 7.0, 1e-6);
}

/*************************************************************************************************/
TEST(functions, initActionGoal)
{
    geometry_msgs::PoseStamped goal;
    goal.pose.position.x = 1.0;
    goal.pose.position.y = 2.0;
    goal.pose.position.z = 3.0;
    
    ifollow_nav_msgs::LocalBehaviorMoveActionGoal action_goal = stoplights_system::initActionGoal(goal);
    EXPECT_NEAR(action_goal.goal.target_pose.pose.position.x, 1.0, 1e-6);
    EXPECT_NEAR(action_goal.goal.target_pose.pose.position.y, 2.0, 1e-6);
    EXPECT_NEAR(action_goal.goal.target_pose.pose.position.z, 3.0, 1e-6);
    EXPECT_EQ(action_goal.goal.target_pose.header.frame_id, "");
    EXPECT_TRUE(action_goal.goal.use_line_tol);
    EXPECT_TRUE(action_goal.goal.authorize_overwriting);
    EXPECT_NEAR(action_goal.goal.eps_position_x, 0.2, 1e-6);
    EXPECT_NEAR(action_goal.goal.eps_position_y, 0.4, 1e-6);
    EXPECT_NEAR(action_goal.goal.eps_orientation, 0.2, 1e-6);
    EXPECT_EQ(action_goal.goal.area_id, stoplights_system::invalid_id);
    EXPECT_EQ(action_goal.goal.goal_profile, "default");
} 
