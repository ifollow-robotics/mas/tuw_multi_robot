
#include <gtest/gtest.h>
#include "ros/ros.h"
#include <node/local_behavior_node.h>

/*************************************************************************************************/
TEST(reservation, DISABLED_constructor)
{
    ros::NodeHandle n;
    // Test 01: Check there is no issu in the constructor
    stoplights_system::DistanceReservation reservation_test = stoplights_system::DistanceReservation(n);
    EXPECT_EQ(reservation_test.last_trajectory_segment, -1);
}

/*************************************************************************************************/
TEST(reservation, isTimeToRequestServer)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation reservation_test = stoplights_system::DistanceReservation(n);
    
    // Test 01: Check when there is no ongoing penalty
    EXPECT_TRUE(reservation_test.isTimeToRequestServer());

    // Test 02: Check when the last rejected time was too close (below the ongoing penalty) 
    reservation_test.last_rejected_reservation_request = ros::Time::now(); 
    reservation_test.rejected_request_penalty = 10;
    EXPECT_FALSE(reservation_test.isTimeToRequestServer());

    // Test 03: Check when the penalty time was too early
    reservation_test.last_rejected_reservation_request = ros::Time::now(); 
    reservation_test.penalty_request_time = ros::Time::now() - ros::Duration(100.);
    EXPECT_FALSE(reservation_test.isTimeToRequestServer());
}

/*************************************************************************************************/
TEST(reservation, updateRequestPenalty)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation reservation_test = stoplights_system::DistanceReservation(n);

    // Test 01: Check the increment is correct
    reservation_test.updateRequestPenalty();
    EXPECT_EQ(1, reservation_test.rejected_request_penalty);

    // Test 02: Check the increment is saturated at 5 [ 1 Seconde penalty for a reservation request]
    for (int index = 0; index < 7; ++index)
    {
        reservation_test.updateRequestPenalty();
    }
    EXPECT_EQ(5, reservation_test.rejected_request_penalty);
}

/*************************************************************************************************/
TEST(reservation, setRequestPenaltyTime)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation reservation_test = stoplights_system::DistanceReservation(n);

    reservation_test.setRequestPenaltyTime(10.0);
    EXPECT_TRUE((reservation_test.penalty_request_time - (ros::Time::now() + ros::Duration(10.0))) < ros::Duration(0.1));
}

/*************************************************************************************************/
TEST(reservation, resetRequestPenalty)
{
    // Test 01: CHeck if the request penaly is properly reset
    ros::NodeHandle n;
    stoplights_system::DistanceReservation reservation_test = stoplights_system::DistanceReservation(n);

    for (int index = 0; index < 7; ++index)
    {
        reservation_test.updateRequestPenalty();
    }
    
    reservation_test.resetRequestPenalty();
    EXPECT_EQ(0, reservation_test.rejected_request_penalty);
}

/*************************************************************************************************/
TEST(reservation, lastRouteSegment_)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation reservation_test = stoplights_system::DistanceReservation(n);

    // Test 01: Set the last route segment value
    std::vector<int32_t> new_route = {1, 2, 3, 4};
    geometry_msgs::Point goal_point;
    reservation_test.initRoute(new_route, goal_point);
    ++reservation_test.current_segment_it;

    reservation_test.setLastRouteSegment_();
    EXPECT_EQ(2, reservation_test.last_trajectory_segment);

    // Test 02: Check that the function getLastRouteSegment is valid
    EXPECT_EQ(reservation_test.getLastRouteSegment_(), reservation_test.last_trajectory_segment);
}

/*************************************************************************************************/
TEST(reservation, isStoplightsClientValid)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation reservation_test = stoplights_system::DistanceReservation(n);

    // Test 01: Check if the function is valid (the variable is set at the constructor)
    EXPECT_FALSE(reservation_test.isStoplightsClientValid());

    reservation_test.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    // Test 02: Check if the function is valid (the variable is set at the constructor)
    EXPECT_TRUE(reservation_test.isStoplightsClientValid());
}

/*************************************************************************************************/
TEST(reservation, advertiseNewRouteToStoplights)
{
    ros::NodeHandle n;
    std::vector<int32_t> new_route = {1, 2, 3, 4};
    stoplights_system::DistanceReservation reservation_test = stoplights_system::DistanceReservation(n);
    reservation_test.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    reservation_test.advertiseNewRouteToStoplights(new_route);
}

/*************************************************************************************************/
TEST(reservation, requestSegmentAuthorization)
{
    ros::NodeHandle n;
    uint32_t current_segment_id = 0;
    uint32_t next_segment_id = 1;
    stoplights_system::DistanceReservation reservation_test = stoplights_system::DistanceReservation(n);
    reservation_test.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();
    ifollow_mas_msgs::StoplightsSysResponse response_test = reservation_test.requestSegmentAuthorization(current_segment_id, next_segment_id);
    EXPECT_TRUE(response_test.ack);
}

/*************************************************************************************************/
TEST(reservation, initSegments)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation reservation_test = stoplights_system::DistanceReservation(n);

    // Test 01: Make sure the segments variable is properly set
    std::map<int32_t, stoplights_system::Segment> segments_tests;
    segments_tests[1] = stoplights_system::Segment();
    reservation_test.initSegments(segments_tests);
    EXPECT_FALSE(reservation_test.segments_.empty());
}

/*************************************************************************************************/
TEST(reservation, updateLastSegmentFromServerRequest)
{
    ros::NodeHandle n;
    stoplights_system::DistanceReservation reservation_test = stoplights_system::DistanceReservation(n);
    std::vector<int32_t> new_route = {1, 2, 3, 4};
    uint16_t last_segment_id = 2;
    uint8_t penalty_time = 10;

    // Test 01: Check when the robot has no current route
    reservation_test.reserved_horizon = 10.;
    EXPECT_TRUE(reservation_test.updateLastSegmentFromServerRequest(last_segment_id, penalty_time));
    EXPECT_NEAR(reservation_test.reserved_horizon, 10.0, 1e-4);

    // Test 02: Check when the input segment is not on the list
    last_segment_id = 10;
    geometry_msgs::Point goal_point;
    reservation_test.initRoute(new_route, goal_point);
    reservation_test.reserved_horizon = 10.;
    EXPECT_TRUE(reservation_test.updateLastSegmentFromServerRequest(last_segment_id, penalty_time));
    EXPECT_NEAR(reservation_test.reserved_horizon, 10.0, 1e-4);

    // Test 03: Check when the input segment is not below the current segment
    last_segment_id = 2;
    EXPECT_TRUE(reservation_test.updateLastSegmentFromServerRequest(last_segment_id, penalty_time));
    EXPECT_NEAR(reservation_test.reserved_horizon, stoplights_system::min_horizon_distance_constant, 1e-4);

    // Test 04: Check when the input segment is below the current segment
    reservation_test.reserved_horizon = 10.;
    reservation_test.current_segment_it = reservation_test.route.begin() + 3;
    EXPECT_FALSE(reservation_test.updateLastSegmentFromServerRequest(last_segment_id, penalty_time));
    EXPECT_NEAR(reservation_test.reserved_horizon, 10., 1e-6);
}

TEST(LocalBehaviorConstructor, Init)
{
    // TODO AGO Check when the param stoplights_reservation_mode is not defined
    // TODO AGO Check when the param stoplights_reservation_mode is defined
    // TODO AGO Check when Segment == stoplights_reservation_mode
    // TODO AGO Check when Distance == stoplights_reservation_mode
    // TODO AGO Check when INVALID == stoplights_reservation_mode
}