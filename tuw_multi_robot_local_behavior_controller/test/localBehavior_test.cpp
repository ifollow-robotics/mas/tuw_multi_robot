#include <gtest/gtest.h>
#include "ros/ros.h"
#include <node/local_behavior_node.h>
#include <std_srvs/SetBool.h>

// typedef actionlib::SimpleActionClient<ifollow_nav_msgs::LocalBehaviorMoveAction> NavClient;
// std::unique_ptr<NavClient> nav_action_client_ = std::unique_ptr<NavClient> (new NavClient("single_robot_router", true));;

// typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;
// typedef std::shared_ptr<MoveBaseClient> MoveBaseClientPtr;

/*************************************************************************************************/
class TestClient02 : public ::testing::Test
{
    public:
        ros::ServiceServer  service_in;
        bool receivedMessage;
        bool return_value;
        ros::NodeHandle n;  

        TestClient02() : 
        receivedMessage(false) 
        ,return_value(false)
        {
            service_in = n.advertiseService("/test_client", &TestClient02::callback, this);
        }

        bool callback(std_srvs::SetBool::Request& request, std_srvs::SetBool::Response& response)
        {
            if (true == request.data)
            {
                receivedMessage = request.data;
            }
            else
            {
                throw std::runtime_error("Test failed.");
            }

            return return_value;
        }
};

/*************************************************************************************************/
TEST_F(TestClient02, callSrv)
{
    ros::AsyncSpinner spinner(1);
    spinner.start();
    
    ros::ServiceClient  test_client_fake    = n.serviceClient<std_srvs::SetBool>("/test_client_fake");
    ros::ServiceClient  test_client         = n.serviceClient<std_srvs::SetBool>("/test_client");

    double waiting_time = 0.01;
    std_srvs::SetBool request;
    request.request.data = true;

    // Test 01: Check when the input service doesn't exist
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();
    EXPECT_TRUE(test_client.exists());
    request.request.data = true;

    EXPECT_FALSE(stoplights_system::callSrv(test_client_fake, request, ros::Duration(0.1)));

    // Test 02: Check when the input service is valid
    return_value = true;
    EXPECT_TRUE(stoplights_system::callSrv(test_client, request, ros::Duration(0.1)));
    EXPECT_TRUE(receivedMessage);

    // Test 03: Check when the input service is valid
    return_value = false;
    EXPECT_FALSE(stoplights_system::callSrv(test_client, request, ros::Duration(0.0)));
    EXPECT_TRUE(receivedMessage);

    // TODO AGO: Need to go inside if (timeout != ros::Duration(0) && !srv_client.waitForExistence(timeout))
    // TODO AGO: Add a test to check the throw
}

/*************************************************************************************************/
TEST(localBehavior, closestPointOnLineSegment2d)
{
    Eigen::Vector2d start_pos_2d    = Eigen::Vector2d(2.0, 3.0);
    Eigen::Vector2d start_point     = Eigen::Vector2d(1.0, 2.0);
    Eigen::Vector2d end_point       = Eigen::Vector2d(5.0, 2.0);

    // Test 01: Check when the input position is in the middle of the line
    Eigen::Vector2d closest_point = stoplights_system::closest_point_on_line_segment_2d(start_pos_2d, start_point, end_point);
    EXPECT_TRUE(2.0 == closest_point.x());
    EXPECT_TRUE(2.0 == closest_point.y());

    // Test 02: Check when the input line size is equal to 0
    end_point = start_point;
    closest_point = stoplights_system::closest_point_on_line_segment_2d(start_pos_2d, start_point, end_point);
    EXPECT_TRUE(1.0 == closest_point.x());
    EXPECT_TRUE(2.0 == closest_point.y());

    // Test 03: Check when the input point is before the line's start
    end_point       = Eigen::Vector2d(5.0, 2.0);
    start_pos_2d    = Eigen::Vector2d(0.0, 2.0);

    closest_point = stoplights_system::closest_point_on_line_segment_2d(start_pos_2d, start_point, end_point);
    EXPECT_TRUE(1.0 == closest_point.x());
    EXPECT_TRUE(2.0 == closest_point.y());

    // Test 04: Check when the input point is after the line's end
    start_pos_2d    = Eigen::Vector2d(6.0, 2.0);

    closest_point = stoplights_system::closest_point_on_line_segment_2d(start_pos_2d, start_point, end_point);
    EXPECT_TRUE(5.0 == closest_point.x());
    EXPECT_TRUE(2.0 == closest_point.y());
}

/*************************************************************************************************/
TEST(localBehavior, stringReq)
{
    ifollow_mas_msgs::MRAreaMngr::Request request;

    // Test 01: Check when the request is empty
    std::string string_request = stoplights_system::stringReq(request);
    EXPECT_TRUE(true);

    // Test 02: CHeck when there is an area list
    request.area_id     = 10;
    request.slot_id     = 1;
    request.vehicle_id  = 2;
    request.occupied_slots.push_back(1);
    request.occupied_slots.push_back(2);
    request.occupied_slots.push_back(3);
    request.occupied_slots_vehicles.push_back(1);
    request.occupied_slots_vehicles.push_back(2);
    request.occupied_slots_vehicles.push_back(3);
    string_request = stoplights_system::stringReq(request);

    EXPECT_TRUE(true);
}

/*************************************************************************************************/
TEST(localBehavior, updateGoalStatus)
{
    double waiting_time = 0.01;
    ros::NodeHandle n;
    stoplights_system::LocalBehaviorNode local_behavior_node(n);
    ifollow_nav_msgs::GoalExtendedStatus goal_status;

    // Test 01: Check the pending value
    local_behavior_node.updateGoalStatus(ifollow_nav_msgs::GoalExtendedStatus::PENDING);
    goal_status = local_behavior_node.getGoalStatus();
    EXPECT_EQ(goal_status.value, ifollow_nav_msgs::GoalExtendedStatus::PENDING);
    EXPECT_EQ(goal_status.text, "Pending - local behavior hasn't update status yet");

    // Test 02: Check the succeded value
    local_behavior_node.updateGoalStatus(ifollow_nav_msgs::GoalExtendedStatus::SUCCEEDED);
    goal_status = local_behavior_node.getGoalStatus();
    EXPECT_EQ(goal_status.value, ifollow_nav_msgs::GoalExtendedStatus::SUCCEEDED);
    EXPECT_EQ(goal_status.text, "Robot has reached the goal");

    // Test 03: Check the stalled value
    local_behavior_node.updateGoalStatus(ifollow_nav_msgs::GoalExtendedStatus::STALLED);
    goal_status = local_behavior_node.getGoalStatus();
    EXPECT_EQ(goal_status.value, ifollow_nav_msgs::GoalExtendedStatus::STALLED);
    EXPECT_EQ(goal_status.text, "Robot is stalled waiting for segment access authorization");

    // Test 04: Check the active value
    local_behavior_node.updateGoalStatus(ifollow_nav_msgs::GoalExtendedStatus::ACTIVE);
    goal_status = local_behavior_node.getGoalStatus();
    EXPECT_EQ(goal_status.value, ifollow_nav_msgs::GoalExtendedStatus::ACTIVE);
    EXPECT_EQ(goal_status.text, "Robot is moving");

    // Test 05: Check the aborted value
    local_behavior_node.updateGoalStatus(ifollow_nav_msgs::GoalExtendedStatus::ABORTED);
    goal_status = local_behavior_node.getGoalStatus();
    EXPECT_EQ(goal_status.value, ifollow_nav_msgs::GoalExtendedStatus::ABORTED);
    EXPECT_EQ(goal_status.text, "Aborting on goal");
}

/*************************************************************************************************/
TEST(localBehavior, initialization_01)
{
    ros::NodeHandle n;
    n.setParam("route_finder_centralized", true);

    stoplights_system::LocalBehaviorNode local_behavior_node(n);

    n.deleteParam("route_finder_centralized");
}

/*************************************************************************************************/
TEST(localBehavior, initialization_02)
{
    ros::NodeHandle n;
    n.setParam("route_finder_centralized", true);

    stoplights_system::LocalBehaviorNode local_behavior_node(n);

    // Test 01: Check when the robot is in standalone and route is not computed by the server
    local_behavior_node.updateRouteFinderMode();

    // TODO AGO Test 02: Check when the robot is not in standalone and route is not computed by the server

    n.deleteParam("route_finder_centralized");
}

/*************************************************************************************************/
TEST(localBehavior, update_01)
{
    ros::NodeHandle n;
    double waiting_time = 0.01;
    stoplights_system::LocalBehaviorNode local_behavior_node(n);

    // Test 01: Call the update function when no graph is defined
    local_behavior_node.update();

    // Test 02: Call the update function when there is a graph defined but its empty
    ros::Publisher segment_pub = n.advertise<ifollow_mas_msgs::Graph>("nav_governor/augmented_graph", 1, this);
    ifollow_mas_msgs::Graph graph;
    ifollow_mas_msgs::Vertex vertex;
    ros::Publisher graph_   = n.advertise<tuw_multi_robot_msgs::Graph>("segments", 1); 
    tuw_multi_robot_msgs::Graph segments;
    segments.resolution = 0.05;

    int nb_segments = 10;

    // Create the graph
    for (auto i_segment = 0; i_segment < nb_segments; ++i_segment)
    {
        tuw_multi_robot_msgs::Vertex v; 
        v.id        = i_segment;
        v.valid     = true;
        v.weight    = 1;
        v.width     = 1;
        v.path.resize(2);

        v.path[0].x = (i_segment) * 1.0 / segments.resolution;
        v.path[0].y = 1.0 / segments.resolution;
        v.path[1].x = (i_segment + 1) * 1.0 / segments.resolution;
        v.path[1].y = 1.0 / segments.resolution;
        
        if (i_segment > 0)
        {
            v.predecessors.push_back(i_segment - 1);
        }

        if (i_segment < (nb_segments - 1))
        {
            v.successors.push_back(i_segment + 1);
        }

        segments.vertices.push_back(v);
    }

    vertex.path.resize(2);
    vertex.area_id = -1;
    vertex.id = 0;
    vertex.path[0].x = 1.0;
    vertex.path[0].y = 2.0;
    vertex.path[1].x = 3.0;
    vertex.path[1].y = 4.0;
    graph.vertices.push_back(vertex);
    vertex.id = 1;
    vertex.path[0].x = 3.0;
    vertex.path[0].y = 2.0;
    vertex.path[1].x = 5.0;
    vertex.path[1].y = 4.0;
    graph.vertices.push_back(vertex);
    vertex.id = 2;
    vertex.path[0].x = 5.0;
    vertex.path[0].y = 2.0;
    vertex.path[1].x = 7.0;
    vertex.path[1].y = 4.0;
    graph.vertices.push_back(vertex);
    vertex.id = 3;
    vertex.path[0].x = 7.0;
    vertex.path[0].y = 2.0;
    vertex.path[1].x = 9.0;
    vertex.path[1].y = 4.0;
    graph.vertices.push_back(vertex);
    
    segment_pub.publish(graph);
    graph_.publish(segments);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();
    local_behavior_node.update();
    EXPECT_TRUE(true);

    // Test 02: Call the update function the robot has a route
    ros::Publisher sub_voronoi_graph_ = n.advertise<nav_msgs::Odometry>("global/odom", 1, true);
    nav_msgs::Odometry odom;
    odom.pose.pose.position.x = 1.5;
    odom.pose.pose.position.y = 1.0;
    odom.header.frame_id = "world";
    sub_voronoi_graph_.publish(odom);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();

    std::vector<int32_t> route = {1, 2, 3, 4};
    local_behavior_node.setRoute(route);
    local_behavior_node.update();
}

/*************************************************************************************************/
TEST(localBehavior, computeViaPointsFromRoute_)
{
    ros::NodeHandle n;
    double waiting_time = 0.01;
    stoplights_system::LocalBehaviorNode local_behavior_node(n);

    // Test 02: Call the update function when there is a graph defined but its empty
    ros::Publisher segment_pub = n.advertise<ifollow_mas_msgs::Graph>("nav_governor/augmented_graph", 1, this);
    ifollow_mas_msgs::Graph graph;
    ifollow_mas_msgs::Vertex vertex;

    vertex.path.resize(2);
    vertex.area_id = -1;
    vertex.id = 0;
    vertex.path[0].x = 1.0;
    vertex.path[0].y = 2.0;
    vertex.path[1].x = 3.0;
    vertex.path[1].y = 4.0;
    graph.vertices.push_back(vertex);
    vertex.id = 1;
    vertex.path[0].x = 3.0;
    vertex.path[0].y = 2.0;
    vertex.path[1].x = 5.0;
    vertex.path[1].y = 4.0;
    graph.vertices.push_back(vertex);
    vertex.id = 2;
    vertex.path[0].x = 5.0;
    vertex.path[0].y = 2.0;
    vertex.path[1].x = 7.0;
    vertex.path[1].y = 4.0;
    graph.vertices.push_back(vertex);
    vertex.id = 3;
    vertex.path[0].x = 7.0;
    vertex.path[0].y = 2.0;
    vertex.path[1].x = 9.0;
    vertex.path[1].y = 4.0;
    graph.vertices.push_back(vertex);
    
    segment_pub.publish(graph);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();

    // Test 01: Check when the robot doesn't have a new route
    local_behavior_node.setGotNewRoute();
    local_behavior_node.computeViaPointsFromRoute_();

    // Test 02: Check when the robot has no current route
    local_behavior_node.computeViaPointsFromRoute_();

    // Test 03: Check when the robot has a route of size 1
    std::vector<int32_t> route = {1};
    local_behavior_node.setRoute(route);
    local_behavior_node.computeViaPointsFromRoute_();
    geometry_msgs::PoseArray via_points = local_behavior_node.getViaPoints();
    EXPECT_EQ(via_points.poses.size(), 2);

    // Test 04: Check when the robot has a route of size 3
    route = {1, 2, 3};
    local_behavior_node.setRoute(route);
    local_behavior_node.computeViaPointsFromRoute_();
    via_points = local_behavior_node.getViaPoints();
    EXPECT_EQ(via_points.poses.size(), 3);
}

/*************************************************************************************************/
TEST(localBehavior, resetRoute)
{
    ros::NodeHandle n;
    stoplights_system::LocalBehaviorNode local_behavior_node(n);

    std::vector<int32_t> route = {1};
    local_behavior_node.setRoute(route);
    local_behavior_node.computeViaPointsFromRoute_();

    // Test 01: call the reset function and make sure the route is properly reset
    local_behavior_node.reset();
    EXPECT_TRUE(local_behavior_node.getRoute().empty());
    EXPECT_TRUE(local_behavior_node.getViaPoints().poses.empty());
}

/*************************************************************************************************/
TEST(localBehavior, planRoute_)
{
    ros::NodeHandle n;
    double waiting_time = 0.01;
    stoplights_system::LocalBehaviorNode local_behavior_node(n);
    ros::Publisher segment_pub = n.advertise<ifollow_mas_msgs::Graph>("nav_governor/augmented_graph", 1, this);
    ros::Publisher graph_   = n.advertise<tuw_multi_robot_msgs::Graph>("segments", 1); 
    tuw_multi_robot_msgs::Graph graph;
    graph.resolution = 0.05;

    int nb_segments = 10;

    // Create the graph
    for (auto i_segment = 0; i_segment < nb_segments; ++i_segment)
    {
        tuw_multi_robot_msgs::Vertex v; 
        v.id        = i_segment;
        v.valid     = true;
        v.weight    = 1;
        v.width     = 1;
        v.path.resize(2);

        v.path[0].x = (i_segment) * 1.0 / graph.resolution;
        v.path[0].y = 1.0 / graph.resolution;
        v.path[1].x = (i_segment + 1) * 1.0 / graph.resolution;
        v.path[1].y = 1.0 / graph.resolution;
        
        if (i_segment > 0)
        {
            v.predecessors.push_back(i_segment - 1);
        }

        if (i_segment < (nb_segments - 1))
        {
            v.successors.push_back(i_segment + 1);
        }

        graph.vertices.push_back(v);
    }

    ifollow_mas_msgs::Graph augmented_graph;
    ifollow_mas_msgs::Vertex vertex;

    vertex.path.resize(2);
    vertex.area_id = -1;
    vertex.id = 0;
    vertex.path[0].x = 1.0;
    vertex.path[0].y = 2.0;
    vertex.path[1].x = 3.0;
    vertex.path[1].y = 4.0;
    augmented_graph.vertices.push_back(vertex);
    vertex.id = 1;
    vertex.path[0].x = 3.0;
    vertex.path[0].y = 2.0;
    vertex.path[1].x = 5.0;
    vertex.path[1].y = 4.0;
    augmented_graph.vertices.push_back(vertex);
    vertex.id = 2;
    vertex.path[0].x = 5.0;
    vertex.path[0].y = 2.0;
    vertex.path[1].x = 7.0;
    vertex.path[1].y = 4.0;
    augmented_graph.vertices.push_back(vertex);
    vertex.id = 3;
    vertex.path[0].x = 7.0;
    vertex.path[0].y = 2.0;
    vertex.path[1].x = 9.0;
    vertex.path[1].y = 4.0;
    augmented_graph.vertices.push_back(vertex);
    
    // Test 01: Compute the route when there is no graph defined
    ifollow_nav_msgs::LocalBehaviorMoveGoal goal;
    goal.target_pose.pose.position.x = 6.5;
    goal.target_pose.pose.position.y = 2.0;
    local_behavior_node.planRoute(goal);
    EXPECT_EQ(local_behavior_node.getRoute().size(), 0);

    segment_pub.publish(augmented_graph);
    graph_.publish(graph);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();

    ros::Publisher sub_voronoi_graph_ = n.advertise<nav_msgs::Odometry>("global/odom", 1, true);
    nav_msgs::Odometry odom;
    odom.pose.pose.position.x = 1.5;
    odom.pose.pose.position.y = 1.0;
    odom.pose.pose.position.z = 0.0;
    odom.header.frame_id = "world";
    sub_voronoi_graph_.publish(odom);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();

    // TODO AGO Test 02: Compute the route when there is a graph defined
    local_behavior_node.planRoute(goal);
}


