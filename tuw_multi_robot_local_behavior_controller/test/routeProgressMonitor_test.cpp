#include <gtest/gtest.h>
#include "ros/ros.h"
#include <node/local_behavior_node.h>

/*************************************************************************************************/
TEST(RouteProgressMonitor, Constructor)
{
    tuw::RouteProgressMonitor route_progress_monitor;
    EXPECT_EQ(0, route_progress_monitor.getProgress());
}

/*************************************************************************************************/
TEST(RouteProgressMonitor, init)
{
    std::map<int32_t, stoplights_system::Segment> segments_test;
    stoplights_system::Segment seg;
    tuw::RouteProgressMonitor route_progress_monitor;

    seg.start << 1.0, 2.0;
    seg.end << 3.0, 4.0;
    segments_test[1] = seg;
    seg.start << 3.0, 2.0;
    seg.end << 5.0, 6.0;
    segments_test[2] = seg;
    seg.start << 5.0, 2.0;
    seg.end << 7.0, 8.0;
    segments_test[3] = seg;
    seg.start << 7.0, 2.0;
    seg.end << 9.0, 10.0;
    segments_test[4] = seg;
    std::vector<int32_t> route = {1, 2, 3, 4};

    route_progress_monitor.init(segments_test, route);
    EXPECT_EQ(0, route_progress_monitor.getProgress());
}

/*************************************************************************************************/
TEST(RouteProgressMonitor, updateProgress)
{
    std::map<int32_t, stoplights_system::Segment> segments_test;
    tuw::RouteProgressMonitor route_progress_monitor;
    Eigen::Vector3d position;
    stoplights_system::Segment seg;

    // Test 01: Check the function when there is no current route defined
    route_progress_monitor.updateProgress(position);
    EXPECT_EQ(0, route_progress_monitor.getProgress());

    // Test 02: Check when there is a route and the input position is valid  
    seg.start << 1.0, 2.0;
    seg.end << 3.0, 4.0;
    segments_test[1] = seg;
    seg.start << 3.0, 2.0;
    seg.end << 5.0, 6.0;
    segments_test[2] = seg;
    seg.start << 5.0, 2.0;
    seg.end << 7.0, 8.0;
    segments_test[3] = seg;
    seg.start << 7.0, 2.0;
    seg.end << 9.0, 10.0;
    segments_test[4] = seg;
    std::vector<int32_t> route = {1, 2, 3, 4};

    route_progress_monitor.init(segments_test, route);

    position[0] = 4.0;
    position[1] = 2.0;
    route_progress_monitor.updateProgress(position);
    EXPECT_EQ(1, route_progress_monitor.getProgress());

    // Test 02: Check when there is only one segment in the route
    route.clear();
    route.push_back(1);
    route_progress_monitor.init(segments_test, route);
    route_progress_monitor.updateProgress(position);
    EXPECT_EQ(0, route_progress_monitor.getProgress());
}

