#include <gtest/gtest.h>
#include "ros/ros.h"
#include <node/local_behavior_node.h>

/*************************************************************************************************/
class TestClient : public ::testing::Test
{
    public:

        ros::ServiceServer  service_in;
        bool receivedMessage;
        bool returned_value;
        ros::NodeHandle n;  
        double distance_to_stop;

        TestClient() : 
            receivedMessage(false),
            returned_value(true)
            {}

        bool callback(ifollow_mas_msgs::StoplightsSys::Request& request, ifollow_mas_msgs::StoplightsSys::Response& response)
        {
            response.ack = receivedMessage;

            return returned_value;
        }

        bool callbackNewRoute(ifollow_mas_msgs::SimplifiedRoute::Request& request, ifollow_mas_msgs::SimplifiedRoute::Response& response)
        {
            return receivedMessage;
        }

        bool callbackHorizon(ifollow_mas_msgs::StoplightsUpdateHorizon::Request& request, ifollow_mas_msgs::StoplightsUpdateHorizon::Response& response)
        {
            response.distance_to_stop = distance_to_stop;
            response.ack = receivedMessage;
            return receivedMessage;
        }

        void initFakeServiceRequestAuthorized()
        {
            service_in = n.advertiseService("stoplights_sys_in", &TestClient::callback, this);
        }

        void initFakeServiceRequestNewRoute()
        {
            service_in = n.advertiseService("stoplights_new_route", &TestClient::callbackNewRoute, this);
        }

        void initFakeServiceRequestHorizon()
        {
            service_in = n.advertiseService("stoplights_reserve_horizon", &TestClient::callbackHorizon, this);
        }
};

/*************************************************************************************************/
TEST(StopLightsClientService, constructor)
{
    ros::NodeHandle n;
    std::string robot_id;

    stoplights_system::StopLightsClientService stoplights_client(n, robot_id);
    EXPECT_TRUE(true);   
}

/*************************************************************************************************/
TEST_F(TestClient, authorization)
{
    ros::AsyncSpinner spinner(1);
    spinner.start();

    std::vector<uint16_t> alternative_seq;
    std::string robot_id="robot_id";
    uint32_t actual_segment_id = 1;
    uint32_t restricted_segment_id = 2;
    bool is_route_required = false;

    stoplights_system::StopLightsClientService stoplights_client(n, robot_id);

    // Test 01: Check when the service is not defined
    EXPECT_FALSE(stoplights_client.requestAuthorization(actual_segment_id, restricted_segment_id, alternative_seq, is_route_required).ack);

    // Test 02: Check when the service return ack==false
    initFakeServiceRequestAuthorized();
    receivedMessage = false;
    EXPECT_FALSE(stoplights_client.requestAuthorization(actual_segment_id, restricted_segment_id, alternative_seq, is_route_required).ack);

    // Test 03: Check when the service return ack==true
    receivedMessage = true;
    EXPECT_TRUE(stoplights_client.requestAuthorization(actual_segment_id, restricted_segment_id, alternative_seq, is_route_required).ack);

    // Test 04: Check when the service return false
    returned_value = false;
    EXPECT_FALSE(stoplights_client.requestAuthorization(actual_segment_id, restricted_segment_id, alternative_seq, is_route_required).ack);
}

/*************************************************************************************************/
TEST_F(TestClient, newRoute)
{
    ros::AsyncSpinner spinner(1);
    spinner.start();

    std::string robot_id="robot_id";
    uint32_t actual_segment_id = 1;
    uint32_t restricted_segment_id = 2;
    std::vector<int32_t> new_route = {1, 2, 3};

    stoplights_system::StopLightsClientService stoplights_client(n, robot_id);

    // Test 01: Check when the service doesn't exist
    EXPECT_FALSE(stoplights_client.advertiseNewRoute(new_route));

    // Test 02: Check when the service return false
    initFakeServiceRequestNewRoute();
    receivedMessage = false;
    EXPECT_FALSE(stoplights_client.advertiseNewRoute(new_route));

    // Test 03: Check when the service return true
    receivedMessage = true;
    EXPECT_TRUE(stoplights_client.advertiseNewRoute(new_route));
}

/*************************************************************************************************/
TEST_F(TestClient, horizon)
{
    ros::AsyncSpinner spinner(1);
    spinner.start();

    std::string robot_id="robot_id";
    uint32_t actual_segment_id = 1;
    uint32_t restricted_segment_id = 2;
    double distance_to_stop_input;
    std::vector<uint16_t> alternative_seq;
    bool rerouting_requiered;
    stoplights_system::StopLightsClientService stoplights_client(n, robot_id);

    // Test 01: Check when the service doesn't exist
    EXPECT_FALSE(stoplights_client.requestHorizon(distance_to_stop_input, alternative_seq, rerouting_requiered).ack);

    // Test 02: Check when the service return false
    initFakeServiceRequestHorizon();
    distance_to_stop = 1.0;
    receivedMessage = false;
    EXPECT_FALSE(stoplights_client.requestHorizon(distance_to_stop_input, alternative_seq, rerouting_requiered).ack);
    EXPECT_TRUE(0.0 == distance_to_stop_input);

    // Test 03: Check when the service return true
    receivedMessage = true;
    EXPECT_TRUE(stoplights_client.requestHorizon(distance_to_stop_input, alternative_seq, rerouting_requiered).ack);
    EXPECT_TRUE(1.0 == distance_to_stop_input);
}

/*************************************************************************************************/
TEST_F(TestClient, resetRoute)
{
    ros::AsyncSpinner spinner(1);
    spinner.start();

    std::string robot_id="robot_id";
    uint32_t actual_segment_id = 1;
    uint32_t restricted_segment_id = 2;

    stoplights_system::StopLightsClientService stoplights_client(n, robot_id);

    // Test 01: Check when the service doesn't exist
    EXPECT_FALSE(stoplights_client.resetRoute());

    // Test 02: Check when the service return false
    initFakeServiceRequestNewRoute();
    receivedMessage = false;
    EXPECT_FALSE(stoplights_client.resetRoute());

    // Test 03: Check when the service return true
    receivedMessage = true;
    EXPECT_TRUE(stoplights_client.resetRoute());
}

/*************************************************************************************************/
TEST(StopLightsClientSingleRobot, authorization)
{
    uint32_t actual_segment_id, restricted_segment_id;
    
    std::vector<uint16_t> alternative_seq;
    bool rerouting_requiered;
    stoplights_system::StopLightsClientSingleRobot stoplights_single_client_test;
    
    // Test 01: Check the authorization request
    EXPECT_TRUE((stoplights_single_client_test.requestAuthorization(actual_segment_id, restricted_segment_id, alternative_seq, rerouting_requiered)).ack);
}

/*************************************************************************************************/
TEST(StopLightsClientSingleRobot, horizon)
{
    double distance_to_stop = 3.0;
    std::vector<uint16_t> alternative_seq;
    bool rerouting_requiered;
    stoplights_system::StopLightsClientSingleRobot stoplights_single_client_test;
    
    // Test 01: Check the horizon reservation request
    EXPECT_TRUE((stoplights_single_client_test.requestHorizon(distance_to_stop, alternative_seq, rerouting_requiered)).ack);
    EXPECT_TRUE(3.0 == distance_to_stop);
}

/*************************************************************************************************/
TEST(StopLightsClientSingleRobot, advertiseNewRoute)
{
    stoplights_system::StopLightsClientSingleRobot stoplights_single_client_test;
    std::vector<int32_t> new_route;

    // Test 01: Check the new route function
    EXPECT_TRUE(stoplights_single_client_test.advertiseNewRoute(new_route));
}

/*************************************************************************************************/
TEST(StopLightsClientSingleRobot, resetRoute)
{
    stoplights_system::StopLightsClientSingleRobot stoplights_single_client_test;
    
    // Test 01: Check the new route function
    EXPECT_TRUE(stoplights_single_client_test.resetRoute());
}
