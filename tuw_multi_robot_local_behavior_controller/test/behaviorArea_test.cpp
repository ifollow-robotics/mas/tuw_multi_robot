#include <gtest/gtest.h>
#include "ros/ros.h"
#include <node/local_behavior_node.h>

/*************************************************************************************************/
class AreaSlotsTest01 : public ::testing::Test
{
    public:
        ros::ServiceServer  service_in;
        bool receivedMessage;
        std::vector<uint16_t> response_occupied_slots;
        std::vector<uint16_t> response_occupied_slots_vehicles;
        bool return_value = false;
        ros::NodeHandle n;  

        AreaSlotsTest01() : receivedMessage(false) 
        {
            service_in = n.advertiseService("mr_area_mngr", &AreaSlotsTest01::callback, this);
        }

        bool callback(ifollow_mas_msgs::MRAreaMngr::Request& request, ifollow_mas_msgs::MRAreaMngr::Response& response)
        {
            response.resp = receivedMessage;
            response.occupied_slots = response_occupied_slots;
            response.occupied_slots_vehicles = response_occupied_slots_vehicles;

            return return_value;
        }
};

/*************************************************************************************************/
TEST_F(AreaSlotsTest01, tryToGoToMrArea)
{
    double waiting_time = 0.01;
    ros::NodeHandle n;
    ros::AsyncSpinner spinner(1);
    spinner.start();

    stoplights_system::LocalBehaviorNode local_behavior_node(n);
    ros::Publisher areas_pub = n.advertise<ifollow_mas_msgs::AreaArray>("nav_governor/graph_areas", 1, this);
    ifollow_mas_msgs::AreaArray area_test;
    ifollow_mas_msgs::Area area_msgs;
    area_msgs.type.push_back(ifollow_mas_msgs::Area::AREA_TYPE_TRAFFIC_PARKING);
    area_msgs.id = 1;
    area_msgs.points.resize(4);
    area_msgs.points[0].x = 1.0;
    area_msgs.points[0].y = 1.0;
    area_msgs.points[1].x = 1.0;
    area_msgs.points[1].y = 5.0;
    area_msgs.points[2].x = 5.0;
    area_msgs.points[2].y = 5.0;
    area_msgs.points[3].x = 5.0;
    area_msgs.points[3].y = 1.0;
    area_test.areas.push_back(area_msgs);

    areas_pub.publish(area_test);

    ros::Duration(waiting_time).sleep();
    ros::spinOnce();
    int32_t area_id = 1;

    // Test 01: Try to reserve the area when when the area id is not valid
    local_behavior_node.tryToGoToMrArea(100);
    
    // Test 02: Try to reserve the area when the first slot is already reserved by the robot
    receivedMessage = true;
    return_value    = true;
    local_behavior_node.tryToGoToMrArea(area_id);

    // Test 03: Try to reserve the area when the first slots is occupied by another robot but second is available
    local_behavior_node.tryToGoToMrArea(area_id);
}
