#include <gtest/gtest.h>
#include "ros/ros.h"
#include <node/local_behavior_node.h>

/*************************************************************************************************/
TEST(node, distanceToSegment)
{
    stoplights_system::Segment segment_test;
    Eigen::Vector3d position;

    // Test 01: Check the function distanceToSegment when the segment length is equal to 0
    // Make sure the distance return is valid
    segment_test.start  = Eigen::Vector2d(1.0, 1.0);
    segment_test.end    = Eigen::Vector2d(1.0, 1.0);

    position = Eigen::Vector3d(2.0, 1.0, 3.14);
    double distance = stoplights_system::distanceToSegment(segment_test, position);
    EXPECT_TRUE(1.0 == distance);

    // Test 02: Check the function distanceToSegment when the position is on the start point of the segment
    // Make sure the distance return is valid
    segment_test.end    = Eigen::Vector2d(5.0, 1.0);
    position = Eigen::Vector3d(1.0, 1.0, 3.14);
    distance = stoplights_system::distanceToSegment(segment_test, position);
    EXPECT_TRUE(0.0 == distance);

    // Test 03: Check the function distanceToSegment when the position is on the end point of the segment
    // Make sure the distance return is valid
    position = Eigen::Vector3d(5.0, 1.0, 3.14);
    distance = stoplights_system::distanceToSegment(segment_test, position);
    EXPECT_TRUE(0.0 == distance);

    // Test 04: Check the function distanceToSegment when the position is not on the segment
    // Make sure the distance return is valid
    position = Eigen::Vector3d(2.0, 2.0, 3.14);
    distance = stoplights_system::distanceToSegment(segment_test, position);
    EXPECT_TRUE(1.0 == distance);
}

/*************************************************************************************************/
TEST(node, getSegment)
{
    Eigen::Vector3d position;
    stoplights_system::Segment segment_test;

    // Test 01: Check the function getSegment when the segment list is empty
    // Make sure the returned value is equal to -1
    std::map<int32_t, stoplights_system::Segment> test_map;

    int32_t segment_id = stoplights_system::getSegment(test_map, position);
    EXPECT_TRUE(-1 == segment_id);

    // Test 02: Check the function getSegment when there is two segment in the list
    // Make sure the returned value is the closest segment
    position = Eigen::Vector3d(7.0, 2.0, 3.14);
    segment_test.start  = Eigen::Vector2d(1.0, 1.0);
    segment_test.end    = Eigen::Vector2d(5.0, 1.0);
    test_map[0] = segment_test;
    segment_test.start  = Eigen::Vector2d(5.0, 1.0);
    segment_test.end    = Eigen::Vector2d(10.0, 1.0);
    test_map[1] = segment_test;
    segment_id = stoplights_system::getSegment(test_map, position);
    EXPECT_TRUE(1 == segment_id);

    // Test 03: Check the function getSegment when there is two segment at identical distance
    // Make sure the returned value is the first in the list between the two segments
    position = Eigen::Vector3d(5.0, 2.0, 3.14);
    segment_id = stoplights_system::getSegment(test_map, position);
    EXPECT_TRUE(0 == segment_id);
}
