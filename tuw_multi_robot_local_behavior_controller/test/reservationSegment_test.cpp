#include <gtest/gtest.h>
#include "ros/ros.h"
#include <node/local_behavior_node.h>

/*************************************************************************************************/
TEST(segmentReservation, constructor)
{
    // Test 01: Make sure all the variable are properly initialized by the constructor
    ros::NodeHandle n;
    stoplights_system::SegmentReservation segment_reservation_test = stoplights_system::SegmentReservation(n);
    
    EXPECT_EQ(2, segment_reservation_test.dist_to_restricted_segment);
    EXPECT_EQ(3, segment_reservation_test.nb_segment_horizon);

    // Test 02: Check when the the parameters are properly set
    n.setParam("local_behavior/dist_to_restricted_segment", 5);
    n.setParam("local_behavior/nb_segment_horizon", 6);
    segment_reservation_test = stoplights_system::SegmentReservation(n);
    
    EXPECT_EQ(5, segment_reservation_test.dist_to_restricted_segment);
    EXPECT_EQ(6, segment_reservation_test.nb_segment_horizon);

    n.deleteParam("local_behavior/dist_to_restricted_segment");
    n.deleteParam("local_behavior/nb_segment_horizon");
}

/*************************************************************************************************/
TEST(segmentReservation, badConstructor)
{
    ros::NodeHandle n;
    n.setParam("local_behavior/dist_to_restricted_segment", 0);

    EXPECT_THROW(
    {
        try
        {
            stoplights_system::SegmentReservation segment_reservation_test = stoplights_system::SegmentReservation(n);
        }
        catch( const std::runtime_error& e )
        {
            throw;
        }
    }, std::runtime_error);

    n.deleteParam("local_behavior/dist_to_restricted_segment");
}

/*************************************************************************************************/
TEST(segmentReservation, isOnLastRouteSegment)
{
    ros::NodeHandle n;
    stoplights_system::SegmentReservation segment_reservation_test = stoplights_system::SegmentReservation(n);
    
    // Check 01: Test when the robot has no current route
    EXPECT_FALSE(segment_reservation_test.isOnLastRouteSegment());

    // Check 02: The robot has a route but is not on its last route and its not reserved neither
    std::vector<int32_t> new_route = {1, 2, 3, 4};
    geometry_msgs::Point goal_point;
    segment_reservation_test.initRoute(new_route, goal_point);
    EXPECT_FALSE(segment_reservation_test.isOnLastRouteSegment());

    // Check 03: The robot is on its last segment
    segment_reservation_test.current_segment_it = segment_reservation_test.current_segment_it + 3;
    EXPECT_TRUE(segment_reservation_test.isOnLastRouteSegment());

    // Check 04: The robot has reserved its last segment
    segment_reservation_test.current_segment_it = segment_reservation_test.route.begin();
    segment_reservation_test.last_accessible_segment_it = segment_reservation_test.last_accessible_segment_it + 3;
    EXPECT_TRUE(segment_reservation_test.isOnLastRouteSegment());

}

/*************************************************************************************************/
TEST(segmentReservation, resetRoute)
{
    ros::NodeHandle n;
    stoplights_system::SegmentReservation segment_reservation_test = stoplights_system::SegmentReservation(n);
    segment_reservation_test.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    // Test 01: Call the function and check that the route is properly updated as well as the related variables
    std::vector<int32_t> new_route = {1, 2, 3, 4};
    geometry_msgs::Point goal_point;
    segment_reservation_test.initRoute(new_route, goal_point);
    segment_reservation_test.current_segment_it = segment_reservation_test.current_segment_it + 1;
    segment_reservation_test.last_accessible_segment_it = segment_reservation_test.last_accessible_segment_it + 2;
    segment_reservation_test.resetRoute();
    EXPECT_TRUE(segment_reservation_test.route.empty());
    EXPECT_EQ(segment_reservation_test.route.begin(),   segment_reservation_test.current_segment_it); 
    EXPECT_EQ(segment_reservation_test.route.begin(),   segment_reservation_test.last_accessible_segment_it);
}

/*************************************************************************************************/
TEST(segmentReservation, initRoute)
{
    ros::NodeHandle n;
    stoplights_system::SegmentReservation segment_reservation_test = stoplights_system::SegmentReservation(n);
    segment_reservation_test.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    // Test 01: Call the function and check that the route is properly updated as well as the related variables
    std::vector<int32_t> new_route = {1, 2, 3, 4};
    geometry_msgs::Point goal_point;
    segment_reservation_test.initRoute(new_route, goal_point);

    EXPECT_EQ(4, segment_reservation_test.route.size());
    EXPECT_EQ(segment_reservation_test.route.begin(),       segment_reservation_test.current_segment_it); 
    EXPECT_EQ(segment_reservation_test.route.begin() + 1,   segment_reservation_test.last_accessible_segment_it);
}

/*************************************************************************************************/
TEST(segmentReservation, updateRemainingAccess)
{
    ros::NodeHandle n;
    stoplights_system::SegmentReservation segment_reservation_test = stoplights_system::SegmentReservation(n);
    segment_reservation_test.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    std::vector<int32_t> new_route = {1, 2, 3, 4};
    geometry_msgs::Point goal_point;
    segment_reservation_test.initRoute(new_route, goal_point);

    // Test 01: Check when the input value in not in the current route
    segment_reservation_test.updateRemainingAccess(10);
    EXPECT_EQ(*(segment_reservation_test.last_accessible_segment_it), *(segment_reservation_test.route.begin() + 1));

    // Test 02: Check when the input value is in the route
    segment_reservation_test.updateRemainingAccess(3);
    EXPECT_EQ(*(segment_reservation_test.last_accessible_segment_it), *(segment_reservation_test.route.begin() + 2));
}

/*************************************************************************************************/
TEST(segmentReservation, isAccessRestricted)
{
    ros::NodeHandle n;
    stoplights_system::SegmentReservation segment_reservation_test = stoplights_system::SegmentReservation(n);
    segment_reservation_test.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    std::vector<int32_t> new_route = {1, 2, 3, 4};
    geometry_msgs::Point goal_point;
    segment_reservation_test.initRoute(new_route, goal_point);

    // Test 01: Check when the robot is restricted
    EXPECT_FALSE(segment_reservation_test.isAccessRestricted());

    // Test 02: Check when the robot is not restricted
    segment_reservation_test.last_accessible_segment_it++;
    EXPECT_TRUE(segment_reservation_test.isAccessRestricted());
}

/*************************************************************************************************/
TEST(segmentReservation, isAccessStillBlocked)
{
    ros::NodeHandle n;
    stoplights_system::SegmentReservation segment_reservation_test = stoplights_system::SegmentReservation(n);
    segment_reservation_test.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    std::vector<int32_t> new_route = {1, 2, 3, 4};
    geometry_msgs::Point goal_point;
    segment_reservation_test.initRoute(new_route, goal_point);

    // Test 01: Check when the robot is not below the restricted treeshold
    segment_reservation_test.last_accessible_segment_it++;
    EXPECT_FALSE(segment_reservation_test.isAccessStillBlocked());

    // Test 02: Check when the robot as already reserved all its segment
    segment_reservation_test.last_accessible_segment_it = segment_reservation_test.last_accessible_segment_it + 2;
    EXPECT_FALSE(segment_reservation_test.isAccessStillBlocked());

    // Test 03: Check when the robot is already at the end of its route
    segment_reservation_test.last_accessible_segment_it = segment_reservation_test.route.begin() + 1; 
    segment_reservation_test.current_segment_it = segment_reservation_test.current_segment_it + 3;
    EXPECT_FALSE(segment_reservation_test.isAccessStillBlocked());

    // Test 04: Check when the robot is blocked by the stoplights
    segment_reservation_test.current_segment_it = segment_reservation_test.route.begin();
    EXPECT_TRUE(segment_reservation_test.isAccessStillBlocked());
}

/*************************************************************************************************/
TEST(segmentReservation, isAccessBlocked)
{
    ros::NodeHandle n;
    stoplights_system::SegmentReservation segment_reservation_test = stoplights_system::SegmentReservation(n);
    segment_reservation_test.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    std::vector<int32_t> new_route = {1, 2, 3, 4};
    geometry_msgs::Point goal_point;
    segment_reservation_test.initRoute(new_route, goal_point);

    // Test 01: Check when the robot is below the blocking treeshold
    EXPECT_TRUE(segment_reservation_test.isAccessBlocked());

    // Test 02: Check when the robot is NOT below the blocking treeshold
    segment_reservation_test.last_accessible_segment_it++;
    EXPECT_FALSE(segment_reservation_test.isAccessBlocked());
}

/*************************************************************************************************/
TEST(segmentReservation, isAccessUnblocked)
{
    ros::NodeHandle n;
    stoplights_system::SegmentReservation segment_reservation_test = stoplights_system::SegmentReservation(n);
    segment_reservation_test.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    std::vector<int32_t> new_route = {1, 2, 3, 4};
    geometry_msgs::Point goal_point;
    segment_reservation_test.initRoute(new_route, goal_point);

    // Test 01: Check when the robot is below the restricted treeshold
    EXPECT_FALSE(segment_reservation_test.isAccessUnblocked());

    // Test 02: Check when the robot is not below the restricted treeshold
    segment_reservation_test.last_accessible_segment_it = segment_reservation_test.last_accessible_segment_it + 2;
    EXPECT_TRUE(segment_reservation_test.isAccessUnblocked());
}

/*************************************************************************************************/
TEST(segmentReservation, getStopPoint)
{
    ros::NodeHandle n;
    stoplights_system::SegmentReservation segment_reservation_test = stoplights_system::SegmentReservation(n);
    segment_reservation_test.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    std::vector<int32_t> new_route = {1, 2, 3, 4};
    geometry_msgs::Point goal_point;
    segment_reservation_test.initRoute(new_route, goal_point);

    std::map<int32_t, stoplights_system::Segment> segments_test;
    stoplights_system::Segment seg;

    seg.start << 1.0, 2.0;
    seg.end << 3.0, 4.0;
    segment_reservation_test.segments_[1] = seg;
    seg.start << 3.0, 2.0;
    seg.end << 5.0, 4.0;
    segment_reservation_test.segments_[2] = seg;
    seg.start << 5.0, 2.0;
    seg.end << 7.0, 4.0;
    segment_reservation_test.segments_[3] = seg;
    seg.start << 7.0, 2.0;
    seg.end << 9.0, 4.0;
    segment_reservation_test.segments_[4] = seg;
    
    // Test 01: Check when the last accessible segment is behind the current robot's segment
    geometry_msgs::PointStamped point_test = segment_reservation_test.getStopPoint();
    EXPECT_TRUE(2.0 == point_test.point.x);
    EXPECT_TRUE(3.0 == point_test.point.y);

    // Test 02: Check when the last accessible segment is ahead of the current robot's segment
    segment_reservation_test.current_segment_it = segment_reservation_test.current_segment_it + 2;
    point_test = segment_reservation_test.getStopPoint();
    EXPECT_TRUE(6.0 == point_test.point.x);
    EXPECT_TRUE(3.0 == point_test.point.y);
}

/*************************************************************************************************/
TEST(segmentReservation, isRequestBehind)
{
    ros::NodeHandle n;
    stoplights_system::SegmentReservation segment_reservation_test = stoplights_system::SegmentReservation(n);

    // Test 01: Check when the request is not behind the current robot's segment
    EXPECT_FALSE(segment_reservation_test.isRequestBehind());

    // Test 02: Check when the request is behind the current robot's segment
    segment_reservation_test.current_segment_it = segment_reservation_test.current_segment_it + 2;
    EXPECT_TRUE(segment_reservation_test.isRequestBehind());
}

/*************************************************************************************************/
TEST(segmentReservation, updateSegmentReservation)
{
    ros::NodeHandle n;
    stoplights_system::SegmentReservation segment_reservation_test = stoplights_system::SegmentReservation(n);

    // Test 01: Check when the robot is already at the end of the route
    std::vector<int32_t> new_route = {1, 2, 3, 4, 5, 6};
    segment_reservation_test.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    geometry_msgs::Point goal_point;
    segment_reservation_test.initRoute(new_route, goal_point);
    segment_reservation_test.updateSegmentReservation();
    EXPECT_EQ(5, *segment_reservation_test.last_accessible_segment_it);

    // Test 02: Check when the robot is authorized on its route
    segment_reservation_test.updateSegmentReservation();
    EXPECT_EQ(5, *segment_reservation_test.last_accessible_segment_it);

    // Test 02: Check when the robot is authorized on its route
    segment_reservation_test.current_segment_it = segment_reservation_test.current_segment_it + 2;
    segment_reservation_test.last_accessible_segment_it = segment_reservation_test.route.begin();
    segment_reservation_test.updateSegmentReservation();
    EXPECT_EQ(segment_reservation_test.route.end(), segment_reservation_test.last_accessible_segment_it);

    // Test 03: CHeck when the robot already has access to its last segment;
    segment_reservation_test.last_accessible_segment_it = segment_reservation_test.route.end();
    segment_reservation_test.updateSegmentReservation();
    EXPECT_EQ(segment_reservation_test.route.end(), segment_reservation_test.last_accessible_segment_it);

}

/*************************************************************************************************/
TEST(segmentReservation, updateReservationFromPosition)
{
    ros::NodeHandle n;
    stoplights_system::SegmentReservation segment_reservation_test = stoplights_system::SegmentReservation(n);
    Eigen::Vector3d robot_pos;
    bool is_in_teleop = false;
    segment_reservation_test.stop_lights_client = std::make_shared<stoplights_system::StopLightsClientSingleRobot>();

    // Test 01: Check when the robot is NOT in teleop
    segment_reservation_test.updateReservationFromPosition(robot_pos, is_in_teleop);

    // Test 02: Check when the robot is in teleop but the last accessible segment is not behind
    is_in_teleop = true;
    segment_reservation_test.updateReservationFromPosition(robot_pos, is_in_teleop);

    // Test 03: Check when the robot is in teleop but the last accessible segment is not behind
    std::vector<int32_t> new_route = {1, 2, 3};
    geometry_msgs::Point goal_point;
    segment_reservation_test.initRoute(new_route, goal_point);
    segment_reservation_test.current_segment_it += 2; 
    segment_reservation_test.updateReservationFromPosition(robot_pos, is_in_teleop);
    EXPECT_EQ(segment_reservation_test.current_segment_it + 1, segment_reservation_test.last_accessible_segment_it);
}