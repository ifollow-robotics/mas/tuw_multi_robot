#include <gtest/gtest.h>
#include "ros/ros.h"
#include <node/local_behavior_node.h>

/*************************************************************************************************/
class TestClient03 : public ::testing::Test
{
    public:
        ros::ServiceServer  service_in;
        bool receivedMessage;
        std::vector<uint16_t> response_occupied_slots;
        std::vector<uint16_t> response_occupied_slots_vehicles;
        bool return_value = false;
        ros::NodeHandle n;  

        TestClient03() : receivedMessage(false) 
        {
            service_in = n.advertiseService("mr_area_mngr", &TestClient03::callback, this);
        }

        bool callback(ifollow_mas_msgs::MRAreaMngr::Request& request, ifollow_mas_msgs::MRAreaMngr::Response& response)
        {
            response.resp = receivedMessage;
            response.occupied_slots = response_occupied_slots;
            response.occupied_slots_vehicles = response_occupied_slots_vehicles;

            return return_value;
        }
};

/*************************************************************************************************/
class TestClient04 : public ::testing::Test
{
    public:
        ros::Subscriber service_in;
        int receivedMessage;
        ros::NodeHandle n;  

        TestClient04() : receivedMessage(false) 
        {
            service_in = n.subscribe("local_behavior/traffic_status", 1,  &TestClient04::callback, this);
        }

        void callback(const std_msgs::UInt8& response)
        {
            receivedMessage = response.data;
        }
};

/*************************************************************************************************/
class TestClient05 : public ::testing::Test
{
    public:
        ros::Subscriber service_in;
        uint32_t receivedMessage;
        ros::NodeHandle n;  

        TestClient05() : receivedMessage(false) 
        {
            service_in = n.subscribe("local_behavior/curr_seg_id", 1,  &TestClient05::callback, this);
        }

        void callback(const std_msgs::UInt32& response)
        {
            receivedMessage = response.data;
        }
};

/*************************************************************************************************/
TEST(BehaviorStoplights, constructor)
{
    EXPECT_TRUE(true);
}

/*************************************************************************************************/
TEST(BehaviorStoplights, init)
{
    stoplights_system::BehaviorStoplights behavior_test;
    ros::NodeHandle n;
    std::string robot_name = "ilogistics_4_0001";

    // Test 01 Check the init function when the parameters stoplights_reservation_mode is not set 
    behavior_test.init(n, robot_name);
    EXPECT_EQ(stoplights_system::NAV_FINISHED, behavior_test.robot_state);
    EXPECT_EQ(false,    behavior_test.try_reach_goal);
    EXPECT_EQ(true,     behavior_test.standalone_mode);
    EXPECT_EQ(false,    behavior_test.prev_standalone_mode);

    // Test 01 Check the init function when the parameters stoplights_reservation_mode is equal to Segment
    n.setParam("local_behavior/stoplights_reservation_mode", "Segment");
    behavior_test.init(n, robot_name);
    EXPECT_EQ(stoplights_system::NAV_FINISHED, behavior_test.robot_state);
    EXPECT_EQ(false,    behavior_test.try_reach_goal);
    EXPECT_EQ(true,     behavior_test.standalone_mode);
    EXPECT_EQ(false,    behavior_test.prev_standalone_mode);

    // Test 02 Check the init function when the parameters stoplights_reservation_mode is equal to Distance
    n.setParam("local_behavior/stoplights_reservation_mode", "Distance");
    behavior_test.init(n, robot_name);
    EXPECT_EQ(stoplights_system::NAV_FINISHED, behavior_test.robot_state);
    EXPECT_EQ(false,    behavior_test.try_reach_goal);
    EXPECT_EQ(true,     behavior_test.standalone_mode);
    EXPECT_EQ(false,    behavior_test.prev_standalone_mode);

    // Test 04 Check the init function when the parameters stoplights_reservation_mode is invalid
    n.setParam("local_behavior/stoplights_reservation_mode", "Bad_Name");
    behavior_test.init(n, robot_name);
    EXPECT_EQ(stoplights_system::NAV_FINISHED, behavior_test.robot_state);
    EXPECT_EQ(false,    behavior_test.try_reach_goal);
    EXPECT_EQ(true,     behavior_test.standalone_mode);
    EXPECT_EQ(false,    behavior_test.prev_standalone_mode);

    n.deleteParam("local_behavior/stoplights_reservation_mode");
}

/*************************************************************************************************/
TEST_F(TestClient03, requestToLeaveMrArea)
{
    ros::AsyncSpinner spinner(1);
    spinner.start();

    stoplights_system::BehaviorStoplights behavior_test;
    uint16_t robot_id = 1;
    behavior_test.current_area = 0;
    behavior_test.current_slot = 0;
    behavior_test.mr_area_mngr_client    = n.serviceClient<ifollow_mas_msgs::MRAreaMngr>("mr_area_mngr");

    // Test 01: Check when the service return true
    return_value = true;
    receivedMessage = true;
    EXPECT_TRUE(behavior_test.requestToLeaveMrArea(robot_id));

    // Test 02: Check when the service return 
    ifollow_mas_msgs::Area area_msgs;
    return_value = false;
    area_msgs.id = 1;
    behavior_test.areas[area_msgs.id] = stoplights_system::MRArea();

    // Test 01: Check when the area is not of type PARKING
    area_msgs.type.push_back(ifollow_mas_msgs::Area::AREA_TYPE_NO_STOP);
    area_msgs.points.resize(4);
    area_msgs.points[0].x = 1.0;
    area_msgs.points[0].y = 1.0;
    area_msgs.points[1].x = 1.0;
    area_msgs.points[1].y = 5.0;
    area_msgs.points[2].x = 5.0;
    area_msgs.points[2].y = 5.0;
    area_msgs.points[3].x = 5.0;
    area_msgs.points[3].y = 1.0;

    behavior_test.segmentAreaIntoParkingSlots(behavior_test.areas[area_msgs.id], area_msgs);
    EXPECT_FALSE(behavior_test.requestToLeaveMrArea(robot_id));

    // Test 02: Check when the area is a parking type but the ID doesn't exist
    area_msgs.id = 2;
    behavior_test.current_area = 1;
    behavior_test.areas[area_msgs.id] = stoplights_system::MRArea();
    
    area_msgs.type.push_back(ifollow_mas_msgs::Area::AREA_TYPE_TRAFFIC_PARKING);
    area_msgs.points.resize(4);
    area_msgs.points[0].x = 1.0;
    area_msgs.points[0].y = 1.0;
    area_msgs.points[1].x = 1.0;
    area_msgs.points[1].y = 5.0;
    area_msgs.points[2].x = 5.0;
    area_msgs.points[2].y = 5.0;
    area_msgs.points[3].x = 5.0;
    area_msgs.points[3].y = 1.0;
    return_value = true;

    behavior_test.segmentAreaIntoParkingSlots(behavior_test.areas[area_msgs.id], area_msgs);
    EXPECT_TRUE(behavior_test.requestToLeaveMrArea(robot_id));

    // Test 03: Check when the input area id is defined in the list
    behavior_test.current_area = 1;
    behavior_test.areas[1].area_definition.id = 1;
    behavior_test.segmentAreaIntoParkingSlots(behavior_test.areas[area_msgs.id], area_msgs);
    response_occupied_slots.push_back(2);
    response_occupied_slots_vehicles.push_back(100);
    EXPECT_EQ(behavior_test.areas[1].parking_slots.size(), 8);
    receivedMessage = false;
    EXPECT_FALSE(behavior_test.requestToLeaveMrArea(robot_id));
    EXPECT_EQ(behavior_test.areas[1].parking_slots[2].occupation_id, 100);

    // Test 04: Check when the request is made twice
    EXPECT_FALSE(behavior_test.requestToLeaveMrArea(robot_id));
    EXPECT_EQ(behavior_test.areas[1].parking_slots[2].occupation_id, 100);
}

/*************************************************************************************************/
TEST(BehaviorStoplights, updateStoplightsMode)
{
    stoplights_system::BehaviorStoplights behavior_test;
    ros::NodeHandle n;
    std::string robot_name = "ilogistics_4_0001";

    behavior_test.init(n, robot_name);
    
    // Test 01: Check the function updateStoplightsMode when the last connected vehicules to too long 
    behavior_test.standalone_mode = true;
    behavior_test.prev_standalone_mode  = true;
    behavior_test.updateStoplightsMode();
    EXPECT_TRUE(behavior_test.standalone_mode);
    // Test 02: Check the function updateStoplightsMode when the previous state was false and the current is true 
    behavior_test.standalone_mode = true;
    behavior_test.prev_standalone_mode  = false;
    behavior_test.updateStoplightsMode();
    EXPECT_TRUE(behavior_test.standalone_mode);

    // Test 03: Check the function updateStoplightsMode from standalone to multiRobots with multi-robots activated
    behavior_test.standalone_mode       = false;
    behavior_test.prev_standalone_mode  = true;
    behavior_test.updateStoplightsMode();
    EXPECT_TRUE(behavior_test.prev_standalone_mode);

    // Test 04: Check the function updateStoplightsMode from standalone to multiRobots with multi-robots deActivated
    behavior_test.standalone_mode       = false;
    behavior_test.prev_standalone_mode  = true;
    n.setParam("local_behavior/ignore_stoplights_even_in_mrs", true);
    behavior_test.updateStoplightsMode();
    EXPECT_TRUE(behavior_test.prev_standalone_mode);

    n.deleteParam("local_behavior/ignore_stoplights_even_in_mrs");
}

/*************************************************************************************************/
TEST(BehaviorStoplights, updateReservationHorizon)
{
    stoplights_system::BehaviorStoplights behavior_test;
    ros::NodeHandle n;
    std::string robot_name = "ilogistics_4_0001";
    Eigen::Vector3d position;

    behavior_test.init(n, robot_name);

    // Test 01: Check when the robot has no current route
    behavior_test.updateReservationHorizon();

    // Test 02: Check when the robot has a route
    std::map<int32_t, stoplights_system::Segment> segments_test;
    stoplights_system::Segment seg;
    tuw::RouteProgressMonitor route_progress_monitor;

    seg.start << 1.0, 2.0;
    seg.end << 3.0, 4.0;
    segments_test[1] = seg;
    seg.start << 3.0, 2.0;
    seg.end << 5.0, 6.0;
    segments_test[2] = seg;
    seg.start << 5.0, 2.0;
    seg.end << 7.0, 8.0;
    segments_test[3] = seg;
    seg.start << 7.0, 2.0;
    seg.end << 9.0, 10.0;
    segments_test[4] = seg;
    std::vector<int32_t> route = {1, 2, 3, 4};

    route_progress_monitor.init(segments_test, route);  
    position[0] = 4.0;
    position[1] = 2.0;
    route_progress_monitor.updateProgress(position);
    behavior_test.updateReservationHorizon();
}

/*************************************************************************************************/
TEST_F(TestClient04, updateTrafficStatus)
{
    stoplights_system::BehaviorStoplights behavior_test;
    std::string robot_name = "ilogistics_4_0001";
    ros::NodeHandle n;
    behavior_test.init(n, robot_name);
    stoplights_system::RobotState robot_state;
    std::vector<int32_t> new_route = {1, 2, 3, 4};
    geometry_msgs::Point goal_point;
    behavior_test.stoplights_reservation_manager->initRoute(new_route, goal_point);

    // Test 01: Check when the robot is in standalone
    behavior_test.standalone_mode = true;
    behavior_test.updateTrafficStatus(robot_state);
    ros::Duration(0.05).sleep();
    ros::spinOnce();
    EXPECT_EQ(5, receivedMessage);

    // Test 02: Check when robot state is equal to MOVE_OFF_ROAD
    behavior_test.standalone_mode = false;
    robot_state = stoplights_system::RobotState::MOVE_OFF_ROAD;
    behavior_test.updateTrafficStatus(robot_state);
    ros::Duration(0.05).sleep();
    ros::spinOnce();
    EXPECT_EQ(0, receivedMessage);

    // Test 03: Check when robot state is equal to MOVE_ON_ROAD
    robot_state = stoplights_system::RobotState::MOVE_ON_ROAD;
    behavior_test.updateTrafficStatus(robot_state);
    ros::Duration(0.05).sleep();
    ros::spinOnce();
    EXPECT_EQ(0, receivedMessage);

    // Test 04: Check when robot state is equal to MOVE_RESTRICTED
    robot_state = stoplights_system::RobotState::MOVE_RESTRICTED;
    behavior_test.updateTrafficStatus(robot_state);
    ros::Duration(0.05).sleep();
    ros::spinOnce();
    EXPECT_EQ(1, receivedMessage);
    
    // Test 05: Check when robot state is equal to WAITING_AUTH
    robot_state = stoplights_system::RobotState::WAITING_AUTH;
    behavior_test.updateTrafficStatus(robot_state);
    ros::Duration(0.05).sleep();
    ros::spinOnce();
    EXPECT_EQ(2, receivedMessage);
    
    // Test 06: Check when robot state is equal to UNKNOWN and the request is behind
    robot_state = stoplights_system::RobotState::UNKNOWN;
    behavior_test.stoplights_reservation_manager->current_segment_it = behavior_test.stoplights_reservation_manager->route.begin() + 2;
    behavior_test.updateTrafficStatus(robot_state);
    ros::Duration(0.05).sleep();
    ros::spinOnce();
    EXPECT_EQ(6, receivedMessage);
    
    // Test 07: Check when robot state is equal to UNKNOWN and the request is not behind
    behavior_test.stoplights_reservation_manager->current_segment_it = behavior_test.stoplights_reservation_manager->route.begin();
    behavior_test.updateTrafficStatus(robot_state);
    ros::Duration(0.05).sleep();
    ros::spinOnce();
    EXPECT_EQ(2, receivedMessage);
    
    // Test 08: Check when robot state is equal to NAV_FINISHED
    robot_state = stoplights_system::RobotState::NAV_FINISHED;
    behavior_test.updateTrafficStatus(robot_state);
    ros::Duration(0.05).sleep();
    ros::spinOnce();
    EXPECT_EQ(3, receivedMessage);

    // Test 09: Check when robot state is equal to NAV_SLAVE
    robot_state = stoplights_system::RobotState::NAV_SLAVE;
    behavior_test.updateTrafficStatus(robot_state);
    ros::Duration(0.05).sleep();
    ros::spinOnce();
    EXPECT_EQ(4, receivedMessage);

    // Test 10: Check when robot state is equal to an invalid value
    behavior_test.updateTrafficStatus(static_cast<stoplights_system::RobotState>(100));
    ros::Duration(0.05).sleep();
    ros::spinOnce();
    EXPECT_EQ(2, receivedMessage);
}

/*************************************************************************************************/
TEST_F(TestClient05, publishCurrSeg)
{
    ros::AsyncSpinner spinner(1);
    spinner.start();

    Eigen::Vector3d robot_pos; 
    std::vector<int32_t> route;
    std::map<int32_t, stoplights_system::Segment> segments;
    stoplights_system::Segment seg, current_seg;
    double waiting_time = 0.01;
    geometry_msgs::Point goal_point;

    seg.start << 1.0, 2.0;
    seg.end << 3.0, 4.0;
    segments[1] = seg;
    seg.start << 3.0, 2.0;
    seg.end << 5.0, 6.0;
    segments[2] = seg;
    seg.start << 5.0, 2.0;
    seg.end << 7.0, 8.0;
    segments[3] = seg;
    seg.start << 7.0, 2.0;
    seg.end << 9.0, 10.0;
    segments[4] = seg;
    robot_pos[0] = 4.0;
    robot_pos[1] = 2.0;
    robot_pos[2] = 0.0;

    stoplights_system::BehaviorStoplights behavior_test;
    std::string robot_name = "ilogistics_4_0001";
    ros::NodeHandle n;
    behavior_test.init(n, robot_name);

    // Test 01: Check when the robot has no current route and its position is valid
    behavior_test.stoplights_reservation_manager->initRoute(route, goal_point);
    behavior_test.publishCurrSeg(robot_pos, route, segments);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();
    behavior_test.last_seg_to_pub_id = 10;
    EXPECT_EQ(receivedMessage, 2);

    // Test 02: Check when the current route is not empty
    route = {1, 2, 3};
    behavior_test.stoplights_reservation_manager->initRoute(route, goal_point);
    behavior_test.publishCurrSeg(robot_pos, route, segments);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();
    EXPECT_EQ(receivedMessage, 1);

    // Test 03: Check when the robot has no current route but its position is not valid
    route.clear();
    behavior_test.stoplights_reservation_manager->initRoute(route, goal_point);
    robot_pos[0] = std::numeric_limits<double>::quiet_NaN();
    behavior_test.publishCurrSeg(robot_pos, route, segments);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();
    behavior_test.last_seg_to_pub_id = 10;
    EXPECT_EQ(receivedMessage, 1);

    // Test 04: Check when the current segment is the same than the previous one
    robot_pos[0] = 4.0;
    behavior_test.last_seg_to_pub_id = 2;
    receivedMessage = 20;
    behavior_test.publishCurrSeg(robot_pos, route, segments);
    ros::Duration(waiting_time).sleep();
    ros::spinOnce();
    EXPECT_EQ(receivedMessage, 20);
}

/*************************************************************************************************/
TEST(stoplightsBehavior, getStopPoint)
{
    stoplights_system::BehaviorStoplights behavior_test;
    std::string robot_name = "ilogistics_4_0001";
    ros::NodeHandle n;
    n.setParam("local_behavior/stoplights_reservation_mode", "Distance");
    behavior_test.init(n, robot_name);

    // Test 01: Check the returned value by the function
    geometry_msgs::PointStamped stop_point = behavior_test.getStopPoint();
    EXPECT_TRUE(std::isnan(stop_point.point.x));
    EXPECT_TRUE(std::isnan(stop_point.point.y));
    EXPECT_EQ(stop_point.header.frame_id, "world");
}

/*************************************************************************************************/
TEST(stoplightsBehavior, getCurrentSegmentOnRoute)
{
    stoplights_system::BehaviorStoplights behavior_test;
    std::string robot_name = "ilogistics_4_0001";
    ros::NodeHandle n;
    std::vector<int32_t> route = {1, 2, 3};

    n.setParam("local_behavior/stoplights_reservation_mode", "Distance");
    behavior_test.init(n, robot_name);
    geometry_msgs::Point goal_point;
    behavior_test.stoplights_reservation_manager->initRoute(route, goal_point);
    EXPECT_EQ(1, behavior_test.getCurrentSegmentOnRoute());
}

/*************************************************************************************************/
TEST_F(TestClient03, requestToEnterMrArea)
{
    ros::AsyncSpinner spinner(1);
    spinner.start();

    stoplights_system::BehaviorStoplights behavior_test;
    std::string robot_name = "ilogistics_4_0001";
    ros::NodeHandle n;
    std::vector<int32_t> route = {1, 2, 3};
    n.setParam("local_behavior/stoplights_reservation_mode", "Distance");
    behavior_test.init(n, robot_name);
    geometry_msgs::Point goal_point;

    ifollow_mas_msgs::Area area_msgs;
    area_msgs.id = 1;
    behavior_test.areas[area_msgs.id] = stoplights_system::MRArea();
    area_msgs.type.push_back(ifollow_mas_msgs::Area::AREA_TYPE_TRAFFIC_PARKING);
    area_msgs.points.resize(4);
    area_msgs.points[0].x = 1.0;
    area_msgs.points[0].y = 1.0;
    area_msgs.points[1].x = 1.0;
    area_msgs.points[1].y = 5.0;
    area_msgs.points[2].x = 5.0;
    area_msgs.points[2].y = 5.0;
    area_msgs.points[3].x = 5.0;
    area_msgs.points[3].y = 1.0;

    behavior_test.segmentAreaIntoParkingSlots(behavior_test.areas[area_msgs.id], area_msgs);

    behavior_test.stoplights_reservation_manager->initRoute(route, goal_point);
    uint16_t robot_id;
    int32_t area_id = 1; 
    Eigen::Vector3d robot_pos;
    std::vector<ifollow_utils::mas::ParkingSlot>::iterator cand_parking_slot;
    geometry_msgs::PoseStamped goal;
    behavior_test.mr_area_mngr_client    = n.serviceClient<ifollow_mas_msgs::MRAreaMngr>("mr_area_mngr");

    cand_parking_slot = behavior_test.areas[area_id].parking_slots.begin();
     
    // Test 01: Check when the service called failed
    receivedMessage = true;
    return_value = false;
    EXPECT_FALSE(behavior_test.requestToEnterMrArea(robot_id, area_id, robot_pos, cand_parking_slot, goal));

    // Test 02: Check when the service called return false
    receivedMessage = false;
    return_value = true;
    EXPECT_FALSE(behavior_test.requestToEnterMrArea(robot_id, area_id, robot_pos, cand_parking_slot, goal));

    // Test 03: Check when the orientation between the robot and the parking slot is below 1.57
    receivedMessage = true;
    return_value = true;
    robot_pos[0] = 0.0;
    robot_pos[1] = 0.0;
    robot_pos[2] = 0.0;
    EXPECT_TRUE(behavior_test.requestToEnterMrArea(robot_id, area_id, robot_pos, cand_parking_slot, goal));

    // Test 04: Check when the orientation between the robot and the parking slot is above 1.57
    receivedMessage = true;
    return_value = true;
    robot_pos[0] = 0.0;
    robot_pos[1] = 0.0;
    robot_pos[2] = 3.14;
    EXPECT_TRUE(behavior_test.requestToEnterMrArea(robot_id, area_id, robot_pos, cand_parking_slot, goal));

    // Test 05: Check to request twice the same parking slots
    robot_pos[0] = 0.0;
    robot_pos[1] = 0.0;
    robot_pos[2] = 3.14;
    EXPECT_TRUE(behavior_test.requestToEnterMrArea(robot_id, area_id, robot_pos, cand_parking_slot, goal));

    n.deleteParam("/local_behavior/stoplights_reservation_mode");
}

/*************************************************************************************************/
TEST(BehaviorStoplights, updateTrafficStatus)
{
    stoplights_system::BehaviorStoplights behavior_test;
    std::string robot_name = "ilogistics_4_0001";
    ros::NodeHandle n;
    std::vector<int32_t> route = {1, 2, 3};
    behavior_test.init(n, robot_name);
    
    // Test 01: Check when the pref doesn't exist
    EXPECT_EQ(stoplights_system::stoplightsRequestToServerState::STOPLIGHTS_REQUEST_STATE_POSSIBLE, behavior_test.getIgnoreStoplightsStatus());

    // Test 02: Check when the pref doesn't exist
    n.setParam("local_behavior/ignore_stoplights_even_in_mrs", true);
    behavior_test.init(n, robot_name);
    EXPECT_EQ(stoplights_system::stoplightsRequestToServerState::STOPLIGHTS_REQUEST_STATE_NO_REQUEST, behavior_test.getIgnoreStoplightsStatus());

    n.deleteParam("local_behavior/ignore_stoplights_even_in_mrs");
}
