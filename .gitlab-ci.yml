include:
  - project: "ifollow-robotics/devops"
    ref: master
    file: "base_configuration.yml"

image: "registry.gitlab.com/ifollow-robotics/nav/ros:melodic"

variables:
  DOCKER_DRIVER: overlay2
  GIT_DEPTH: 1 # by default CI uses a shallow clone of 50
  GIT_STRATEGY: clone # options are fetch and clone. clone is slow but is from scratch, fetch uses previous job repo git clean and git fetch differences (can pose problems)

stages:
  - Test

default:
  before_script:
    - |
      ########## Download bash scripts ##########
      git clone --depth=1 https://gitlab.com/ifollow-robotics/devops.git
      mv devops/select_branch.sh select_branch.sh
      rm -rf devops
      chmod +x select_branch.sh
      . ./select_branch.sh

      # DEVOPS_BRANCH var is set by the select_branch.sh script
      git clone -b $DEVOPS_BRANCH --depth=1 https://gitlab.com/ifollow-robotics/devops.git

cpp_static_analysis:
  stage: Test
  needs: []
  image: registry.gitlab.com/ifollow-robotics/dev/docker
  allow_failure: true
  script:
    - |
      ########## Cpp Static Analysis job ##########
      . ./devops/config/static_code_analysis.sh
  artifacts:
    reports:
      codequality: ./cppcheck.json
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: always

test_non_regression:
  stage: Test
  script:
    - |
      ######### Build and run unit tests ##########
      PROJECT_DIR=$PWD
      cd /builds/
      ROOT_DIR=$PWD
      mkdir -p ./catkin_ws/src/${CI_PROJECT_NAME}
      CATKIN_DIR=$ROOT_DIR/catkin_ws
      cp -r $PROJECT_DIR ./catkin_ws/src/

      ########## Clone ifollow deps projects in src directory

      which ssh-agent > /dev/null || ( apk update && apk add openssh-client git ) > /dev/null
      eval $(ssh-agent -s) > /dev/null
      echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null 2>&1
      mkdir -p ~/.ssh
      chmod 700 ~/.ssh
      echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
      cd $CATKIN_DIR/src

      ########## Get dependencies ##########

      function get_dep()
      {
          BRANCH_EXISTS=$(git ls-remote $1 --heads origin $CI_COMMIT_REF_NAME | wc -l)
          if [[ $BRANCH_EXISTS -eq 1 ]]; then
              git clone --depth 1 -b $CI_COMMIT_REF_NAME --recurse-submodules $1
          else
              git clone --depth 1 --recurse-submodules $1
          fi
      }

      get_dep git@gitlab.com:ifollow-robotics/common/ifollow_msgs.git
      get_dep git@gitlab.com:ifollow-robotics/common/tuw_msgs.git
      get_dep git@gitlab.com:ifollow-robotics/nav/nmp_modules.git
      get_dep git@gitlab.com:ifollow-robotics/common/ifollow_utils.git
      get_dep git@gitlab.com:ifollow-robotics/common/groups_utils.git
      get_dep git@gitlab.com:ifollow-robotics/common/percep_utils.git
      get_dep git@gitlab.com:ifollow-robotics/common/spline_tools.git
      get_dep git@gitlab.com:ifollow-robotics/nav/footprint_selectors.git
      get_dep git@gitlab.com:ifollow-robotics/driver/main_board.git
      get_dep git@gitlab.com:ifollow-robotics/common/iipp_recursive_approach.git
      get_dep git@gitlab.com:ifollow-robotics/mas/route_finder.git

      ########## Build ##########

      cd $CATKIN_DIR
      catkin config --init
      catkin config --extend /opt/ros/melodic
      if [ -s src/${CI_PROJECT_NAME}/.catkin_blacklist ]; then
          echo -e "\033[1;33m[WARN] Skip building the following pkgs:\033[0m";
          while read p; do
              echo "  - $p [marked]"
              catkin locate $p &> /dev/null && touch $(catkin locate $p)/CATKIN_IGNORE
              echo -e "\033[1;33m    \xE2\x9C\x94 IGNORED\033[0m";
          done < src/${CI_PROJECT_NAME}/.catkin_blacklist || true;
      fi
      export CPUS=$(echo "$(nproc) / 4" | bc)
      if [ "$CPUS" -eq 0 ]; then
          CPUS=1;
      fi

      catkin build -j"$CPUS" --mem-limit=25% --force-color --no-status \
        -DCMAKE_CXX_FLAGS="-DPACKAGE_ROS_VERSION=1 -DOCC_EXTRA_LOGS"

      catkin build -j"$CPUS" --mem-limit=25% --force-color --no-status \
        -DENABLE_COVERAGE=true \
        -DCMAKE_CXX_FLAGS="-DPACKAGE_ROS_VERSION=1 -DOCC_EXTRA_LOGS" \
        --catkin-make-args run_tests

      BUILD_TEST_RETURN=$?

      ########## Was building the tests successful? ##########

      if (( $BUILD_TEST_RETURN == 0 )); then
          echo -e "\e[32m\xE2\x9C\x94 Building tests was successful!\e[0m"
      else
          echo -e "\e[31;1mBuilding tests failed!!!\e[0m"
          exit 1
      fi

      ########## Check if all tests passed ##########

      set +e                 # disable exitting on error temporarily
      catkin_test_results
      TEST_RETURN=$?
      set -e                 # enable exitting on error

      ########## Retrieve tests reports ##########

      mkdir $PROJECT_DIR/tests_reports
      cd build
      for i in $(find . -type f -regex ".*test_results.*\.xml" -not -regex ".*rostest.*");
      do
          cp "$i" $PROJECT_DIR/tests_reports/$(echo "${i:2}" | tr / _);
      done

      ########## Generate coverage report ##########

      source $CATKIN_DIR/devel/setup.bash
      roscd $CI_PROJECT_NAME
      SRC_DIR=$PWD
      cd $CATKIN_DIR

      gcovr --xml-pretty --exclude-unreachable-branches --exclude-directories='./catkin_ws/src/${CI_PROJECT_NAME}/test' --print-summary\
         -o $PROJECT_DIR/tests_reports/coverage.xml --filter $SRC_DIR

      ########## Exit with success or failure based on TEST_RETURN ##########

      if (( $TEST_RETURN == 0 )); then
        echo -e "\e[32m\xE2\x9C\x94 All tests were successful!\e[0m"
      else
        echo -e "\e[31;1mNot all tests were successful! Check the tests' reports\e[0m"
        catkin_test_results --verbose
        if [[ $(catkin_test_results) ]]; then exit 0; else exit 1; fi
        exit 1
      fi
      exit 0
  coverage: /^\s*lines:\s*\d+.\d+\%/
  artifacts:
    paths:
      - tests_reports/*.xml
    name: ${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHA}
    when: always
    reports:
      junit: tests_reports/*.xml
      coverage_report:
        coverage_format: cobertura
        path: tests_reports/coverage.xml
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: always
