// -*- mode: c++; coding: utf-8; tab-width: 4 -*-
/*******************************************************************************
* 2-Clause BSD License
*
* Copyright (c) 2019, iFollow Robotics
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
*   list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*   this list of conditions and the following disclaimer in the documentation
*   and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* Authors: José Mendes (mendesfilho@pm.me)
*******************************************************************************/

#ifndef INTERACTIVE_GRAPH_H
#define INTERACTIVE_GRAPH_H

namespace interactive_graph {

struct Segment
{
    int32_t id;
    Eigen::Vector2d start;
    Eigen::Vector2d origin;
    Eigen::Vector2d end;
    bool directed;
    bool directed_towards_end;
    bool directed_towards_start;
    bool mark_as_bidirected;
    std::shared_ptr<visualization_msgs::InteractiveMarker> end_marker;
    std::shared_ptr<visualization_msgs::InteractiveMarker> start_marker;
    std::shared_ptr<visualization_msgs::InteractiveMarker> arrow_to_end;
    std::shared_ptr<visualization_msgs::InteractiveMarker> arrow_to_start;
    std::shared_ptr<visualization_msgs::InteractiveMarker> width_line;
    bool arrow_to_end_selected;
    bool arrow_to_start_selected;
    int8_t end_marker_selected;
    int8_t start_marker_selected;
    std::vector<int32_t> end_neigs;
    std::vector<int32_t> start_neigs;
    bool visited;
    double width;
    double init_width;
};

struct Area
{
    int32_t id;
    std::vector<std::shared_ptr<visualization_msgs::InteractiveMarker>> vertices_markers;
    std::shared_ptr<visualization_msgs::InteractiveMarker> validation_button;
    bool finished;
    std::vector<uint8_t> type;
};

class Color
{
public:
    static std_msgs::ColorRGBA white;
    static std_msgs::ColorRGBA black;
    static std_msgs::ColorRGBA gray;
    static std_msgs::ColorRGBA red;
    static std_msgs::ColorRGBA red2;
    static std_msgs::ColorRGBA green;
    static std_msgs::ColorRGBA blue;
    static std_msgs::ColorRGBA navy;
    static std_msgs::ColorRGBA light_blue;
    static std_msgs::ColorRGBA purple;
    static std_msgs::ColorRGBA magenta;
    static std_msgs::ColorRGBA orange_red;
    static std_msgs::ColorRGBA orange_yellow;
    static std_msgs::ColorRGBA good_green;
    static std_msgs::ColorRGBA pale_green;

    static std_msgs::ColorRGBA bottom;
    static std_msgs::ColorRGBA cover;
    static std_msgs::ColorRGBA top_plate;
    static std_msgs::ColorRGBA light_gray;
    static std_msgs::ColorRGBA gold;
    static std_msgs::ColorRGBA goldenrod;

    static inline std_msgs::ColorRGBA ColorRGBA_init(double const r = 1.0, double const g = 1.0, const double b = 1.0, double const a = 1.0)
    {
        std_msgs::ColorRGBA out;
        out.r = r;
        out.g = g;
        out.b = b;
        out.a = a;
        return out;
    };
};

class InteractiveGraph
{
public:
    InteractiveGraph(ros::NodeHandle);
    ~InteractiveGraph();
    void run();

    void updateValButton();
    void toggleValidationButton();
    std::map<int32_t, Area> areas;

private:
    ros::NodeHandle n_;
    ros::Subscriber graph_sub_;
    ros::Subscriber i_graph_sub_;
    ros::Subscriber graph_areas_sub_;
    ros::Subscriber i_graph_update_sub_;
    ros::Subscriber clicked_point_sub_;
    ros::Publisher  graph_pub_;
    ros::Publisher  i_graph_pub_;

    std::map<std::string, std::pair<ros::Publisher, ros::Publisher> > layers_pub_;
    ros::Publisher seg_ids_pub_;

    ros::ServiceServer save_srv_;
    bool viz_update_srv_called_;
    tf2_ros::TransformBroadcaster br_;

    void graphCB_(const ros::MessageEvent<const tuw_multi_robot_msgs::Graph> &event);
    void iGraphCB_(const ifollow_mas_msgs::Graph& msg);
    void iGraphUpdateCB_(const ifollow_mas_msgs::Graph& msg);
    void clickedPointCB_(const geometry_msgs::PointStamped& msg);
    void graphAreasCB_(const ifollow_mas_msgs::AreaArray& msg);
    void getDefaultValues_();

    tuw_multi_robot_msgs::Graph ori_graph_msg_;
    tuw_multi_robot_msgs::Graph new_graph_msg_;
    std::map<int32_t, Segment> segments_;
    Area* current_area;
    bool got_graph_;
    bool got_aug_graph_;
    bool got_graph_areas_;
    bool interac_width_;
    std::string path_to_save_;
    float min_save_period_;
    ros::Time last_save_;

    const std::string nname_;

    std::vector<double> map_to_world_tf_;

    int edge_mk_id_;
    int arrow_mk_id_;
    int line_list_mk_id_;
    int mk_id_;
    int robot_mk_id_;
    std::string robot_env_;
    int area_id_;
    int area_vertex_id_;

    ros::Publisher mkrs_pub_;
    visualization_msgs::MarkerArray static_mks_;

    visualization_msgs::InteractiveMarker makeRobotMarker_();
    void makeLoadedRobotMarker_();
    visualization_msgs::InteractiveMarker makeGraphEdgeMarker_(const geometry_msgs::Point& position);
    visualization_msgs::InteractiveMarker makeGraphVertex_(const geometry_msgs::Point& start, const geometry_msgs::Point& end);
    visualization_msgs::InteractiveMarker makeWidthLine_(double width, double length, const std::string& frame_id);
    visualization_msgs::InteractiveMarker makeAreaVertex_(const geometry_msgs::Point& position);
    visualization_msgs::InteractiveMarker makeAreaValidationButton_(const geometry_msgs::Point& position);
    void createInteractiveGraph_();

    bool polarize_(Segment* ori_seg, Segment* prev_seg, Segment* seg);
    void polarizeTowardsStart_(Segment* ori_seg);
    void polarizeTowardsEnd_(Segment* ori_seg);
    bool unPolarize_(Segment* ori_seg, Segment* prev_seg, Segment* seg);
    void unPolarizeTowardsEnd_(Segment* ori_seg);
    void unPolarizeTowardsStart_(Segment* ori_seg);
    void setBidirected_(Segment* seg);
    void setDirected_(Segment* seg, bool towards_start);
    void updateGraph_();
    void publishGraph_();
    void save_(const std::string& post_fix, const std::string& frame_id);

    void reassignIDs_(std::map<int32_t, Segment>& tf_segments);

    void transformSegs_(std::map<int32_t, Segment>& tf_segments,
                        Eigen::Vector2d& origin,
                        const std::string& frame_id);

    bool saveState_(ifollow_ll_msgs::SetString::Request& req,
                     ifollow_ll_msgs::SetString::Response& res);

    void vizIfollowGraph_();

    void initTfs_();

    bool init_pub_;

    void unselectEdges_(const std::string& edge_a,
                        const std::string& edge_b);

    std::vector<std::string> shortestPath_(const std::string& edge_a,
                                         const std::string& edge_b,
                                         const Segment* seg_a,
                                         const Segment* seg_b);

    std::vector<std::string> shortestPathDirec_(const std::string& edge_a, const std::string& edge_b, int32_t id);

    /**
     * @brief Create a new node in the graph
     *
     * @param first_marker marker associated with the new node
     * @param seg_first_marker segment associated with the node
     *
     */
    void createNode_(std::shared_ptr<visualization_msgs::InteractiveMarker> first_marker, Segment* seg_first_marker);

    /**
     * @brief Remove the input node from the graph
     *
     * @param segment_id id of the segment to be removed
     * @param first_marker marker associated with the node to be removed
     * @param seg_first_marker segment associated with the node
     */
    void removeNode_(int segment_id, std::shared_ptr<visualization_msgs::InteractiveMarker> first_marker, Segment* seg_first_marker);

    /**
     * @brief Align all the segment of the two inputs nodes
     *
     * @param first_marker_name first marker name to start the segments alignment
     * @param sec_marker_name last marker name to end the segments alignment
     * @param seg_first_marker first segment to start the alignment
     * @param seg_second_marker last segment to end the alignment
     *
     */
    void alignSegmentsBetweenNodes_(std::string first_marker_name, std::string sec_marker_name, Segment* seg_first_marker, Segment* seg_second_marker);

    /**
     * @brief Create a Segments Between Nodes_ object
     *
     * @param first_marker
     * @param second_marker
     * @param seg_first_marker
     * @param seg_second_marker
     */
    void createSegmentsBetweenNodes_(std::shared_ptr<visualization_msgs::InteractiveMarker> first_marker, std::shared_ptr<visualization_msgs::InteractiveMarker> second_marker, Segment* seg_first_marker, Segment* seg_second_marker);

    /**
     * @brief Check If Segment Already Exist Between the two input Nodes
     *
     * @param first_marker
     * @param second_marker
     * @param seg_first_marker
     * @param seg_second_marker
     */
    bool checkIfSegmentAlreadyExistBetweenNodes_(std::shared_ptr<visualization_msgs::InteractiveMarker> first_marker, std::shared_ptr<visualization_msgs::InteractiveMarker> second_marker, Segment* seg_first_marker, Segment* seg_second_marker);

    /**
     * @brief Update the arrow associated with the input segment
     *
     * @param in_segment input segment to create the arrow
     * @param tf_array
     */
    void updateArrowForSegment(Segment& in_segment, std::vector<geometry_msgs::TransformStamped>& tf_array);

    /**
     * @brief Update the arrow associated with the input segment
     *
     * @param in_segment input segment to create the arrow
     * @param tf_array
     */
    void createArrowForSegment(Segment& in_segment);


    /**
     * @brief Create a multiple nodes on line
     * 
     * @param first_marker_point 
     * @param seg_first_marker 
     * @param new_marker_point 
     * @param distance 
     */
    void createNodesOnLine_(const geometry_msgs::Point & first_marker_point, Segment* seg_first_marker, const geometry_msgs::Point& new_marker_point, float distance);

    /**
     * @brief Create a Single Node object
     * 
     * @param prev_marker_point 
     * @param seg_first_marker 
     * @param new_marker_point 
     */
    void createSingleNode_(const geometry_msgs::Point &prev_marker_point, Segment* seg_first_marker, const geometry_msgs::Point& new_marker_point);

    /**
     * @brief Create a Single Segment Between Nodes object
     * 
     * @param first_marker 
     * @param second_marker 
     * @param seg_first_marker 
     * @param seg_second_marker 
     */
    void createSingleSegmentBetweenNodes(std::shared_ptr<visualization_msgs::InteractiveMarker> first_marker, std::shared_ptr<visualization_msgs::InteractiveMarker> second_marker, Segment* seg_first_marker, Segment* seg_second_marker);

    /**
     * @brief Create Multiple Segments Between Nodes object
     * 
     * @param first_marker 
     * @param second_marker 
     * @param seg_first_marker 
     * @param seg_second_marker 
     * @param distance 
     */
    void createMultipleSegmentsBetweenNodes(std::shared_ptr<visualization_msgs::InteractiveMarker> first_marker, std::shared_ptr<visualization_msgs::InteractiveMarker> second_marker, Segment* seg_first_marker, Segment* seg_second_marker, float distance);

    /**
     * @brief Aligne the input edges list
     *
     * @param edges_list
     */
    void align_(const std::vector<std::string>& edges_list);

    std::mutex              edge_pose_update_mutex_;
    bool                    edge_pose_update_;
    std::mutex              vertex_width_update_mutex_;
    bool                    vertex_width_update_;
    std::mutex              augmented_graph_mutex_;
    int8_t                  clicked_point_update_;
    geometry_msgs::Point    clicked_point_;
    std::mutex              clicked_point_update_mutex_;

    std::shared_ptr<interactive_markers::InteractiveMarkerServer> interactive_mk_server_;
    std::vector<std::shared_ptr<visualization_msgs::InteractiveMarker>> edges_markers_;
    std::vector<std::string > edges_markers_names_;

    ifollow_mas_msgs::Graph augmented_graph_;
    ifollow_mas_msgs::Vertex default_vertex_;

};

/**
 * @brief Check if the segment is a deadEnd type or not
 *
 * @param Segment* prev_seg
 * @param Segment* seg
 * @param std::map<int32_t, Segment>* segments
 * @return bool true is the segment is deadEnd, false otherwise
 *
 */
bool isDeadend(Segment* prev_seg, Segment* seg, std::map<int32_t, Segment>* segments);

/**
 * @brief
 *
 * @param feedback
 * @param segments
 * @param server
 */
void graphVertexProcessFb(const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback, std::map<int32_t, Segment>* segments,
                          std::shared_ptr<interactive_markers::InteractiveMarkerServer>& server);

/**
 * @brief   Create a new sphere object
 *
 * @param   msg input interactive message
 * @param   scale scale of the new object
 * @param   color color or the new sphere
 * @return  visualization_msgs::Marker
 */
visualization_msgs::Marker makeSphere2_(visualization_msgs::InteractiveMarker &msg, double scale=1, std_msgs::ColorRGBA color=Color::red2);

/**
 * @brief   Create a new mesh on rviz
 *
 * @param mesh_path input mesh's path
 * @param scale scale of the object
 * @param r red color
 * @param g green color
 * @param b blue color
 * @param a transparency value
 * @param x x position [m]
 * @param y y position [m]
 * @param z z position [z]
 * @param qx x orientation
 * @param qy y orientation
 * @param qz z orientation
 * @param qw w orientation
 * @return visualization_msgs::Marker
 */
visualization_msgs::Marker makeMesh(std::string mesh_path, float scale, float r, float g, float b, float a, double x=0, double y=0, double z=0, double qx=0, double qy=0, double qz=0, double qw=1);

/**
 * @brief
 *
 * @param feedback
 * @param area
 * @param server
 * @param data
 */
void areaProcessFb(const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback, Area* area, std::shared_ptr<interactive_markers::InteractiveMarkerServer>& server, InteractiveGraph* data);

/**
 * @brief   Create a new a new sphere object
 *
 * @param   msg
 * @return  visualization_msgs::Marker
 */
visualization_msgs::Marker makeFlattenedSphere2_(visualization_msgs::InteractiveMarker &msg);

/**
 * @brief Update the visualization on rviz
 *
 * @param feedback
 * @param segments
 * @param server
 * @param vertex_width_update
 * @param vertex_width_update_mutex
 */
void graphVertexWidthProcessFb(const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback, std::map<int32_t, Segment>* segments,
                               std::shared_ptr<interactive_markers::InteractiveMarkerServer>& server, bool* vertex_width_update, std::mutex* vertex_width_update_mutex);

/**
 * @brief   Create an arrow object on rviz
 *
 * @param   start start point of the line
 * @param   end end point of the line
 * @param   scale scale of the new object
 * @return  visualization_msgs::Marker new object created
 */
visualization_msgs::Marker makeArrow_(const geometry_msgs::Point& start, const geometry_msgs::Point& end, float scale);

/**
 * @brief   Create a footprint marker
 *
 * @param   x displacement for the footprint in the x-axis
 * @param   y displacement for the footprint in the y-axis
 * @param   theta rotation for the footprint around z-axis
 * @return  visualization_msgs::Marker new object created
 */
visualization_msgs::Marker makeFooprintMarker(double x = 0.0, double y = 0.0, double theta = 0.0);

/**
 * @brief   Create a line object on rviz
 *
 * @param   width width of the line
 * @param   length length of the line
 * @return  visualization_msgs::Marker new object created
 */
visualization_msgs::Marker makeLine_(double width, double length);

/**
 * @brief
 *
 * @param feedback
 * @param segments
 * @param server
 * @param edge_pose_update
 * @param edge_pose_update_mutex
 * @param clicked_point
 * @param clicked_point_update
 * @param clicked_point_update_mutex
 * @param br
 */
void graphEdgeProcessFb(const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback, std::map<int32_t, Segment>* segments,
                        std::shared_ptr<interactive_markers::InteractiveMarkerServer>& server, bool* edge_pose_update, std::mutex* edge_pose_update_mutex,
                        geometry_msgs::Point* clicked_point, int8_t* clicked_point_update, std::mutex* clicked_point_update_mutex,
                        tf2_ros::TransformBroadcaster* br);

/**
 * @brief
 *
 * @param br
 * @param frame_id
 * @param non_origin_pt
 * @param origin_pt
 */
void updateTF(tf2_ros::TransformBroadcaster* br, const std::string& frame_id, const geometry_msgs::Point& non_origin_pt, const geometry_msgs::Point& origin_pt);

/**
 * @brief
 *
 * @param tf_array
 * @param frame_id
 * @param non_origin_pt
 * @param origin_pt
 */
void updateTF(std::vector<geometry_msgs::TransformStamped>& tf_array, const std::string& frame_id, const geometry_msgs::Point& non_origin_pt, const geometry_msgs::Point& origin_pt);

};

#endif
