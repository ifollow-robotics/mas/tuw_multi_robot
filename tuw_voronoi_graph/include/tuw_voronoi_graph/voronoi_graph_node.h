#ifndef VORONOI_GRAPH_NODE_H
#define VORONOI_GRAPH_NODE_H

namespace tuw_graph
{
    class VoronoiGeneratorNode : public voronoi_map::VoronoiPathGenerator, public VoronoiGraphGenerator, public Serializer
    {

        public:
            VoronoiGeneratorNode(ros::NodeHandle & n);

            ros::NodeHandle n_;
            ros::NodeHandle n_param_;
            void publishMap();
            void publishSegments();

        private:
            void globalMapCallback(const nav_msgs::OccupancyGrid::ConstPtr& _map);
            void createGraph(const nav_msgs::OccupancyGrid::ConstPtr& _map);
            bool loadGraph();
            bool loadCustomGraph(std::string _path);
            
            ros::Publisher                          pubVoronoiMapImage_;
            ros::Publisher                          pubSegments_;
            ros::Subscriber                         subMap_;
  
            std::string                             graphCachePath_;
            std::string                             customGraphPath_;
    
            std::string                             frameGlobalMap_;
            std::string                             frameVoronoiMap_;

            bool                                    publishVoronoiMapImage_;    /// for debuging
            Eigen::Vector2d                         origin_;
            float                                   resolution_;
            float                                   robot_width_;
            cv::Mat                                 map_;
            cv::Mat                                 distField_;
            cv::Mat                                 voronoiMap_;
            float                                   segment_length_;
            std::unique_ptr<float[]>                potential;
            std::vector<Segment>                    segments_;
            int                                     smoothing_;
            double                                  inflation_;
            float                                   crossingOptimization_;
            float                                   endSegmentOptimization_;
            bool                                    is_map_loaded_;
            bool                                    is_segment_published_;
            
            
            nav_msgs::OccupancyGrid voronoiMapImage_;
    };

}

#endif // TUW_NAV_COSTMAP_NODE_H

