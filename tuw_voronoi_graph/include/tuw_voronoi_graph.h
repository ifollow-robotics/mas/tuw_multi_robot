#include <eigen3/Eigen/Dense>
#include <memory>
#include <queue>
#include <string>
#include <limits>
#include <vector>
#include <iostream>
#include <sstream>
#include <mutex>
#include <algorithm>
#include <fstream>
#include <math.h>
#include <opencv/cv.hpp>
#include <opencv/cv.h>
#include <ros/ros.h>
#include <ros/package.h>

#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/filesystem.hpp>
#include <boost/functional/hash.hpp>
#include <boost/program_options.hpp>

#include "yaml-cpp/yaml.h"
#include <nav_msgs/OccupancyGrid.h>
#include <dxflib/dl_creationadapter.h>
#include <dxflib/dl_dxf.h>
#include <ifollow_ll_msgs/SetString.h>
#include <interactive_markers/interactive_marker_server.h>
#include <tf2_ros/transform_broadcaster.h>
#include <ifollow_mas_msgs/Graph.h>
#include <ifollow_mas_msgs/Vertex.h>
#include <ifollow_mas_msgs/AreaArray.h>
#include <tf2/LinearMath/Quaternion.h>
#include <visualization_msgs/MarkerArray.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PointStamped.h>
#include <ifollow_utils/conv_ops.h>
#include <ifollow_utils/str_ops.h>
#include <ifollow_utils/footprints.h>
#include <tuw_multi_robot_msgs/Vertex.h>
#include <tuw_multi_robot_msgs/Graph.h>

#define DEFAULT_MAP_NAME    "voronoi_map"

// LOCAL FILE
#include "tuw_voronoi_graph/segment.h"
#include "tuw_serialization/serializer.h"

#include "tuw_voronoi_graph/crossing.h"
#include "tuw_voronoi_graph/dxf_line_arc_parser.h"
#include "tuw_voronoi_graph/dxf_to_graph.h"
#include "tuw_voronoi_graph/interactive_graph.h"
#include "tuw_voronoi_graph/segment_expander.h"
#include "tuw_voronoi_map/thinning.h"
#include "tuw_voronoi_map/voronoi_path_generator.h"
#include "tuw_voronoi_graph/voronoi_graph_generator.h"
#include "tuw_voronoi_graph/voronoi_graph_node.h"
