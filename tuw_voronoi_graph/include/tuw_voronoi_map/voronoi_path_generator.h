
#ifndef _GENERATOR
#define _GENERATOR

#define DEFAULT_MAP_NAME    "voronoi_map"

namespace voronoi_map
{

class VoronoiPathGenerator
{
public:
    /**
     * @brief prepares the map by smoothing it
     * @param map the map
     * @param processed_map the smoothed map
     * @param erode_size the erode map [pix]
     */
    static void prepareMap(const cv::Mat& map, cv::Mat& processed_map, int erode_size);

    /**
     * @brief  computes the distance field of a map
     * @param map the _map
     * @param dist_field the resulting distance fieldW
     */
    static void computeDistanceField(const cv::Mat& map, cv::Mat& dist_field);

    /**
     * @brief computes the voronoi _map
     * @param dist_field the dist Field
     * @param voronoi_map the resulting voronoi map
     */
    static void computeVoronoiMap(const cv::Mat& dist_field, cv::Mat& voronoi_map);
};

}

#endif
