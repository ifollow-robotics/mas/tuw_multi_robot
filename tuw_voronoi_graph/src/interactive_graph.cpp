// -*- mode: c++; coding: utf-8; tab-width: 4 -*-
/*******************************************************************************
* 2-Clause BSD License
*
* Copyright (c) 2019, iFollow Robotics
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
*   list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*   this list of conditions and the following disclaimer in the documentation
*   and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* Authors: José Mendes (mendesfilho@pm.me)
*******************************************************************************/

#include <tuw_voronoi_graph.h>

namespace interactive_graph {

#define FG_L_RED "\033[0;91m"
#define RESET "\033[0m"

std_msgs::ColorRGBA Color::white            = Color::ColorRGBA_init();
std_msgs::ColorRGBA Color::black            = Color::ColorRGBA_init(0, 0, 0);
std_msgs::ColorRGBA Color::gray             = Color::ColorRGBA_init(0.3, 0.3, 0.3);
std_msgs::ColorRGBA Color::red              = Color::ColorRGBA_init(1, 0, 0);
std_msgs::ColorRGBA Color::red2             = Color::ColorRGBA_init(1, 0.5, 0.5);
std_msgs::ColorRGBA Color::green            = Color::ColorRGBA_init(0, 1, 0);
std_msgs::ColorRGBA Color::blue             = Color::ColorRGBA_init(0, 0, 1);
std_msgs::ColorRGBA Color::navy             = Color::ColorRGBA_init(0, 0, 0.5);
std_msgs::ColorRGBA Color::light_blue       = Color::ColorRGBA_init(77 / 255., 177 / 255., 219 / 255.);
std_msgs::ColorRGBA Color::purple           = Color::ColorRGBA_init(0.4157, 0.051, 0.6784);
std_msgs::ColorRGBA Color::magenta          = Color::ColorRGBA_init(0.7647, 0, 0.7647);
std_msgs::ColorRGBA Color::orange_red       = Color::ColorRGBA_init(1, 0.4824, 0);
std_msgs::ColorRGBA Color::orange_yellow    = Color::ColorRGBA_init(1, 0.6667, 0);
std_msgs::ColorRGBA Color::good_green       = Color::ColorRGBA_init(5 / 255., 200 / 255., 128 / 255.);
std_msgs::ColorRGBA Color::pale_green       = Color::ColorRGBA_init(152. / 255, 251. / 255, 152. / 255);

std_msgs::ColorRGBA Color::bottom       = Color::ColorRGBA_init(0.1, 0.4, 0.4);
std_msgs::ColorRGBA Color::cover        = Color::ColorRGBA_init(0.01, 0.4, 1.0);
std_msgs::ColorRGBA Color::top_plate    = Color::ColorRGBA_init(0.1, 0.4, 0.4);
std_msgs::ColorRGBA Color::light_gray   = Color::ColorRGBA_init(0.7, 0.7, 0.7);
std_msgs::ColorRGBA Color::gold         = Color::ColorRGBA_init(255 / 255., 215 / 255., 0);
std_msgs::ColorRGBA Color::goldenrod    = Color::ColorRGBA_init(218 / 255., 165 / 255., 32 / 255.);

// Overload operator<<() for vector<T> so we can easily print it
template <class T>
std::ostream& operator<< (std::ostream& os, const std::vector<T>& v)
{
    os << "[";
    for (typename std::vector<T>::const_iterator i = v.begin(); i != v.end(); ++i)
    {
        os << *i << ", ";
    }
    if (!v.empty())
    {
        os.seekp(-2, std::ios_base::end);
    }
    os << "]";
    return os;
}

/*************************************************************************************************/
InteractiveGraph::InteractiveGraph(ros::NodeHandle n)
    : n_(n)
    , got_graph_(false)
    , got_aug_graph_(false)
    , interactive_mk_server_(new interactive_markers::InteractiveMarkerServer("interactive_graph"))
    , edge_mk_id_(0)
    , arrow_mk_id_(0)
    , line_list_mk_id_(0)
    , interac_width_(true)
    , edge_pose_update_(false)
    , clicked_point_update_(0)
    , init_pub_(false)
    , mk_id_(0)
    , robot_mk_id_(0)
    , area_id_(0)
    , current_area(NULL)
    , area_vertex_id_(0)
    , got_graph_areas_(false)
    , nname_(ros::this_node::getName())
{
    if (const char* env_p = std::getenv("ROBOT_ENV"))
    {
        ROS_INFO_STREAM(nname_ << " ROBOT_ENV is: " << env_p);
        robot_env_ = std::string(env_p);
    }
    else
    {
        ROS_ERROR_STREAM(nname_ << " ROBOT_ENV is not defined");
        throw std::runtime_error("ROBOT_ENV is not defined");
    }

    n_.param("use_interac_width", interac_width_, interac_width_);

    path_to_save_ = getenv("HOME");
    path_to_save_ += "/.ifollow/cache/graph/" + robot_env_;

    n_.param("path_to_save", path_to_save_, path_to_save_);

    if (path_to_save_.back() != '/')
    {
        path_to_save_ += "/";
    }

    if (!n_.getParam("map_to_world", map_to_world_tf_))
    {
        ROS_ERROR_STREAM(nname_ << " parameter \"map_to_world\" wasn't found");
        throw std::runtime_error("parameter \"map_to_world\" wasn't found");
    }
    ROS_INFO_STREAM(nname_ << " map_to_world: " << map_to_world_tf_);

    std::string cmmd = "mkdir -p " + path_to_save_;
    system(cmmd.c_str());

    min_save_period_ = 2;
    n_.param("min_save_period", min_save_period_, min_save_period_);

    graph_sub_          = n_.subscribe("segments",                  1, &InteractiveGraph::graphCB_,         this);
    i_graph_sub_        = n_.subscribe("augmented_graph",           1, &InteractiveGraph::iGraphCB_,        this);
    graph_areas_sub_    = n_.subscribe("graph_areas",               1, &InteractiveGraph::graphAreasCB_,    this);
    i_graph_update_sub_ = n_.subscribe("augmented_graph_update",    1, &InteractiveGraph::iGraphUpdateCB_,  this);
    clicked_point_sub_  = n_.subscribe("clicked_point",             1, &InteractiveGraph::clickedPointCB_,  this);
    graph_pub_      = n_.advertise<tuw_multi_robot_msgs::Graph>("new_graph", 4, true);
    i_graph_pub_    = n_.advertise<ifollow_mas_msgs::Graph>("new_augmented_graph", 4, true);

    layers_pub_["turtle"].first                 = n_.advertise<visualization_msgs::Marker>("/turtle_on", 1, true);
    layers_pub_["turtle"].second                = n_.advertise<visualization_msgs::MarkerArray>("/turtle_on_text", 1, true);
    layers_pub_["max_vel"].first                = n_.advertise<visualization_msgs::Marker>("/vel_heatmap", 1, true);
    layers_pub_["max_vel"].second               = n_.advertise<visualization_msgs::MarkerArray>("/vel_text", 1, true);
    layers_pub_["pose0"].first                  = n_.advertise<visualization_msgs::Marker>("/pose0_heatmap", 1, true);
    layers_pub_["pose0"].second                 = n_.advertise<visualization_msgs::MarkerArray>("/pose0_text", 1, true);
    layers_pub_["pose1"].first                  = n_.advertise<visualization_msgs::Marker>("/pose1_heatmap", 1, true);
    layers_pub_["pose1"].second                 = n_.advertise<visualization_msgs::MarkerArray>("/pose1_text", 1, true);
    layers_pub_["odom0_twist"].first            = n_.advertise<visualization_msgs::Marker>("/odom0_twist_heatmap", 1, true);
    layers_pub_["odom0_twist"].second           = n_.advertise<visualization_msgs::MarkerArray>("/odom0_twist_text", 1, true);
    layers_pub_["precise_fp"].first             = n_.advertise<visualization_msgs::Marker>("/precise_fp_heatmap", 1, true);
    layers_pub_["precise_fp"].second            = n_.advertise<visualization_msgs::MarkerArray>("/precise_fp_text", 1, true);
    layers_pub_["beep"].first                   = n_.advertise<visualization_msgs::Marker>("/beep_heatmap", 1, true);
    layers_pub_["beep"].second                  = n_.advertise<visualization_msgs::MarkerArray>("/beep_text", 1, true);
    layers_pub_["obst_coll_hor"].first          = n_.advertise<visualization_msgs::Marker>("/obst_coll_hor_heatmap", 1, true);
    layers_pub_["obst_coll_hor"].second         = n_.advertise<visualization_msgs::MarkerArray>("/obst_coll_hor_text", 1, true);
    layers_pub_["laser_coll_hor"].first         = n_.advertise<visualization_msgs::Marker>("/laser_coll_hor_heatmap", 1, true);
    layers_pub_["laser_coll_hor"].second        = n_.advertise<visualization_msgs::MarkerArray>("/laser_coll_hor_text", 1, true);
    layers_pub_["cam_coll_hor"].first           = n_.advertise<visualization_msgs::Marker>("/cam_coll_hor_heatmap", 1, true);
    layers_pub_["cam_coll_hor"].second          = n_.advertise<visualization_msgs::MarkerArray>("/cam_coll_hor_text", 1, true);
    layers_pub_["safe_lift_inflation"].first    = n_.advertise<visualization_msgs::Marker>("/safe_lift_inflation_heatmap", 1, true);
    layers_pub_["safe_lift_inflation"].second   = n_.advertise<visualization_msgs::MarkerArray>("/safe_lift_inflation_text", 1, true);

    layers_pub_["force_alt_plan_use"].first = n_.advertise<visualization_msgs::Marker>("/force_alt_plan_use_heatmap", 1, true);
    layers_pub_["force_alt_plan_use"].second = n_.advertise<visualization_msgs::MarkerArray>("/force_alt_plan_use_text", 1, true);

    layers_pub_["allow_alt_plan_use"].first = n_.advertise<visualization_msgs::Marker>("/allow_alt_plan_use_heatmap", 1, true);
    layers_pub_["allow_alt_plan_use"].second = n_.advertise<visualization_msgs::MarkerArray>("/allow_alt_plan_use_text", 1, true);

    layers_pub_["allow_spontaneous_alt_plan_use"].first = n_.advertise<visualization_msgs::Marker>("/allow_spontaneous_alt_plan_use_heatmap", 1, true);
    layers_pub_["allow_spontaneous_alt_plan_use"].second = n_.advertise<visualization_msgs::MarkerArray>("/allow_spontaneous_alt_plan_use_text", 1, true);

    layers_pub_["use_line_on_floor_detection"].first = n_.advertise<visualization_msgs::Marker>("/use_line_on_floor_detection_heatmap", 1, true);
    layers_pub_["use_line_on_floor_detection"].second = n_.advertise<visualization_msgs::MarkerArray>("/use_line_on_floor_detection_text", 1, true);

    layers_pub_["bs_mode"].first = n_.advertise<visualization_msgs::Marker>("/bs_mode_heatmap", 1, true);
    layers_pub_["bs_mode"].second = n_.advertise<visualization_msgs::MarkerArray>("/bs_mode_text", 1, true);

    layers_pub_["use_mr_comm"].first = n_.advertise<visualization_msgs::Marker>("/use_mr_comm_heatmap", 1, true);
    layers_pub_["use_mr_comm"].second = n_.advertise<visualization_msgs::MarkerArray>("/use_mr_comm_text", 1, true);

    layers_pub_["amcl_localization_on"].first = n_.advertise<visualization_msgs::Marker>("/amcl_localization_on_heatmap", 1, true);
    layers_pub_["amcl_localization_on"].second = n_.advertise<visualization_msgs::MarkerArray>("/amcl_localization_on_text", 1, true);

    seg_ids_pub_ = n_.advertise<visualization_msgs::MarkerArray>("/seg_ids", 1, true);

    mkrs_pub_ = n_.advertise<visualization_msgs::MarkerArray>("static_markers", 1, true);

    makeRobotMarker_();
    makeLoadedRobotMarker_();

    ros::NodeHandle pn("~");
    save_srv_ = pn.advertiseService("save_state", &InteractiveGraph::saveState_, this);

    getDefaultValues_();
}

/*************************************************************************************************/
InteractiveGraph::~InteractiveGraph()
{
    save_("./.backup/", "world");
}

/*************************************************************************************************/
void InteractiveGraph::getDefaultValues_()
{

    bool allow_alt_plan_use = true;
    bool allow_spontaneous_alt_plan_use = false;
    bool force_alt_plan_use = false;
    n_.getParam("/graph_completion/default/allow_alt_plan_use", allow_alt_plan_use);
    n_.getParam("/graph_completion/default/allow_spontaneous_alt_plan_use", allow_spontaneous_alt_plan_use);
    n_.getParam("/graph_completion/default/force_alt_plan_use", force_alt_plan_use);
    default_vertex_.allow_alt_plan_use = allow_alt_plan_use;
    default_vertex_.allow_spontaneous_alt_plan_use = allow_spontaneous_alt_plan_use;
    default_vertex_.force_alt_plan_use = force_alt_plan_use;

    bool use_line_on_floor_detection = false;
    n_.getParam("/graph_completion/default/use_line_on_floor_detection", use_line_on_floor_detection);
    default_vertex_.use_line_on_floor_detection = use_line_on_floor_detection;

    int bs_mode = 0;
    n_.param("/graph_completion/default/bs_mode", bs_mode, bs_mode);
    default_vertex_.bs_mode = bs_mode;

    bool amcl_localization_on = false;
    n_.getParam("/graph_completion/default/amcl_localization_on", amcl_localization_on);
    default_vertex_.amcl_localization_on = amcl_localization_on;

    bool use_mr_comm = true;
    n_.getParam("/graph_completion/default/use_mr_comm", use_mr_comm);
    default_vertex_.use_mr_comm = use_mr_comm;

    bool turtle_on = false;
    n_.getParam("/graph_completion/default/turtle_on", turtle_on);
    default_vertex_.turtle_on = turtle_on;

    bool use_precise_fp = false;
    n_.getParam("/graph_completion/default/use_precise_fp", use_precise_fp);
    default_vertex_.use_precise_fp = use_precise_fp;

    bool beep_if_blinkers_on = false;
    n_.getParam("/graph_completion/default/beep_if_blinkers_on", beep_if_blinkers_on);
    default_vertex_.beep_if_blinkers_on = beep_if_blinkers_on;

    default_vertex_.safe_lift_inflation = -1;
    n_.param("/graph_completion/default/safe_lift_inflation", default_vertex_.safe_lift_inflation, default_vertex_.safe_lift_inflation);

    n_.param("/graph_completion/default/allowed_max_lvel", default_vertex_.allowed_max_lvel, default_vertex_.allowed_max_lvel);

    if (default_vertex_.allowed_max_lvel == -1)
    {
        std::string key;
        std::string lp_name;
        if (n_.searchParam("move_base/base_local_planner", key))
        {
            n_.getParam(key, lp_name);
            lp_name = str_ops::strReplaceAll(lp_name, "::", "/");
            std::string delimiter("/");
            lp_name = lp_name.substr(
                lp_name.find_last_of(delimiter) + delimiter.length(),
                lp_name.length());

            if (n_.searchParam("move_base/" + lp_name + "/max_vel_x", key))
            {
                n_.getParam(key, default_vertex_.allowed_max_lvel);
                ROS_INFO_STREAM(nname_ << " param " << key << " value: " << default_vertex_.allowed_max_lvel);
            }
            else
            {
                default_vertex_.allowed_max_lvel = 1.0;
            }
        }
        else
        {
            default_vertex_.allowed_max_lvel = 1.0;
        }
    }

    n_.param("/graph_completion/default/multi_robot_min_dist", default_vertex_.multi_robot_min_dist, default_vertex_.multi_robot_min_dist);
    if (default_vertex_.multi_robot_min_dist == -1)
    {
        std::string key;
        if (n_.searchParam("move_base/coll_checker/custom_obstacles/min_allowed_dist", key))
        {
            n_.getParam(key, default_vertex_.multi_robot_min_dist);
            ROS_INFO_STREAM(nname_ << " param " << key << " value: " << default_vertex_.multi_robot_min_dist);
        }
        else
        {
            default_vertex_.multi_robot_min_dist = 1.0;
        }
    }

    n_.param("/graph_completion/default/laser_min_dist", default_vertex_.laser_min_dist, default_vertex_.laser_min_dist);
    if (default_vertex_.laser_min_dist == -1)
    {
        std::string key;
        if (n_.searchParam("move_base/coll_checker/obstacle_layer/min_allowed_dist", key))
        {
            n_.getParam(key, default_vertex_.laser_min_dist);
            ROS_INFO_STREAM(nname_ << " param " << key << " value: " << default_vertex_.laser_min_dist);
        }
        else
        {
            default_vertex_.laser_min_dist = 1.0;
        }
    }

    n_.param("/graph_completion/default/cam_min_dist", default_vertex_.cam_min_dist, default_vertex_.cam_min_dist);
    if (default_vertex_.cam_min_dist == -1)
    {
        std::string key;
        if (n_.searchParam("move_base/coll_checker/camera/activated/scan/min_allowed_dist", key))
        {
            n_.getParam(key, default_vertex_.cam_min_dist);
            ROS_INFO_STREAM(nname_ << " param " << key << " value: " << default_vertex_.cam_min_dist);
        }
        else
        {
            default_vertex_.cam_min_dist = 1.0;
        }
    }
}

/*************************************************************************************************/
bool isDeadend(Segment* prev_seg, Segment* seg, std::map<int32_t, Segment>* segments)
{
    if (seg->visited)
    {
        return false;
    }

    if (std::find(seg->end_neigs.begin(), seg->end_neigs.end(), prev_seg->id) != seg->end_neigs.end())
    {
        if (seg->start_neigs.size() == 0)
        {
            return true;
        }
        else
        {
            int nb_dead_ends = 0;
            for (auto id : seg->start_neigs)
            {
                seg->visited = true;
                if (isDeadend(seg, &segments->at(id), segments))
                {
                    nb_dead_ends++;
                }
            }

            if (nb_dead_ends == seg->start_neigs.size())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    if (std::find(seg->start_neigs.begin(), seg->start_neigs.end(), prev_seg->id) != seg->start_neigs.end())
    {
        if (seg->end_neigs.size() == 0)
        {
            return true;
        }
        else
        {
            int nb_dead_ends = 0;
            for (auto id : seg->end_neigs)
            {
                seg->visited = true;
                if (isDeadend(seg, &segments->at(id), segments))
                {
                    nb_dead_ends++;
                }
            }
            if (nb_dead_ends == seg->end_neigs.size())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    return true;
}

/*************************************************************************************************/
void updateTF(tf2_ros::TransformBroadcaster* br, const std::string& frame_id, const geometry_msgs::Point& non_origin_pt, const geometry_msgs::Point& origin_pt)
{
    geometry_msgs::TransformStamped transformStamped;

    transformStamped.header.stamp = ros::Time::now();
    transformStamped.header.frame_id = "world";
    transformStamped.child_frame_id = frame_id;
    transformStamped.transform.translation.x = origin_pt.x;
    transformStamped.transform.translation.y = origin_pt.y;
    transformStamped.transform.translation.z = origin_pt.z;
    double yaw = atan2((non_origin_pt.y - origin_pt.y), (non_origin_pt.x - origin_pt.x));
    tf2::Quaternion q;
    q.setRPY(0, 0, yaw);
    transformStamped.transform.rotation.x = q.x();
    transformStamped.transform.rotation.y = q.y();
    transformStamped.transform.rotation.z = q.z();
    transformStamped.transform.rotation.w = q.w();
    br->sendTransform(transformStamped);
}

/*************************************************************************************************/
void updateTF(std::vector<geometry_msgs::TransformStamped>& tf_array,
              const std::string& frame_id, const geometry_msgs::Point& non_origin_pt,
              const geometry_msgs::Point& origin_pt)
{
    geometry_msgs::TransformStamped transformStamped;

    transformStamped.header.stamp = ros::Time::now();
    transformStamped.header.frame_id = "world";
    transformStamped.child_frame_id = frame_id;
    transformStamped.transform.translation.x = origin_pt.x;
    transformStamped.transform.translation.y = origin_pt.y;
    transformStamped.transform.translation.z = origin_pt.z;
    double yaw = atan2((non_origin_pt.y - origin_pt.y), (non_origin_pt.x - origin_pt.x));
    tf2::Quaternion q;
    q.setRPY(0, 0, yaw);
    transformStamped.transform.rotation.x = q.x();
    transformStamped.transform.rotation.y = q.y();
    transformStamped.transform.rotation.z = q.z();
    transformStamped.transform.rotation.w = q.w();
    tf_array.push_back(transformStamped);
}

/*************************************************************************************************/
void areaProcessFb(const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback, Area* area,
                   std::shared_ptr<interactive_markers::InteractiveMarkerServer>& server, InteractiveGraph* data)
{
    switch (feedback->event_type)
    {
    case visualization_msgs::InteractiveMarkerFeedback::BUTTON_CLICK:

        data->toggleValidationButton();
        break;

    case visualization_msgs::InteractiveMarkerFeedback::POSE_UPDATE:

        if (area->finished)
            return;

        for (auto vert : area->vertices_markers)
        {
            if (vert->name == feedback->marker_name)
            {
                vert->pose = feedback->pose;
            }
        }
        data->updateValButton();
        server->applyChanges();
        break;
    }
}

/*************************************************************************************************/
void graphVertexProcessFb(const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback, std::map<int32_t, Segment>* segments,
                          std::shared_ptr<interactive_markers::InteractiveMarkerServer>& server)
{
    switch (feedback->event_type)
    {
    case visualization_msgs::InteractiveMarkerFeedback::BUTTON_CLICK:
    {
        for (auto& [id, seg] : *segments)
        {
            if (seg.arrow_to_end->name == feedback->marker_name)
            {
                ROS_INFO_STREAM("BUTTON_CLICK on arrow_to_end of seg " << seg.id);

                if (!seg.directed_towards_end)
                {
                    bool prev_selected = seg.arrow_to_end_selected;
                    seg.arrow_to_end_selected = ((false == seg.arrow_to_start_selected) && (false == seg.arrow_to_end_selected)) ? true : false;
                    if (seg.arrow_to_end_selected && !prev_selected)
                    {
                        visualization_msgs::InteractiveMarker arrow_to_end;
                        server->get(seg.arrow_to_end->name, arrow_to_end);
                        arrow_to_end.controls.back().markers.back().color.r = 1.0;
                        arrow_to_end.controls.back().markers.back().color.g = 0.0;
                        arrow_to_end.controls.back().markers.back().color.b = 0.0;
                        server->erase(arrow_to_end.name);
                        server->insert(arrow_to_end);
                        server->setCallback(arrow_to_end.name, boost::bind(&graphVertexProcessFb, _1, segments, server));
                        server->applyChanges();
                    }
                    else if (prev_selected)
                    {
                        visualization_msgs::InteractiveMarker arrow_to_end;
                        server->get(seg.arrow_to_end->name, arrow_to_end);
                        arrow_to_end.controls.back().markers.back().color.r = 0.0;
                        arrow_to_end.controls.back().markers.back().color.g = 0.7;
                        arrow_to_end.controls.back().markers.back().color.b = 1.0;
                        server->erase(arrow_to_end.name);
                        server->insert(arrow_to_end);
                        server->setCallback(arrow_to_end.name, boost::bind(&graphVertexProcessFb, _1, segments, server));
                        server->applyChanges();
                    }
                    else
                    {
                        // TODO
                    }
                }
                else if (!seg.directed_towards_start)
                {
                    // seg.arrow_to_end_selected = !seg.arrow_to_start_selected && !seg.arrow_to_end_selected ? true : false;
                    // click on a arrow on the same direction as the vertex
                    // this means the user wants to make this segment bidirected
                    // let's make it bidirected if:
                    // 1. there is only one neigh to arrow's direc and that neig
                    //    is bidirected
                    // 2. there is more then one neig. on that direc. and at
                    //    least 2 of them are are not dead ends
                    // if (seg.arrow_to_end_selected)
                    // {
                    // bool mark_as_bidirected = false;
                    seg.mark_as_bidirected = false;

                    if ((seg.end_neigs.size() == 1) &&
                        (segments->find(seg.end_neigs[0]) != segments->end()) &&
                        (segments->at(seg.end_neigs.front()).directed == false))
                    {
                        seg.mark_as_bidirected = true;
                    }
                    // mark_as_bidirected = true;
                    else if (seg.end_neigs.size() >= 2)
                    {
                        int n_non_dead_end = 0;
                        for (const auto& n_id : seg.end_neigs)
                        {
                            if (false == isDeadend(&seg, &segments->at(n_id), segments))
                            {
                                n_non_dead_end++;

                                if (n_non_dead_end >= 2)
                                {
                                    break;
                                }
                            }
                        }
                        if (n_non_dead_end >= 2)
                        {
                            seg.mark_as_bidirected = true;
                        }
                    }
                    else
                    {
                        // TODO
                    }
                }
                else
                {
                    // TODO
                }
            }
            else if (seg.arrow_to_start->name == feedback->marker_name)
            {
                ROS_INFO_STREAM("BUTTON_CLICK on arrow_to_start of seg " << seg.id);
                if (!seg.directed_towards_start)
                {
                    bool prev_selected = seg.arrow_to_start_selected;
                    seg.arrow_to_start_selected = !seg.arrow_to_end_selected && !seg.arrow_to_start_selected ? true : false;
                    if ((true == seg.arrow_to_start_selected) && (false == !prev_selected))
                    {
                        visualization_msgs::InteractiveMarker arrow_to_start;
                        server->get(seg.arrow_to_start->name, arrow_to_start);
                        arrow_to_start.controls.back().markers.back().color.r = 1.0;
                        arrow_to_start.controls.back().markers.back().color.g = 0.0;
                        arrow_to_start.controls.back().markers.back().color.b = 0.0;
                        server->erase(arrow_to_start.name);
                        server->insert(arrow_to_start);
                        server->setCallback(arrow_to_start.name, boost::bind(&graphVertexProcessFb, _1, segments, server));
                        server->applyChanges();
                    }
                    else if (true == prev_selected)
                    {
                        visualization_msgs::InteractiveMarker arrow_to_start;
                        server->get(seg.arrow_to_start->name, arrow_to_start);
                        arrow_to_start.controls.back().markers.back().color.r = 0.0;
                        arrow_to_start.controls.back().markers.back().color.g = 0.7;
                        arrow_to_start.controls.back().markers.back().color.b = 1.0;
                        server->erase(arrow_to_start.name);
                        server->insert(arrow_to_start);
                        server->setCallback(arrow_to_start.name, boost::bind(&graphVertexProcessFb, _1, segments, server));
                        server->applyChanges();
                    }
                    else
                    {
                        // TODO
                    }
                }
                else if (false == seg.directed_towards_end)
                {
                    // seg.arrow_to_start_selected = !seg.arrow_to_end_selected && !seg.arrow_to_start_selected ? true : false;
                    // click on a arrow on the same direction as the vertex
                    // this means the user wants to make this segment bidirected
                    // let's make it bidirected if:
                    // 1. there is only one neigh to arrow's direc and that neig
                    //    is bidirected
                    // 2. there is more then one neig. on that direc. and at
                    //    least 2 of them are are not dead ends
                    // if (seg.arrow_to_start_selected)
                    // {

                    seg.mark_as_bidirected = false;
                    if (seg.start_neigs.size() == 1 &&
                        segments->find(seg.start_neigs[0]) != segments->end() &&
                        segments->at(seg.start_neigs.front()).directed == false)
                    {
                        seg.mark_as_bidirected = true;
                    }
                    else if (seg.start_neigs.size() >= 2)
                    {
                        int n_non_dead_end = 0;
                        for (const auto& n_id : seg.start_neigs)
                        {
                            if (!isDeadend(&seg, &segments->at(n_id), segments))
                            {
                                n_non_dead_end++;
                            }
                            if (n_non_dead_end >= 2)
                            {
                                break;
                            }
                        }
                        if (n_non_dead_end >= 2)
                        {
                            seg.mark_as_bidirected = true;
                        }
                    }
                    else
                    {
                        // TODO
                    }
                }
            }
        }
    }
    }
}

/*************************************************************************************************/
void graphVertexWidthProcessFb(const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback, std::map<int32_t, Segment>* segments,
                               std::shared_ptr<interactive_markers::InteractiveMarkerServer>& server, bool* vertex_width_update, std::mutex* vertex_width_update_mutex)
{
    switch (feedback->event_type)
    {
    case visualization_msgs::InteractiveMarkerFeedback::POSE_UPDATE:
    {
        for (auto& [id, seg] : *segments)
        {
            if (seg.width_line->name == feedback->marker_name)
            {
                seg.width = std::abs(seg.init_width + feedback->pose.position.y * 2.);

                {
                    std::scoped_lock lock(*vertex_width_update_mutex);
                    *vertex_width_update = true;
                }
            }
        }
    }
    }
    server->applyChanges();
}

/*************************************************************************************************/
void graphEdgeProcessFb(const visualization_msgs::InteractiveMarkerFeedbackConstPtr &feedback, std::map<int32_t, Segment>* segments,
                        std::shared_ptr<interactive_markers::InteractiveMarkerServer>& server, bool* edge_pose_update, std::mutex* edge_pose_update_mutex,
                        geometry_msgs::Point* clicked_point, int8_t* clicked_point_update, std::mutex* clicked_point_update_mutex,
                        tf2_ros::TransformBroadcaster* br)
{
    switch (feedback->event_type)
    {
    case visualization_msgs::InteractiveMarkerFeedback::BUTTON_CLICK:
    {
        bool button_set = false;
        ROS_INFO_STREAM(" BUTTON_CLICK " << feedback->marker_name);
        clicked_point->x = feedback->pose.position.x;
        clicked_point->y = feedback->pose.position.y;

        for (auto& [id, seg] : *segments)
        {
            if (!button_set)
            {
                visualization_msgs::InteractiveMarker marker;

                if (seg.end_marker->name.compare(feedback->marker_name) == 0)
                {
                    // Update the marker status
                    seg.end_marker_selected = (seg.end_marker_selected + 1) % 3;

                    server->get(seg.end_marker->name, marker);

                    if (1 == seg.end_marker_selected)
                    {
                        marker.controls.back().markers.back().color.r = 1.0;
                        marker.controls.back().markers.back().color.g = 0.0;
                        marker.controls.back().markers.back().color.b = 0.0;
                        marker.controls.back().markers.back().color.a = 1.0;
                    }
                    else if (2 == seg.end_marker_selected)
                    {
                        marker.controls.back().markers.back().color.r = 0.0;
                        marker.controls.back().markers.back().color.g = 0.0;
                        marker.controls.back().markers.back().color.b = 1.0;
                        marker.controls.back().markers.back().color.a = 1.0;
                        *clicked_point_update = 1;
                    }
                    else
                    {
                        marker.controls.back().markers.back().color.r = 0.0;
                        marker.controls.back().markers.back().color.g = 0.0;
                        marker.controls.back().markers.back().color.b = 0.0;
                        marker.controls.back().markers.back().color.a = 0.4;
                        *clicked_point_update = 0;
                    }

                    server->erase(marker.name);
                    server->insert(marker);
                    server->setCallback(marker.name, boost::bind(&graphEdgeProcessFb, _1, segments, server, edge_pose_update, edge_pose_update_mutex, clicked_point, clicked_point_update, clicked_point_update_mutex, br));

                    server->applyChanges();
                    button_set = true;
                }
                else if (seg.start_marker->name.compare(feedback->marker_name) == 0)
                {
                    seg.start_marker_selected = (seg.start_marker_selected + 1) % 3;

                    server->get(seg.start_marker->name, marker);

                    if (1 == seg.start_marker_selected)
                    {
                        marker.controls.back().markers.back().color.r = 1.0;
                        marker.controls.back().markers.back().color.g = 0.0;
                        marker.controls.back().markers.back().color.b = 0.0;
                        marker.controls.back().markers.back().color.a = 1.0;
                    }
                    else if (2 == seg.start_marker_selected)
                    {
                        marker.controls.back().markers.back().color.r = 0.0;
                        marker.controls.back().markers.back().color.g = 0.0;
                        marker.controls.back().markers.back().color.b = 1.0;
                        marker.controls.back().markers.back().color.a = 1.0;
                        *clicked_point_update = 1;
                    }
                    else
                    {
                        marker.controls.back().markers.back().color.r = 0.0;
                        marker.controls.back().markers.back().color.g = 0.0;
                        marker.controls.back().markers.back().color.b = 0.0;
                        marker.controls.back().markers.back().color.a = 0.4;
                        *clicked_point_update = 0;
                    }

                    server->erase(marker.name);
                    server->insert(marker);
                    server->setCallback(marker.name, boost::bind(&graphEdgeProcessFb, _1, segments, server, edge_pose_update, edge_pose_update_mutex, clicked_point, clicked_point_update, clicked_point_update_mutex, br));
                    server->applyChanges();
                    button_set = true;
                }
                else
                {
                    // Do nothing, because neither start nor end
                }
            }
        }
        break;
    }
    case visualization_msgs::InteractiveMarkerFeedback::POSE_UPDATE:
    {
        for (auto& [id, seg] : *segments)
        {
            bool update = false;

            if (seg.end_marker->name == feedback->marker_name)
            {
                seg.end.x() = feedback->pose.position.x;
                seg.end.y() = feedback->pose.position.y;

                seg.end_marker->pose.position.x = feedback->pose.position.x;
                seg.end_marker->pose.position.y = feedback->pose.position.y;

                update = true;
            }
            else if (seg.start_marker->name == feedback->marker_name)
            {
                seg.start.x() = feedback->pose.position.x;
                seg.start.y() = feedback->pose.position.y;

                seg.start_marker->pose.position.x = feedback->pose.position.x;
                seg.start_marker->pose.position.y = feedback->pose.position.y;

                update = true;
            }
            else
            {
                // Do nothing
            }

            if (update)
            {
                geometry_msgs::Point arrow_to_end_end, arrow_to_end_start, arrow_to_start_start, arrow_to_start_end;
                visualization_msgs::InteractiveMarker arrow_to_start, arrow_to_end;

                arrow_to_start_start.x  = (seg.start.x() * .5) + (seg.end.x() * .5);
                arrow_to_start_start.y  = (seg.start.y() * .5) + (seg.end.y() * .5);
                arrow_to_end_start.x    = (seg.start.x() * .5) + (seg.end.x() * .5);
                arrow_to_end_start.y    = (seg.start.y() * .5) + (seg.end.y() * .5);

                arrow_to_start_end.x    = seg.start.x();
                arrow_to_start_end.y    = seg.start.y();
                arrow_to_end_end.x      = seg.end.x();
                arrow_to_end_end.y      = seg.end.y();

                if (NULL != seg.arrow_to_end)
                {
                    seg.arrow_to_end->controls.back().markers.back().points[0] = arrow_to_end_start;
                    seg.arrow_to_end->controls.back().markers.back().points[1] = arrow_to_end_end;

                    server->get(seg.arrow_to_end->name, arrow_to_end);
                    arrow_to_end.controls.back().markers.back().points[0] = arrow_to_end_start;
                    arrow_to_end.controls.back().markers.back().points[1] = arrow_to_end_end;
                    server->erase(arrow_to_end.name);
                    server->insert(arrow_to_end);

                    server->setCallback(arrow_to_end.name, boost::bind(&graphVertexProcessFb, _1, segments, server));
                }

                if (NULL != seg.arrow_to_start)
                {
                    seg.arrow_to_start->controls.back().markers.back().points[0] = arrow_to_start_start;
                    seg.arrow_to_start->controls.back().markers.back().points[1] = arrow_to_start_end;

                    server->get(seg.arrow_to_start->name, arrow_to_start);
                    arrow_to_start.controls.back().markers.back().points[0] = arrow_to_start_start;
                    arrow_to_start.controls.back().markers.back().points[1] = arrow_to_start_end;
                    server->erase(arrow_to_start.name);
                    server->insert(arrow_to_start);
                    server->setCallback(arrow_to_start.name, boost::bind(&graphVertexProcessFb, _1, segments, server));
                }

                if ((NULL != seg.arrow_to_end) && (NULL != seg.arrow_to_start))
                {
                    // TF broadcaster update here
                    updateTF(br, arrow_to_start.name, arrow_to_start_start, arrow_to_start_end);
                }

                {
                    std::scoped_lock lock(*edge_pose_update_mutex);
                    *edge_pose_update = true;
                }
            }
        }
        server->applyChanges();
        break;
    }
    case visualization_msgs::InteractiveMarkerFeedback::KEEP_ALIVE:
    case visualization_msgs::InteractiveMarkerFeedback::MENU_SELECT:
    default:
    {
        break;
    }
    }
}

/*************************************************************************************************/
visualization_msgs::InteractiveMarker InteractiveGraph::makeRobotMarker_()
{
    visualization_msgs::InteractiveMarker marker;
    marker.header.frame_id = "world";
    geometry_msgs::Point pt;
    pt.x = 32.8;
    pt.y = 2.6;
    marker.pose.position = pt;
    marker.pose.orientation.x = 0;
    marker.pose.orientation.y = 0;
    marker.pose.orientation.z = M_SQRT1_2;
    marker.pose.orientation.w = M_SQRT1_2;

    marker.scale = 1;

    // marker.name = "robot";
    marker.name = "robot_" + std::to_string(robot_mk_id_++);

    visualization_msgs::InteractiveMarkerControl control;

    control.orientation.x = 0;
    control.orientation.y = M_SQRT1_2;
    control.orientation.z = 0;
    control.orientation.w = M_SQRT1_2;
    control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_ROTATE;

    // cover:                  [0.01, 0.4, 1.0, 1.0]
    // top_plate:              [0.1, 0.4, 0.4, 1.0]
    // bottom:                 [0.1, 0.4, 0.4, 1.0]
    std::vector<float> robot_bottom_c({0.1, 0.4, 0.4, 1.0});
    std::vector<float> robot_cover_c({0.01, 0.4, 1.0, 1.0});
    std::vector<float> robot_top_plate_c({0.1, 0.4, 0.4, 1.0});

    tf2::Quaternion q;
    q.setRPY(0, 0, M_PI);
    control.markers.push_back(makeMesh("package://tuw_voronoi_graph/meshes/il-v3/bottom.stl", marker.scale / 1000., robot_bottom_c[0], robot_bottom_c[1], robot_bottom_c[2], robot_bottom_c[3]));
    control.markers.push_back(makeMesh("package://tuw_voronoi_graph/meshes/il-v3/cover.stl", marker.scale / 1000., robot_cover_c[0], robot_cover_c[1], robot_cover_c[2], robot_cover_c[3]));
    control.markers.push_back(makeMesh("package://tuw_voronoi_graph/meshes/il-v3/cover.stl", marker.scale / 1000., robot_cover_c[0], robot_cover_c[1], robot_cover_c[2], robot_cover_c[3], 0, 0, 0, q.x(), q.y(), q.z(), q.w()));
    control.markers.push_back(makeMesh("package://tuw_voronoi_graph/meshes/il-v3/top_plate.stl", marker.scale / 1000., robot_top_plate_c[0], robot_top_plate_c[1], robot_top_plate_c[2], robot_top_plate_c[3]));

    control.markers.push_back(makeFooprintMarker(0.0, 0.0, -M_PI_2));

    control.always_visible = true;
    marker.controls.push_back(control);

    interactive_mk_server_->insert(marker);

    return marker;
}

/*************************************************************************************************/
void InteractiveGraph::makeLoadedRobotMarker_()
{
    visualization_msgs::InteractiveMarker marker;
    marker.header.frame_id = "world";
    geometry_msgs::Point pt;
    pt.x = 42.8;
    pt.y = 2.6;
    marker.pose.position = pt;
    marker.pose.orientation.x = 0;
    marker.pose.orientation.y = 0;
    marker.pose.orientation.z = M_SQRT1_2;
    marker.pose.orientation.w = M_SQRT1_2;

    marker.scale = 1;

    marker.name = "robot_" + std::to_string(robot_mk_id_++);

    visualization_msgs::InteractiveMarkerControl control;

    control.orientation.x = 0;
    control.orientation.y = M_SQRT1_2;
    control.orientation.z = 0;
    control.orientation.w = M_SQRT1_2;
    control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_ROTATE;

    // cover:                  [0.01, 0.4, 1.0, 1.0]
    // top_plate:              [0.1, 0.4, 0.4, 1.0]
    // bottom:                 [0.1, 0.4, 0.4, 1.0]
    std::vector<float> robot_bottom_c({0.1, 0.4, 0.4, 1.0});
    std::vector<float> robot_cover_c({0.01, 0.4, 1.0, 1.0});
    std::vector<float> robot_top_plate_c({0.1, 0.4, 0.4, 1.0});

    tf2::Quaternion q;
    q.setRPY(0, 0, M_PI);
    control.markers.push_back(makeMesh(
        "package://tuw_voronoi_graph/meshes/il-v3/bottom.stl",
        marker.scale / 1000., robot_bottom_c[0], robot_bottom_c[1], robot_bottom_c[2], robot_bottom_c[3]));
    control.markers.push_back(makeMesh(
        "package://tuw_voronoi_graph/meshes/il-v3/cover.stl",
        marker.scale / 1000., robot_cover_c[0], robot_cover_c[1], robot_cover_c[2], robot_cover_c[3]));
    control.markers.push_back(makeMesh(
        "package://tuw_voronoi_graph/meshes/il-v3/cover.stl",
        marker.scale / 1000., robot_cover_c[0], robot_cover_c[1], robot_cover_c[2], robot_cover_c[3], 0, 0, 0, q.x(), q.y(), q.z(), q.w()));
    control.markers.push_back(makeMesh(
        "package://tuw_voronoi_graph/meshes/il-v3/top_plate.stl",
        marker.scale / 1000., robot_top_plate_c[0], robot_top_plate_c[1], robot_top_plate_c[2], robot_top_plate_c[3]));

    q.setRPY(0, 0, M_PI_2);

    if (robot_env_ == "stef_eurocentre")
    {
        control.markers.push_back(makeMesh(
            "package://tuw_voronoi_graph/meshes/" + robot_env_ + "/frame.stl",
            marker.scale / 1000., Color::gray.r, Color::gray.g, Color::gray.b,
            Color::gray.a, 0, 0, 0.109 + 0.13, q.x(), q.y(), q.z(), q.w()));
        control.markers.push_back(makeMesh(
            "package://tuw_voronoi_graph/meshes/" + robot_env_ + "/roll_lite.stl",
            marker.scale / 1000., Color::light_gray.r, Color::light_gray.g,
            Color::light_gray.b, Color::light_gray.a, 0, 0.405, 0.13 + 0.209));
        control.markers.push_back(makeMesh(
            "package://tuw_voronoi_graph/meshes/" + robot_env_ + "/roll_lite.stl",
            marker.scale / 1000., Color::light_gray.r, Color::light_gray.g,
            Color::light_gray.b, Color::light_gray.a, 0, -0.405, 0.13 + 0.209));
    }
    else
    {
        control.markers.push_back(makeMesh(
            "package://tuw_voronoi_graph/meshes/pallet.stl", 1e-4, 0.863, 0.71,
            0.475, Color::light_gray.a, 0, 0, 0.16, 0, 0, M_SQRT1_2, M_SQRT1_2));
    }

    control.markers.push_back(makeFooprintMarker(0.0, 0.0, -M_PI_2));

    control.always_visible = true;
    marker.controls.push_back(control);

    interactive_mk_server_->insert(marker);
}

/*************************************************************************************************/
void InteractiveGraph::createInteractiveGraph_()
{
    // #pragma omp parallel for schedule(static)
    for (auto& [id, seg] : segments_)
    {
        bool end_mk_exists      = false;
        bool start_mk_exists    = false;
        geometry_msgs::Point pt, arrow_to_end_end, arrow_to_end_start, arrow_to_start_start, arrow_to_start_end;

        for (auto mk : edges_markers_)
        {
            if (mk->pose.position.x == seg.end.x() && mk->pose.position.y == seg.end.y())
            {
                // end marker already exists
                seg.end_marker = mk;
                end_mk_exists = true;
            }

            if (mk->pose.position.x == seg.start.x() && mk->pose.position.y == seg.start.y())
            {
                // start marker already exists
                seg.start_marker = mk;
                start_mk_exists = true;
            }
        }

        if (!end_mk_exists)
        {
            pt.x = seg.end.x();
            pt.y = seg.end.y();
            edges_markers_.push_back(std::make_shared<visualization_msgs::InteractiveMarker>(makeGraphEdgeMarker_(pt)));
            edges_markers_names_.push_back(edges_markers_.back()->name);
            seg.end_marker = edges_markers_.back();
        }

        if (!start_mk_exists)
        {
            pt.x = seg.start.x();
            pt.y = seg.start.y();
            edges_markers_.push_back(std::make_shared<visualization_msgs::InteractiveMarker>(makeGraphEdgeMarker_(pt)));
            edges_markers_names_.push_back(edges_markers_.back()->name);
            seg.start_marker = edges_markers_.back();
        }

        arrow_to_start_start.x  = (seg.start.x() * .5) + (seg.end.x() * .5);
        arrow_to_start_start.y  = (seg.start.y() * .5) + (seg.end.y() * .5);
        arrow_to_end_start.x    = (seg.start.x() * .5) + (seg.end.x() * .5);
        arrow_to_end_start.y    = (seg.start.y() * .5) + (seg.end.y() * .5);

        arrow_to_start_end.x    = seg.start.x();
        arrow_to_start_end.y    = seg.start.y();
        arrow_to_end_end.x      = seg.end.x();
        arrow_to_end_end.y      = seg.end.y();

        seg.arrow_to_end    = std::make_shared<visualization_msgs::InteractiveMarker>(makeGraphVertex_(arrow_to_end_start, arrow_to_end_end));
        seg.arrow_to_start  = std::make_shared<visualization_msgs::InteractiveMarker>(makeGraphVertex_(arrow_to_start_start, arrow_to_start_end));
    }

    if (interac_width_)
    {
        for (auto& [id, seg] : segments_)
        {
            seg.width_line = std::make_shared<visualization_msgs::InteractiveMarker>(makeWidthLine_(seg.width, 0.4, seg.arrow_to_start->name));
        }
    }

    // Orient the interactive graph if the received graph is oriented

    // if either end_neig or front_neig are empty
    //   search all other segs for seg.id in their neigs
    //      if seg.id is nowhere to be found
    //         seg is a severed node? anyway, don't bother about orientation
    //      else if all finds have seg in the same side as seg is not empty
    //         seg is a dead end
    //      else if end_neig is not empty -> oriented towards end
    //      else if start_neig is not empty -> oriented towards start
    for (auto& [id, seg] : segments_)
    {
        if (seg.end_neigs.empty() || seg.start_neigs.empty())
        {
            std::vector<int> founds_from_end_side, founds_from_start_side;

            for (auto& [oth_id, oth_seg] : segments_)
            {
                if (oth_id != id)
                {
                    if (std::find(oth_seg.end_neigs.begin(), oth_seg.end_neigs.end(), id) != oth_seg.end_neigs.end() ||
                        std::find(oth_seg.start_neigs.begin(), oth_seg.start_neigs.end(), id) != oth_seg.start_neigs.end())
                    {
                        if ((oth_seg.start.array() == seg.start.array()).all() ||
                            (oth_seg.end.array() == seg.start.array()).all())
                        {
                            founds_from_start_side.push_back(oth_id);
                        }

                        if ((oth_seg.start.array() == seg.end.array()).all() ||
                            (oth_seg.end.array() == seg.end.array()).all())
                        {
                            founds_from_end_side.push_back(oth_id);
                        }
                    }
                }
            }

            if ((founds_from_end_side.empty() && founds_from_start_side.empty()) ||
                (!seg.end_neigs.empty() && founds_from_start_side.empty()) ||
                (!seg.start_neigs.empty() && founds_from_end_side.empty()))
            {
                continue;
            }

            if (!seg.end_neigs.empty())
            {
                setDirected_(&seg, false);
            }
            else if (!seg.start_neigs.empty())
            {
                setDirected_(&seg, true);
            }
            else
            {
                // Do nothing
            }
        }
    }

    // Recreate the complete list of neighbors for Segmets in order to make editing
    // loaded configuration possible

    for (auto& [id, seg] : segments_)
    {
        for (auto& [nid, nseg] : segments_)
        {
            if (id == nid ||
                std::find(seg.start_neigs.begin(), seg.start_neigs.end(), nid) != seg.start_neigs.end() ||
                std::find(seg.end_neigs.begin(), seg.end_neigs.end(), nid) != seg.end_neigs.end())
            {
                continue;
            }

            if ((nseg.start.array() == seg.start.array()).all() ||
                (nseg.end.array() == seg.start.array()).all())
            {
                seg.start_neigs.push_back(nid);
            }

            if ((nseg.start.array() == seg.end.array()).all() ||
                (nseg.end.array() == seg.end.array()).all())
            {
                seg.end_neigs.push_back(nid);
            }
        }
    }
}

/*************************************************************************************************/
void InteractiveGraph::updateValButton()
{
    double avg_x = 0;
    double avg_y = 0;

    if (current_area->validation_button->controls.front().markers.size() == 2 && current_area->vertices_markers.size() > 1)
    {
        visualization_msgs::Marker line_strip;
        line_strip.type = visualization_msgs::Marker::LINE_STRIP;
        line_strip.header.frame_id = "world";
        line_strip.scale.x = 0.06;
        line_strip.color = Color::black;
        current_area->validation_button->controls.front().markers.push_back(line_strip);
    }

    current_area->validation_button->controls.front().markers.back().points.clear();

    for (auto vert : current_area->vertices_markers)
    {
        avg_x += vert->pose.position.x;
        avg_y += vert->pose.position.y;
        current_area->validation_button->controls.front().markers.back().points.push_back(vert->pose.position);
    }

    current_area->validation_button->controls.front().markers.back().points.push_back(current_area->vertices_markers.front()->pose.position);

    current_area->validation_button->pose.position.x = avg_x / current_area->vertices_markers.size();
    current_area->validation_button->pose.position.y = avg_y / current_area->vertices_markers.size();

    interactive_mk_server_->erase(current_area->validation_button->name);
    interactive_mk_server_->insert(*(current_area->validation_button));
}

/*************************************************************************************************/
void InteractiveGraph::initTfs_()
{
    // Update all segment frames transformation with respect to world
    std::vector<geometry_msgs::TransformStamped> tf_array;

    for (auto& [id, seg] : segments_)
    {
        geometry_msgs::Point start, end;
        start.x = seg.start.x();
        start.y = seg.start.y();
        end.x = seg.end.x();
        end.y = seg.end.y();

        updateTF(tf_array, seg.arrow_to_start->name, end, start);
    }

    br_.sendTransform(tf_array);
}

/*************************************************************************************************/
void InteractiveGraph::updateGraph_()
{
    std::scoped_lock lock(edge_pose_update_mutex_, vertex_width_update_mutex_, augmented_graph_mutex_);

    // Reset the data of the augmented_graph
    augmented_graph_.vertices.resize(segments_.size());
    new_graph_msg_.vertices.resize(segments_.size());

    for (auto& [id, seg] : segments_)
    {
        int iIndex = 0;
        for (auto& [tmp_id, seg] : segments_)
        {
            if (tmp_id == id)
            {
                break;
            }
            else
            {
                iIndex++;
            }
        }

        augmented_graph_.stamp = new_graph_msg_.header.stamp = ros::Time::now();
        new_graph_msg_.vertices[iIndex].path.resize(2);
        augmented_graph_.vertices[iIndex].path.resize(2);

        // Update the id
        new_graph_msg_.vertices[iIndex].id      = seg.id;
        augmented_graph_.vertices[iIndex].id    = seg.id;

        new_graph_msg_.vertices[iIndex].path.back().x       = (seg.end.x() - new_graph_msg_.origin.position.x) / new_graph_msg_.resolution;
        augmented_graph_.vertices[iIndex].path.back().x     = seg.end.x();
        new_graph_msg_.vertices[iIndex].path.back().y       = (seg.end.y() - new_graph_msg_.origin.position.y) / new_graph_msg_.resolution;
        augmented_graph_.vertices[iIndex].path.back().y     = seg.end.y();
        new_graph_msg_.vertices[iIndex].path.front().x      = (seg.start.x() - new_graph_msg_.origin.position.x) / new_graph_msg_.resolution;
        augmented_graph_.vertices[iIndex].path.front().x    = seg.start.x();
        augmented_graph_.vertices[iIndex].path.front().y    = new_graph_msg_.vertices[iIndex].path.front().y = (seg.start.y() - new_graph_msg_.origin.position.y) / new_graph_msg_.resolution;
        augmented_graph_.vertices[iIndex].path.front().y    = seg.start.y();

        new_graph_msg_.vertices[iIndex].weight      = uint32_t((seg.end - seg.start).norm() / new_graph_msg_.resolution);
        augmented_graph_.vertices[iIndex].weight    = (seg.end - seg.start).norm();
        new_graph_msg_.vertices[iIndex].width       = seg.width / new_graph_msg_.resolution;
        augmented_graph_.vertices[iIndex].width     = seg.width;

        new_graph_msg_.vertices[iIndex].predecessors.clear();
        augmented_graph_.vertices[iIndex].predecessors.clear();
        new_graph_msg_.vertices[iIndex].successors.clear();
        augmented_graph_.vertices[iIndex].successors.clear();

        if (!seg.directed_towards_start)
        {
            new_graph_msg_.vertices[iIndex].predecessors.resize(seg.end_neigs.size());
            augmented_graph_.vertices[iIndex].predecessors.resize(seg.end_neigs.size());

            for (auto i = 0; i < seg.end_neigs.size(); ++i)
            {
                new_graph_msg_.vertices[iIndex].predecessors[i] = seg.end_neigs[i];
                augmented_graph_.vertices[iIndex].predecessors[i] = seg.end_neigs[i];
            }
        }
        if (!seg.directed_towards_end)
        {
            new_graph_msg_.vertices[iIndex].successors.resize(seg.start_neigs.size());
            augmented_graph_.vertices[iIndex].successors.resize(seg.start_neigs.size());

            for (auto i = 0; i < seg.start_neigs.size(); ++i)
            {
                new_graph_msg_.vertices[iIndex].successors[i] = seg.start_neigs[i];
                augmented_graph_.vertices[iIndex].successors[i] = seg.start_neigs[i];
            }
        }
    }
    edge_pose_update_ = false;
    vertex_width_update_ = false;
}

/*************************************************************************************************/
void InteractiveGraph::publishGraph_()
{
    graph_pub_.publish(new_graph_msg_);
    i_graph_pub_.publish(augmented_graph_);
}

/*************************************************************************************************/
void InteractiveGraph::save_(const std::string& post_fix, const std::string& frame_id)
{
    std::vector<tuw_graph::Segment> tuw_segments; // TUW Graph data

    Eigen::Vector2d origin;
    origin << new_graph_msg_.origin.position.x, new_graph_msg_.origin.position.y;

    std::map<int32_t, Segment> tf_segments;

    transformSegs_(tf_segments, origin, frame_id);

    // size_t i = 0;
    // for (auto& [s_id, seg] : tf_segments)
    // {
    //     for (auto nid : new_graph_msg_.vertices[i].predecessors)
    //     {
    //         seg.start_neigs.push_back(nid);
    //     }
    //     for (auto nid : new_graph_msg_.vertices[i].successors)
    //     {
    //         seg.end_neigs.push_back(nid);
    //     }
    //     ++i;
    // }
    // WARNING if you call this, then graph_completion may contain properties
    // for the wrong segments!!!
    // reassignIDs_(tf_segments);

    // get neighbours from new_graph_msg_ data
    // (segments_ keeps bi-directional connections)
    size_t i = 0;
    for (auto& [s_id, seg] : tf_segments)
    {

        tuw_graph::Segment s(
            s_id,
            {
                (seg.start - origin) / new_graph_msg_.resolution,
                (seg.end - origin) / new_graph_msg_.resolution
            },
            seg.width / new_graph_msg_.resolution);

        tuw_segments.push_back(s);

        for (auto nid : new_graph_msg_.vertices[i].predecessors)
        {
            int iIndex = 0;
            for (auto i_index = new_graph_msg_.vertices.begin(); i_index != new_graph_msg_.vertices.end(); i_index++)
            {
                if (i_index->id == nid)
                {
                    break;
                }
                else
                {
                    iIndex++;
                }
            }

            if (((new_graph_msg_.vertices[i].path.front().x == new_graph_msg_.vertices[iIndex].path.front().x) &&
                (new_graph_msg_.vertices[i].path.front().y == new_graph_msg_.vertices[iIndex].path.front().y)) ||
                ((new_graph_msg_.vertices[i].path.front().x == new_graph_msg_.vertices[iIndex].path.back().x) &&
                (new_graph_msg_.vertices[i].path.front().y == new_graph_msg_.vertices[iIndex].path.back().y)))
            {
                tuw_segments[i].addPredecessor(nid);
            }
            else if (((new_graph_msg_.vertices[i].path.back().x == new_graph_msg_.vertices[iIndex].path.front().x) &&
                (new_graph_msg_.vertices[i].path.back().y == new_graph_msg_.vertices[iIndex].path.front().y)) ||
                ((new_graph_msg_.vertices[i].path.back().x == new_graph_msg_.vertices[iIndex].path.back().x) &&
                (new_graph_msg_.vertices[i].path.back().y == new_graph_msg_.vertices[iIndex].path.back().y)))
            {
                tuw_segments[i].addSuccessor(nid);
            }
            else
            {
                ROS_ERROR_STREAM("The connection between " << new_graph_msg_.vertices[i].id << " and " << nid << " is not valid.");
            }
        }

        for (auto nid : new_graph_msg_.vertices[i].successors)
        {
            int iIndex = 0;
            for (auto i_index = new_graph_msg_.vertices.begin(); i_index != new_graph_msg_.vertices.end(); i_index++)
            {
                if (i_index->id == nid)
                {
                    break;
                }
                else
                {
                    iIndex++;
                }
            }

            if (((new_graph_msg_.vertices[i].path.front().x == new_graph_msg_.vertices[iIndex].path.front().x) &&
                (new_graph_msg_.vertices[i].path.front().y == new_graph_msg_.vertices[iIndex].path.front().y)) ||
                ((new_graph_msg_.vertices[i].path.front().x == new_graph_msg_.vertices[iIndex].path.back().x) &&
                (new_graph_msg_.vertices[i].path.front().y == new_graph_msg_.vertices[iIndex].path.back().y)))
            {
                tuw_segments[i].addPredecessor(nid);
            }
            else if (((new_graph_msg_.vertices[i].path.back().x == new_graph_msg_.vertices[iIndex].path.front().x) &&
                (new_graph_msg_.vertices[i].path.back().y == new_graph_msg_.vertices[iIndex].path.front().y)) ||
                ((new_graph_msg_.vertices[i].path.back().x == new_graph_msg_.vertices[iIndex].path.back().x) &&
                (new_graph_msg_.vertices[i].path.back().y == new_graph_msg_.vertices[iIndex].path.back().y)))
            {
                tuw_segments[i].addSuccessor(nid);
            }
            else
            {
                ROS_ERROR_STREAM("The connection between " << new_graph_msg_.vertices[i].id << " and " << nid << " is not valid.");
            }
        }

        ++i;
    }

    // tuw_segments is done, time to use serializer

    tuw_graph::Serializer s;
    s.save(path_to_save_ + post_fix, tuw_segments, origin, new_graph_msg_.resolution);
}

/*************************************************************************************************/
void InteractiveGraph::reassignIDs_(std::map<int32_t, Segment>& tf_segments)
{
    // Make sure seg id follows indexation
    for (size_t i = 0; i < segments_.size(); ++i)
    {
        if (tf_segments.find(i) == tf_segments.end())
        {
            ROS_DEBUG_STREAM(nname_ << " No seg_id equals to index: " << i);
            // this index should be an s_id
            // find first s_id that is >= tf_segments.size()
            // rename this id as index
            int rename_id = -1;
            for (const auto& [s_id, seg] : tf_segments)
            {
                if (s_id >= tf_segments.size())
                {
                    rename_id = s_id;
                    break;
                }
            }
            if (rename_id == -1)
                break;

            ROS_DEBUG_STREAM(nname_ << " Going to replace seg_id " << rename_id <<
                             " by " << i);

            Segment s = tf_segments[rename_id];
            s.id = i;
            tf_segments[i] = s;
            tf_segments.erase(rename_id);

            // Update neighbors too
            for (auto& [s_id, seg] : tf_segments)
            {
                auto it = std::find(seg.end_neigs.begin(), seg.end_neigs.end(), rename_id);
                if (it != seg.end_neigs.end())
                {
                    *it = i;
                }

                it = std::find(seg.start_neigs.begin(), seg.start_neigs.end(), rename_id);
                if (it != seg.start_neigs.end())
                {
                    *it = i;
                }
            }
        }
    }
}

/*************************************************************************************************/
void InteractiveGraph::transformSegs_(std::map<int32_t, Segment>& tf_segments,
                                      Eigen::Vector2d& origin,
                                      const std::string& frame_id)
{
    if (frame_id == "world")
    {
        for (const auto& [s_id, seg] : segments_)
        {
            tf_segments[s_id].start = seg.start;
            tf_segments[s_id].end = seg.end;
            tf_segments[s_id].width = seg.width;
        }
    }
    else if (frame_id == "map")
    {
        origin.setZero();

        const auto c_theta = std::cos(map_to_world_tf_[2]);
        const auto s_theta = std::sin(map_to_world_tf_[2]);

        Eigen::Matrix3d map_to_world;

        // cppcheck-suppress constStatement
        map_to_world << c_theta, -s_theta, map_to_world_tf_[0], s_theta, c_theta, map_to_world_tf_[1], 0, 0, 1;

        Eigen::Matrix3d world_to_map = map_to_world.inverse();

        for (const auto& [s_id, seg] : segments_)
        {
            Eigen::Vector3d start({seg.start(0, 0), seg.start(1, 0), 1});
            Eigen::Vector3d end({seg.end(0, 0), seg.end(1, 0), 1});

            tf_segments[s_id].start = (world_to_map * start).block<2, 1>(0, 0);
            tf_segments[s_id].end = (world_to_map * end).block<2, 1>(0, 0);
            tf_segments[s_id].width = seg.width;
        }
    }
    else
    {
        std::stringstream ss;
        ss << nname_ << " [MISSING IMPLEMENTATION] unable to"
            " transform to frame " << frame_id <<
            ". Only world and map are supported";
        std::string s(ss.str());
        ROS_ERROR_STREAM(s);
        throw std::runtime_error(s);
    }
}

/*************************************************************************************************/
void InteractiveGraph::run()
{
    while (ros::ok() && !got_graph_)
    {
        ros::spinOnce();
        ros::Duration(0.1).sleep();
    }

    // Local variable
    bool directional_update_occured;

    // Create the interactive graph from the file
    createInteractiveGraph_();

    initTfs_();

    interactive_mk_server_->applyChanges();

    while (ros::ok())
    {
        // Call the node callback
        ros::spinOnce();

        // Reset the data
        directional_update_occured = false;

        #pragma omp parallel for schedule(static)

        // Loop on the segment list
        for (auto& [id, seg] : segments_)
        {
            // Check if the segment should be set at bidirectionnel
            if (seg.mark_as_bidirected)
            {
                ROS_WARN_STREAM_THROTTLE(5.0, "Arrow to start of seg " << seg.id << " was selected. Seg if going to be marked as bidirected");

                setBidirected_(&seg);
                directional_update_occured = true;
                break;
            }
            else if (seg.arrow_to_start_selected)
            {
                if (!seg.directed)
                {
                    ROS_WARN_STREAM_THROTTLE(5.0, "Arrow to start of seg " << seg.id << " is selected");
                    polarizeTowardsStart_(&seg);
                    directional_update_occured = true;
                    break;
                }
                else if (seg.directed_towards_end)
                {
                    ROS_WARN_STREAM_THROTTLE(5.0, "Arrow to start of seg " << seg.id << " is selected but seg is directed_towards_end");
                    unPolarizeTowardsStart_(&seg);
                    directional_update_occured = true;
                    break;
                }
                else
                {
                    // Do nothing
                }
            }
            else if (seg.arrow_to_end_selected)
            {
                if (!seg.directed)
                {
                    ROS_WARN_STREAM_THROTTLE(5.0, "Arrow to end of seg " << seg.id << " is selected");
                    polarizeTowardsEnd_(&seg);
                    directional_update_occured = true;
                    break;
                }
                else if (seg.directed_towards_start)
                {
                    ROS_WARN_STREAM_THROTTLE(5.0, "Arrow to end of seg " << seg.id << " is selected but seg is directed_towards_start");
                    unPolarizeTowardsEnd_(&seg);
                    directional_update_occured = true;
                    break;
                }
                else
                {
                    // TODO
                }
            }
            else
            {
                // Do nothing
            }

            std::shared_ptr<visualization_msgs::InteractiveMarker> first_marker, second_marker;
            std::string first_marker_name, sec_marker_name;
            bool first_marker_found     = false;
            bool second_marker_found    = false;
            Segment* seg_first_marker, * seg_second_marker;

            // Check if the start node of the current segment is selected or not
            if (0 < seg.start_marker_selected)
            {
                first_marker_name = seg.start_marker->name;
                seg_first_marker = &seg;
                first_marker_found = true;
                first_marker = seg.start_marker;
            }
            else if (0 < seg.end_marker_selected)
            {
                first_marker_name = seg.end_marker->name;
                seg_first_marker = &seg;
                first_marker_found = true;
                first_marker = seg.end_marker;
            }
            else
            {
                // Do nothing
            }

            // Check if the first marker is selected or not
            if (first_marker_found)
            {
                // search for anothers markers selected
                for (auto& [alt_id, alt_seg] : segments_)
                {
                    // Check if the alternative segments is also selected or not
                    if (alt_seg.start_marker_selected && first_marker_name.compare(alt_seg.start_marker->name) != 0)
                    {
                        sec_marker_name = alt_seg.start_marker->name;
                        seg_second_marker = &alt_seg;
                        second_marker_found = true;
                        second_marker = alt_seg.start_marker;
                        // No need to continue the for loop
                        break;
                    }
                    else if (alt_seg.end_marker_selected && first_marker_name.compare(alt_seg.end_marker->name) != 0)
                    {
                        sec_marker_name = alt_seg.end_marker->name;
                        seg_second_marker = &alt_seg;
                        second_marker_found = true;
                        second_marker = alt_seg.end_marker;

                        // No need to continue the for loop
                        break;
                    }
                    else
                    {
                        // Do nothing
                    }
                }

                // Check if we found the second associated marker
                if (second_marker_found)
                {
                    // Check if both of the marker are at level 1 (both red)
                    if (2 == (seg_first_marker->end_marker_selected + seg_first_marker->start_marker_selected + seg_second_marker->end_marker_selected + seg_second_marker->start_marker_selected))
                    {
                        alignSegmentsBetweenNodes_(first_marker_name, sec_marker_name, seg_first_marker, seg_second_marker);

                        // Reset the value
                        first_marker_found  = false;
                        second_marker_found = false;
                    }
                    // Check if one of the marker is at level 1 and the other at level 2 (blue and red)
                    else if (3 == (seg_first_marker->end_marker_selected + seg_first_marker->start_marker_selected + seg_second_marker->end_marker_selected + seg_second_marker->start_marker_selected))
                    {
                        // Check if the the segment already exist between the two nodes
                        if (false == checkIfSegmentAlreadyExistBetweenNodes_(first_marker, second_marker, seg_first_marker, seg_second_marker))
                        {
                            // Create a new segment between the two nodes
                            createSegmentsBetweenNodes_(first_marker, second_marker, seg_first_marker, seg_second_marker);
                        }
                        else
                        {
                            // Unselect the two markers
                            unselectEdges_(first_marker->name, second_marker->name);
                        }
                    }
                    else
                    {
                        // Do nothing
                    }

                    break;
                }
                else
                {
                    // Check if we already used the clicked point
                    if (((2 == seg_first_marker->end_marker_selected) || (2 == seg_first_marker->start_marker_selected)) && (2 == clicked_point_update_))
                    {
                        // Check if the point is not on the first marker
                        auto dx = first_marker->pose.position.x - clicked_point_.x;
                        auto dy = first_marker->pose.position.y - clicked_point_.y;
                        if (dx * dx + dy * dy < 0.01)
                        {
                            // Remove the node
                            removeNode_(id, first_marker, seg_first_marker);
                        }
                        else
                        {
                            // Create a new node and the associated segment
                            createNode_(first_marker, seg_first_marker);
                        }

                        break;
                    }
                }
            }
        }

        interactive_mk_server_->applyChanges();

        // Check if we need to update the graph
        if (got_graph_ && got_aug_graph_)
        {
            if (init_pub_ == false)
            {
                publishGraph_();
                vizIfollowGraph_();

                init_pub_ = true;
            }

            if (directional_update_occured || edge_pose_update_ || vertex_width_update_)
            {
                updateGraph_();
                publishGraph_();
                vizIfollowGraph_();
            }
        }

        // Sleep
        ros::Duration(0.05).sleep();
    }
}

}
