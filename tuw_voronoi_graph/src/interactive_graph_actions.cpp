// -*- mode: c++; coding: utf-8; tab-width: 4 -*-
/*******************************************************************************
* 2-Clause BSD License
*
* Copyright (c) 2019, iFollow Robotics
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
*   list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*   this list of conditions and the following disclaimer in the documentation
*   and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* Authors: José Mendes (mendesfilho@pm.me)
*******************************************************************************/

#include <tuw_voronoi_graph.h>

namespace interactive_graph
{

/*************************************************************************************************/
bool InteractiveGraph::polarize_(Segment* ori_seg, Segment* prev_seg, Segment* seg)
{
    if (ori_seg->id == seg->id)
    {
        return true;
    }

    if (std::find(seg->end_neigs.begin(), seg->end_neigs.end(), prev_seg->id) != seg->end_neigs.end())
    {
        if(true == seg->start_neigs.empty())
        {
            setDirected_(seg, true);
            return true;
        }
        if (seg->start_neigs.size() == 1)
        {
            if (polarize_(ori_seg, seg, &segments_[seg->start_neigs[0]]))
            {
                setDirected_(seg, true);
                return true;
            }

            return false;
        }
        else if (seg->start_neigs.size() > 1)
        {
            int32_t not_dead_end_id = -1;
            int non_dead_end_number = 0;
            for (auto id : seg->start_neigs)
            {
                seg->visited = true;
                bool dead_end = isDeadend(seg, &segments_[id], &segments_);
                if (false == dead_end)
                {
                    not_dead_end_id = id;
                    non_dead_end_number++;

                    if (non_dead_end_number > 1)
                    {
                        not_dead_end_id = -1;
                        break;
                    }
                }
            }

            for (auto& [id, seg] : segments_)
            {
                seg.visited = false;
            }

            if (not_dead_end_id != -1)
            {
                if (true == polarize_(ori_seg, seg, &segments_[not_dead_end_id]))
                {
                    setDirected_(seg, true);

                    return true;
                }

                return false;
            }

            setDirected_(seg, true);

            return true;
        }
        else
        {
            // TODO
        }

        return false;
    }
    else if (std::find(seg->start_neigs.begin(), seg->start_neigs.end(), prev_seg->id) != seg->start_neigs.end())
    {
        if(true == seg->end_neigs.empty())
        {
            setDirected_(seg, false);
            return true;
        }
        else if (seg->end_neigs.size() == 1)
        {
            if (true == polarize_(ori_seg, seg, &segments_[seg->end_neigs[0]]))
            {
                setDirected_(seg, false);

                return true;
            }

            return false;
        }
        else if (seg->end_neigs.size() > 1)
        {
            int32_t not_dead_end_id = -1;
            int non_dead_end_number = 0;

            for (auto id : seg->end_neigs)
            {
                seg->visited = true;
                bool dead_end = isDeadend(seg, &segments_[id], &segments_);
                ROS_INFO_STREAM(" Seg " << id << " dead end " << std::boolalpha << dead_end);

                if (false == dead_end)
                {
                    not_dead_end_id = id;
                    non_dead_end_number++;

                    if (non_dead_end_number > 1)
                    {
                        not_dead_end_id = -1;
                        break;
                    }
                }
            }
            for (auto& [id, seg] : segments_)
            {
                seg.visited = false;
            }

            if (not_dead_end_id != -1)
            {
                if (true == polarize_(ori_seg, seg, &segments_[not_dead_end_id]))
                {
                    setDirected_(seg, false);

                    return true;
                }

                return false;
            }

            setDirected_(seg, false);

            return true;
        }

        return false;
    }
    else
    {
        // TODO
    }

    return false;
}

/*************************************************************************************************/
bool InteractiveGraph::unPolarize_(Segment* ori_seg, Segment* prev_seg, Segment* seg)
{
    if ((ori_seg->id == seg->id) || (false == seg->directed))
    {
        return true;
    }

    if (std::find(seg->end_neigs.begin(), seg->end_neigs.end(), prev_seg->id) != seg->end_neigs.end())
    {
        if (seg->start_neigs.size() == 1)
        {
            if (true == unPolarize_(ori_seg, seg, &segments_[seg->start_neigs[0]]))
            {
                setBidirected_(seg);

                return true;
            }

            return false;
        }
        else if (seg->start_neigs.size() > 1)
        {
            int32_t not_dead_end_id = -1;
            int non_dead_end_number = 0;

            for (auto id : seg->start_neigs)
            {
                seg->visited = true;
                bool dead_end = isDeadend(seg, &segments_[id], &segments_);

                if (false == dead_end)
                {
                    not_dead_end_id = id;
                    non_dead_end_number++;

                    if (non_dead_end_number > 1)
                    {
                        not_dead_end_id = -1;
                        break;
                    }
                }
            }
            for (auto& [id, seg] : segments_)
            {
                seg.visited = false;
            }

            if (not_dead_end_id != -1)
            {
                if (unPolarize_(ori_seg, seg, &segments_[not_dead_end_id]))
                {
                    setBidirected_(seg);

                    return true;
                }

                return false;
            }

            setBidirected_(seg);

            return true;
        }

        return false;
    }
    else if (std::find(seg->start_neigs.begin(), seg->start_neigs.end(), prev_seg->id) != seg->start_neigs.end())
    {
        if (seg->end_neigs.size() == 1)
        {
            if (true == unPolarize_(ori_seg, seg, &segments_[seg->end_neigs[0]]))
            {
                setBidirected_(seg);

                return true;
            }

            return false;
        }
        else if (seg->end_neigs.size() > 1)
        {
            int32_t not_dead_end_id = -1;
            int non_dead_end_number = 0;

            for (auto id : seg->end_neigs)
            {
                seg->visited = true;
                bool dead_end = isDeadend(seg, &segments_[id], &segments_);
                ROS_INFO_STREAM(" Seg " << id << " dead end " << std::boolalpha << dead_end);

                if (false == dead_end)
                {
                    not_dead_end_id = id;
                    non_dead_end_number++;

                    if (non_dead_end_number > 1)
                    {
                        not_dead_end_id = -1;
                        break;
                    }
                }
            }

            for (auto& [id, seg] : segments_)
            {
                seg.visited = false;
            }

            if (not_dead_end_id != -1)
            {
                if (true == unPolarize_(ori_seg, seg, &segments_[not_dead_end_id]))
                {
                    setBidirected_(seg);

                    return true;
                }

                return false;
            }

            setBidirected_(seg);

            return true;
        }

        return false;
    }

    return false;
}

/*************************************************************************************************/
void InteractiveGraph::polarizeTowardsEnd_(Segment* ori_seg)
{
    if (ori_seg->end_neigs.size() == 1)
    {
        if (true == polarize_(ori_seg, ori_seg, &(segments_[ori_seg->end_neigs[0]])))
        {
            setDirected_(ori_seg, false);
        }
    }
    else if ((ori_seg->end_neigs.size() == 0) || ((ori_seg->end_neigs.size() > 1) && (false == ori_seg->start_neigs.empty())))
    {
        setDirected_(ori_seg, false);
    }
    else
    {
        // Can't happened unconnected segment case
    }
}

/*************************************************************************************************/
void InteractiveGraph::polarizeTowardsStart_(Segment* ori_seg)
{
    if (ori_seg->start_neigs.size() == 1)
    {
        if (polarize_(ori_seg, ori_seg, &(segments_[ori_seg->start_neigs[0]])))
        {
            setDirected_(ori_seg, true);
        }
    }
    else if ((ori_seg->start_neigs.size() == 0) || ((ori_seg->start_neigs.size() > 1) && (false == ori_seg->end_neigs.empty())))
    {
        setDirected_(ori_seg, true);
    }
    else
    {
        // Can't happened unconnected segment case
    }
}

/*************************************************************************************************/
void InteractiveGraph::unPolarizeTowardsEnd_(Segment* ori_seg)
{
    if (ori_seg->end_neigs.size() == 1)
    {
        if (true == unPolarize_(ori_seg, ori_seg, &(segments_[ori_seg->end_neigs[0]])))
        {
            setBidirected_(ori_seg);
        }
    }
    else if ((ori_seg->end_neigs.size() == 0) || ((ori_seg->end_neigs.size() > 1) && (false ==  ori_seg->start_neigs.empty())))
    {
        setBidirected_(ori_seg);
    }
    else
    {
        // Can't happened unconnected segment case
    }
}

/*************************************************************************************************/
void InteractiveGraph::unPolarizeTowardsStart_(Segment* ori_seg)
{
    if (ori_seg->start_neigs.size() == 1)
    {
        if (true == unPolarize_(ori_seg, ori_seg, &(segments_[ori_seg->start_neigs[0]])))
        {
            setBidirected_(ori_seg);
        }
    }
    else if ((ori_seg->start_neigs.size() == 0) || ((ori_seg->start_neigs.size() > 1) && (ori_seg->end_neigs.size() != 0)))
    {
        setBidirected_(ori_seg);
    }
    else
    {
        // Can't happened unconnected segment case
    }
}

/*************************************************************************************************/
void InteractiveGraph::alignSegmentsBetweenNodes_(std::string first_marker_name, std::string sec_marker_name, Segment* seg_first_marker, Segment* seg_second_marker)
{
    // get shortest seg path beteween two selected markers
    auto list_edges_names = shortestPath_(first_marker_name, sec_marker_name, seg_first_marker, seg_second_marker);

    // if length > 0, align segs
    if (false == list_edges_names.empty())
    {
        list_edges_names.insert(list_edges_names.begin(), sec_marker_name);
        list_edges_names.push_back(first_marker_name);
        align_(list_edges_names);
    }

    // unselect (changing other segments that have a record of this marker being selected, and changing the button) and done
    unselectEdges_(first_marker_name, sec_marker_name);
}

/*************************************************************************************************/
void InteractiveGraph::createSegmentsBetweenNodes_(std::shared_ptr<visualization_msgs::InteractiveMarker> first_marker, std::shared_ptr<visualization_msgs::InteractiveMarker> second_marker, Segment* seg_first_marker, Segment* seg_second_marker)
{
    // Compute the distance between the two nodes
    float distance = std::hypot((first_marker->pose.position.x - second_marker->pose.position.x), (first_marker->pose.position.y - second_marker->pose.position.y));

    // Unselect the first marker
    unselectEdges_(first_marker->name, second_marker->name);

    // Check if the distance is bigger than 1 meter
    if (distance < 1.f)
    {
        // Create a single segment between the two nodes
        createSingleSegmentBetweenNodes(first_marker, second_marker, seg_first_marker, seg_second_marker);
    }
    else
    {
        // Create multiple segments between the two nodes
        createMultipleSegmentsBetweenNodes(first_marker, second_marker, seg_first_marker, seg_second_marker, distance);
    }

    // Push a new graph on the topic
    edge_pose_update_ = true;
}

/*************************************************************************************************/
void InteractiveGraph::createSingleSegmentBetweenNodes(std::shared_ptr<visualization_msgs::InteractiveMarker> first_marker, std::shared_ptr<visualization_msgs::InteractiveMarker> second_marker, Segment* seg_first_marker, Segment* seg_second_marker)
{
    // Local variable
    Segment new_segment;
    geometry_msgs::Point start, end;
    tuw_multi_robot_msgs::Vertex new_vertice;

    // Create a segment between the two marker
    new_segment.id = (segments_.rbegin())->first+1;

    new_segment.start[0] = first_marker->pose.position.x;
    new_segment.start[1] = first_marker->pose.position.y;

    new_segment.end[0] = second_marker->pose.position.x;
    new_segment.end[1] = second_marker->pose.position.y;

    new_segment.width = 0.1;
    new_segment.init_width = new_segment.width;

    new_segment.directed                = false;
    new_segment.arrow_to_end_selected   = false;
    new_segment.arrow_to_start_selected = false;
    new_segment.end_marker_selected     = 0;
    new_segment.start_marker_selected   = false;
    new_segment.directed_towards_end    = false;
    new_segment.directed_towards_start  = false;
    new_segment.visited                 = false;
    new_segment.mark_as_bidirected      = false;

    // Update the segment interactions
    seg_second_marker->start_neigs.push_back(new_segment.id);
    new_segment.end_neigs.push_back(seg_second_marker->id);
    seg_first_marker->end_neigs.push_back(new_segment.id);
    new_segment.start_neigs.push_back(seg_first_marker->id);
    new_segment.end_marker      = second_marker;
    new_segment.start_marker    = first_marker;

    createArrowForSegment(new_segment);

    // Add the new segment in the segment map
    segments_[new_segment.id] = new_segment;

    // Set the vertice segment
    new_vertice.id = new_segment.id;

    new_vertice.weight = (new_segment.start - new_segment.end).norm();

    start.x = new_segment.start.x();
    start.y = new_segment.start.y();
    start.z = 0;

    end.x = new_segment.start.x();
    end.y = new_segment.start.y();
    end.z = 0;

    // Add the point on the vertices
    new_vertice.path.push_back(start);
    new_vertice.path.push_back(end);

    // Update the vertice link
    new_vertice.predecessors.push_back(seg_first_marker->id);
    new_vertice.successors.push_back(seg_second_marker->id);

    new_graph_msg_.vertices.push_back(new_vertice);

    ifollow_mas_msgs::Vertex localVertex;

    localVertex.id      = new_vertice.id;
    localVertex.valid   = new_vertice.valid;
    localVertex.path.resize(new_vertice.path.size());
    localVertex.width = new_vertice.width * new_graph_msg_.resolution < 1.2 ? 1.2 : new_vertice.width * new_graph_msg_.resolution;

    std::string param_name ("graph_completion/vertices/" + std::to_string(new_vertice.id) + "/height");
    float height = 0.0;
    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<float>(param_name, height, 0.0);
    }
    localVertex.height = height;

    localVertex.successors      = new_vertice.successors;
    localVertex.predecessors    = new_vertice.predecessors;

    float allowed_max_lvel = 0.0;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/allowed_max_lvel";
    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<float>(param_name, allowed_max_lvel, 0.0);
    }

    localVertex.allowed_max_lvel = allowed_max_lvel;


    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/allowed_max_avel";
    float allowed_max_avel = 0.0;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<float>(param_name, allowed_max_avel, 0.0);
    }

    localVertex.allowed_max_avel = allowed_max_avel;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/turtle_on";
    bool turtle_on = false;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<bool>(param_name, turtle_on, 0.0);

    }

    localVertex.turtle_on = turtle_on;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/odom_sources_to_turn_off";
    std::vector<std::string> odom_sources_to_turn_off;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<std::vector<std::string>>(param_name, odom_sources_to_turn_off, odom_sources_to_turn_off);
    }

    localVertex.odom_sources_to_turn_off = odom_sources_to_turn_off;

    // internal to the traj_checker plugin ---------------------------------
    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/cam_min_dist";
    float cam_min_dist = 0.0;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<float>(param_name, cam_min_dist, cam_min_dist);
    }

    localVertex.cam_min_dist = cam_min_dist;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/laser_min_dist";
    float laser_min_dist = 0.0;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<float>(param_name, laser_min_dist, laser_min_dist);
    }

    localVertex.laser_min_dist = laser_min_dist;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/multi_robot_min_dist";
    float multi_robot_min_dist = 0.0;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<float>(param_name, multi_robot_min_dist, multi_robot_min_dist);
    }

    localVertex.multi_robot_min_dist = multi_robot_min_dist;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/safe_lift_inflation";
    float safe_lift_inflation = 0.0;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<float>(param_name, safe_lift_inflation, safe_lift_inflation);
    }

    localVertex.safe_lift_inflation = safe_lift_inflation;
    // ---------------------------------------------------------------------

    // take into accound at the local planner ------------------------------
    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/use_precise_fp";
    bool use_precise_fp = false;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<bool>(param_name, use_precise_fp, use_precise_fp);
    }

    localVertex.use_precise_fp = use_precise_fp;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/fp_padding";
    float fp_padding = 0.0;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<float>(param_name, fp_padding, fp_padding);
    }

    localVertex.fp_padding = fp_padding;
    // ---------------------------------------------------------------------

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/area_id";
    int area_id = 0;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<int>(param_name, area_id, area_id);
    }

    localVertex.area_id = area_id;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/beep_if_blinkers_on";
    bool beep_if_blinkers_on = false;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<bool>(param_name, beep_if_blinkers_on, beep_if_blinkers_on);
    }

    localVertex.beep_if_blinkers_on = beep_if_blinkers_on;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/use_line_on_floor_detection";
    bool use_line_on_floor_detection = false;
    if (true == n_.searchParam(param_name, param_name))
    {
        localVertex.use_line_on_floor_detection = use_line_on_floor_detection;
    }

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/bs_mode";
    int bs_mode= 0; // normal
    if (true == n_.searchParam(param_name, param_name))
    {
        localVertex.bs_mode = bs_mode;
    }

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/use_mr_comm";
    bool use_mr_comm = false;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<bool>(param_name, use_mr_comm, use_mr_comm);
    }

    localVertex.use_mr_comm = use_mr_comm;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/allow_alt_plan_use";
    bool allow_alt_plan_use = false;

    if (true == n_.searchParam(param_name, param_name))
    {
        localVertex.allow_alt_plan_use = allow_alt_plan_use;

    }

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/allow_spontaneous_alt_plan_use";
    bool allow_spontaneous_alt_plan_use = false;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<bool>(param_name, allow_spontaneous_alt_plan_use, allow_spontaneous_alt_plan_use);
    }

    localVertex.allow_spontaneous_alt_plan_use = allow_spontaneous_alt_plan_use;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/force_alt_plan_use";
    bool force_alt_plan_use = false;

    if (true == n_.searchParam(param_name, param_name))
    {
        localVertex.force_alt_plan_use = force_alt_plan_use;
    }

    double length = 0;

    localVertex.path[0].x = new_vertice.path[0].x * new_graph_msg_.resolution;
    localVertex.path[0].y = new_vertice.path[0].y * new_graph_msg_.resolution;
    localVertex.path[0].x += new_graph_msg_.origin.position.x;
    localVertex.path[0].y += new_graph_msg_.origin.position.y;

    for (auto i = 1; i <= (new_vertice.path.size() - 1); ++i)
    {
        localVertex.path[i].x = new_vertice.path[i].x * new_graph_msg_.resolution;
        localVertex.path[i].y = new_vertice.path[i].y * new_graph_msg_.resolution;
        localVertex.path[i].x += new_graph_msg_.origin.position.x;
        localVertex.path[i].y += new_graph_msg_.origin.position.y;

        length += std::hypot((localVertex.path[i].x - localVertex.path[i - 1].x), (localVertex.path[i].y - localVertex.path[i - 1].y));
    }

    localVertex.weight = length / localVertex.allowed_max_lvel;

    augmented_graph_.vertices.push_back(localVertex);

}

/*************************************************************************************************/
void InteractiveGraph::createMultipleSegmentsBetweenNodes(std::shared_ptr<visualization_msgs::InteractiveMarker> first_marker, std::shared_ptr<visualization_msgs::InteractiveMarker> second_marker, Segment* seg_first_marker, Segment* seg_second_marker, float distance)
{
    // Create node until one meters before the end node
    createNodesOnLine_(first_marker->pose.position, seg_first_marker, second_marker->pose.position, (distance-1.0));

    createSingleSegmentBetweenNodes(edges_markers_.back(), second_marker, &(segments_.rbegin()->second), seg_second_marker);
}

/*************************************************************************************************/
bool InteractiveGraph::checkIfSegmentAlreadyExistBetweenNodes_(std::shared_ptr<visualization_msgs::InteractiveMarker> first_marker, std::shared_ptr<visualization_msgs::InteractiveMarker> second_marker, Segment* seg_first_marker, Segment* seg_second_marker)
{
    // Output variable
    bool already_exists = false;

    // Loop on the the first segment start list
    for(int32_t first_segment_id : seg_first_marker->start_neigs)
    {
        // Check if the end segment is the same than the second segment
        if(first_segment_id != seg_second_marker->id)
        {
            // Loop on the end list of the second segment
            for(int32_t second_segment_id : seg_second_marker->end_neigs)
            {
                // Check if this is the same segment and same start and end marker
                if((first_segment_id == second_segment_id) &&
                    (((segments_[first_segment_id].start_marker->name == first_marker->name) || (segments_[first_segment_id].start_marker->name == second_marker->name)) &&
                     ((segments_[first_segment_id].end_marker->name == first_marker->name) || (segments_[first_segment_id].end_marker->name == second_marker->name))))
                {
                    already_exists = true;
                    break;
                }
            }
        }
        else
        {
            already_exists = true;
        }

        // Check if the node are the same
        if(true == already_exists)
        {
            break;
        }
    }

    if(false == already_exists)
    {
        // Loop on the the second segment start list
        for(int32_t first_segment_id : seg_second_marker->start_neigs)
        {
            // Check if the end segment is the same than the second segment
            if(first_segment_id != seg_first_marker->id)
            {
                // Loop on the end list of the first segment
                for(int32_t second_segment_id : seg_first_marker->end_neigs)
                {
                    // Check if this is the same segment and same start and end marker
                    if((first_segment_id == second_segment_id) &&
                        (((segments_[first_segment_id].start_marker->name == first_marker->name) || (segments_[first_segment_id].start_marker->name == second_marker->name)) &&
                         ((segments_[first_segment_id].end_marker->name == first_marker->name) || (segments_[first_segment_id].end_marker->name == second_marker->name))))
                    {
                        already_exists = true;
                        break;
                    }
                }
            }
            else
            {
                already_exists = true;
            }

            // Check if the node are the same
            if(true == already_exists)
            {
                break;
            }
        }
    }

    if(true == already_exists)
    {
        ROS_WARN_STREAM("The segment between the segments " << seg_first_marker->id << " and " << seg_second_marker->id << " already exists.");
    }

    return already_exists;
}

/*************************************************************************************************/
void InteractiveGraph::removeNode_(int segment_id, std::shared_ptr<visualization_msgs::InteractiveMarker> first_marker, Segment* seg_first_marker)
{
    // Local variable
    std::vector<std::string> marker_name_to_remove;
    bool to_remove = false;

    // Loop on the segment list
    for(std::map<int32_t, Segment>::iterator itr = segments_.begin() ; itr != segments_.end(); itr++)
    {
        to_remove = false;
        auto id_iterator = std::find(itr->second.start_neigs.begin(), itr->second.start_neigs.end(), seg_first_marker->id);

        // Check if the first segment is in the start list of the current segment
        if(id_iterator != itr->second.start_neigs.end())
        {
            // Remove the input segment from the start list
            itr->second.start_neigs.erase(id_iterator);

            // Loop on the vertice list
            for(auto &local_vertices : new_graph_msg_.vertices)
            {
                auto id_iterator = std::find(local_vertices.predecessors.begin(), local_vertices.predecessors.end(), itr->first);

                // Check if the input segment is in the predecessors vertices list
                if(id_iterator != local_vertices.predecessors.end())
                {
                    local_vertices.predecessors.erase(id_iterator);
                }

                id_iterator = std::find(local_vertices.successors.begin(), local_vertices.successors.end(), itr->first);

                // Check if the input segment is in the sucessors vertices list
                if(id_iterator != local_vertices.successors.end())
                {
                    local_vertices.successors.erase(id_iterator);
                }
            }

            // Check if the input marker os the start marker of the current segment
            if(itr->second.start_marker->name == first_marker->name)
            {
                if(NULL != itr->second.arrow_to_end)
                {
                    interactive_mk_server_->erase(itr->second.arrow_to_end->name);
                }

                if(NULL != itr->second.arrow_to_start)
                {
                    interactive_mk_server_->erase(itr->second.arrow_to_start->name);
                }

                // Set the output variable as true
                to_remove = true;
            }
        }
        else
        {
            id_iterator = std::find(itr->second.end_neigs.begin(), itr->second.end_neigs.end(), seg_first_marker->id);

            // Check if the input segment is in the end list of the current list
            if(id_iterator != itr->second.end_neigs.end())
            {
                itr->second.end_neigs.erase(id_iterator);

                // Loop on the vertice list
                for(auto &local_vertices : new_graph_msg_.vertices)
                {
                    auto id_iterator = std::find(local_vertices.predecessors.begin(), local_vertices.predecessors.end(), itr->first);

                    if(id_iterator != local_vertices.predecessors.end())
                    {
                        local_vertices.predecessors.erase(id_iterator);
                    }

                    id_iterator = std::find(local_vertices.successors.begin(), local_vertices.successors.end(), itr->first);

                    if(id_iterator != local_vertices.successors.end())
                    {
                        local_vertices.successors.erase(id_iterator);
                    }
                }

                if(itr->second.end_marker->name == first_marker->name)
                {
                    if(NULL != itr->second.arrow_to_end)
                    {
                        interactive_mk_server_->erase(itr->second.arrow_to_end->name);
                    }

                    if(NULL != itr->second.arrow_to_start)
                    {
                        interactive_mk_server_->erase(itr->second.arrow_to_start->name);
                    }

                    to_remove = true;
                }
            }
        }

        // Check if the current segment is empty
        if(itr->second.end_neigs.size() == 0 && itr->second.start_neigs.size() == 0)
        {
            // The marker of the empty marker will be deleted
            marker_name_to_remove.push_back(itr->second.end_marker->name);
            marker_name_to_remove.push_back(itr->second.start_marker->name);

            // Remove the end arrow
            if(NULL != seg_first_marker->arrow_to_end)
            {
                interactive_mk_server_->erase(itr->second.arrow_to_end->name);
            }

            // Remove the start arrow
            if(NULL != seg_first_marker->arrow_to_start)
            {
                interactive_mk_server_->erase(itr->second.arrow_to_start->name);
            }

            to_remove = true;
        }

        if(true == to_remove)
        {
            // Loop on the vertice list
            for(auto &local_vertices : new_graph_msg_.vertices)
            {
                if(local_vertices.id != itr->first)
                {
                    auto id_iterator = std::find(local_vertices.predecessors.begin(), local_vertices.predecessors.end(), itr->first);

                    if(id_iterator != local_vertices.predecessors.end())
                    {
                        local_vertices.predecessors.erase(id_iterator);
                    }

                    id_iterator = std::find(local_vertices.successors.begin(), local_vertices.successors.end(), itr->first);

                    if(id_iterator != local_vertices.successors.end())
                    {
                        local_vertices.successors.erase(id_iterator);
                    }
                }
            }

            for(std::map<int32_t, Segment>::iterator itr_tmp = segments_.begin() ; itr_tmp != segments_.end(); itr_tmp++)
            {
                auto id_iterator = std::find(itr_tmp->second.start_neigs.begin(), itr_tmp->second.start_neigs.end(), itr->first);

                // Check if the first segment is in the start list of the current segment
                if(id_iterator != itr_tmp->second.start_neigs.end())
                {
                    itr_tmp->second.start_neigs.erase(id_iterator);
                }
                else
                {
                    auto id_iterator = std::find(itr_tmp->second.end_neigs.begin(), itr_tmp->second.end_neigs.end(), itr->first);

                    // Check if the first segment is in the start list of the current segment
                    if(id_iterator != itr_tmp->second.end_neigs.end())
                    {
                        itr_tmp->second.end_neigs.erase(id_iterator);
                    }
                }
            }

            // Remove the current segment from the list
            segments_.erase(itr--);
        }
        else
        {
        }
    }

    // Check if the segment is not connected at the end
    if(true == seg_first_marker->end_neigs.empty())
    {
        marker_name_to_remove.push_back(seg_first_marker->end_marker->name);
    }

    // Check if the segment is not connected at the start
    if(true == seg_first_marker->start_neigs.empty())
    {
        marker_name_to_remove.push_back(seg_first_marker->start_marker->name);
    }

    // Loop on the edges marker to remove the marker
    for(std::vector<std::shared_ptr<visualization_msgs::InteractiveMarker>>::iterator current_marker = edges_markers_.begin(); current_marker != edges_markers_.end();current_marker++)
    {
        auto mult5 = std::find(marker_name_to_remove.begin(), marker_name_to_remove.end(), (*current_marker)->name);

        // Check if the current marker need to be remove
        if(((*current_marker)->name == first_marker->name) || (mult5 != marker_name_to_remove.end()))
        {
            auto id_iterator = std::find(edges_markers_names_.begin(), edges_markers_names_.end(), (*current_marker)->name);

            if(id_iterator != edges_markers_names_.end())
            {
                // Remove from the edges markers name list
                edges_markers_names_.erase(id_iterator);
            }

            // Remove from the interactive
            interactive_mk_server_->erase((*current_marker)->name);

            // Remove from the edges markers list
            edges_markers_.erase(current_marker--);
        }
    }

    // Remove the end arrow
    if(NULL != seg_first_marker->arrow_to_end)
    {
        interactive_mk_server_->erase(seg_first_marker->arrow_to_end->name);
    }

    // Remove the start arrow
    if(NULL != seg_first_marker->arrow_to_start)
    {
        interactive_mk_server_->erase(seg_first_marker->arrow_to_start->name);
    }

    auto id_iterator = segments_.find(segment_id);

    // Remove the segment
    if(id_iterator != segments_.end())
    {
        segments_.erase(segment_id);
    }

    // Loop on the vertice list
    for(auto &local_vertices : new_graph_msg_.vertices)
    {
        if(local_vertices.id != seg_first_marker->id)
        {
            auto id_iterator = std::find(local_vertices.predecessors.begin(), local_vertices.predecessors.end(), segment_id);

            if(id_iterator != local_vertices.predecessors.end())
            {
                local_vertices.predecessors.erase(id_iterator);
            }

            id_iterator = std::find(local_vertices.successors.begin(), local_vertices.successors.end(), segment_id);

            if(id_iterator != local_vertices.successors.end())
            {
                local_vertices.successors.erase(id_iterator);
            }
        }
    }

    // Reset the variable
    clicked_point_update_ = 0;

    // Push a new graph on the topic
    edge_pose_update_ = true;
}

/*************************************************************************************************/
void InteractiveGraph::createNode_(std::shared_ptr<visualization_msgs::InteractiveMarker> first_marker, Segment* seg_first_marker)
{
    // Compute the distance between the first marker and the clicked point
    float distance = std::hypot((first_marker->pose.position.x - clicked_point_.x), (first_marker->pose.position.y - clicked_point_.y));

    // Unselect the first marker
    unselectEdges_(first_marker->name, first_marker->name);

    // Check if we are above the limit
    if (distance < 1.f)
    {
        // Create a sig
        createSingleNode_(first_marker->pose.position, seg_first_marker, clicked_point_);
    }
    else
    {
        // Create multiple segments projected on a line
        createNodesOnLine_(first_marker->pose.position, seg_first_marker, clicked_point_, distance);
    }

    clicked_point_update_ = 0;

    // Push a new graph on the topic
    edge_pose_update_ = true;
}

/*************************************************************************************************/
void InteractiveGraph::createNodesOnLine_(const geometry_msgs::Point& first_marker_point, Segment* seg_first_marker, const geometry_msgs::Point& new_marker_point, float distance)
{
    // Local variables
    geometry_msgs::Point new_point, last_marker_point;
    Segment* last_segment = seg_first_marker;
    const float line_angle = atan2((new_marker_point.y - first_marker_point.y), (new_marker_point.x - first_marker_point.x));
    const float delta_distance = distance / (round(distance / 1.f) + 1);

    // Set the first start point
    last_marker_point = first_marker_point;
    const float x_delta = delta_distance * cos(line_angle);
    const float y_delta = delta_distance * sin(line_angle);

    // We keep creating a new node while the distance is too big
    while (distance > 0.001)
    {
        // Compute the new point
        new_point.x = last_marker_point.x + x_delta;
        new_point.y = last_marker_point.y + y_delta;

        // Create a new node
        createSingleNode_(last_marker_point, last_segment, new_point);

        // Update the distance
        distance -= delta_distance;

        // Update the start marker
        last_marker_point = edges_markers_.back()->pose.position;

        // Update the start segment
        last_segment = &segments_[(segments_.rbegin())->first];
    }
}

/*************************************************************************************************/
void InteractiveGraph::createSingleNode_(const geometry_msgs::Point &prev_marker_point, Segment* seg_first_marker, const geometry_msgs::Point& new_marker_point)
{
    // Local variable
    Segment new_segment;
    geometry_msgs::Point start, end;
    tuw_multi_robot_msgs::Vertex new_vertice;

    // Create a new node on the click position and connected to the first maker node
    edges_markers_.push_back(std::make_shared<visualization_msgs::InteractiveMarker>(makeGraphEdgeMarker_(new_marker_point)));
    edges_markers_names_.push_back(edges_markers_.back()->name);

    new_segment.id = (segments_.rbegin())->first+1;

    new_segment.start[0] = prev_marker_point.x;
    new_segment.start[1] = prev_marker_point.y;

    new_segment.end[0] = new_marker_point.x;
    new_segment.end[1] = new_marker_point.y;

    new_segment.width = 0.1;
    new_segment.init_width = new_segment.width;

    new_segment.directed                = false;
    new_segment.arrow_to_end_selected   = false;
    new_segment.arrow_to_start_selected = false;
    new_segment.end_marker_selected     = 0;
    new_segment.start_marker_selected   = false;
    new_segment.directed_towards_end    = false;
    new_segment.directed_towards_start  = false;
    new_segment.visited                 = false;
    new_segment.mark_as_bidirected      = false;

    // Add the first segment in the start list
    new_segment.start_neigs.push_back(seg_first_marker->id);

    // Add the new segment in the end list of the first segment
    seg_first_marker->end_neigs.push_back(new_segment.id);

    new_segment.end_marker      = edges_markers_.back();
    new_segment.start_marker    = seg_first_marker->end_marker;

    createArrowForSegment(new_segment);

    // Add the new segments to the list
    segments_[new_segment.id] = new_segment;

    new_vertice.id = new_segment.id;

    new_vertice.weight = (new_segment.start - new_segment.end).norm();

    start.x = new_segment.start.x();
    start.y = new_segment.start.y();
    start.z = 0;

    end.x = new_segment.start.x();
    end.y = new_segment.start.y();
    end.z = 0;

    new_vertice.path.push_back(start);
    new_vertice.path.push_back(end);

    new_vertice.predecessors.push_back(seg_first_marker->id);
    new_vertice.successors.push_back(new_segment.id);

    new_graph_msg_.vertices.push_back(new_vertice);

    ifollow_mas_msgs::Vertex localVertex;

    localVertex.id      = new_vertice.id;
    localVertex.valid   = new_vertice.valid;
    localVertex.path.resize(new_vertice.path.size());
    localVertex.width = ((new_vertice.width * new_graph_msg_.resolution) < 1.2) ? 1.2 : (new_vertice.width * new_graph_msg_.resolution);

    std::string param_name ("graph_completion/vertices/" + std::to_string(new_vertice.id) + "/height");
    float height = 0.0;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<float>(param_name, height, 0.01);
    }

    localVertex.height = height;

    localVertex.successors      = new_vertice.successors;
    localVertex.predecessors    = new_vertice.predecessors;

    float allowed_max_lvel = 0.0;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/allowed_max_lvel";
    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<float>(param_name, allowed_max_lvel, 0.0);
    }

    localVertex.allowed_max_lvel = allowed_max_lvel;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/allowed_max_avel";
    float allowed_max_avel = 0.0;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<float>(param_name, allowed_max_avel, 0.0);
    }

    localVertex.allowed_max_avel = allowed_max_avel;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/turtle_on";
    bool turtle_on = false;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<bool>(param_name, turtle_on, false);

    }

    localVertex.turtle_on = turtle_on;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/odom_sources_to_turn_off";
    std::vector<std::string> odom_sources_to_turn_off;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<std::vector<std::string>>(param_name, odom_sources_to_turn_off, odom_sources_to_turn_off);
    }

    localVertex.odom_sources_to_turn_off = odom_sources_to_turn_off;

    // internal to the traj_checker plugin ---------------------------------
    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/cam_min_dist";
    float cam_min_dist = 0.0;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<float>(param_name, cam_min_dist, 0.0);
    }

    localVertex.cam_min_dist = cam_min_dist;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/laser_min_dist";
    float laser_min_dist = 0.0;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<float>(param_name, laser_min_dist, 0.0);
    }

    localVertex.laser_min_dist = laser_min_dist;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/multi_robot_min_dist";
    float multi_robot_min_dist = 0.0;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<float>(param_name, multi_robot_min_dist, 0.0);
    }

    localVertex.multi_robot_min_dist = multi_robot_min_dist;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/safe_lift_inflation";
    float safe_lift_inflation = 0.0;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<float>(param_name, safe_lift_inflation, 0.0);
    }

    localVertex.safe_lift_inflation = safe_lift_inflation;

    // take into accound at the local planner ------------------------------
    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/use_precise_fp";
    bool use_precise_fp = 0.0;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<bool>(param_name, use_precise_fp, 0.0);
    }

    localVertex.use_precise_fp = use_precise_fp;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/fp_padding";
    float fp_padding = 0.0;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<float>(param_name, fp_padding, 0.0);
    }

    localVertex.fp_padding = fp_padding;
    // ---------------------------------------------------------------------

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/area_id";
    int area_id = 0;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<int>(param_name, area_id, 0);
    }

    localVertex.area_id = area_id;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/beep_if_blinkers_on";
    bool beep_if_blinkers_on = false;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<bool>(param_name, beep_if_blinkers_on, false);
    }

    localVertex.beep_if_blinkers_on = beep_if_blinkers_on;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/use_line_on_floor_detection";
    bool use_line_on_floor_detection = false;
    if (true == n_.searchParam(param_name, param_name))
    {
        localVertex.use_line_on_floor_detection = use_line_on_floor_detection;
    }

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/bs_mode";
    int bs_mode = 0;
    if (true == n_.searchParam(param_name, param_name))
    {
        localVertex.bs_mode = bs_mode;
    }

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/use_mr_comm";
    bool use_mr_comm = false;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<bool>(param_name, use_mr_comm, false);
    }

    localVertex.use_mr_comm = use_mr_comm;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/allow_alt_plan_use";

    if (true == n_.searchParam(param_name, param_name))
    {
        localVertex.allow_alt_plan_use = false;

    }

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/allow_spontaneous_alt_plan_use";
    bool allow_spontaneous_alt_plan_use = false;

    if (true == n_.searchParam(param_name, param_name))
    {
        n_.param<bool>(param_name, allow_spontaneous_alt_plan_use, false);
    }

    localVertex.allow_spontaneous_alt_plan_use = allow_spontaneous_alt_plan_use;

    param_name = "graph_completion/vertices/" + std::to_string(new_vertice.id) + "/force_alt_plan_use";

    if (true == n_.searchParam(param_name, param_name))
    {
        localVertex.force_alt_plan_use = false;
    }

    double length = 0;

    localVertex.path[0].x = new_vertice.path[0].x * new_graph_msg_.resolution;
    localVertex.path[0].y = new_vertice.path[0].y * new_graph_msg_.resolution;
    localVertex.path[0].x += new_graph_msg_.origin.position.x;
    localVertex.path[0].y += new_graph_msg_.origin.position.y;

    for (auto i = 1; i <= (new_vertice.path.size() - 1); ++i)
    {
        localVertex.path[i].x = new_vertice.path[i].x * new_graph_msg_.resolution;
        localVertex.path[i].y = new_vertice.path[i].y * new_graph_msg_.resolution;
        localVertex.path[i].x += new_graph_msg_.origin.position.x;
        localVertex.path[i].y += new_graph_msg_.origin.position.y;

        length += std::hypot((localVertex.path[i].x - localVertex.path[i - 1].x), (localVertex.path[i].y - localVertex.path[i - 1].y));
    }

    localVertex.weight = length / localVertex.allowed_max_lvel;

    augmented_graph_.vertices.push_back(localVertex);

    ROS_INFO_STREAM("Create a new node " << edges_markers_.back()->name << ", start: " << new_segment.start_marker->name <<  ", end: " << new_segment.end_marker->name);
}

/*************************************************************************************************/
void InteractiveGraph::updateArrowForSegment(Segment& in_segment, std::vector<geometry_msgs::TransformStamped>& tf_array)
{
    // Local variable
    geometry_msgs::Point arrow_to_end_end, arrow_to_end_start, arrow_to_start_start, arrow_to_start_end;
    visualization_msgs::InteractiveMarker arrow_to_end, arrow_to_start;

    arrow_to_start_start.x  = (in_segment.start.x()*.5) + (in_segment.end.x()*.5);
    arrow_to_start_start.y  = (in_segment.start.y()*.5) + (in_segment.end.y()*.5);
    arrow_to_end_start.x    = (in_segment.start.x()*.5) + (in_segment.end.x()*.5);
    arrow_to_end_start.y    = (in_segment.start.y()*.5) + (in_segment.end.y()*.5);

    arrow_to_start_end.x    = in_segment.start.x();
    arrow_to_start_end.y    = in_segment.start.y();
    arrow_to_end_end.x      = in_segment.end.x();
    arrow_to_end_end.y      = in_segment.end.y();

    in_segment.arrow_to_end->controls.back().markers.back().points[0] = arrow_to_end_start;
    in_segment.arrow_to_end->controls.back().markers.back().points[1] = arrow_to_end_end;

    in_segment.arrow_to_end->controls.back().markers.back().points[0] = arrow_to_start_start;
    in_segment.arrow_to_end->controls.back().markers.back().points[1] = arrow_to_start_end;

    interactive_mk_server_->get(in_segment.arrow_to_end->name, arrow_to_end);
    arrow_to_end.controls.back().markers.back().points[0] = arrow_to_end_start;
    arrow_to_end.controls.back().markers.back().points[1] = arrow_to_end_end;
    interactive_mk_server_->erase(arrow_to_end.name);
    interactive_mk_server_->insert(arrow_to_end);
    interactive_mk_server_->setCallback(arrow_to_end.name, boost::bind(&graphVertexProcessFb, _1, &segments_, interactive_mk_server_));

    interactive_mk_server_->get(in_segment.arrow_to_start->name, arrow_to_start);
    arrow_to_start.controls.back().markers.back().points[0] = arrow_to_start_start;
    arrow_to_start.controls.back().markers.back().points[1] = arrow_to_start_end;
    interactive_mk_server_->erase(arrow_to_start.name);
    interactive_mk_server_->insert(arrow_to_start);
    interactive_mk_server_->setCallback(arrow_to_start.name, boost::bind(&graphVertexProcessFb, _1, &segments_, interactive_mk_server_));

    // TF broadcaster update here
    updateTF(tf_array, arrow_to_start.name, arrow_to_start_start, arrow_to_start_end);
}

/*************************************************************************************************/
void InteractiveGraph::createArrowForSegment(Segment& new_segment)
{
    // Local variable
    geometry_msgs::Point arrow_to_start_start, arrow_to_start_end, arrow_to_end_start, arrow_to_end_end;

    // Set the arrow visual data
    arrow_to_start_start.x  = (new_segment.start.x()*.5) + (new_segment.end.x()*.5);
    arrow_to_start_start.y  = (new_segment.start.y()*.5) + (new_segment.end.y()*.5);
    arrow_to_end_start.x    = (new_segment.start.x()*.5) + (new_segment.end.x()*.5);
    arrow_to_end_start.y    = (new_segment.start.y()*.5) + (new_segment.end.y()*.5);

    arrow_to_start_end.x    = new_segment.start.x();
    arrow_to_start_end.y    = new_segment.start.y();
    arrow_to_end_end.x      = new_segment.end.x();
    arrow_to_end_end.y      = new_segment.end.y();

    // Set the segment arrow
    new_segment.arrow_to_end    = std::make_shared<visualization_msgs::InteractiveMarker>(makeGraphVertex_(arrow_to_end_start, arrow_to_end_end));
    new_segment.arrow_to_start  = std::make_shared<visualization_msgs::InteractiveMarker>(makeGraphVertex_(arrow_to_start_start, arrow_to_start_end));
}

/*************************************************************************************************/
std::vector<std::string> InteractiveGraph::shortestPath_(const std::string& edge_a, const std::string& edge_b, const Segment* seg_a, const Segment* seg_b)
{
    // Check if the start and end segment are equal
    if (seg_a->id == seg_b->id)
    {
        // Return an empty string list
        return std::vector<std::string>();
    }

    // Local variable
    const std::vector<int32_t>* neighbors_a, *neighbors_b;
    std::vector<std::string> shortest_path;

    // Check if we need to use the start segments or the end
    if (seg_a->start_marker->name == edge_a)
    {
        neighbors_a = &(seg_a->start_neigs);
    }
    else
    {
        neighbors_a = &(seg_a->end_neigs);
    }

    if (std::find(neighbors_a->begin(), neighbors_a->end(), seg_b->id) != neighbors_a->end())
    {
        return std::vector<std::string>();
    }

    if (seg_b->start_marker->name == edge_b)
    {
        neighbors_b = &(seg_b->start_neigs);
    }
    else
    {
        neighbors_b = &(seg_b->end_neigs);
    }

    for (auto id : *neighbors_b)
    {
        if (std::find(neighbors_a->begin(), neighbors_a->end(), id) != neighbors_a->end())
        {
            // Return an empty string list
            return std::vector<std::string>();
        }
    }

    std::vector<int32_t> edge_a_segs = *neighbors_a;
    std::vector<int32_t> edge_b_segs = *neighbors_b;

    // Add the start and end segment
    edge_a_segs.push_back(seg_a->id);
    edge_b_segs.push_back(seg_b->id);

    if (edge_b_segs.size() < edge_a_segs.size())
    {
        for (auto id : edge_b_segs)
        {
            auto candidate = shortestPathDirec_(edge_b, edge_a, id);
            if (candidate.size() < shortest_path.size() || shortest_path.empty())
            {
                shortest_path = candidate;
            }
        }

        std::reverse(shortest_path.begin(), shortest_path.end());
    }
    else
    {
        for (auto id : edge_a_segs)
        {
            for (auto& [id, seg] : segments_)
            {
                seg.visited = false;
            }
            auto candidate = shortestPathDirec_(edge_a, edge_b, id);

            if ((false == candidate.empty()) && ((candidate.size() < shortest_path.size()) || (shortest_path.empty())))
            {
                shortest_path = candidate;
            }
        }
    }

    for (auto& [id, seg] : segments_)
    {
        seg.visited = false;
    }

    return shortest_path;
}

/*************************************************************************************************/
std::vector<std::string> InteractiveGraph::shortestPathDirec_(const std::string& edge_a, const std::string& edge_b, int32_t id)
{
    std::vector<int32_t> next_edge_segs;
    std::string next_edge;
    std::vector<std::string> shortest_path;

    if (segments_[id].end_marker->name.compare(edge_a) == 0)
    {
        next_edge = segments_[id].start_marker->name;
        next_edge_segs = segments_[id].start_neigs;
    }
    else
    {
        next_edge = segments_[id].end_marker->name;
        next_edge_segs = segments_[id].end_neigs;
    }

    if (next_edge.compare(edge_b) == 0)
    {
        return std::vector<std::string>();
    }

    bool shortest_path_not_init = true;

    segments_[id].visited = true;

    for (auto nid : next_edge_segs)
    {
        if (false == segments_[nid].visited)
        {
            auto candidate = shortestPathDirec_(next_edge, edge_b, nid);
            if (((candidate.size() < shortest_path.size()) || (shortest_path_not_init) && candidate.size() < edges_markers_names_.size()))
            {
                shortest_path = candidate;
                shortest_path_not_init = false;
            }
        }
    }

    if (true == shortest_path_not_init)
    {
        return edges_markers_names_;
    }
    else
    {
        shortest_path.push_back(next_edge);
    }

    return shortest_path;
}

/*************************************************************************************************/
void InteractiveGraph::align_(const std::vector<std::string>& edges_list)
{
    // Local variable
    visualization_msgs::InteractiveMarker first, last;
    std::vector<geometry_msgs::TransformStamped> tf_array;

    interactive_mk_server_->get(edges_list.front(), first);
    interactive_mk_server_->get(edges_list.back(), last);

    auto markers_x = Eigen::ArrayXf::LinSpaced(edges_list.size(), first.pose.position.x, last.pose.position.x);
    auto markers_y = Eigen::ArrayXf::LinSpaced(edges_list.size(), first.pose.position.y, last.pose.position.y);

    for (auto i = 1; i < (edges_list.size() - 1); ++i)
    {
        visualization_msgs::InteractiveMarker marker;
        interactive_mk_server_->get(edges_list[i], marker);
        marker.pose.position.x = markers_x[i];
        marker.pose.position.y = markers_y[i];

        interactive_mk_server_->erase(edges_list[i]);
        interactive_mk_server_->insert(marker);
        interactive_mk_server_->setCallback(marker.name, boost::bind(&graphEdgeProcessFb, _1, &segments_, interactive_mk_server_, &edge_pose_update_, &edge_pose_update_mutex_, &clicked_point_, &clicked_point_update_, &clicked_point_update_mutex_, &br_));

        for (auto& [id, seg] : segments_)
        {
            bool update = false;

            if (seg.end_marker->name.compare(edges_list[i]) == 0)
            {
                seg.end.x() = markers_x[i];
                seg.end.y() = markers_y[i];
                update = true;
            }
            else if (seg.start_marker->name.compare(edges_list[i]) == 0)
            {
                seg.start.x() = markers_x[i];
                seg.start.y() = markers_y[i];
                update = true;
            }
            else
            {
                // Do nothing
            }

            if (update)
            {
                // Create the arro from the input segment
                updateArrowForSegment(seg, tf_array);

                {
                    std::scoped_lock lock(edge_pose_update_mutex_);
                    edge_pose_update_ = true;
                }
            }
        }
    }
    br_.sendTransform(tf_array);

    interactive_mk_server_->applyChanges();
}

/*************************************************************************************************/
void InteractiveGraph::unselectEdges_(const std::string& edge_a, const std::string& edge_b)
{
    visualization_msgs::InteractiveMarker marker;

    interactive_mk_server_->get(edge_a, marker);
    marker.controls.back().markers.back().color.r = 0.0;
    marker.controls.back().markers.back().color.g = 0.0;
    marker.controls.back().markers.back().color.b = 0.0;
    marker.controls.back().markers.back().color.a = 0.4;
    interactive_mk_server_->erase(marker.name);
    interactive_mk_server_->insert(marker);
    interactive_mk_server_->setCallback(marker.name, boost::bind(&graphEdgeProcessFb, _1, &segments_, interactive_mk_server_, &edge_pose_update_, &edge_pose_update_mutex_, &clicked_point_, &clicked_point_update_, &clicked_point_update_mutex_, &br_));

    // Check if the two nodes are different
    if(edge_a != edge_b)
    {
        interactive_mk_server_->get(edge_b, marker);
        marker.controls.back().markers.back().color.r = 0.0;
        marker.controls.back().markers.back().color.g = 0.0;
        marker.controls.back().markers.back().color.b = 0.0;
        marker.controls.back().markers.back().color.a = 0.4;
        interactive_mk_server_->erase(marker.name);
        interactive_mk_server_->insert(marker);
        interactive_mk_server_->setCallback(marker.name, boost::bind(&graphEdgeProcessFb, _1, &segments_, interactive_mk_server_, &edge_pose_update_, &edge_pose_update_mutex_, &clicked_point_, &clicked_point_update_, &clicked_point_update_mutex_, &br_));
        interactive_mk_server_->applyChanges();
    }

    // Loop on the segments list
    for (auto& [id, seg] : segments_)
    {
        if ((seg.end_marker->name == edge_a) || (seg.end_marker->name == edge_b))
        {
            seg.end_marker_selected = 0;
        }

        if ((seg.start_marker->name == edge_a) || (seg.start_marker->name == edge_b))
        {
            seg.start_marker_selected = 0;
        }
    }
}

/*************************************************************************************************/
void InteractiveGraph::setDirected_(Segment* seg, bool towards_start)
{
    seg->arrow_to_start_selected    = false;
    seg->arrow_to_end_selected      = false;

    if (true == towards_start)
    {
        seg->directed_towards_start = true;
        seg->directed = true;

        visualization_msgs::InteractiveMarker arrow_to_start;
        interactive_mk_server_->get(seg->arrow_to_start->name, arrow_to_start);
        arrow_to_start.controls.back().markers.back().color.r = 0.0;
        arrow_to_start.controls.back().markers.back().color.g = 1.0;
        arrow_to_start.controls.back().markers.back().color.b = 0.0;
        interactive_mk_server_->erase(arrow_to_start.name);
        interactive_mk_server_->insert(arrow_to_start);
        interactive_mk_server_->setCallback(arrow_to_start.name, boost::bind(&graphVertexProcessFb, _1, &segments_, interactive_mk_server_));
    }
    else
    {
        seg->directed_towards_end = true;
        seg->directed = true;

        visualization_msgs::InteractiveMarker arrow_to_end;
        interactive_mk_server_->get(seg->arrow_to_end->name, arrow_to_end);
        arrow_to_end.controls.back().markers.back().color.r = 0.0;
        arrow_to_end.controls.back().markers.back().color.g = 1.0;
        arrow_to_end.controls.back().markers.back().color.b = 0.0;
        interactive_mk_server_->erase(arrow_to_end.name);
        interactive_mk_server_->insert(arrow_to_end);
        interactive_mk_server_->setCallback(arrow_to_end.name, boost::bind(&graphVertexProcessFb, _1, &segments_, interactive_mk_server_));
    }
}

/*************************************************************************************************/
void InteractiveGraph::setBidirected_(Segment* seg)
{
    seg->directed                   = false;
    seg->directed_towards_end       = false;
    seg->directed_towards_start     = false;
    seg->arrow_to_start_selected    = false;
    seg->arrow_to_end_selected      = false;
    seg->mark_as_bidirected         = false;

    visualization_msgs::InteractiveMarker arrow_to_start, arrow_to_end;
    interactive_mk_server_->get(seg->arrow_to_start->name, arrow_to_start);
    arrow_to_start.controls.back().markers.back().color.r = 0.0;
    arrow_to_start.controls.back().markers.back().color.g = 0.7;
    arrow_to_start.controls.back().markers.back().color.b = 1.0;
    interactive_mk_server_->erase(arrow_to_start.name);
    interactive_mk_server_->insert(arrow_to_start);
    interactive_mk_server_->setCallback(arrow_to_start.name, boost::bind(&graphVertexProcessFb, _1, &segments_, interactive_mk_server_));

    interactive_mk_server_->get(seg->arrow_to_end->name, arrow_to_end);
    arrow_to_end.controls.back().markers.back().color.r = 0.0;
    arrow_to_end.controls.back().markers.back().color.g = 0.7;
    arrow_to_end.controls.back().markers.back().color.b = 1.0;
    interactive_mk_server_->erase(arrow_to_end.name);
    interactive_mk_server_->insert(arrow_to_end);
    interactive_mk_server_->setCallback(arrow_to_end.name, boost::bind(&graphVertexProcessFb, _1, &segments_, interactive_mk_server_));
}

}
