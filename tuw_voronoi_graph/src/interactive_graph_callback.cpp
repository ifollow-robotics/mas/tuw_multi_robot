// -*- mode: c++; coding: utf-8; tab-width: 4 -*-
/*******************************************************************************
* 2-Clause BSD License
*
* Copyright (c) 2019, iFollow Robotics
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
*   list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*   this list of conditions and the following disclaimer in the documentation
*   and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* Authors: Aurélien Gosseye
*******************************************************************************/

#include <tuw_voronoi_graph.h>

namespace interactive_graph {

/*************************************************************************************************/
void InteractiveGraph::graphCB_(const ros::MessageEvent<const tuw_multi_robot_msgs::Graph> &event)
{
    const tuw_multi_robot_msgs::Graph_<std::allocator<void>>::ConstPtr &msg = event.getMessage();

    if (msg->vertices.empty())
    {
        return;
    }

    new_graph_msg_ = *msg;

    for (const auto& v : msg->vertices)
    {
        Segment seg;

        seg.id = v.id;

        // cppcheck-suppress constStatement
        seg.start << (v.path.front().x * msg->resolution), (v.path.front().y * msg->resolution);
        seg.start[0] += msg->origin.position.x;
        seg.start[1] += msg->origin.position.y;

        // cppcheck-suppress constStatement
        seg.end << (v.path.back().x * msg->resolution), (v.path.back().y * msg->resolution);
        seg.end[0] += msg->origin.position.x;
        seg.end[1] += msg->origin.position.y;

        seg.width = v.width * msg->resolution;
        seg.init_width = seg.width;

        seg.directed                = false;
        seg.arrow_to_end_selected   = false;
        seg.arrow_to_start_selected = false;
        seg.end_marker_selected     = 0;
        seg.start_marker_selected   = false;
        seg.directed_towards_end    = false;
        seg.directed_towards_start  = false;
        seg.visited                 = false;
        seg.mark_as_bidirected      = false;

        for (int n_id : v.predecessors)
        {
            int iIndex = 0;
            for (auto i_index = msg->vertices.begin(); i_index != msg->vertices.end(); i_index++)
            {
                if (i_index->id == n_id)
                {
                    break;
                }
                else
                {
                    iIndex++;
                }
            }

            if (msg->vertices[iIndex].path.front().x == v.path.front().x &&
                msg->vertices[iIndex].path.front().y == v.path.front().y)
            {
                seg.start_neigs.push_back(n_id);
            }
            else if (msg->vertices[iIndex].path.back().x == v.path.front().x &&
                     msg->vertices[iIndex].path.back().y == v.path.front().y)
            {
                seg.start_neigs.push_back(n_id);
            }
            else
            {
                // TODO
            }

            if (msg->vertices[iIndex].path.front().x == v.path.back().x &&
                msg->vertices[iIndex].path.front().y == v.path.back().y)
            {
                seg.end_neigs.push_back(n_id);
            }
            else if (msg->vertices[iIndex].path.back().x == v.path.back().x &&
                     msg->vertices[iIndex].path.back().y == v.path.back().y)
            {
                seg.end_neigs.push_back(n_id);
            }
            else
            {
                // TODO
            }
        }

        for (int n_id : v.successors)
        {
            int iIndex = 0;
            for (auto i_index = msg->vertices.begin(); i_index != msg->vertices.end(); i_index++)
            {
                if (i_index->id == n_id)
                {
                    break;
                }
                else
                {
                    iIndex++;
                }
            }

            if (msg->vertices[iIndex].path.front().x == v.path.front().x &&
                msg->vertices[iIndex].path.front().y == v.path.front().y)
            {
                seg.start_neigs.push_back(n_id);
            }
            else if (msg->vertices[iIndex].path.back().x == v.path.front().x &&
                     msg->vertices[iIndex].path.back().y == v.path.front().y)
            {
                seg.start_neigs.push_back(n_id);
            }
            else
            {
                // TODO
            }

            if (msg->vertices[iIndex].path.front().x == v.path.back().x &&
                msg->vertices[iIndex].path.front().y == v.path.back().y)
            {
                seg.end_neigs.push_back(n_id);
            }
            else if (msg->vertices[iIndex].path.back().x == v.path.back().x &&
                     msg->vertices[iIndex].path.back().y == v.path.back().y)
            {
                seg.end_neigs.push_back(n_id);
            }
            else
            {
                // TODO
            }
        }
        segments_[v.id] = seg;
    }

    ori_graph_msg_ = *msg;
    got_graph_ = true;
    graph_sub_.shutdown();
}

/*************************************************************************************************/
void InteractiveGraph::iGraphCB_(const ifollow_mas_msgs::Graph& msg)
{
    std::scoped_lock lock(augmented_graph_mutex_);
    augmented_graph_ = msg;
    got_aug_graph_ = true;
    i_graph_sub_.shutdown();
}

/*************************************************************************************************/
void InteractiveGraph::graphAreasCB_(const ifollow_mas_msgs::AreaArray& msg)
{
    std::map<int, ifollow_mas_msgs::Area> areas;

    for (const auto& area : msg.areas)
    {
        areas[area.id] = area;
    }

    for (int i = 0; i < msg.areas.size(); ++i)
    {
        for (const auto& pt : areas[i].points)
        {
            geometry_msgs::PointStamped ptst;
            ptst.header = msg.header;
            ptst.point.x = pt.x;
            ptst.point.y = pt.y;
            clickedPointCB_(ptst);
        }

        // Set the area type
        areas[msg.areas[i].id].type = areas[i].type; 

        toggleValidationButton();
    }

    interactive_mk_server_->applyChanges();

    got_graph_areas_ = true;
    graph_areas_sub_.shutdown();
}

/*************************************************************************************************/
void InteractiveGraph::iGraphUpdateCB_(const ifollow_mas_msgs::Graph& msg)
{
    std::scoped_lock lock(augmented_graph_mutex_);

    for (auto v : msg.vertices)
    {
        int iIndex = 0;
        for (auto i_index = msg.vertices.begin(); i_index != msg.vertices.end(); i_index++)
        {
            if (i_index->id == v.id)
            {
                break;
            }
            else
            {
                iIndex++;
            }
        }

        augmented_graph_.vertices[iIndex] = v;
    }

    vizIfollowGraph_();
}

/*************************************************************************************************/
void InteractiveGraph::clickedPointCB_(const geometry_msgs::PointStamped& msg)
{
    if (0 == clicked_point_update_)
    {
        if (current_area == NULL)
        {
            area_vertex_id_ = 0;
            areas[area_id_] = Area();
            areas[area_id_].finished = false;
            current_area = &(areas[area_id_]);
            areas[area_id_].id = area_id_;
            areas[area_id_].vertices_markers.push_back(std::make_shared<visualization_msgs::InteractiveMarker>(makeAreaVertex_(msg.point)));
            areas[area_id_].validation_button = std::make_shared<visualization_msgs::InteractiveMarker>(makeAreaValidationButton_(msg.point));
            interactive_mk_server_->applyChanges();
            area_id_++;
        }
        else if (current_area->finished)
        {
            area_vertex_id_ = 0;
            areas[area_id_] = Area();
            areas[area_id_].finished = false;
            current_area = &(areas[area_id_]);
            areas[area_id_].id = area_id_;
            areas[area_id_].vertices_markers.push_back(std::make_shared<visualization_msgs::InteractiveMarker>(makeAreaVertex_(msg.point)));
            areas[area_id_].validation_button = std::make_shared<visualization_msgs::InteractiveMarker>(makeAreaValidationButton_(msg.point));
            interactive_mk_server_->applyChanges();
            area_id_++;
        }
        else
        {
            area_vertex_id_++;
            current_area->vertices_markers.push_back(std::make_shared<visualization_msgs::InteractiveMarker>(makeAreaVertex_(msg.point)));
            updateValButton();
            interactive_mk_server_->applyChanges();
        }
    }
    else
    {
        clicked_point_update_++;
        clicked_point_ = msg.point;
    }
}

/*************************************************************************************************/
bool InteractiveGraph::saveState_(ifollow_ll_msgs::SetString::Request& req, ifollow_ll_msgs::SetString::Response& res)
{
    std::string delimiter = ";";

    std::vector<std::string> data;
    if(req.data.find(';') != std::string::npos)
    {
        std::string reminder = req.data;
        for (size_t i = 0; !reminder.empty(); ++i)
        {
            data.emplace_back(reminder.substr(
                0, reminder.find_first_of(delimiter)));

            if (data.back().size() == reminder.size())
            {
                break;
            }

            reminder = reminder.substr(
                data[i].size() + delimiter.length(), reminder.size());
        }
    }
    else
    {
        data.emplace_back(req.data);
        data.emplace_back("world");
    }

    save_(data[0], data[1]);
    res.success = true;
    res.message = "graph saved to " + path_to_save_ + data[0];

    return true;
}

}
