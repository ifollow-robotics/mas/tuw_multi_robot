// -*- mode: c++; coding: utf-8; tab-width: 4 -*-
/*******************************************************************************
* 2-Clause BSD License
*
* Copyright (c) 2019, iFollow Robotics
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice, this
*   list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*   this list of conditions and the following disclaimer in the documentation
*   and/or other materials provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* Authors: Aurélien Gosseye
*******************************************************************************/

#include <tuw_voronoi_graph.h>

namespace interactive_graph
{

/*************************************************************************************************/
visualization_msgs::InteractiveMarker InteractiveGraph::makeAreaVertex_(const geometry_msgs::Point& position)
{
    visualization_msgs::InteractiveMarker marker;
    marker.header.frame_id = "world";
    marker.pose.position = position;
    marker.pose.orientation.x = 0;
    marker.pose.orientation.y = 0;
    marker.pose.orientation.z = 0;
    marker.pose.orientation.w = 1;
    marker.scale = .3;

    marker.name = "area_" + std::to_string(current_area->id) + "_vtx_" + std::to_string(area_vertex_id_++);

    visualization_msgs::InteractiveMarkerControl control;

    control.orientation.x = 0;
    control.orientation.y = M_SQRT1_2;
    control.orientation.z = 0;
    control.orientation.w = M_SQRT1_2;
    control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_PLANE;

    // create a control which will move the box
    // this control does not contain any markers,
    // which will cause RViz to insert a disc
    // marker.controls.push_back(control);

    // create another control that has a cube marker
    // make a box which also moves in the plane
    control.markers.push_back(makeSphere2_(marker, 0.5, Color::orange_red));
    control.always_visible = true;
    marker.controls.push_back(control);

    interactive_mk_server_->insert(marker);
    interactive_mk_server_->setCallback(marker.name, boost::bind(&areaProcessFb, _1, current_area, interactive_mk_server_, this));

    return marker;
}

/*************************************************************************************************/
visualization_msgs::InteractiveMarker InteractiveGraph::makeAreaValidationButton_(const geometry_msgs::Point& position)
{
    visualization_msgs::InteractiveMarker marker;
    marker.header.frame_id = "world";
    marker.pose.position = position;
    marker.pose.orientation.x = 0;
    marker.pose.orientation.y = 0;
    marker.pose.orientation.z = 0;
    marker.pose.orientation.w = 1;
    marker.scale = .3;

    marker.name = "area_" + std::to_string(current_area->id) + "_val";

    visualization_msgs::InteractiveMarkerControl but;
    but.interaction_mode = visualization_msgs::InteractiveMarkerControl::BUTTON;
    but.markers.push_back(makeFlattenedSphere2_(marker));

    visualization_msgs::Marker text_mk;
    text_mk.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
    text_mk.text = "area_" + std::to_string(current_area->id);
    text_mk.id = current_area->id;
    text_mk.pose.position.x = 0.4;
    text_mk.pose.position.y = 0.4;
    text_mk.scale.z = 0.35;
    text_mk.color.r = 0.0;
    text_mk.color.g = 0.0;
    text_mk.color.b = 0.0;
    text_mk.color.a = 1.0;

    but.markers.push_back(text_mk);

    but.always_visible = true;
    marker.controls.push_back(but);

    interactive_mk_server_->insert(marker);
    interactive_mk_server_->setCallback(marker.name, boost::bind(&areaProcessFb, _1, current_area, interactive_mk_server_, this));

    return marker;
}

/*************************************************************************************************/
visualization_msgs::InteractiveMarker InteractiveGraph::makeGraphEdgeMarker_(const geometry_msgs::Point& position)
{
    visualization_msgs::InteractiveMarker marker;
    marker.header.frame_id = "world";
    marker.pose.position = position;
    marker.pose.orientation.x = 0;
    marker.pose.orientation.y = 0;
    marker.pose.orientation.z = 0;
    marker.pose.orientation.w = 1;
    marker.scale = .3;

    marker.name = "edge_" + std::to_string(edge_mk_id_++);

    visualization_msgs::InteractiveMarkerControl control;

    control.orientation.x = 0;
    control.orientation.y = M_SQRT1_2;
    control.orientation.z = 0;
    control.orientation.w = M_SQRT1_2;
    control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_PLANE;

    // create a control which will move the box
    // this control does not contain any markers,
    // which will cause RViz to insert a disc
    // marker.controls.push_back(control);

    // create another control that has a cube marker
    // make a box which also moves in the plane
    control.markers.push_back(makeSphere2_(marker));
    control.always_visible = true;
    marker.controls.push_back(control);

    visualization_msgs::InteractiveMarkerControl but;
    but.interaction_mode = visualization_msgs::InteractiveMarkerControl::BUTTON;
    but.markers.push_back(makeFlattenedSphere2_(marker));
    but.always_visible = true;
    marker.controls.push_back(but);

    interactive_mk_server_->insert(marker);
    interactive_mk_server_->setCallback(marker.name, boost::bind(&graphEdgeProcessFb, _1, &segments_, interactive_mk_server_, &edge_pose_update_, &edge_pose_update_mutex_, &clicked_point_, &clicked_point_update_, &clicked_point_update_mutex_, &br_));

    return marker;
}

/*************************************************************************************************/
visualization_msgs::InteractiveMarker InteractiveGraph::makeWidthLine_(double width, double length, const std::string& frame_id)
{
    visualization_msgs::InteractiveMarker marker;
    marker.header.frame_id = frame_id;
    marker.pose.orientation.x = 0;
    marker.pose.orientation.y = 0;
    marker.pose.orientation.z = 0;
    marker.pose.orientation.w = 1;
    marker.name = "line_list_" + std::to_string(line_list_mk_id_++);

    visualization_msgs::InteractiveMarkerControl control;
    control.orientation.x = 0;
    control.orientation.y = 0;
    control.orientation.z = M_SQRT1_2;
    control.orientation.w = M_SQRT1_2;
    control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_AXIS;
    control.always_visible = true;

    control.markers.push_back(makeLine_(width, length));
    marker.controls.push_back(control);

    interactive_mk_server_->insert(marker);
    interactive_mk_server_->setCallback(marker.name, boost::bind(&graphVertexWidthProcessFb, _1, &segments_, interactive_mk_server_, &vertex_width_update_, &vertex_width_update_mutex_));

    return marker;
}

/*************************************************************************************************/
visualization_msgs::InteractiveMarker InteractiveGraph::makeGraphVertex_(const geometry_msgs::Point& start, const geometry_msgs::Point& end)
{
    visualization_msgs::InteractiveMarker marker;
    marker.header.frame_id = "world";
    marker.pose.orientation.x = 0;
    marker.pose.orientation.y = 0;
    marker.pose.orientation.z = 0;
    marker.pose.orientation.w = 1;
    marker.scale = .3;

    marker.name = "arrow_" + std::to_string(arrow_mk_id_++);

    // create a non-interactive control which contains the box
    visualization_msgs::InteractiveMarkerControl control;
    control.orientation.x = 0;
    control.orientation.y = M_SQRT1_2;
    control.orientation.z = 0;
    control.orientation.w = M_SQRT1_2;
    control.interaction_mode = visualization_msgs::InteractiveMarkerControl::BUTTON;
    control.always_visible = true;
    control.markers.push_back(makeArrow_(start, end, marker.scale));
    marker.controls.push_back(control);

    interactive_mk_server_->insert(marker);
    interactive_mk_server_->setCallback(marker.name, boost::bind(&graphVertexProcessFb, _1, &segments_, interactive_mk_server_));

    return marker;
}

/*************************************************************************************************/
std::vector<double> rgbFromScalar(double min, double max, double value)
{
    /*plot short rainbow RGB*/
    value = (value - min)/(max - min);

    double a = (1 - value) / 0.25;      // invert and group
    int X = floor(a);                   // this is the integer part
    double Y = floor(255 * (a - X));    // fractional part from 0 to 255
    int r, g, b;
    switch(X)
    {
        case 0: r=255; g=Y; b=0; break;
        case 1: r=255-Y; g=255; b=0; break;
        case 2: r=0; g=255; b=Y; break;
        case 3: r=0; g=255-Y; b=255; break;
        case 4: r=0; g=0; b=255; break;
    }
    std::vector<double> color(4);
    color[0] = r/255.;
    color[1] = g/255.;
    color[2] = b/255.;
    color[3] = 1;

    return color;
}

/*************************************************************************************************/
visualization_msgs::Marker makeText(const std::string& frame, const std::string& text, int id, double x, double y, ros::Time now)
{
    visualization_msgs::Marker m;
    m.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
    m.pose.position.x = x;
    m.pose.position.y = y;
    m.pose.position.z = 0.5;
    m.pose.orientation.w = 1;
    m.text = text;
    m.id = id;
    m.scale.z = 0.35;
    m.color.r = 0.0;
    m.color.g = 0.0;
    m.color.b = 0.0;
    m.color.a = 1.0;
    m.header.stamp = now;
    m.header.frame_id = frame;

    return m;
}

/*************************************************************************************************/
visualization_msgs::Marker makeFooprintMarker(double x, double y, double theta)
{
    static footprints::Footprints<float> footprints_;
    ros::NodeHandle n;
    footprints_.fromParam(n, "nav_governor");
    if (!footprints_.isValid())
    {
        ROS_ERROR_STREAM(ros::this_node::getName() << ": Failed to correctly load the footprints");
        throw std::runtime_error("failed to correctly load the footprints");
    }

    geometry_msgs::Polygon pol;
    ifollow_nav_msgs::SimplifiedFpModel fp_model;
    footprints_.simplFpAt(fp_model, 1.0f, uint16_t(1), uint16_t(1), x, y, theta);
    footprints::simplFpModelToPoly(fp_model, pol, 10);

    // footprint marker
    static visualization_msgs::Marker fp_mk_;
    fp_mk_.action = visualization_msgs::Marker::ADD;
    fp_mk_.type = visualization_msgs::Marker::LINE_STRIP;
    fp_mk_.scale.x = 0.05;

    visualization_msgs::Marker fp_mk(fp_mk_);

    for (auto pt : pol.points)
    {
        geometry_msgs::Point fpt;
        fpt.x = pt.x;
        fpt.y = pt.y;
        fpt.z = pt.z + 0.11;
        fp_mk.points.push_back(fpt);
    }
    geometry_msgs::Point fpt;
    fpt.x = pol.points.front().x;
    fpt.y = pol.points.front().y;
    fpt.z = pol.points.front().z + 0.11;
    fp_mk.points.push_back(fpt);

    fp_mk.color.r = 1;
    fp_mk.color.a = 1;
    return fp_mk;
}

/*************************************************************************************************/
visualization_msgs::Marker makeMesh(std::string mesh_path, float scale, float r, float g, float b, float a, double x/*=0*/, double y/*=0*/, double z/*=0*/, double qx/*=0*/, double qy/*=0*/, double qz/*=0*/, double qw/*=1*/)
{
    visualization_msgs::Marker marker;
    marker.type = visualization_msgs::Marker::MESH_RESOURCE;
    marker.mesh_resource = mesh_path;
    marker.pose.position.x = x;
    marker.pose.position.y = y;
    marker.pose.position.z = z;
    marker.pose.orientation.x = qx;
    marker.pose.orientation.y = qy;
    marker.pose.orientation.z = qz;
    marker.pose.orientation.w = qw;
    marker.scale.x = scale;
    marker.scale.y = scale;
    marker.scale.z = scale;
    marker.color.r = r;  // 1.0;
    marker.color.g = g;  // 0.5;
    marker.color.b = b;  // 0.5;
    marker.color.a = a;  // 1.0;

    return marker;
}

/*************************************************************************************************/
visualization_msgs::Marker makeSphere2_(visualization_msgs::InteractiveMarker &msg, double scale/*=1*/, std_msgs::ColorRGBA color/*=Color::red2*/)
{
    visualization_msgs::Marker marker;

    marker.type = visualization_msgs::Marker::MESH_RESOURCE;
    marker.mesh_resource = "package://tuw_voronoi_graph/meshes/dodecagon.stl";

    marker.scale.x = scale * 0.8/1000.;
    marker.scale.y = scale * 0.8/1000.;
    marker.scale.z = scale * 4/1000.;
    marker.color.r = color.r;
    marker.color.g = color.g;
    marker.color.b = color.b;
    marker.color.a = color.a;
    marker.pose.orientation.x = 0;
    marker.pose.orientation.y = 0;
    marker.pose.orientation.z = 0;
    marker.pose.orientation.w = 1;

    return marker;
}

/*************************************************************************************************/
visualization_msgs::Marker makeFlattenedSphere2_(visualization_msgs::InteractiveMarker &msg)
{
    visualization_msgs::Marker marker;

    marker.type = visualization_msgs::Marker::MESH_RESOURCE;
    marker.mesh_resource = "package://tuw_voronoi_graph/meshes/dodecagon.stl";

    marker.scale.x = 1.6/1000.;
    marker.scale.y = 1.6/1000.;
    marker.scale.z = 1.25/1000.;
    marker.color.r = 0.0;
    marker.color.g = 0.0;
    marker.color.b = 0.0;
    marker.color.a = 0.4;
    marker.pose.orientation.x = 0;
    marker.pose.orientation.y = 0;
    marker.pose.orientation.z = 0;
    marker.pose.orientation.w = 1;

    return marker;
}

/*************************************************************************************************/
visualization_msgs::Marker makeArrow_(const geometry_msgs::Point& start, const geometry_msgs::Point& end, float scale)
{
    visualization_msgs::Marker marker;
    marker.type = visualization_msgs::Marker::ARROW;
    marker.scale.x = scale * .55 * 0.5; // shaft diameter
    marker.scale.y = scale * .55;       // head diameter
    marker.scale.z = scale * .55 * 2;   // head length
    marker.points.resize(2);
    marker.points[0] = start;
    marker.points[1] = end;
    marker.color.r = 0.0;
    marker.color.g = 0.7;
    marker.color.b = 1.0;
    marker.color.a = 1.0;
    return marker;
}

/*************************************************************************************************/
visualization_msgs::Marker makeLine_(double width, double length)
{
    visualization_msgs::Marker marker;
    marker.type = visualization_msgs::Marker::LINE_LIST;
    marker.points.resize(2);

    marker.points[0].x = 0.0;
    marker.points[0].y = width / 2.;
    marker.points[0].z = 0.01;

    marker.points[1].x = length;
    marker.points[1].y = width / 2.;
    marker.points[1].z = 0.01;

    marker.scale.x = 0.1 * .7;
    marker.color.r = 0.576;
    marker.color.g = 0.2039;
    marker.color.b = 1.0;
    marker.color.a = 1.0;

    return marker;
}

/*************************************************************************************************/
void InteractiveGraph::vizIfollowGraph_()
{
    if (augmented_graph_.vertices.size() == segments_.size())
    {
        auto now = ros::Time::now();

        std::map<std::string, std::tuple<visualization_msgs::Marker, visualization_msgs::MarkerArray, double, double> > layers;

        layers["max_vel"] = std::make_tuple(
            visualization_msgs::Marker(),
            visualization_msgs::MarkerArray(),
            default_vertex_.allowed_max_lvel,
            default_vertex_.allowed_max_lvel);

        layers["turtle"] = std::make_tuple(
            visualization_msgs::Marker(),
            visualization_msgs::MarkerArray(),
            default_vertex_.turtle_on,
            default_vertex_.turtle_on);

        layers["pose0"] = std::make_tuple(
            visualization_msgs::Marker(),
            visualization_msgs::MarkerArray(), 1, 1);

        layers["pose1"] = std::make_tuple(
            visualization_msgs::Marker(),
            visualization_msgs::MarkerArray(), 1, 1);

        layers["odom0_twist"] = std::make_tuple(
            visualization_msgs::Marker(),
            visualization_msgs::MarkerArray(),  1, 1);
        layers["precise_fp"] = std::make_tuple(
            visualization_msgs::Marker(),
            visualization_msgs::MarkerArray(), 0, 0);

        layers["beep"] = std::make_tuple(
            visualization_msgs::Marker(),
            visualization_msgs::MarkerArray(), 0, 0);

        layers["obst_coll_hor"] = std::make_tuple(
            visualization_msgs::Marker(),
            visualization_msgs::MarkerArray(),
            default_vertex_.multi_robot_min_dist,
            default_vertex_.multi_robot_min_dist);

        layers["laser_coll_hor"] = std::make_tuple(
            visualization_msgs::Marker(),
            visualization_msgs::MarkerArray(),
            default_vertex_.laser_min_dist,
            default_vertex_.laser_min_dist);

        layers["cam_coll_hor"] = std::make_tuple(
            visualization_msgs::Marker(),
            visualization_msgs::MarkerArray(),
            default_vertex_.cam_min_dist,
            default_vertex_.cam_min_dist);

        layers["safe_lift_inflation"] = std::make_tuple(
            visualization_msgs::Marker(),
            visualization_msgs::MarkerArray(),
            default_vertex_.safe_lift_inflation,
            default_vertex_.safe_lift_inflation);

        layers["force_alt_plan_use"] = std::make_tuple(
            visualization_msgs::Marker(),
            visualization_msgs::MarkerArray(),
            default_vertex_.force_alt_plan_use,
            default_vertex_.force_alt_plan_use);

        layers["allow_alt_plan_use"] = std::make_tuple(
            visualization_msgs::Marker(),
            visualization_msgs::MarkerArray(),
            default_vertex_.allow_alt_plan_use,
            default_vertex_.allow_alt_plan_use);

        layers["allow_spontaneous_alt_plan_use"] = std::make_tuple(
            visualization_msgs::Marker(),
            visualization_msgs::MarkerArray(),
            default_vertex_.allow_spontaneous_alt_plan_use,
            default_vertex_.allow_spontaneous_alt_plan_use);

        layers["use_line_on_floor_detection"] = std::make_tuple(
            visualization_msgs::Marker(),
            visualization_msgs::MarkerArray(),
            default_vertex_.use_line_on_floor_detection,
            default_vertex_.use_line_on_floor_detection);

        layers["bs_mode"] = std::make_tuple(
            visualization_msgs::Marker(),
            visualization_msgs::MarkerArray(),
            default_vertex_.bs_mode,
            default_vertex_.bs_mode);

        layers["use_mr_comm"] = std::make_tuple(
            visualization_msgs::Marker(),
            visualization_msgs::MarkerArray(),
            default_vertex_.use_mr_comm,
            default_vertex_.use_mr_comm);

        layers["amcl_localization_on"] = std::make_tuple(
            visualization_msgs::Marker(),
            visualization_msgs::MarkerArray(),
            0.0,
            1.0);

        visualization_msgs::MarkerArray seg_id_markers;

        for (auto& [k, v] : layers)
        {
            std::get<1>(v).markers.resize(segments_.size());
            std::get<0>(v).type = visualization_msgs::Marker::LINE_LIST;
            std::get<0>(v).points.resize(augmented_graph_.vertices.size()*2);
            std::get<0>(v).colors.resize(augmented_graph_.vertices.size()*2);
            std::get<0>(v).header.stamp = now;
            std::get<0>(v).header.frame_id = "world";
            std::get<0>(v).pose.orientation.w = 1;
            std::get<0>(v).scale.x = 0.25;
        }

        // seg_id_markers.markers.resize(segments_.size());

        for (auto v : augmented_graph_.vertices)
        {
            for (auto& [key, val] : layers)
            {
                double seg_scalar;

                if (key == "max_vel")
                {
                    seg_scalar = v.allowed_max_lvel <= 0 ? default_vertex_.allowed_max_lvel : v.allowed_max_lvel;
                }
                else if (key == "obst_coll_hor")
                {
                    seg_scalar = v.multi_robot_min_dist <= 0 ? default_vertex_.multi_robot_min_dist : v.multi_robot_min_dist;
                }
                else if (key == "laser_coll_hor")
                {
                    seg_scalar = v.laser_min_dist <= 0 ? default_vertex_.laser_min_dist : v.laser_min_dist;
                }
                else if (key == "cam_coll_hor")
                {
                    seg_scalar = v.cam_min_dist <= 0 ? default_vertex_.cam_min_dist : v.cam_min_dist;
                }
                else if (key == "safe_lift_inflation")
                {
                    seg_scalar = v.safe_lift_inflation <= 0 ? default_vertex_.safe_lift_inflation : v.safe_lift_inflation;
                }
                else if (key == "force_alt_plan_use")
                {
                    seg_scalar = v.force_alt_plan_use <= 0 ? default_vertex_.force_alt_plan_use : v.force_alt_plan_use;
                }
                else if (key == "allow_alt_plan_use")
                {
                    seg_scalar = v.allow_alt_plan_use <= 0 ? default_vertex_.allow_alt_plan_use : v.allow_alt_plan_use;
                }
                else if (key == "allow_spontaneous_alt_plan_use")
                {
                    seg_scalar = v.allow_spontaneous_alt_plan_use <= 0 ? default_vertex_.allow_spontaneous_alt_plan_use : v.allow_spontaneous_alt_plan_use;
                }
                else if (key == "use_line_on_floor_detection")
                {
                    seg_scalar = v.use_line_on_floor_detection <= 0 ? default_vertex_.use_line_on_floor_detection : v.use_line_on_floor_detection;
                }
                else if (key == "bs_mode")
                {
                    seg_scalar = v.bs_mode <= 0 ? default_vertex_.bs_mode : v.bs_mode;
                }
                else if (key == "use_mr_comm")
                {
                    seg_scalar = v.use_mr_comm <= 0 ? default_vertex_.use_mr_comm : v.use_mr_comm;
                }
                else if (key == "turtle")
                {
                    seg_scalar = v.turtle_on < 0 ? default_vertex_.turtle_on : v.turtle_on;
                }
                else if (key == "pose0" || key == "pose1" || key == "odom0_twist")
                {
                    if (std::find(v.odom_sources_to_turn_off.begin(), v.odom_sources_to_turn_off.end(), key) != v.odom_sources_to_turn_off.end())
                    {
                        seg_scalar = 0;
                    }
                    else
                    {
                        seg_scalar = 1;
                    }
                }
                else if (key == "precise_fp")
                {
                    seg_scalar = v.use_precise_fp < 0 ? default_vertex_.use_precise_fp : v.use_precise_fp;
                }
                else if (key == "beep")
                {
                    seg_scalar = v.beep_if_blinkers_on < 0 ? default_vertex_.beep_if_blinkers_on : v.beep_if_blinkers_on;
                }
                else
                {
                    // ROS_WARN_STREAM("Unknown key: " << key << ". Set value to zero");
                    seg_scalar = 0.0;
                }

                if (seg_scalar < std::get<2>(val))
                {
                    std::get<2>(val) = seg_scalar;
                }
                if (seg_scalar > std::get<3>(val))
                {
                    std::get<3>(val) = seg_scalar;
                }
            }
        }

        int i = 0;

        for (auto v : augmented_graph_.vertices)
        {
            double init_x = v.path[0].x;
            double init_y = v.path[0].y;
            double final_x = v.path[1].x;
            double final_y = v.path[1].y;

            for (auto& [key, val] : layers)
            {
                std::get<0>(val).points[i].x = init_x;
                std::get<0>(val).points[i].y = init_y;
                std::get<0>(val).points[i].z = 0;
                std::get<0>(val).points[i+1].x = final_x;
                std::get<0>(val).points[i+1].y = final_y;
                std::get<0>(val).points[i+1].z = 0;

                double seg_scalar;

                if (key == "max_vel")
                {
                    seg_scalar = v.allowed_max_lvel <= 0 ? default_vertex_.allowed_max_lvel : v.allowed_max_lvel;
                }
                else if (key == "obst_coll_hor")
                {
                    seg_scalar = v.multi_robot_min_dist <= 0 ? default_vertex_.multi_robot_min_dist : v.multi_robot_min_dist;
                }
                else if (key == "laser_coll_hor")
                {
                    seg_scalar = v.laser_min_dist <= 0 ? default_vertex_.laser_min_dist : v.laser_min_dist;
                }
                else if (key == "cam_coll_hor")
                {
                    seg_scalar = v.cam_min_dist <= 0 ? default_vertex_.cam_min_dist : v.cam_min_dist;
                }
                else if (key == "safe_lift_inflation")
                {
                    seg_scalar = v.safe_lift_inflation <= 0 ? default_vertex_.safe_lift_inflation : v.safe_lift_inflation;
                }
                else if (key == "force_alt_plan_use")
                {
                    seg_scalar = v.force_alt_plan_use <= 0 ? default_vertex_.force_alt_plan_use : v.force_alt_plan_use;
                }
                else if (key == "allow_alt_plan_use")
                {
                    seg_scalar = v.allow_alt_plan_use <= 0 ? default_vertex_.allow_alt_plan_use : v.allow_alt_plan_use;
                }
                else if (key == "allow_spontaneous_alt_plan_use")
                {
                    seg_scalar = v.allow_spontaneous_alt_plan_use <= 0 ? default_vertex_.allow_spontaneous_alt_plan_use : v.allow_spontaneous_alt_plan_use;
                }
                else if (key == "use_line_on_floor_detection")
                {
                    seg_scalar = v.use_line_on_floor_detection <= 0 ? default_vertex_.use_line_on_floor_detection : v.use_line_on_floor_detection;
                }
                else if (key == "bs_mode")
                {
                    seg_scalar = v.bs_mode <= 0 ? default_vertex_.bs_mode : v.bs_mode;
                }
                else if (key == "use_mr_comm")
                {
                    seg_scalar = v.use_mr_comm <= 0 ? default_vertex_.use_mr_comm : v.use_mr_comm;
                }
                else if (key == "turtle")
                {
                    seg_scalar = v.turtle_on < 0 ? default_vertex_.turtle_on : v.turtle_on;
                }
                else if (key == "pose0" || key == "pose1" || key == "odom0_twist")
                {
                    if (std::find(v.odom_sources_to_turn_off.begin(), v.odom_sources_to_turn_off.end(), key) != v.odom_sources_to_turn_off.end())
                    {
                        seg_scalar = 0;
                    }
                    else
                    {
                        seg_scalar = 1;
                    }
                }
                else if (key == "precise_fp")
                {
                    seg_scalar = v.use_precise_fp < 0 ? default_vertex_.use_precise_fp : v.use_precise_fp;
                }
                else if (key == "beep")
                {
                    seg_scalar = v.beep_if_blinkers_on < 0 ? default_vertex_.beep_if_blinkers_on : v.beep_if_blinkers_on;
                }
                else if (key == "amcl_localization_on")
                {
                    seg_scalar = v.amcl_localization_on < 0 ? default_vertex_.amcl_localization_on : v.amcl_localization_on;
                }
                else
                {
                    // ROS_WARN_STREAM("Unknown key: " << key << ". Set value to zero");
                    seg_scalar = 0.0;
                }

                auto color = rgbFromScalar(std::get<2>(val), std::get<3>(val), seg_scalar);
                std::get<0>(val).colors[i].r = color[0];
                std::get<0>(val).colors[i].g = color[1];
                std::get<0>(val).colors[i].b = color[2];
                std::get<0>(val).colors[i].a = color[3];
                std::get<0>(val).colors[i+1] = std::get<0>(val).colors[i];

                std::ostringstream text;
                text << std::setprecision(3);
                text << seg_scalar;

                std::get<1>(val).markers.push_back(makeText("world", text.str(), (i/2.), (init_x + final_x)/2., (init_y + final_y)/2., now));
            }

            seg_id_markers.markers.push_back(makeText("world", std::to_string(v.id), (i/2.), (init_x + final_x)/2., (init_y + final_y)/2., now));

            i += 2;
        }

        for (auto& [key, val] : layers_pub_)
        {
            val.first.publish(std::get<0>(layers[key]));
            val.second.publish(std::get<1>(layers[key]));
        }

        seg_ids_pub_.publish(seg_id_markers);
    }
    else
    {
        ROS_ERROR_STREAM("Inconsistant graph can't updated segment id. augmented: " << augmented_graph_.vertices.size() << ", graph: " << segments_.size());
    }
}

/*************************************************************************************************/
void InteractiveGraph::toggleValidationButton()
{
    if (!current_area) return;

    current_area->finished = !current_area->finished;

    for (auto vert : current_area->vertices_markers)
    {
        vert->controls.front().markers.front().color = current_area->finished ? Color::black : Color::orange_red;
        interactive_mk_server_->erase(vert->name);
        interactive_mk_server_->insert(*(vert));
    }

    if (current_area->finished)
    {
        current_area->validation_button->controls.front().markers.front().color.a = 1;
    }
    else
    {
        current_area->validation_button->controls.front().markers.front().color.a = 0.4;
    }

    interactive_mk_server_->erase(current_area->validation_button->name);
    interactive_mk_server_->insert(*(current_area->validation_button));

    if (current_area->finished)
    {
        ros::param::set("/graph_completion/areas/nb_areas", int(areas.size()));

        for (const auto& [id, current_area] : areas)
        {
            XmlRpc::XmlRpcValue polygon;
            polygon.setSize(current_area.vertices_markers.size());
            int i = 0;

            for (const auto& vertex : current_area.vertices_markers)
            {
                polygon[i].setSize(2);
                polygon[i][0] = vertex->pose.position.x;
                polygon[i++][1] = vertex->pose.position.y;
            }

            // Add the polygon
            ros::param::set("/graph_completion/areas/area_" + std::to_string(id) + "/polygon", polygon);

            XmlRpc::XmlRpcValue type_list;
            if (false == current_area.type.empty())
            {
                type_list.setSize(current_area.type.size());

                for (size_t i_type = 0; i_type < current_area.type.size(); ++i_type)
                {
                    switch(current_area.type[i_type])
                    {
                        case(ifollow_mas_msgs::Area::AREA_TYPE_TRAFFIC_PARKING):
                        {
                            type_list[i_type] = std::string("Traffic_parking");
                            break;
                        }
                        case(ifollow_mas_msgs::Area::AREA_TYPE_NO_STOP):
                        {
                            type_list[i_type] = std::string("No_stop_zone");
                            break;
                        }
                        case(ifollow_mas_msgs::Area::AREA_TYPE_MAX_SPEED):
                        {
                            type_list[i_type] = std::string("Max_speed");
                            break;
                        }
                        case(ifollow_mas_msgs::Area::AREA_TYPE_MAX_ROBOTS):
                        {
                            type_list[i_type] = std::string("Max_robots");
                            break;
                        }
                        default:
                        {
                            break;
                        }
                    }
                }
            }
            else
            {
                type_list.setSize(1);
                type_list[0] = std::string("Traffic_parking");
            }

            ROS_INFO_STREAM("Area id: " << id);

            // Add the area type
            ros::param::set("/graph_completion/areas/area_" + std::to_string(id) + "/type", type_list);
        }
    }

    interactive_mk_server_->applyChanges();
}

}
